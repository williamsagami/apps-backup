import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Showtime Barbershop",

      homeDescription: 
        `One of the best barbershops in  Portland offering many years of experience with expert barbers to do their service ready to make you a flawless cut, 
        offering excellent customer service and with a family atmosphere so that your whole family has the best haircut, 
        do not hesitate to call us and you will have a unique experience because for us your haircut is the most important thing`,

      homeSlider:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fhome%2F1s.jpg?alt=media&token=d38c2a7f-a318-4a0a-9dca-66d788b00ed2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fhome%2F2s.jpg?alt=media&token=0475c5fb-4edd-431a-88ff-b970de5d955b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fhome%2F3s.jpg?alt=media&token=57989bdf-106e-4cb7-b9b4-96a448830c6c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fhome%2F4s.jpg?alt=media&token=cd9ff6bb-fc1f-45ec-a4c4-a5edd940c7ef",
      ],

      homeServices: [
        {
          name:"Haircut",
          price: "$30"
        },
        {
          name:"Haircut & Beard",
          price: "$40"
        },
        {
          name:"Kids Cut",
          price: "$28"
        },
        {
          name:"Beard Trim",
          price: "$15"
        },
      ],
      homeSchedules: [
        {
          day: "Monday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Tuesday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Wednesday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Thursday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Friday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Saturday",
          hour: "09:00 am – 05:00 pm",
        },
        {
          day: "Sunday",
          hour: "CLOSED",
        },
      ],
      homeVideos: [
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fhome%2F1.mp4?alt=media&token=3cf78099-3953-4fbb-9560-337d80bcfba3",        
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fhome%2F2.mp4?alt=media&token=3a4e0121-3c83-4a7d-9042-79f11af09edc",
        ],

      f: [
        
      ],

      phoneNumbers: [
        {
          text: "Showtime Barbershop",
          number: "503 451 4515",
        }       
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F1.jpg?alt=media&token=dd944c0a-0c9b-405e-9278-062fde9c71e1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F10.jpg?alt=media&token=75405596-801f-4e1f-8cb2-78accc8792f0",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F11.jpg?alt=media&token=2af8e912-c9a2-487c-b6dd-c4f2a58c87b8",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F12.jpg?alt=media&token=be8f50a0-c9ed-49ee-974a-5456b4aeacbc",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F13.jpg?alt=media&token=bd363471-30e8-4f4a-ab28-07b8dde71e50",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F14.jpg?alt=media&token=fc912e57-dd86-4c2e-82f5-a3166a68083a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F15.jpg?alt=media&token=900717ae-3b5e-47d7-8b9a-2045129d4fbb",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F16.jpg?alt=media&token=7202f579-a8cf-4b99-9113-f82580e86791",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F17.jpg?alt=media&token=88e79cad-7d2c-46fa-864e-8a611da20183",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F18.jpg?alt=media&token=37599fda-d8fe-40a6-a894-f36da37b637d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F19.jpg?alt=media&token=9bbb02da-0dd3-4a4d-b26c-6e3b436e7dca",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F1s.jpg?alt=media&token=2fac11eb-e77c-4af9-a78f-720c6e7df1ee",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F2.jpg?alt=media&token=d331cb49-6aa1-4201-95b0-4c29414de45c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F20.jpg?alt=media&token=1149d6b3-b478-46ac-9472-00b29b1269e1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F21.jpg?alt=media&token=3b5e5ded-76b4-46cb-b3cc-196afdbb6fa1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F22.jpg?alt=media&token=52862bfa-8cdb-4f1c-930a-5c5b58fce7a2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F2s.jpg?alt=media&token=d329c409-ecdb-44ac-9a02-031b90a9e4d2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F3.jpg?alt=media&token=9f511211-d8d2-4c9b-a56c-4d7a379dd4fb",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F3s.jpg?alt=media&token=7ad03dd6-3662-4524-a196-da258fb9b325",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F4.jpg?alt=media&token=0c33239e-da97-4974-bd41-46486ccc7d69",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F4s.jpg?alt=media&token=f7510e0e-e1ae-4ec7-a95c-3b49e780bae6",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F5.jpg?alt=media&token=bf66f4dc-4c3d-4960-bfc3-27b70601239a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F6.jpg?alt=media&token=7c8c72fc-dfb2-4423-a9a5-01d8a951b9f7",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F7.jpg?alt=media&token=aea4f576-0e80-48fa-a6f0-17335e126108",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F8.jpg?alt=media&token=ad0ddc5c-5e53-4882-9822-c0fbbb952e77",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fgallery%2F9.jpg?alt=media&token=42eae8b7-4b13-48f5-a673-ff1c739f0d79",
      ],

      socialNetworks: [
        {
          text: "showtimebarbershoppdx1",
          link: "https://www.instagram.com/showtimebarbershoppdx1/?hl=es",
          icon: "logo-instagram",
        },
        {
          text: "Showtime Barbershop PDX",
          link: "https://www.facebook.com/showtimebarbershoppdx/",
          icon: "logo-Facebook",
        },
        {
          text: "Showtime Barbershop PDX",
          link: "https://booksy.com/en-us/436097_showtime-barbershop-pdx_barber-shop_134776_portland?do=invite",
          icon: "book",
        },
      ],

      address: "2801 SE 122nd Ave, Portland, OR 97236, United States",

      addressRoute: "2801 SE 122nd Ave, Portland, OR 97236, United States",

      mapSource: "https://maps.google.com/maps?q=showtime%20barbershop%20portland&t=&z=15&ie=UTF8&iwloc=&output=embed",
    
      barbers:[
        {
          name: "Juan",
          description: "Barber Expert",
          socialNetworks:[
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fappoiments%2Fb1_512x512.jpg?alt=media&token=cb42fcc5-60ac-40a3-a335-a46a5418263a",

        },

        {
          name: "Castro",
          description: "Master Barbershop",
          socialNetworks:[
            {
              text: "castro_thebarber",
              link: "https://www.instagram.com/castro_thebarber/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fappoiments%2Fb2_512x512.jpg?alt=media&token=b9424acd-f0c6-430f-8825-ace3acd49bf9",

        },
        {
          name: "Diego",
          description: "Veteran Barber",
          socialNetworks:[
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fappoiments%2Fb3_512x512.jpg?alt=media&token=96da64cd-ddb7-449e-a44d-b5fcf3816f35",

        },
        {
          name: "Claudia",
          description: "The best cuts in barbershop",
          socialNetworks:[
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShowtime%20Barbershop%2Fappoiments%2Fb4_512x512.jpg?alt=media&token=303c2b0f-7a1f-412c-af28-a869e6ada0ac",
        },
      ],
    }

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

  // this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
