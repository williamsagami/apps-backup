import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "The Classic Barbershop",

      homeDescription: 
        `We are a brand the best barbershop on Hawthorne Blvd with 4 hard working barbers in counting ready to have you ready for the weekend or for whatever
        occasion you need your haircut for ! Walk ins are more than welcome! <br> 
        Where with our new application you can schedule your appointment and you will have an experience with our professionals, 
        where you will just take a seat and see one of the best haircuts because with us your haircut is the most important, 
        thing you can bring your family and you will see the incredible experience do not hesitate to schedule your appointment NOW`,

        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fhome%2F11_512x512.jpg?alt=media&token=0bff5910-85ec-4007-b985-60859f481485",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fhome%2F14_512x512.jpg?alt=media&token=a8dcf475-3756-4ecf-9bc0-d6f24e613113",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fhome%2F16_512x512.jpg?alt=media&token=caa11aa8-ba29-4adc-adeb-6e8f6746ab4a",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fhome%2F2_512x512.jpg?alt=media&token=095bbe7f-913e-4d89-8409-b5a1970a4ab1",
        ],


        // homeServices: [
        //   {
        //     name:"Short Hair Cut",
        //     time:"30 Minute",
        //    // description:"Utilization of clippers and scissors (including texturizing shears), optional shaving around the hair edges and optional pomade application.",
        //     price: "$37"
        //   },
        //   {
        //     name:"Face mask",
        //     time:"10 minutes",
        //    //  description:"Haircut & style followed by massage therapy utilizing our percussive handheld device over the shoulders for relief & relaxation.",
        //     price: "$10"
        //   },
        //   {
        //     name:"Under eye patch",
        //     time:"10 minutes",
        //    // description:"For this classic & delicate service, we utilize fine shears (including texturizing shears) to cut the hair to the desired length.",
        //     price: "$10"
        //   },
        //   {
        //     name:"Hair treatment and Blow-dry",
        //     time:"1 hour 15 minutes",
        //     //description:"Two hot towels, hot lather, and gentle straight-edge razor shave around the scalp, lime aftershave & balm is then applied.",
        //     price: "$65"
        //   },
        //   {
        //     name:"Hair Treatment",
        //     time:"40 minutes",
        //   //  description:"From low tapers to high fades, we give you our closest, cleanest and long lasting skin fades with state of the art instruments.",
        //     price: "$45"
        //   },
        //   {
        //     name:"Hot Towel Shave",
        //     time:"45 minutes",
        //    //description:"For special occasions, sometimes you need service in a pinch. Please TEXT (310) 988-5675 to inquire about before/after hours and same day services.",
        //     price: "$45"
        //   },
        //   {
        //     name:"Color refresh (toner)",
        //     time:"1 hour 30 minutes",
        //    //description:"Experience the tradition of fresh haircut service for you and your son.",
        //     price: "$65"
        //   },
        //   {
        //     name:"Airtouch Technique (color)",
        //     time:"6 hours",
        //     //description:"Contact us to set a date for a Master Barber to come to you. Prices may vary in accordance with services and distance.",
        //     price: "$500"
        //   },
        //   {
        //     name:"Buzz Cut",
        //     time:"30 minutes",
        //    //description:"Treat yourself and a family member or friend for a haircut service.",
        //     price: "$20"
        //   },
        //   {
        //     name:"Beard Trim",
        //     time:"15 minutes",
        //     //description:"Contact us to set a date for a Master Barber to come to you. Prices may vary in accordance with services and distance.",
        //     price: "$10"
        //   },
        //   {
        //     name:"Sides touch up (cut or fade)",
        //     time:"30 minutes",
        //     //description:"Treat yourself and a family member or friend for a haircut service.",
        //     price: "$25"
        //   },
        //   {
        //     name:"Sides Clean up (clean up around ears and neck)",
        //     time:"10 minutes",
        //     //description:"Contact us to set a date for a Master Barber to come to you. Prices may vary in accordance with services and distance.",
        //     price: "$10"
        //   },
        //   {
        //     name:"Bang trim",
        //     time:"15 minutes",
        //     //description:"Treat yourself and a family member or friend for a haircut service.",
        //     price: "$10"
        //   },
        //   {
        //     name:"Blow Dry",
        //     time:"45 minutes",
        //     //description:"Treat yourself and a family member or friend for a haircut service.",
        //     price: "$46"
        //   },
        //   {
        //     name:"Color roots",
        //     time:"2 Hours",
        //     //description:"Treat yourself and a family member or friend for a haircut service.",
        //     price: "$85"
        //   },
        //   {
        //     name:"Highlights",
        //     time:"3 Hours",
        //     //description:"Treat yourself and a family member or friend for a haircut service.",
        //     price: "$165"
        //   },
        //   {
        //     name:"Color complete",
        //     time:"2 Hours 30 Minutes",
        //     //description:"Treat yourself and a family member or friend for a haircut service.",
        //     price: "$165"
        //   },
        //   {
        //     name:"Ombré",
        //     time:"3 Hours 30 Minutes",
        //     //description:"Treat yourself and a family member or friend for a haircut service.",
        //     price: "$200"
        //   },
        //   {
        //     name:"Man Color",
        //     time:"1 Hour",
        //     //description:"Treat yourself and a family member or friend for a haircut service.",
        //     price: "$45"
        //   },
        //   {
        //     name:"Color base + highlights",
        //     time:"4 Hours",
        //     //description:"Treat yourself and a family member or friend for a haircut service.",
        //     price: "$185"
        //   },
        //   {
        //     name:"Balayage",
        //     time:"4 Hour",
        //     //description:"Treat yourself and a family member or friend for a haircut service.",
        //     price: "$180"
        //   },
        //   {
        //     name:"Makeup",
        //     time:"45 minutes",
        //     //description:"Treat yourself and a family member or friend for a haircut service.",
        //     price: "$60"
        //   },
        //   {
        //     name:"Hair Updo",
        //     time:"30 minutes",
        //     //description:"Treat yourself and a family member or friend for a haircut service.",
        //     price: "$65"
        //   },
        // ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "Closed",
        },
        {
          day: "Tuesday",
          hour: "9:00 AM – 8:00 PM",
        },
        {
          day: "Wednesday",
          hour: "9:00 AM – 8:00 PM",
        },
        {
          day: "Thursday",
          hour: "9:00 AM – 8:00 PM",
        },
        {
          day: "Friday",
          hour: "9:00 AM – 8:00 PM",
        },
        {
          day: "Saturday",
          hour: "9:00 AM – 8:00 PM",
        },
        {
          day: "Sunday",
          hour: "9:00 AM – 8:00 PM",
        },
      ],
      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fhome%2F1.mp4?alt=media&token=c7748a21-913d-4166-8fc4-2318fb8f1189",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fhome%2F2.mp4?alt=media&token=7d74c1db-b2b8-4355-be54-095de7be7a21",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "323 360 8589",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F7_512x512.jpg?alt=media&token=3786c356-bf68-4818-a713-385270ae6f75",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F19_512x512.jpg?alt=media&token=314efb95-59df-40cf-9248-1a8880d47106",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F22_512x512.jpg?alt=media&token=565b8ff2-c668-4b73-9e42-dd73cea6868f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F16_512x512.jpg?alt=media&token=c76b2dbf-a27c-4792-8d21-a547100f42cd",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F21_512x512.jpg?alt=media&token=7fbd1448-a1f1-4c6e-9f1f-8e768cf12150",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F8_512x512.jpg?alt=media&token=9a8c3ec0-c21f-4a57-af39-0cb02ac3d0dc",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F18_512x512.jpg?alt=media&token=1e558535-4cbe-42e1-a247-e345cee0169c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F11_512x512.jpg?alt=media&token=c37d43c2-faef-4728-bc9b-d9c154e27e7c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F17_512x512.jpg?alt=media&token=3489fdd3-29f2-4e0e-8e97-4f96ae40ec58",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F14_512x512.jpg?alt=media&token=3a1cb546-57d8-4f28-8153-ec22a7fee138",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F5_512x512.jpg?alt=media&token=fe259b61-1426-4f6b-98e7-b431f85b7f3d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F13_512x512.jpg?alt=media&token=00de7638-1547-466d-be96-ca7846b9d97d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F4_512x512.jpg?alt=media&token=b6d681d8-7f75-4268-90dd-b82eff999806",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F4_512x512.jpg?alt=media&token=b6d681d8-7f75-4268-90dd-b82eff999806",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F12_512x512.jpg?alt=media&token=bf00978b-4e54-4526-b98e-5b9270791b44",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F10_512x512.jpg?alt=media&token=3a6dfdad-07ab-43c2-9a85-b4ce09153e33",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F15_512x512.jpg?alt=media&token=507c9a0f-0c88-4c28-9f7e-57887af479a7",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F6_512x512.jpg?alt=media&token=c36d68c5-dbb8-4cc4-b84a-29a676a30047",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F3_512x512.jpg?alt=media&token=2aa63fbb-c305-4d41-8da4-dc1437b69164",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F9_512x512.jpg?alt=media&token=9e3d8424-6804-46bb-ae9a-00ebf42f5164",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F1_512x512.jpg?alt=media&token=1571af80-5aaa-4bef-8e0d-76c311c5f019",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F20_512x512.jpg?alt=media&token=57e769b0-7022-426f-bd77-6f23c04f8fee",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fgallery%2F2_512x512.jpg?alt=media&token=b9b9ca56-a119-47c4-8580-93829453b484",
      ],

      socialNetworks: [
        {
          text: "The Classic Barber Shop",
          link: "https://www.yelp.com/biz/the-classic-barber-shop-hawthorne-3",
          icon: "logo-yahoo",
        },
        {
          text: "theclassic_barbershop",
          link: "https://www.instagram.com/trimsandtrends/?hl=en",
          icon: "logo-instagram",
        },
        {
          text: "The Classic Barber Shop",
          link: "https://www.google.com/maps/place/The+classic+barber+shop/@33.9285311,-118.3522443,15z/data=!4m2!3m1!1s0x0:0x68b926a6a62cefc4?sa=X&ved=2ahUKEwiLuNKM7pv1AhWyslYBHbTEAx4Q_BJ6BAg8EAU",
          icon: "logo-google",
        },
      ],

      address: "11628 Hawthorne Blvd, Hawthorne, CA 90250",

      addressRoute: "11628 Hawthorne Blvd, Hawthorne, CA 90250",

      mapSource: "https://maps.google.com/maps?q=The%20classic%20barber%20shop,%2011628%20Hawthorne%20Blvd&t=&z=15&ie=UTF8&iwloc=&output=embed",
     
      barbers:[
        {
          name: "Elvis (owner)",
          description: "Number: 3233608589",
          socialNetworks:[
            {
              text: "theclassic_barbershop",
              link: "https://www.instagram.com/theclassic_barbershop/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fappointments%2Fb1_512x512.jpg?alt=media&token=7100fc76-c232-4212-b4d9-3a3f0694e2d7",
        },
        {
          name: "Mike villa",
          description: "Number: +1 (310) 933-2261",
          socialNetworks:[
            {
              text: "golden_cuts_mike",
              link: "https://www.instagram.com/golden_cuts_mike/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fappointments%2Fb2_512x512.jpg?alt=media&token=00c41b9d-11ce-4edf-94f1-f5b6b71c7a2e",
  
        },
        {
          name: "David luna",
          description: "Number: +1 (323) 762-4165",
          socialNetworks:[
            {
              text: "king_david_fadez",
              link: "https://www.instagram.com/king_david_fadez/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fappointments%2Fb4_512x512.jpg?alt=media&token=1af66c0f-0d35-4ba2-8371-5dd44d479d97",
  
        },
  
        {
          name: "Brandon blandón",
          description: "Number: +1 (424) 240-2000",
          socialNetworks:[
            {
              text: "marlonjoseblandon",
              link: "https://www.instagram.com/marlonjoseblandon/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fappointments%2Fb3_512x512.jpg?alt=media&token=30f6ecbd-2c0c-4356-a239-8fa55d0911b0",
        },
          
        {
          name: "Ervin",
          description: "Number: +1 (424) 339-3008",
          socialNetworks:[
            {
              text: "Ervin.Cutz",
              link: "https://www.instagram.com/ervin.cutz/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fappointments%2Fb6_512x512.jpg?alt=media&token=08d28e5d-1697-4934-80a3-a1b601109763",
        },
                  
        {
          name: "Moy delgado",
          description: "Number: +1 (310) 933-2261",
          socialNetworks:[
            {
              text: "moy_the_classicbarber",
              link: "https://www.instagram.com/moy_the_classicbarber/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20classic%20barbershop%2Fappointments%2Fb5_512x512.jpg?alt=media&token=dd7dfc88-9e16-493e-b5d6-9967066f73eb",
        },
      ]

    
    }

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

    // this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)}); 
  }



  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
