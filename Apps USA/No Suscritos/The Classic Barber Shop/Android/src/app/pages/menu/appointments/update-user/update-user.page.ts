import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController, NavParams, LoadingController } from '@ionic/angular';
import { FirebaseService } from 'src/app/services/firebase.service';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.page.html',
  styleUrls: ['./update-user.page.scss'],
})
export class UpdateUserPage implements OnInit {

  userData: any;
  allowRequests = true;
  rating = 0;

  private loading: any;

  constructor(
    private modalController: ModalController, 
    private toastController: ToastController, 
    private navParams: NavParams, 
    private loadingController: LoadingController,
    private firebaseService: FirebaseService,
    private callNumber: CallNumber) { 

      this.userData = navParams.get('userData');
      this.allowRequests = this.userData.allowRequests;
      this.rating = this.userData.rating;
    }

  ngOnInit() {
  }

  closeView(){
	  this.modalController.dismiss();
  }

  async showInfo(info: string) {
    const toast = await this.toastController.create({
      message: info,
      duration: 6000,
      color: 'secondary',
    });
    toast.present();
  }

  saveUserChanges() {
    this.loadingController.create({
      message: 'Saving...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      this.userData.allowRequests = this.allowRequests;
      this.userData.rating = this.rating;
      this.firebaseService.updateUser(this.userData).then(showInfo => {
        this.loading.dismiss();
        this.showInfo("The changes have been saved successfully.");
        this.closeView();
      });
    });
  }

  getAdminText() {
    return this.userData.admin ? "Admin" : "User";
  }

  setRating(event) {
    this.rating = event.detail.value;
  }

  callUser() {
	  this.callNumber.callNumber(this.userData.phone, false)
	  .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

}
