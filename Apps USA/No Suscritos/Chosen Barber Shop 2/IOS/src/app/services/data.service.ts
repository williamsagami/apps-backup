import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "CHOSEN BARBER SHOP",

      homeDescription: 
        `One of the best Barber shops in New York with many years of experience. 
        <br> Our professionals will attend to you quickly. 
        <br> Once you're seated in one of our chairs, just relax and we're make sure you have an unforgettable visit. We offer great services with amazing prices. 
        <br> Its more than just a haircut, we offer a great experience. 
        <br> Do not hesitate to schedule an appointment.`,

        phoneNumbers1: [
          {
            text: "Chosen Barber Shop",
            number: "347 596 9106",
          },
        ],

        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fhome%2F1.jpg?alt=media&token=53fd4326-410a-4aad-b6fc-bb56d7be166d",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fhome%2F2.jpg?alt=media&token=5d68cf11-9e37-40dc-9666-2c1123b6d37c",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fhome%2F3.jpg?alt=media&token=4a6152e4-1700-4a44-bbae-658a8bac48f4",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fhome%2F4.jpg?alt=media&token=dd704969-83ba-4e81-a7e2-7953d49ed3dc",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fhome%2F16.jpg?alt=media&token=083835bf-8e8a-4641-9c59-9e226ee860c7",
          ],
      homeServices: [
        {
          name:"Signature Haircut",
          price: "$30"
        },
        {
          name:"Haircut",
          price: "$25"
        },
        {
          name:"Beard Shaving/Hot Towel",
          price: "$18"
        },
        {
          name:"Beard Shaving/Hot Towel",
          price: "$18"
        },
        {
          name:"Beard Trim",
          price: "$15"
        },
        {
          name:"Kids Haircut",
          price: "$20"
        },
        {
          name:"FULL SERVICE",
          price: "$35"
        }
      ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "08:00 am – 08:00 pm",
        },
        {
          day: "Tuesday",
          hour: "08:00 am – 08:00 pm",
        },
        {
          day: "Wednesday",
          hour: "08:00 am – 08:00 pm",
        },
        {
          day: "Thursday",
          hour: "08:00 am – 08:00 pm",
        },
        {
          day: "Friday",
          hour: "08:00 am – 08:00 pm",
        },
        {
          day: "Saturday",
          hour: "08:00 am – 08:00 pm",
        },
        {
          day: "Sunday",
          hour: "Closed",
        },
      ],
      homeVideos: [
          
      ],

      phoneNumbers: [
        {
          text: "Chosen Barber Shop",
          number: "347 596 9106",
        },
        {
          text: "Antonia",
          number: "646 399 2650",
        },
        {
          text: "Malaquias",
          number: "929 381 9612",
        },
        {
          text: "Jeffrey",
          number: "518 495 1606",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F1.jpg?alt=media&token=df1f4953-bae5-4440-af56-2657832758c4",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F2.jpg?alt=media&token=01d9d7e5-b3e2-441d-bb64-fb22f0814ac1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F10.jpg?alt=media&token=a25513ce-5d36-485f-b3f6-b8d63e50d6c6",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F11.jpg?alt=media&token=a6b0ce64-d9ae-4a98-9bf8-d90914e72f56",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F12.jpg?alt=media&token=199ac935-c020-4944-9f1e-39a87f305701",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F6.jpg?alt=media&token=ec9cafb6-c852-4bfa-be86-96b10c498a7d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F13.jpg?alt=media&token=f1b1ae4c-788e-4d93-83ee-d34cb9c1f25d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F14.jpg?alt=media&token=6c3866e2-64ea-4555-b28b-c35d50fe66fd",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F16.jpg?alt=media&token=d4687f1a-d5db-41b4-b542-0eb076eae217",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F3.jpg?alt=media&token=a6fe4c8f-53a7-4f8d-9529-875a76660af9",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F4.jpg?alt=media&token=5ea5f405-f9f5-4840-a9f3-1f7d5e5c5bfc",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F5.jpg?alt=media&token=b4dccea9-dc5a-431f-a6fd-5a4099b4425f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F7.jpg?alt=media&token=0987114e-7ce0-436f-b5bb-64fb5414143e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F8.jpg?alt=media&token=f07aa3fc-433b-4da0-bbf9-82783d9a8792",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F9.jpg?alt=media&token=bdcc7d75-680c-4b65-8d46-a65b4be1b37d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FCHOSEN%20BARBER%20SHOP%2Fgallery%2F15.png?alt=media&token=edb28e46-a98a-4441-a47d-aad4ee56e106",
      ],


      socialNetworks: [
        {
          text: "chosenbarbershopinc",
          link: "https://www.instagram.com/chosenbarbershopinc/",
          icon: "logo-instagram",
        },
        {
          text: "Chosen Barber Shop",
          link: "https://www.yelp.com/biz/chosen-barber-shop-the-bronx",
          icon: "logo-yahoo",
        },
        {
          text: "Chosen Barber Shop",
          link: "https://www.google.com.mx/maps/place/Chosen+Barber+Shop/@40.8371887,-73.8464586,15z/data=!4m5!3m4!1s0x0:0x83d643997ff65f2a!8m2!3d40.8371887!4d-73.8464586",
          icon: "logo-google",
        },
      ],

      address: "2401B Westchester Ave, The Bronx, NY 10461, USA",

      addressRoute:"2401B Westchester Ave, The Bronx, NY 10461, USA",

      mapSource: "https://maps.google.com/maps?q=Chosen%20Barber%20Shop,%202401B%20Westchester%20Ave,%20The%20Bronx,%20NY%2010461,%20Estados%20Unidos&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

 //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
