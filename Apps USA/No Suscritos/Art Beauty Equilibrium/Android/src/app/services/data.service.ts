import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Art Beauty Equilibrium",

      homeDescription: 
        `Art Beauty Equilibrium Barber Shop Salon Makeup Studio, is the best barbershops / SalonMakeup in California with professional barbers,
         with spectacular experience counting on quality products and cuts of the best styles to choose for us, 
         your cut is the most important thing you can bring you in addition to that we have one of the best podcasts that currently exist for your entertainment and information,
         you can listen to it whenever and wherever we will always think of you do not hesitate to hire us and receive an incredible experience.`,


        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F1_512x512.jpg?alt=media&token=45ac7ba1-30f3-4543-ad9a-21c2ac9be4b1",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fhome%2F1s_512x512.jpg?alt=media&token=9303bd7a-fcbb-4f25-a675-6b88b56f9c2e",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fhome%2F2s_512x512.jpg?alt=media&token=a145c44d-6f39-4e38-8df8-5b49c2cbeeab",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fhome%2F3s_512x512.jpg?alt=media&token=16bdcc1f-dd39-4f1a-ad14-c4f8bb3fdabb",
        ],


        homeServices: [
          {
            name:"Wax Stick with Argan Oil",
            // time:"30 minutes",
           // description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
            price: "$11.99"
          },
          {
            name:"Coconut Oil Revitalizing Shampoo with Biotin",
            // time:"30 minutes",
           // description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
            price: "$13.99"
          },
          {
            name:"Coconut Hair Mask with Biotin",
            // time:"30 minutes",
           // description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
            price: "$15.99"
          },
          {
            name:"Black Soap (Natural)",
            // time:"30 minutes",
           // description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
            price: "$7.00"
          },
          {
            name:"Shea Body Butter Cups",
            // time:"30 minutes",
           // description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
            price: "$12.00"
          },
          {
            name:"Edge Control with Argan Oil",
            // time:"30 minutes",
           // description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
            price: "$12.99"
          },

        ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "Closed",
        },
        {
          day: "Tuesday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Wednesday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Thursday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Friday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Saturday",
          hour: "09:00 am – 07:00 pm",
        },
        {
          day: "Sunday",
          hour: "Closed",
        },
      ],
      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fhome%2F1.mp4?alt=media&token=d3a847e4-f680-4d45-b1f6-c5a6eb1478e6",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fhome%2F2.mp4?alt=media&token=8798cc25-97e8-4baa-be5e-43bab0851bb9",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fhome%2F3.mp4?alt=media&token=64da7bab-660d-4214-bdc9-f2ea2a590be7",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "323 920 4118",
        },
      ],

      gallery:[
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F10_512x512.jpg?alt=media&token=7e0879ed-1951-4bdd-a456-60a3e7c60d4c",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F11_512x512.jpg?alt=media&token=cb66cea4-e0a2-48ff-bc41-0678e79a81f0",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F12_512x512.jpg?alt=media&token=f80dba37-1386-499e-9736-69cce9f920dc",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F13_512x512.jpg?alt=media&token=8279ce6c-07cb-44fb-9e9b-892554247ada",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F14_512x512.jpg?alt=media&token=2b522444-6587-4802-9b97-91cc84367611",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F15_512x512.jpg?alt=media&token=a1003eaf-ccb0-4cdd-9bdb-741d6916335c",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F16_512x512.jpg?alt=media&token=f235aaaf-996e-46da-af49-b6556ee814ce",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F17_512x512.jpg?alt=media&token=da3da743-12b4-445a-85e6-4153ae29039a",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F18_512x512.jpg?alt=media&token=635f5845-56a2-4e52-84f3-aae7db9f21a5",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F19_512x512.jpg?alt=media&token=3c588bdc-929f-4d08-8205-cff49fa8c4ce",
  // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F1_512x512.jpg?alt=media&token=45ac7ba1-30f3-4543-ad9a-21c2ac9be4b1",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F20_512x512.jpg?alt=media&token=371f0f7d-2188-4148-a773-57c623008130",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F21_512x512.jpg?alt=media&token=5ad77f60-38b6-4781-81a0-3bb0f9d32f15",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F22_512x512.jpg?alt=media&token=26047e88-b8a6-4f20-a9df-a54aebc476cd",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F23_512x512.jpg?alt=media&token=781f4e66-de13-416f-b886-247c572a6bbc",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F24_512x512.jpg?alt=media&token=35c415a4-4874-4186-a6af-f18884d5aca6",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F2_512x512.jpg?alt=media&token=88719d5c-4891-4d08-b046-ca32d62c51c9",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F3_512x512.jpg?alt=media&token=27812a1b-2127-4192-a100-1048003393fb",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F4_512x512.jpg?alt=media&token=19d82759-e1d8-4519-907d-538d760108d0",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F5_512x512.jpg?alt=media&token=8e61ba71-cbfc-4705-b9be-ce12ea9d0258",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F6_512x512.jpg?alt=media&token=49b0af15-cb78-40ad-8854-36f00ab35ff3",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F7_512x512.jpg?alt=media&token=afa15efb-f5a7-439e-abc2-24aed15bc511",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F8_512x512.jpg?alt=media&token=c59bd6bb-2f8f-46f0-961a-46ea5ed86d7f",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fgallery%2F9_512x512.jpg?alt=media&token=47a2b629-b12f-45c9-9ead-97a983fdfc73",
      ],

      products:[
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp7_512x512.jpg?alt=media&token=b6d4e33d-1c68-4b0b-851b-7b62e53351dd",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp10_512x512.png?alt=media&token=748f2e7b-98a9-4004-82ed-04820e25720b",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp11_512x512.png?alt=media&token=1d6643ec-49ae-4dd3-a88c-21c267358a95",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp1_512x512.png?alt=media&token=86f2a73e-f5d9-4ddf-8395-5e4e0969d262",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp2_512x512.png?alt=media&token=31228310-1351-48b7-95ec-ffdfa60795d9",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp3_512x512.png?alt=media&token=22c3709f-06e1-48bf-8600-8158b6063b53",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp4_512x512.png?alt=media&token=8265b522-a537-40e8-b7ec-6de553d6764e",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp5_512x512.png?alt=media&token=0602c55c-3f08-48a2-a315-5cfe41535fa0",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp6_512x512.png?alt=media&token=4e58f52b-67b3-48c6-b227-f34e8bf7c117",
        //"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp8_512x512.png?alt=media&token=c6792ffc-a7c4-4d36-8044-47c1ce287664",
       // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp9_512x512.png?alt=media&token=2bd52675-c1d8-4ddc-be58-51378228da92",
       ],

      socialNetworks: [
        {
          text: "Art Beauty Equilibrium",
          link: "https://www.youtube.com/channel/UCrnf_VjLUp1cj6SyiZe2MCA",
          icon: "logo-youtube",
        },
        {
          text: "@artbeautyequilibrium",
          link: "https://www.instagram.com/artbeautyequilibrium/?hl=es",
          icon: "logo-instagram",
        },
        {
          text: "Art Beauty Equilibrium",
          link: "https://www.facebook.com/artbeautyequilibrium/",
          icon: "logo-facebook",
        },
        {
          text: "Art Beauty Equilibrium",
          link: "https://open.spotify.com/show/5vjZ9BAnv94Gx0BS9VrY4M",
          icon: "play",
        },
        {
          text: "Art Beauty Equilibrium",
          link: "https://www.yelp.com/biz/art-beauty-equilibrium-hawthorne",
          icon: "logo-yahoo",
        },
        {
          text: "artbeautyequilibrium",
          link: "https://www.artbeautyequilibrium.com/",
          icon: "globe",
        },
      ],

      address: "2610 W Imperial Hwy, Hawthorne, CA 90250, USA",

      addressRoute: "2610 W Imperial Hwy, Hawthorne, CA 90250, USA",

      mapSource: "https://maps.google.com/maps?q=Art%20Beauty%20Equilibrium%20Barbershop%20Salon%20Makeup%20Studio&t=&z=15&ie=UTF8&iwloc=&output=embed",
      
      barbers:[
        {
          name: "George",
          description: "Veteran Barber",
          socialNetworks:[
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fappointment%2FGeorge_512x512.jpg?alt=media&token=d616bede-4ec0-4347-970e-644477a026fe",
        },
        {
          name: "Jason",
          description: "Profesional Barber",
          socialNetworks:[
  
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fappointment%2FJason_512x512.jpg?alt=media&token=cef754ea-d8b6-401e-b009-2a06c7e3ab5e",
  
        },
        {
          name: "Lynden",
          description: "Master Barber",
          socialNetworks:[
  
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fappointment%2FLynden_512x512.jpg?alt=media&token=f3f9ccd9-e24b-4419-bd92-225cd81f6a5e",
  
        },
  
        {
          name: "Shima",
          description: "stylist specialist",
          socialNetworks:[
  
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fappointment%2FShima_512x512.JPG?alt=media&token=9d2c4e93-8cb3-4deb-9c24-8b6a5701ef57",
        },
      ],
    
    }

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

     //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)}); 
  }



  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
