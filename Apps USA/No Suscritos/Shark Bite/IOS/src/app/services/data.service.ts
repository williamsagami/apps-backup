import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Shark Bite Image",

      homeDescription: 
        `5 star Upscale barbershop with an great relaxing vibe that only a visit experience can explain,
         all the barbers are detailed to there art and takes pride in being the most talented staff in the city area. <br>
         Come for a hair cut and leave with some much more,
         the most trendy shop by far. Shark Bites.`,


        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fhome%2F1s.jpg?alt=media&token=32313184-ec61-4fb7-a9c4-de31d66e660e",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fhome%2F2s.jpg?alt=media&token=1b74bc7b-154e-4870-a54f-d1fd4b4ba855",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fhome%2F3s.jpg?alt=media&token=1da496f2-85f0-4e47-833a-a784b70da3c1",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fhome%2F4s.jpg?alt=media&token=4bfe8066-23e1-4f55-9d90-2eefcc4373a8",
        ],


      homeServices: [
        {
          name:"house call / Hotel call",
          price: "$200"
        },
        {
          name:"Hair styling only",
          price: "$30"
        },
        {
          name:"A-Regular Hair cut",
          price: "$35"
        },
        {
          name:"Kids cut up to 10 years old.",
          price: "$30"
        },
        {
          name:"Hot towel Head shave only",
          price: "$50"
        },
        {
          name:"Perm for curls",
          price: "$50"
        },
        {
          name:"A-VIP full service",
          price: "$100"
        },
        {
          name:"A-Senior price 65 year +",
          price: "$20"
        },
        {
          name:"In person consultation",
          price: "$25"
        },
        {
          name:"Facial service",
          price: "$25"
        },
        {
          name:"Hair treatment",
          price: "$25"
        },
        {
          name:"Long hair cut",
          price: "$85"
        },
        {
          name:"Beard Trim & Style",
          price: "$45"
        },
        {
          name:"All Scissor mens haircut",
          price: "$45"
        },
        {
          name:"Nose & Ear Hair -wax service",
          price: "$20"
        },
        {
          name:"Eyebrow Trim",
          price: "$25"
        },

        {
          name:"Blck Mask",
          price: "$25"
        },
        {
          name:"Hot towel shave",
          price: "$50"
        },
        {
          name:"Detail Fade —“side line up enhancement” optional",
          price: "$45"
        },

        {
          name:"Hair wash",
          price: "$20"
        },
        {
          name:"Bleach hair",
          price: "$75"
        },
        {
          name:"A-Hair cut & beard trim combo with hot towel",
          price: "$60"
        },
      ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "Closed",
        },
        {
          day: "Tuesday",
          hour: "06:00 am – 05:00 pm",
        },
        {
          day: "Wednesday",
          hour: "06:00 am – 05:00 pm",
        },
        {
          day: "Thursday",
          hour: "06:00 am – 05:00 pm",
        },
        {
          day: "Friday",
          hour: "06:00 am – 05:00 pm",
        },
        {
          day: "Saturday",
          hour: "06:00 am – 05:00 pm",
        },
        {
          day: "Sunday",
          hour: "Closed",
        },
      ],

      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fhome%2F1.mp4?alt=media&token=fcb84381-d203-4e9c-9558-37295ca3f18b",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "727 313 8587",
        },
      ],

      gallery:[
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F1.jpg?alt=media&token=5c4f039c-3716-46ff-bc58-d1169be4e157",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F10.JPG?alt=media&token=1a743f5c-2e2e-44ef-abe7-a2793dc7ccb1",
       ///"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F11.JPG?alt=media&token=87c809ec-23a7-4f22-8c93-4efe469b5c9f",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F1s.jpg?alt=media&token=4afc4ac8-c020-4833-af1d-e87120adaa4b",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F15.jpg?alt=media&token=43a7d77c-8248-4c34-a635-edc2dbd476f9",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F2.jpg?alt=media&token=a8dde648-d86d-42ad-9b19-9830187c10c7",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F2s.jpg?alt=media&token=440d81bf-53fe-4c42-ad4a-5ddd3cc2c740",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F3.jpg?alt=media&token=4e2bf500-0cc9-4552-9bdf-1120a42b3adb",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F3s.jpg?alt=media&token=4d264a58-a0dc-456c-8fd0-7a4022963ab7",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F4.jpg?alt=media&token=6c4ce5e2-2f3a-46b5-91f3-3cc93ed18bc9",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F4s.jpg?alt=media&token=3144fc32-3fcf-4ea1-bb54-0b6bfe0f6b22",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F5.jpg?alt=media&token=8d3296dd-c9c5-493d-88d9-5ec77c09dba8",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F6.jpg?alt=media&token=22b51215-5c73-4947-a22d-3e42f0f610c4",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F7.jpg?alt=media&token=41505c4c-188f-4b8f-a2c9-34222beb3c20",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F8.jpg?alt=media&token=9cef41c1-4f07-49b1-a12b-fe328967db02",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F12.jpg?alt=media&token=103499d4-26cf-4ee3-8a39-5fdf9eb1c3f3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F13.jpg?alt=media&token=1d7b842e-228d-4138-9f18-67a0d6acea39",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FShark%20Bite%20Image%2Fgallery%2F14.jpg?alt=media&token=730a2e51-661b-4821-a849-efd6e20611ab",
      ],

      socialNetworks: [
        {
          text: "Shark Bite Image Barbershop",
          link: "https://www.facebook.com/Shark-Bite-Image-Barbershop-590026524761714/",
          icon: "logo-facebook",
        },
        {
          text: "shark_bite_image",
          link: "https://www.instagram.com/shark_bite_image/",
          icon: "logo-instagram",
        },
        {
          text: "Shark Bite Image Barbershop",
          link: "https://www.yelp.com/biz/shark-bite-image-barbershop-saint-petersburg",
          icon: "logo-yahoo",
        },
        {
          text: "Shark Bite Image",
          link: "https://www.linkedin.com/in/shark-bite-image-4a4a05174",
          icon: "logo-linkedin",
        },
      ],

      address: "3612 49th St N, St. Petersburg, FL 33710, United States",

      addressRoute:"3612 49th St N, St. Petersburg, FL 33710, United States",

      mapSource: "https://maps.google.com/maps?q=Shark%20Bite%20Image%20Barbershop&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

 //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
