import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { productPage } from './product.page';

const routes: Routes = [
  {
    path: '',
    component: productPage
  },
  {
    path: 'image',
    loadChildren: () => import('./products/products.module').then( m => m.productsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class productPageRoutingModule {}
