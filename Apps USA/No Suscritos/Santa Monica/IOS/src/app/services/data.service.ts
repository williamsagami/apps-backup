import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  hours=[
    // {
    //   text: "00:00 AM",
    //   value: moment().hours(0).minutes(0).seconds(0)
    // },
    // {
    //   text: "00:30 AM",
    //   value: moment().hours(0).minutes(30).seconds(0)
    // },
    // {
    //   text: "01:00 AM",
    //   value: moment().hours(1).minutes(0).seconds(0)
    // },
    // {
    //   text: "01:30 AM",
    //   value: moment().hours(1).minutes(30).seconds(0)
    // },
    // {
    //   text: "02:00 AM",
    //   value: moment().hours(2).minutes(0).seconds(0)
    // },
    // {
    //   text: "02:30 AM",
    //   value: moment().hours(2).minutes(30).seconds(0)
    // },
    // {
    //   text: "03:00 AM",
    //   value: moment().hours(3).minutes(0).seconds(0)
    // },
    // {
    //   text: "03:30 AM",
    //   value: moment().hours(3).minutes(30).seconds(0)
    // },
    // {
    //   text: "04:00 AM",
    //   value: moment().hours(4).minutes(0).seconds(0)
    // },
    // {
    //   text: "04:30 AM",
    //   value: moment().hours(4).minutes(30).seconds(0)
    // },
    // {
    //   text: "05:00 AM",
    //   value: moment().hours(5).minutes(0).seconds(0)
    // },
    // {
    //   text: "05:30 AM",
    //   value: moment().hours(5).minutes(30).seconds(0)
    // },
    // {
    //   text: "06:00 AM",
    //   hours: 6,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "06:30 AM",
    //   hours: 6,
    //   minutes:30,
    //   seconds: 0
    // },
    // {
    //   text: "07:00 AM",
    //   hours: 7,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "07:30 AM",
    //   hours: 7,
    //   minutes:30,
    //   seconds: 0
    // },
    {
      text: "08:00 AM",
      hours: 8,
      minutes:0,
      seconds: 0
    },
    {
      text: "08:30 AM",
      hours: 8,
      minutes:30,
      seconds: 0
    },
    {
      text: "09:00 AM",
      hours: 9,
      minutes:0,
      seconds: 0
    },
    {
      text: "09:30 AM",
      hours: 9,
      minutes:30,
      seconds: 0
    },
    {
      text: "10:00 AM",
      hours: 10,
      minutes:0,
      seconds: 0
    },
    {
      text: "10:30 AM",
      hours: 10,
      minutes:30,
      seconds: 0
    },
    {
      text: "11:00 AM",
      hours: 11,
      minutes:0,
      seconds: 0
    },
    {
      text: "11:30 AM",
      hours: 11,
      minutes:30,
      seconds: 0
    },

    {
      text: "12:00 AM",
      hours: 12,
      minutes:0,
      seconds: 0
    },
    {
      text: "12:30 AM",
      hours: 12,
      minutes:30,
      seconds: 0
    },
    {
      text: "13:00 AM",
      hours: 13,
      minutes:0,
      seconds: 0
    },
    {
      text: "01:30 PM",
      hours: 13,
      minutes:30,
      seconds: 0
    },
    {
      text: "02:00 PM",
      hours: 14,
      minutes:0,
      seconds: 0
    },
    {
      text: "02:30 PM",
      hours: 14,
      minutes:30,
      seconds: 0
    },
    {
      text: "03:00 PM",
      hours: 15,
      minutes:0,
      seconds: 0
    },
    {
      text: "03:30 PM",
      hours: 15,
      minutes:30,
      seconds: 0
    },
    {
      text: "04:00 PM",
      hours: 16,
      minutes:0,
      seconds: 0
    },
    {
      text: "04:30 PM",
      hours: 16,
      minutes:30,
      seconds: 0
    },
    {
      text: "05:00 PM",
      hours: 17,
      minutes:0,
      seconds: 0
    },
    {
      text: "05:30 PM",
      hours: 17,
      minutes:30,
      seconds: 0
    },
    {
      text: "06:00 PM",
      hours: 18,
      minutes:0,
      seconds: 0
    },
    {
      text: "06:30 PM",
      hours: 18,
      minutes:30,
      seconds: 0
    },
    {
      text: "07:00 PM",
      hours: 19,
      minutes:0,
      seconds: 0
    },
    {
      text: "07:30 PM",
      hours: 19,
      minutes:30,
      seconds: 0
    },
    // {
    //   text: "08:00 PM",
    //   value: moment().hours(20).minutes(0).seconds(0)
    // },
    // {
    //   text: "08:30 PM",
    //   value: moment().hours(20).minutes(30).seconds(0)
    // },
    // {
    //   text: "09:00 PM",
    //   value: moment().hours(21).minutes(0).seconds(0)
    // },
    // {
    //   text: "09:30 PM",
    //   value: moment().hours(21).minutes(30).seconds(0)
    // },
    // {
    //   text: "10:00 PM",
    //   value: moment().hours(22).minutes(0).seconds(0)
    // },
    // {
    //   text: "10:30 PM",
    //   value: moment().hours(22).minutes(30).seconds(0)
    // },
    // {
    //   text: "11:00 PM",
    //   value: moment().hours(23).minutes(0).seconds(0)
    // },
    // {
    //   text: "11:30 PM",
    //   value: moment().hours(23).minutes(30).seconds(0)
    // },
  ]
  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {

    //Datos de Contacto
    data: {

      appTitle: "Santa Monica Hair Lounge",

      homeDescription: 
        `Santa Monica Hair Lounge is a new barbershop 
        that we are starting and we have shown their talent since our cuts are professional compared to haircuts from other barbershops with years of experience,
        we make sure our clients have the best haircut, our family atmosphere makes You can take your whole family so that everyone has a unique experience. <br>
        Do not hesitate to schedule your appointment with us.`,

        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fhome%2F1_512x512.jpg?alt=media&token=1d7fa7bb-b6cc-4825-9373-c07f54d4b31c",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fhome%2F2_512x512.jpg?alt=media&token=e41bf91f-0bf6-4412-88f4-1310f9005e8c",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fhome%2F3_512x512.jpg?alt=media&token=f6c191c5-f725-4880-807d-3dcbe3dccf0e",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fhome%2F4_512x512.jpg?alt=media&token=484046b8-4b16-41d3-a34c-ad2a8e699747",
        ],


      homeServices: [
        // {
        //   name:"Beard maintenance",
        //   time:"15 minutes",
        //   //description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
        //   price: "$25"
        // },
        // {
        //   name:"Regular Haircut",
        //   time:"30 minutes",
        //  // description:"Extra 15$-20$",
        //   price: "$20"
        // },
        // {
        //   name:"Kids cut under 10yrs old",
        //   time:"45 minutes",
        //   //description:"High fade low fade",
        //   price: "$35"
        // },
        // {
        //   name:"Shave Fade",
        //   time:"35 minutes",
        //  // description:"High fade low fade",
        //   price: "$20"
        // },
        // {
        //   name:"EdgeUp",
        //   time:"10 minutes",
        //   //description:"High fade low fade",
        //   price: "$10"
        // },
        // {
        //   name:"Double Shave hot towel",
        //   time:"35 minutes",
        //  // description:"High fade low fade",
        //   price: "$25"
        // },
        // {
        //   name:"Hair color",
        //   time:"30 minutes",
        //   //description:"High fade low fade",
        //   price: "$30"
        // },
        // {
        //   name:"Men Color & haircut",
        //   time:"60 minutes",
        //   //description:"High fade low fade",
        //   price: "$50"
        // },
        // {
        //   name:"Men Bleaching and haircut and color",
        //   time:"2 Hours 30 Minutes",
        //   //description:"High fade low fade",
        //   price: "$60"
        // },
        // {
        //   name:"Women haircuts",
        //   time:"40 Minutes",
        //   //description:"High fade low fade",
        //   price: "$25"
        // },
        // {
        //   name:"Women color short hair",
        //   time:"60 Minutes",
        //  // description:"High fade low fade",
        //   price: "$50"
        // },
        // {
        //   name:"Women color long length",
        //   time:"1 Hour 45 Minutes",
        //   //description:"High fade low fade",
        //   price: "$125"
        // },
        // {
        //   name:"Women bleach and color short hair",
        //   time:"2 Hours 30 Minutes",
        //   //description:"High fade low fade",
        //   price: "$125"
        // },
        // {
        //   name:"Women color and bleach highlights long hair",
        //   time:"4 Hours 30 Minutes",
        //   //description:"High fade low fade",
        //   price: "$125"
        // },
      ],
      
      homeSchedules: [
        // {
        //   day: "Monday",
        //   hour: "Closed",
        // },
        // {
        //   day: "Tuesday",
        //   hour: "09:30 am – 08:00 pm",
        // },
        // {
        //   day: "Wednesday",
        //   hour: "09:30 am – 08:00 pm",
        // },
        // {
        //   day: "Thursday",
        //   hour: "09:30 am – 08:00 pm",
        // },
        // {
        //   day: "Friday",
        //   hour: "09:30 am – 08:00 pm",
        // },
        // {
        //   day: "Saturday",
        //   hour: "08:00 am – 08:00 pm",
        // },
        // {
        //   day: "Sunday",
        //   hour: "09:00 am – 03:00 pm",
        // },
      ],

      // homeBreaktime: [
      //   {
      //     day: "Monday",
      //     hour: "Close",
      //   },
      //   {
      //     day: "Tuesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Wednesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Thursday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Friday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Saturday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Sunday",
      //     hour: "Close",
      //   },
      // ],

      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fhome%2F1.mp4?alt=media&token=919ce25b-512a-47c2-8a03-a16b52112790",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fhome%2F2.mp4?alt=media&token=50619d95-4fd9-4522-a438-37f949015cc1",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fhome%2F3.mp4?alt=media&token=7dcbf79f-e622-49b4-8a2a-200504f15cb5",

      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "818 530 3434",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F9_512x512.jpg?alt=media&token=6bc43da1-e3b7-41bc-a861-d70b1e4a6d90",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F10_512x512.jpg?alt=media&token=fce076a4-3ef9-4a31-972f-39acecdf6090",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F11_512x512.jpg?alt=media&token=3ef73bc6-e7f6-47ae-b745-a7bf05e4c523",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F12_512x512.jpg?alt=media&token=58a9ee67-979e-4f28-8104-27b38f7bcac6",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F13_512x512.jpg?alt=media&token=da7a891e-b574-4afa-9a55-9ad8396cabde",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F14_512x512.jpg?alt=media&token=04203bd4-13d5-4688-88fa-7c167a595aa7",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F15_512x512.jpg?alt=media&token=7e2ac8c2-7a08-471f-bba0-3904348354ba",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F16_512x512.jpg?alt=media&token=e59ced6c-e654-4f35-84f8-f683425e46a2",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fhome%2F1_512x512.jpg?alt=media&token=1d7fa7bb-b6cc-4825-9373-c07f54d4b31c",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fhome%2F2_512x512.jpg?alt=media&token=e41bf91f-0bf6-4412-88f4-1310f9005e8c",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fhome%2F3_512x512.jpg?alt=media&token=f6c191c5-f725-4880-807d-3dcbe3dccf0e",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fhome%2F4_512x512.jpg?alt=media&token=484046b8-4b16-41d3-a34c-ad2a8e699747",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F17_512x512.jpg?alt=media&token=d920161b-2275-4339-88a5-74b3f03abbeb",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F18_512x512.jpg?alt=media&token=ca295b9b-53f6-4de2-b2b9-87479436f033",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F19_512x512.jpg?alt=media&token=71616bae-3879-4258-863b-5a13fd06a46a",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F20_512x512.jpg?alt=media&token=e897ab75-29b2-4ac4-bb06-9f3d22e2d36b",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F21_512x512.jpg?alt=media&token=08b75a52-4c05-4341-9723-dbe02d148d69",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F22_512x512.jpg?alt=media&token=428a2c70-b081-49cb-8eea-7903d2a2a45c",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F24_512x512.jpg?alt=media&token=a478fc61-5ca0-4f06-adef-e98556051b1a",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F25_512x512.jpg?alt=media&token=27e7bfb6-98da-4fab-b748-a61f47657c86",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F26_512x512.jpg?alt=media&token=655c828d-152d-4eed-ae31-f0487e1ac9e5",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F5_512x512.jpg?alt=media&token=e5c1344a-d16c-420e-aaf7-d9e301edebe1",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F6_512x512.jpg?alt=media&token=591b0163-1d26-472d-88d5-dc581b832266",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F7_512x512.jpg?alt=media&token=beb7e320-e357-4c54-b4b5-09505ebfc9a1",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fgallery%2F8_512x512.jpg?alt=media&token=c9750d83-e155-4179-a6ec-8ce245392b44",
        
      ],

      socialNetworks: [
        // {
        //   text: "straightupbarbers",
        //   link: "https://www.instagram.com/straightupbarbers/",
        //   icon: "logo-instagram",
        // },
        // {
        //   text: "Straightupbarbers",
        //   link: "https://booksy.com/en-us/659011_straightupbarbers_barber-shop_103721_long-beach?do=invite&_branch_match_id=1008476610453898129&utm_medium=merchant_customer_invite&_branch_referrer=H4sIAAAAAAAAA8soKSkottLXT07J0UvKz88urtRLzs%2FVz0tNDDBNrixzyk0CAOPXazEiAAAA",
        //   icon: "book",
        // },
        // {
        //   text: "straight up barber's",
        //   link: "https://www.yelp.com/biz/straight-up-barbers-long-beach",
        //   icon: "logo-yahoo",
        // },
      ],

      address: "1358 5th #505 St. Santa Monica, CA 90401",
      addressRoute: "1358 5th St. Santa Monica, CA 90401",
      mapSource: "https://maps.google.com/maps?q=1358%205th%20St.%20Santa%20Monica,%20CA%2090401&t=&z=13&ie=UTF8&iwloc=&output=embed", 
    
      barbers:[
        {
          name: "Jonny D",
          description: "Business Owner",
          // socialNetworks:[
          //   {
          //     // text: "straightupbarbers",
          //     // link: "https://www.instagram.com/straightupbarbers/",
          //     // icon: "logo-instagram",
          //   },
          // ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fappoiments%2FLogo_512x512.png?alt=media&token=dad58947-5a1c-4f43-9c6e-c16950e9828b",
        },
      ]
    },

    appointmentsDays:[
      {
        day: 1,
        hours: this.hours,
        closed: false
      },
      {
        day: 2,
        hours: this.hours,
        closed: false
      },
      {
        day: 3,
        hours: this.hours,
        closed: false
      },
      {
        day: 4,
        hours: this.hours,
        closed: false
      },
      {
        day: 5,
        hours: this.hours,
        closed: false
      },
      {
        day: 6,
        hours: this.hours,
        closed: false
      },
      {
        day: 0,
        hours: this.hours,
        closed: false
      },
    ],
    
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
    //this.firebaseService.updateAppData(this.appData).then(t => {console.log(this.appData)}); 
  }


  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
     return this.firebaseService.getAppData().then(appData => {
        if(appData) {
          this.appData = appData as any;
          console.log(appData);
        }
      });
  }
}
