import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController, AlertController, PickerController } from '@ionic/angular';
import { UpdateUserPage } from './update-user/update-user.page';
import { FirebaseService, solicitud, state } from 'src/app/services/firebase.service';
import * as moment from 'moment';
import { UpdateAppointmentPage } from './update-appointment/update-appointment.page';
import { DataService } from 'src/app/services/data.service';
import { PickerOptions } from '@ionic/core';
import { ViewBarberPage } from './update-appointment/view-barber/view-barber.page';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.page.html',
  styleUrls: ['./appointments.page.scss'],
})
export class AppointmentsPage implements OnInit {

  d: any;
  userReady = false;
  private tempUserData: any;
  appConfiguration: any;
  public requests: any = [];
  private invalidRequests: any = [];
  selectedTab = 'pending';
  barbers: any;
  selectedBarber: any;
  currentBarber: any;
  solicitud = {} as solicitud;

  private loading: any;

  private requestRef: any;

  slideOpts = {
    loop: true,
    zoom: true,
    passiveListeners: false,
  };

  constructor(
    public data: DataService,
    private pickerController: PickerController,
    public firebaseService: FirebaseService, 
    private modalController: ModalController, 
    private loadingController: LoadingController,
    private alertController: AlertController) {
    }

  ngOnInit() {
    this.getAppData();

    this.data.getAppData().then(cloudTranslation => {
      this.d = this.data.appData.data;
      this.barbers = this.d.barbers;
        if(this.solicitud)
        {
          let solicitud=this.solicitud as any;
          this.currentBarber= solicitud.barber ? solicitud.barber: this.barbers[0];
        }
        else 
        {
          this.currentBarber= this.barbers[0];
        }
    });
  }

  

  getAppData() {
    this.loadingController.create({
      message: 'Loading...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();


      //Tomar Configuracion
      this.firebaseService.getAppConfiguration().then(appConfiguration => {
        this.appConfiguration = appConfiguration as any;

        //Datos de Usuario
        this.firebaseService.userSetup().then(userDataExist => {
          //var userExist = userDataExist as any;
          if(this.firebaseService.userData) {
            this.getRequests();
          }
          else {
            this.requests = [];
            this.invalidRequests = [];
          } 
        });
      });
    });
  }

  getRequests() {
    if(this.firebaseService.userData.admin) {
      //Admin Request List
      this.firebaseService.getRequests().then(requests => {
        if(requests !== undefined) this.requests = this.getValidRequests(this.getSortedRequests(requests));
        this.userReady = true;
        this.loading.dismiss();
      });
    }
    else {
      //User Requests List
      this.firebaseService.getUserRequests(this.firebaseService.userData.id).then(requests => {
        if(requests !== undefined) this.requests = this.getValidRequests(requests);
        this.userReady = true;
        this.loading.dismiss();
      });
    }
    this.getDataRealTime();
  }

  getDataRealTime() {
    if(this.firebaseService.userData.admin) {
      //Admin Request List
      if(this.requestRef) this.requestRef.unsubscribe();
      this.requestRef = this.firebaseService.getRequestsRealTime().subscribe(requests => {
        if(requests !== undefined) this.requests = this.getValidRequests(this.getSortedRequests(requests));
      });
    }
    else {
      //User Requests List
      if(this.requestRef) this.requestRef.unsubscribe();
      this.requestRef = this.firebaseService.getUserRequestsRealTime(this.firebaseService.userData.id).subscribe(requests => {
        if(requests !== undefined) this.requests = this.getValidRequests(requests);
      });
    }
  }

  getValidRequests(requests: any) {
    var validRequests = [];
    this.invalidRequests = [];
    var validDate = moment(moment().subtract(1, 'hours')).toDate();
    
    requests.forEach(request => {
      if(moment(validDate).isBefore(request.date)) validRequests.push(request);
      else this.invalidRequests.push(request);
    });

    if(this.invalidRequests.length > 0) this.deleteInvalidRequests();
    return validRequests;
  }

  getSortedRequests(requests: any) {
    var sortedRequests = [];

    requests.forEach(request => {
      if(this.isVIP(request.user.rating)) {
        sortedRequests.push(request);
      }
    });

    requests.forEach(request => {
      if(!this.isVIP(request.user.rating)) {
        sortedRequests.push(request);
      }
    });

    return sortedRequests;
  }

  deleteInvalidRequests() {
    if(this.firebaseService.userData.admin) {
      this.invalidRequests.forEach(request => {
        var invalidRequest = request;
        invalidRequest.valid = false;
        this.firebaseService.updateRequest(invalidRequest).then(deleteRequest => {
          this.firebaseService.deleteRequest(invalidRequest, false);
        });
      });
    }
  }

  getRequestState(requestState: number) {
    //AGREGAR CITA EN CURSO
    switch(requestState)
    {
      case state.pending: return {text: "Pending", color: "warning"};
      case state.accepted: return {text: "Accepted", color: "success"};
      case state.rejected: return {text: "Rejected", color: "danger"};
      default: return {text: "Pending", color: "warning"};
    }
  }

  allowRequest() {
    return ((this.firebaseService.userData.requestNumber < this.appConfiguration.maxUserRequestNumber && this.firebaseService.userData.allowRequests) || this.firebaseService.userData.admin)
  }

  acceptRequest(request: solicitud) {
    var coolRequest = request as any;
    coolRequest.lastChange = this.firebaseService.userData;
    this.firebaseService.acceptRequest(coolRequest);
  }

  rejectRequest(request: solicitud) {
    var coolRequest = request as any;
    coolRequest.lastChange = this.firebaseService.userData;
    this.firebaseService.rejectRequest(coolRequest);
  } 

  getRequestsByState(state: number) {
    var requests = [];
    this.requests.forEach(request => {
      if(request.state === state) requests.push(request);
    });
    return requests;
  }

  async createAppointment() {
    const ca = await this.modalController.create({
      component: UpdateAppointmentPage,
      componentProps : {
        userData: this.firebaseService.userData,
        appConfiguration: this.appConfiguration,
      }
    });
    await ca.present();
  }

  async updateAppointment(solicitud: solicitud) {
    const va = await this.modalController.create({
      component: UpdateAppointmentPage,
      componentProps : {
        solicitud: solicitud,
        userData: this.firebaseService.userData,
        appConfiguration: this.appConfiguration,
      }
    });
    await va.present();
  }

  getTempUser(user_id: string) {
    this.tempUserData = {} as any;

    this.loadingController.create({
      message: 'Loading...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      this.firebaseService.getUser(user_id).then(userData => {
        this.tempUserData = userData;
        this.viewTempUser();
      });
    });
  }

  viewTempUser() {
    this.modalController.create({
      component: UpdateUserPage,
      componentProps : {
        userData: this.tempUserData,
      }
    }).then(viewUserModal => {
      const vu = viewUserModal;
      vu.present();
      this.loading.dismiss();
    });
  }

  isVIP (rating: number) {
    return (rating >= this.appConfiguration.maxUserRating);
  }

  async rejectConfirm(request: solicitud) {
    const alert = await this.alertController.create({
      cssClass: 'cool-alert',
      header: "Reject Request",
      message: "Are you sure you want to reject this request?",
      buttons: [
        {
          text: "Cancel",
          role: 'cancel',
          cssClass: 'secondary',
        }, 
        {
          text: "Reject",
          handler: () => {
            this.rejectRequest(request);
          }
        }
      ]
    });

    await alert.present();
  }

  getBarbers()
  {
    var barbers = [];
    this.barbers.forEach(barber => {
      barbers .push({
        text: barber.name,
        value: barber.name,
      })
    });
    return barbers;
  }

  async selectBarber() {
    var options = this.getBarbers();
    var po: PickerOptions = {
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.selectedBarber=this.barbers[0].name;

          }
        },
        {
          text: 'Select',
          handler: () => {
            // this.selectBarber= null;
          }
        }
      ],
      columns: [
        {
          name: 'Barber',
          options: options,
        }
      ]
    }

    var picker = await this.pickerController.create(po);
    picker.present();
    picker.onWillDismiss().then(pickerValue => {
      if(pickerValue.data != undefined) 
      {
        this.selectedBarber = pickerValue.data.Barber.value;
        this.getBarber();
      }
    });
  }

  getBarber()
  {
    this.barbers.forEach(barber => {
      if (barber.name === this.selectedBarber)
      {
        this.currentBarber=barber;
      }
      
    });
    return this.currentBarber;
  }


  async viewBarber(barber: any) {
    const va = await this.modalController.create({
      component: ViewBarberPage,
      componentProps : {
        barber: barber
      }
    });
    await va.present();
  }

  
}

 