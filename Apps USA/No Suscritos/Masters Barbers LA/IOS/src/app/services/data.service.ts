import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Master Barbers LA",

      homeDescription: 
        `Master Barbers was established in 2016. Border-lining Westchester & Inglewood, we are located at Gateway Plaza. 
        Offering exceptional service with highly skilled barbers providing hairstyles for each individual lifestyle. 
        Services include clipper haircuts, all scissor haircuts, premium skin fades, beard detailing, and hot towel straight-edge razor shaves.
        An inventory of natural hair & shave products available. Experience our user friendly online booking system for children and adults of all ages. 
        Services also include, hair style consultation, hair texture with professional shears, kids haircuts, and facial clay masks. Complementary WiFi & phone chargers available.`,


        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F1_512x512.jpg?alt=media&token=66ec2133-6066-4d79-a38c-3dbee157c74c",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F2_512x512.jpg?alt=media&token=492b2970-dc7e-4a5a-9b4b-af8e9fdd8f6c",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F7_512x512.jpg?alt=media&token=71f85141-aea4-45d4-8ecd-9bfec28ba366",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F18_512x512.jpg?alt=media&token=a1ca46a8-a118-42e0-91c4-0747fa88abdc"
        ],


        homeServices: [
          {
            name:"Haircut & Style",
            time:"45 Minute",
            description:"Utilization of clippers and scissors (including texturizing shears), optional shaving around the hair edges and optional pomade application.",
            price: "$40"
          },
          {
            name:"Haircut & Shoulder Massage",
            time:"45 minutes",
            description:"Haircut & style followed by massage therapy utilizing our percussive handheld device over the shoulders for relief & relaxation.",
            price: "$43"
          },
          {
            name:"All Scissor Haircut & Style",
            time:"45 minutes",
            description:"For this classic & delicate service, we utilize fine shears (including texturizing shears) to cut the hair to the desired length.",
            price: "$45"
          },
          {
            name:"Head Shave",
            time:"45 minutes",
            description:"Two hot towels, hot lather, and gentle straight-edge razor shave around the scalp, lime aftershave & balm is then applied.",
            price: "$40"
          },
          {
            name:"Premium Skin Fade",
            time:"45 minutes",
            description:"From low tapers to high fades, we give you our closest, cleanest and long lasting skin fades with state of the art instruments.",
            price: "$40"
          },
          {
            name:"Services on Demand",
            time:"45 minutes",
            description:"For special occasions, sometimes you need service in a pinch. Please TEXT (310) 988-5675 to inquire about before/after hours and same day services.",
            price: "$60"
          },
          {
            name:"Father & Son Haircut",
            time:"90 minutes",
            description:"Experience the tradition of fresh haircut service for you and your son.",
            price: "$75"
          },
          {
            name:"Concierge Services",
            time:"120 minutes",
            description:"Contact us to set a date for a Master Barber to come to you. Prices may vary in accordance with services and distance.",
            price: "$100"
          },
          {
            name:"Two Haircut Reservation",
            time:"90 minutes",
            description:"Treat yourself and a family member or friend for a haircut service.",
            price: "$80"
          },
        ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Tuesday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Wednesday",
          hour: "09:00 am – 06:00 pm",
        },
        {
          day: "Thursday",
          hour: "09:00 am – 06:00 pm",
        },
        {
          day: "Friday",
          hour: "09:00 am – 06:00 pm",
        },
        {
          day: "Saturday",
          hour: "08:00 am – 06:00 pm",
        },
        {
          day: "Sunday",
          hour: "08:00 am – 06:00 pm",
        },
      ],
      
      homeVideos: [
        // "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fhome%2F1.mp4?alt=media&token=a96d19cf-203b-4ac6-880c-9e7e11f71744",
        // "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fhome%2F2.mp4?alt=media&token=eea0b8f9-e0e5-4084-b150-ec2998aa6d67",
        // "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fhome%2F3.mp4?alt=media&token=92e5a0e8-fd4f-4145-8c44-df4c81e51a73",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "424 393 4015",
        },
      ],

      gallery:[
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F10_512x512.jpg?alt=media&token=ce597244-62f5-46ee-89d6-38239bcc5d4e",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F11_512x512.jpg?alt=media&token=5d30677c-8148-47e2-a50d-dcebb647a6b0",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F12_512x512.jpg?alt=media&token=350a73a6-651b-4bc0-b53b-2f58350cc80c",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F13_512x512.jpg?alt=media&token=82cc1938-ff21-4394-b4a6-749f6ef45794",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F14_512x512.jpg?alt=media&token=e13226fe-1660-4133-8b3d-2432d7c6e124",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F15_512x512.jpg?alt=media&token=6dd528ab-43f1-4f61-8b66-da9838736e25",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F16_512x512.jpg?alt=media&token=7842898a-7627-4283-9e67-0ce8cc8b95b7",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F1_512x512.jpg?alt=media&token=66ec2133-6066-4d79-a38c-3dbee157c74c",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F2_512x512.jpg?alt=media&token=492b2970-dc7e-4a5a-9b4b-af8e9fdd8f6c",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F3_512x512.jpg?alt=media&token=e2ce8beb-0484-4f4f-8c5e-6de730807f4a",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F4_512x512.jpg?alt=media&token=86c53526-cba3-48cd-ac8e-4479bfa7a1aa",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F5_512x512.jpg?alt=media&token=cfc0e673-21a3-43e9-8ab7-88c0a8c4b1fd",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F6_512x512.jpg?alt=media&token=3dd6f0e2-49f7-4568-8bd2-e942ad74b15e",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F7_512x512.jpg?alt=media&token=71f85141-aea4-45d4-8ecd-9bfec28ba366",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F8_512x512.jpg?alt=media&token=b928d09f-212d-4a32-b47f-115677426d84",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F9_512x512.jpg?alt=media&token=13a1953f-0895-4a06-9f3a-f342b392bc3c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F17_512x512.jpg?alt=media&token=42d714f9-72cf-4709-866e-81a6f779d31b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F18_512x512.jpg?alt=media&token=a1ca46a8-a118-42e0-91c4-0747fa88abdc",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F19_512x512.jpg?alt=media&token=d11aeae9-231f-4451-bf07-573ddf5a8fba",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fgallery%2F20_512x512.jpg?alt=media&token=b211875f-bbcd-47a4-a2d6-786952685504", 
      ],

      products:[
      
       ],

      socialNetworks: [
        {
          text: "Master Barbers",
          link: "https://www.yelp.com/biz/master-barbers-los-angeles",
          icon: "logo-yahoo",
        },
        {
          text: "masterbarbersla",
          link: "https://www.instagram.com/masterbarbersla/?hl=en",
          icon: "logo-instagram",
        },
        {
          text: "Master Barbers",
          link: "https://www.facebook.com/masterbarbersla/",
          icon: "logo-facebook",
        },
      ],

      address: "5610 W. Manchester Ave. Los Angeles, CA 90045",

      addressRoute: "5610 W Manchester Ave, Los Angeles, CA 90045, USA",

      mapSource: "https://maps.google.com/maps?q=Master%20Barbers%20LA&t=&z=15&ie=UTF8&iwloc=&output=embed",
      
      barbers:[
        {
          name: "Edgar",
          description: "Veteran Barber",
          socialNetworks:[
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fappointments%2Fb1_512x512.jpg?alt=media&token=924bf9bf-ea69-4d97-a604-de279875500f",
        },
        {
          name: "Jackie",
          description: "Profesional Barber",
          socialNetworks:[
  
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fappointments%2Fb2_512x512.jpg?alt=media&token=f1950044-58ae-4011-a70a-eb48fa32f083",
  
        },
        {
          name: "Israel",
          description: "Master Barber",
          socialNetworks:[
  
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fappointments%2Fb4_512x512.jpg?alt=media&token=1d8cf132-43fe-459d-9f5e-cbbb44f0f9a6",
  
        },
  
        {
          name: "Mya",
          description: "stylist specialist",
          socialNetworks:[
  
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fappointments%2Fb3_512x512.jpg?alt=media&token=5e3ce5b9-e486-4d0f-9bc5-70c5e310c98e",
        },
      ],
    
    }

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

     //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)}); 
  }



  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
