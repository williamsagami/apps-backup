import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Homeboys Shaving Parlor",

      homeDescription: 
      `Homeboys shaving parlour is located In the city of Covina ca, We are located on the corner of Barranca and San Bernardino rd. <br>
      Across from Covina burger. <br>
      With the best barbers in the Covina, 
      many years of experience, providing an impeccable service for all the clients, where they can get to sit back, relax, and have an excellent hair cuts, 
      Do not hesitate to schedule the appointment with us, our professionals will be waiting for you. <br>
      We are closed on Sunday and Monday. <br>
      We also provide house calls with a scheduled appointment. Starts at $100`,


        homeSlider:[
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fhome%2F11_512x512.jpg?alt=media&token=72823241-1012-489c-8bd3-d9e66a109c6b",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fhome%2F13_512x512.jpg?alt=media&token=11f2a66f-337d-41c8-a79e-472a401fa01c",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fhome%2F15_512x512.jpg?alt=media&token=4d2da7ca-2900-4016-b1c8-5cbce31a3db4",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fhome%2F16_512x512.jpg?alt=media&token=a33d328b-c769-4633-b682-43dfe4df3b0e",          
        ],


        homeServices: [
          {
            name:"Walk in",
            // time:"30 minutes",
           description:"Of our best services book now",
            price: "$30"
          },
          {
            name:"Appoinments",
            // time:"30 minutes",
           description:"We also provide house calls with a scheduled appointment. Starts at $100",
            price: "$35"
          },
          {
            name:"Hair cut + shave",
            // time:"30 minutes",
           description:"Enjoy Hair cut and shave at an incredible price make your appointment now",
            price: "$40"
          },
          {
            name:"kids (3-9)",
            // time:"30 minutes",
           description:"Only children from 3 to 9 years old do not hesitate to book your appointment :)",
            price: "$25"
          },
        ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "Closed",
        },
        {
          day: "Tuesday",
          hour: "09:00 am – 08:00 pm",
        },
        {
          day: "Wednesday",
          hour: "09:00 am – 08:00 pm",
        },
        {
          day: "Thursday",
          hour: "09:00 am – 08:00 pm",
        },
        {
          day: "Friday",
          hour: "09:00 am – 08:00 pm",
        },
        {
          day: "Saturday",
          hour: "09:00 am – 08:00 pm",
        },
        {
          day: "Sunday",
          hour: "Closed",
        },
      ],
      
      homeVideos: [
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fhome%2F1.mp4?alt=media&token=273d421d-7831-4331-a7e8-1766ab9c3240",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fhome%2F2.mp4?alt=media&token=c240b22a-8035-4052-9c80-f5856f3ec92d",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fhome%2F3.mp4?alt=media&token=8b91a2b9-ad63-4eea-85f8-0887137ce115",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "760 508 3464",
        },
      ],

      gallery:[
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2Fcall_512x512.jpg?alt=media&token=e1700f74-0fe3-41f1-b842-9b491f2af499  ",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F10_512x512.jpg?alt=media&token=58d9ba65-621e-4fcc-8b27-0a39be887a99",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F11_512x512.jpg?alt=media&token=c1245364-b753-48cb-a76c-61b30b349eb7",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F12_512x512.jpg?alt=media&token=79ab9479-2bda-41e4-9af6-47d67fd95bc0",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F13_512x512.jpg?alt=media&token=e2fbab35-8c51-4267-9fd4-bfb1464d23a3",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F14_512x512.jpg?alt=media&token=1df334b6-08b9-4959-99c0-b113c7022250",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F15_512x512.jpg?alt=media&token=cc7e9a88-5bb7-439b-af8c-e316f456e673",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F16_512x512.jpg?alt=media&token=00424820-4392-460b-883f-e4a173ab550f",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F1_512x512.jpg?alt=media&token=08b43d50-cbb9-4e76-b6df-fba7ecfaed03",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F2_512x512.jpg?alt=media&token=60c2ac6b-753b-43e6-b984-02c59739eb35",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F3_512x512.jpg?alt=media&token=4cd15d89-039e-4afe-bc19-28b56c8c1dff",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F4_512x512.jpg?alt=media&token=c3812d82-e165-4847-a3dd-b2742ebd8af8",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F5_512x512.jpg?alt=media&token=556bbc24-32d3-461c-b63a-c428cb7160eb",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F6_512x512.jpg?alt=media&token=9525ba8a-c282-4db0-90a6-e465b1a4d61b",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F7_512x512.jpg?alt=media&token=c9536ce0-0339-4b41-bd4b-a92f74cc820c",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F8_512x512.jpg?alt=media&token=46c44502-8886-4de0-aad4-28a9581ae49b",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F8_512x512.jpg?alt=media&token=46c44502-8886-4de0-aad4-28a9581ae49b",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fgallery%2F9_512x512.jpg?alt=media&token=bcc6ca29-9583-4528-8df7-1e939fca5c33",
      ],

      products:[
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp7_512x512.jpg?alt=media&token=b6d4e33d-1c68-4b0b-851b-7b62e53351dd",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp10_512x512.png?alt=media&token=748f2e7b-98a9-4004-82ed-04820e25720b",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp11_512x512.png?alt=media&token=1d6643ec-49ae-4dd3-a88c-21c267358a95",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp1_512x512.png?alt=media&token=86f2a73e-f5d9-4ddf-8395-5e4e0969d262",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp2_512x512.png?alt=media&token=31228310-1351-48b7-95ec-ffdfa60795d9",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp3_512x512.png?alt=media&token=22c3709f-06e1-48bf-8600-8158b6063b53",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp4_512x512.png?alt=media&token=8265b522-a537-40e8-b7ec-6de553d6764e",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp5_512x512.png?alt=media&token=0602c55c-3f08-48a2-a315-5cfe41535fa0",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp6_512x512.png?alt=media&token=4e58f52b-67b3-48c6-b227-f34e8bf7c117",
        //"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp8_512x512.png?alt=media&token=c6792ffc-a7c4-4d36-8044-47c1ce287664",
       // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FArt%20Beauty%20Equilibrium%2Fproducts%2Fp9_512x512.png?alt=media&token=2bd52675-c1d8-4ddc-be58-51378228da92",
       ],

      socialNetworks: [
        {
          text: "homeboysshavingparlour",
          link: "https://www.instagram.com/homeboysshavingparlour/",
          icon: "logo-instagram",
        },
        {
          text: "Homeboys Shaving Parlor",
          link: "https://www.google.com/maps/place/Homeboys+Shaving+Parlor/@34.0906781,-117.8805686,15z/data=!4m2!3m1!1s0x0:0x9193b6918da02b65?sa=X&ved=2ahUKEwjmkNiSyoX0AhXLoFsKHRQKAUIQ_BJ6BAg8EAU",
          icon: "logo-google",
        },
        {
          text: "Homeboy’s Shaving Parlour",
          link: "https://www.yelp.com/biz/homeboy-s-shaving-parlour-covina-3",
          icon: "logo-yahoo",
        },
      ],
      barbers:[
        {
          name: "Motafadez_",
          description: "Veteran Barber",
          socialNetworks:[
            {
              text: "Motafadez_",
              link: "https://www.instagram.com/motafadez_/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fappointments%2Fb1_512x512.jpg?alt=media&token=e00b7156-574e-47ec-955b-98ec67061a61",
        },
        {
          name: "ajcut.s",
          description: "Master barber",
          socialNetworks:[
            {
              text: "@ajcut.s",
              link: "https://www.instagram.com/ajcut.s/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fappointments%2Fb2_512x512.jpg?alt=media&token=97e54c1b-6887-4d41-bb22-f63d5fd89d16",
  
        },
        {
          name: "fadesbysteph",
          description: "Expert barber",
          socialNetworks:[
            {
              text: "@fadesbysteph",
              link: "https://www.instagram.com/fadesbysteph/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fappointments%2Fb3_512x512.jpg?alt=media&token=59124858-7197-41c4-a375-1dd46ed3985c",
        },
  
        {
          name: "Kevin.a626",
          description: "professional barber",
          socialNetworks:[
            {
              text: "@Kevin.a626",
              link: "https://www.instagram.com/kevin.a626/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fappointments%2Fb4_512x512.jpg?alt=media&token=1df93f1b-cf72-4d8f-9b47-fdac53b264fa",
  
        },  
        {
          name: "rocky.Nash",
          description: "haircut specialist",
          socialNetworks:[
            {
              text: "@rocky.Nash",
              link: "https://www.instagram.com/rocky.nash/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fappointments%2Fb5_512x512.jpg?alt=media&token=cdec3ff2-f8bd-4dd3-ab93-d817035dd356",
  
        },
        {
          name: "23cuts_",
          description: "specialist Barber",
          socialNetworks:[
            {
              text: "@23cuts_",
              link: "https://www.instagram.com/23cuts_/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FHomeboys%20Shaving%20Parlor%2Fappointments%2Fb6_512x512.jpg?alt=media&token=bd672590-ac5e-431a-bb10-94c9acd9a233",
  
        },
      ],
      address: "526 E San Bernardino Rd, Covina, CA 91723, USA",

      addressRoute: "526 E San Bernardino Rd, Covina, CA 91723, USA",

      mapSource: "https://maps.google.com/maps?q=Homeboys%20Shaving%20Parlor&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }
  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 
    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
        //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)}); 
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
