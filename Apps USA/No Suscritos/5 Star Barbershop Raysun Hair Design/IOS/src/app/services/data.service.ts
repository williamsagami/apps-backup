import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "5 Star Barbershop Raysun Hair Design",

      homeDescription: 
        `5 Star Barbershop Raysun Hair Design is the best head designer in New York, 
        with more than 25 years of experience offering all kinds of styles and cuts, 
        seeking satisfaction to our customers, for us always having a positive energy is important at all times
        And our priority is you and your hair, to give you the best design you will have.
        I can do everything in Christ who strengthens me. PDT. I can face any situation because Christ gives me the power to do it.`,

      homeServices: [

      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "518 331 8824",
        }
      ],

      socialNetworks: [
        {
          text: "raysundesigns",
          link: "https://www.instagram.com/raysundesigns/",
          icon: "logo-instagram",
        },
        {
          text: "Raysun Brayan Lantigua",
          link: "https://www.facebook.com/raysundesign",
          icon: "logo-facebook",
        },
      ],

      barbers:[
        {
          name: "TekNik Hairizmything",
          description: "CelebrityStylist/Barber |Cosmetology & Barber Instructor Production Crew AV Tech/Wardrobe",
          socialNetworks:[
            {
              text: "teknikhairizmything",
              link: "https://www.instagram.com/teknikhairizmything/",
              icon: "logo-instagram",
            },
            {
              text: "Mobile Stylist/ Barber",
              link: "https://tekstylesbeautybrand.square.site/",
              icon: "globe",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2F5%20stars%20Barbershop%2Fappoiments%2Fb1_1024x1024.jpg?alt=media&token=bb6248a1-c402-4371-885e-d641abd5cf52",
        },
        {
          name: "Raysun",
          description: "Boss Barber and Veteran Barber",
          socialNetworks:[
            {
              text: "raysundesigns",
              link: "https://www.instagram.com/raysundesigns/",
              icon: "logo-instagram",
            },
            {
              text: "Raysun Brayan Lantigua",
              link: "https://www.facebook.com/raysundesign",
              icon: "logo-facebook",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2F5%20stars%20Barbershop%2Fappoiments%2FLogo_1024x1024.png?alt=media&token=a1d30332-50cf-4f95-ae68-ab373e947999",

        },
      ],

      address: "110 Quail St, Albany, NY 12206, USA",

      addressRoute: "110 Quail St, Albany, NY 12206, USA",

      mapSource: "https://maps.google.com/maps?q=5%20Star%20Barbershop,%20110%20Quail%20St,%20Albany,%20NY%2012206,%20Estados%20Unidos&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
    
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

 // this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
