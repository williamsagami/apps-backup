import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: "5 Star Barbershop Raysun Hair Design",
      en: "5 Star Barbershop Raysun Hair Design"
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    '11:30am to 7:30pm',
    '2:00pm to 7:00pm on Sunday',
    'Servicio a Domicilio',
  ];
  

  translateIndex = [
    {
    es:`5 Star Barbershop Raysun Hair Design es la mejor head designer de todo New York, con mas de 25 años de experiencia
    ofreciendo todo tipo de estilos y cortes, buscando la satisfaccion a nuestros clientes, para nosotros siempre tener una energia positiva es importante en todo momento
    Y nuestra prioridad eres tu y tu cabello, para darte el mejor diseño que tendras.
    Todo lo puedo en Cristo que me fortalece. PDT. Puedo enfrentar cualquier situación porque Cristo me da el poder para hacerlo.`,
    en:`5 Star Barbershop Raysun Hair Design is the best head designer in New York, 
    with more than 25 years of experience offering all kinds of styles and cuts, 
    seeking satisfaction to our customers, for us always having a positive energy is important at all times
    And our priority is you and your hair, to give you the best design you will have.
    I can do everything in Christ who strengthens me. PDT. I can face any situation because Christ gives me the power to do it.`
    },
    {
      es: 'Schedule',
      en: 'Schedule'
    },
  ];

  translateProcedures = [
    {
      es: '11:30am to 7:30pm',
      en: '11:30am to 7:30pm'
    },
    {
      es: '2:00pm to 7:00pm on Sunday',
      en: '2:00pm to 7:00pm on Sunday'
    },
    {
      es: 'Servicio a Domicilio',
      en: 'Home Service'
    },
  
    
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = true;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });


      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
