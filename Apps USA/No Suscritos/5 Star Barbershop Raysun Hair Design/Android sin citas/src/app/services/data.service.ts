import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { LlamadaPage } from '../pages/llamada/llamada.page';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  //slider
  sliderImages = [
    '../assets/images/1s.jpg',
    '../assets/images/2s.jpg',
    '../assets/images/3s.jpg',
    '../assets/images/4s.jpg',
  ]

  //galeria
  images = [
    'assets/images/1.jpg',
    'assets/images/2.jpg',
    'assets/images/3.jpg',
    'assets/images/4.jpg',
    'assets/images/5.jpg',
    'assets/images/6.jpg',
    'assets/images/7.jpg',
     'assets/images/8.jpg',
     'assets/images/9.jpg',
    'assets/images/10.jpg',
    'assets/images/11.jpg',
    'assets/images/12.jpg',
    'assets/images/13.jpg',
    'assets/images/14.jpg',
    'assets/images/15.jpg',
    'assets/images/16.jpg',
    'assets/images/17.jpg',
    'assets/images/18.jpg',
    'assets/images/19.jpg',
    'assets/images/20.jpg',
    'assets/images/21.JPG',
    'assets/images/22.JPG',
    'assets/images/23.JPG',
    'assets/images/24.JPG',
    'assets/images/25.JPG',
    'assets/images/26.JPG',
    'assets/images/27.jpg',
    'assets/images/28.jpg',
    'assets/images/29.jpg',
    //'assets/images/30.jpg',
    'assets/images/30.jpg',
    'assets/images/31.jpg',
    'assets/images/32.jpg',
    'assets/images/33.jpg',
    'assets/images/34.jpg',
    'assets/images/35.jpg',
    'assets/images/36.jpg',
    'assets/images/37.jpg',
    'assets/images/38.jpg',
    'assets/images/39.jpg',
    'assets/images/40.jpg',
    'assets/images/41.jpg',
    'assets/images/42.jpg',
    'assets/images/43.jpg',
    'assets/images/44.jpg',
    'assets/images/45.jpg',
    'assets/images/46.jpg',
    'assets/images/47.jpg',
    'assets/images/48.jpg',
    'assets/images/49.jpg',
    'assets/images/50.jpg',
    'assets/images/51.jpg',
    'assets/images/52.jpg',
    'assets/images/53.jpg',
    'assets/images/54.jpg',
    'assets/images/55.jpg',
    'assets/images/56.jpg',
    
  ]

  //contacto
  phones = [
    {
      number: '+1 518-331-8824', 
      call: '5183318824'
    },
  ];
  direction = '110 Quail St, Albany, NY 12206, Estados Unidos';

  redesSociales = [
    {
      name: "Raysun Brayan Lantigua",
      link: "https://www.facebook.com/raysundesign",
      icon: "logo-facebook",
    },
    {
      name: "raysundesigns",
      link: "https://www.instagram.com/raysundesigns/",
      icon: "logo-instagram",
    }
  ];
  constructor(private modalController: ModalController, private iab : InAppBrowser) { }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: LlamadaPage ,
      componentProps : {
        number: phoneNumber
      }
    });
    await modal.present();
  }

  openLink(link: string) {
      this.iab.create(link);
  }

}
