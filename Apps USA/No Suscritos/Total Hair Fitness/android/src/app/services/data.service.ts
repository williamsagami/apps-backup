import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Total Hair Fitness",

      homeDescription: 
      `Total Hair Fitness was established in 1991 in Old Town Torrance. Known as the best Hair salon in Torrance and surrounding areas. <br>
      With the vision of providing old-style specialized service with the newest in hair treatments, Hair colors, Hair styles, Blowouts, 
      Keratin treatment, Hair highlights, Men's haircuts,
      Balyage and more to be found on the fashion runways and red carpets and bringing them to the carpool. <br>
      Please call Today: (310) 320-1309`,

        homeSlider:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fhome%2F11_512x512.jpg?alt=media&token=75a53507-2d00-4051-8a4c-a52b7abecdd9",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fhome%2F20_512x512.jpg?alt=media&token=9a6ab742-ca1e-40e9-a530-2ddcf7b47da6",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fhome%2F21_512x512.jpg?alt=media&token=3a57183e-9aaa-4726-8e8c-62af9adf595f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fhome%2F3_512x512.jpg?alt=media&token=e3224194-04da-4216-ab8d-287130106044",          
        ],

        homeServices: [
          {
            name:"FULL WEAVE",
            // time:"30 minutes",
           description:"Track placement is key when trying to achieve a flat natural look.",
            price: "$300+"
          },
          {
            name:"MICRO BEAD TRACKS",
            // time:"30 minutes",
           description:"Careful placement hair is sewn onto an anchor created by your stylist, one of the easiest ways to achieve long beautiful hair.",
            price: "$50 per track "
          },
          {
            name:"TAPE-IN EXTENSIONS",
            // time:"30 minutes",
           description:"Full head placement,this method is great for fine hair. Using babe quality hair. (50% deposit is required at fitting)",
            price: "$220+"
          },
          {
            name:"U-TIP EXTENSIONS",
            // time:"30 minutes",
           description:"Individual hair extension technique using u-tip keratin bonds, recommended for any hair type. last 2-3 months with proper care.",
            price: "Price varies"
          },
          {
            name:"MICRO BEAD TRACKS",
            // time:"30 minutes",
           description:"areful placement hair is sewn onto an anchor created by your stylist, one of the easiest ways to achieve long beautiful hair.",
            price: "$50 per track "
          },
          {
            name:"DREAM CATCHER MICRO LINKS",
            // time:"30 minutes",
           description:"Individual hair extension technique using u-tip keratin bonds, recommended for any hair type. last 2-3 months with proper care.",
            price: "Price varies"
          },
          {
            name:"Q&A",
            // time:"30 minutes",
           description:"How long do the extensions last? Most extensions last 2-3 months with proper care. Remember you never want to leave any extensions in too long that breakage can occur. Two fingers from the scalp is a great indication to have them redone.",
            price: "Price varies"
          },
        ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "Closed",
        },
        {
          day: "Tuesday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Wednesday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Thursday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Friday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Saturday",
          hour: "08:30 am – 04:00 pm",
        },
        {
          day: "Sunday",
          hour: "Closed",
        },
      ],
      
      homeVideos: [
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fhome%2F1.mp4?alt=media&token=2bba8fcd-f809-4def-9ce5-6cfe0ff8e0e6",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "310 738 6826",
        },
      ],

      gallery:[
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F10_512x512.jpg?alt=media&token=64e8b797-38b9-49a9-a351-ff4855879626",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F11_512x512.jpg?alt=media&token=fa761c29-b7a5-438f-b9d5-0e5c3b4b5bba",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F12_512x512.jpg?alt=media&token=ee717003-afa3-4bfa-a71e-da8e27620973",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F13_512x512.jpg?alt=media&token=cef998ee-5d08-4a94-98bc-e0ac43ab1116",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F14_512x512.jpg?alt=media&token=5dfc8f6b-258c-422b-ab3b-c69fa4d32eb8",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F15_512x512.jpg?alt=media&token=567c1743-722b-4f90-b464-6947d71b6d34",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F16_512x512.jpg?alt=media&token=7d4e49ef-4f21-4ce4-a6d4-c0e07220b766",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F17_512x512.jpg?alt=media&token=ca9473a4-a573-432c-b14b-0b1ed3847fb8",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F18_512x512.jpg?alt=media&token=0d4133e5-e032-4ec9-b177-d5d2aa076925",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F19_512x512.jpg?alt=media&token=10f44394-61b8-4ee6-8fc6-a7d0a0954647",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F1_512x512.jpg?alt=media&token=bab45372-f139-48b9-8059-ecbfe7b62ec7",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F20_512x512.jpg?alt=media&token=05a6fc81-5d70-4491-aeff-a4b6581efdb1",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F21_512x512.jpg?alt=media&token=3f16f465-b513-47d2-994f-a2a845036cfa",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F22_512x512.jpg?alt=media&token=d0bc54a9-1218-4369-b6e3-30960511b4f5",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F23_512x512.jpg?alt=media&token=e0c21624-4188-4527-b7de-92ee439c542e",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F2_512x512.jpg?alt=media&token=71b08a59-7878-48b8-a06d-1ebeaa42bd25",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F3_512x512.jpg?alt=media&token=57565a7b-fcaa-40c2-abc4-eee8993bafdc",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F4_512x512.jpg?alt=media&token=6d17e7b3-91d9-4915-82d9-96731d2c1c86",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F5_512x512.jpg?alt=media&token=a06decac-4940-40c4-abde-a7b76e5e09ae",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F6_512x512.jpg?alt=media&token=90f85366-9fbf-40df-b50b-87fb04bf991f",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F7_512x512.jpg?alt=media&token=ed391738-45bc-4130-95f6-6bfb6369d0fa",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F8_512x512.jpg?alt=media&token=a592c1cb-9c1f-4c3b-888f-94e857e6b113",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F9_512x512.jpg?alt=media&token=227484d4-2fcf-44dd-8578-2ae767217dd7",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTotal%20Hair%20Fitness%2Fgallery%2F24_512x512.jpg?alt=media&token=0320c088-4c4b-4333-b3e6-0d8960a2ac3d",
    ],

      products:[
        
       ],

      socialNetworks: [
        {
          text: "totalhairfitness",
          link: "https://totalhairfitness.com/contact-us",
          icon: "globe",
        },
        {
          text: "totalhairfitness_2515",
          link: "https://www.instagram.com/totalhairfitness_2515/",
          icon: "logo-instagram",
        },
        {
          text: "totalhairfitness",
          link: "https://www.facebook.com/totalhairfitness",
          icon: "logo-facebook",
        },
        {
          text: "Total Hair Fitness",
          link: "https://www.yelp.com/biz/total-hair-fitness-torrance",
          icon: "logo-yahoo",
        },
      ],
      address: "2515 W Carson St Ste 106, Torrance, CA 90503, USA",
      addressRoute: "2515 W Carson St Ste 106, Torrance, CA 90503, USA",
      mapSource: "https://maps.google.com/maps?q=Total%20Hair%20Fitness%20%7C%20Best%20Hair%20Salon%20in%20Torrance!&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }
  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 
    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
        //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)}); 
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
