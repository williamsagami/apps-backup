import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Crossbronx Beauty Salon/Barbershop",

      homeDescription: 
        `Crossbronx Gear is one of the best Barbers in New York, with many years of experience and high quality cuts, with modern and innovative hair machines.
        Seeking that you can take incredible comfort with our services, we have excellent customer service because for us the most important thing is that you and your family can feel comfortable and have a wonderful experience.
        Our professionals will attend you and you will see that for an incredible and economical price your hair can have one of the best cuts and with our wide variety of haircuts you can take a unique experience.
        Do not hesitate to call us and be able to schedule your appointment, we will gladly assist you!`,
        homeDescription1: 
        `Crossbronx Gear is one of the best clothing store in New York with high quality clothing with one of the most attractive prices, we have a very large variety of clothing and impressive quality.  <br>
        We seek that our clients can buy their products with the best customer service and the fastest attention so that you and your family can wear one of the best clothes in all of New York.  <br>
        Our commitment to the client is to make them happy with our product, along with our service, and lastly, they can save money.  <br>
        Do not hesitate to visit us so that you have some of your best purchases that you can have.`,

      homeServices: [
        {
          name:"Mens Cuts",
          price: "$25"
        },
        {
          name:"18-14",
          price: "$20"
        },
        {
          name:"Womens",
          price: "$20"
        },
        {
          name:"Edge Ups, Eyebrows, Beards",
          price: "$15"
        },
        {
          name:"Designs + Extrase",
          price: "$15"
        },
        {
          name:"Shampoo",
          price: "$15"
        },
        {
          name:"The FakeOut",
          price: "$25"
        },
        {
          name:"13and Under",
          price: "$15"
        },
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "646 418 3815",
        }
      ],

      socialNetworks: [
        {
          text: "Cross Bronx Gear",
          link: "https://www.facebook.com/crossbronxgear",
          icon: "logo-facebook",
        },
        {
          text: "crossbronx_clothing",
          link: "https://www.instagram.com/crossbronx_clothing/",
          icon: "logo-instagram",
        },
        {
          text: "@CrossBronxGear",
          link: "https://twitter.com/crossbronxgear",
          icon: "logo-twitter",
        },
        {
          text: "Cross Bronx",
          link: "https://www.yelp.com/biz/cross-bronx-unisex-beauty-salon-and-barber-shop-bronx",
          icon: "logo-yahoo",
        },
        {
          text: "crossbronxgearny",
          link: "https://www.crossbronxgearny.com/",
          icon: "globe",
        }
      ],

      address: "1954 Cross Bronx Expy, The Bronx, NY 10472, Estados Unidos",
      
      addressRoute : "1954 Cross Bronx Expy, The Bronx, NY 10472, Estados Unidos",

      mapSource: "https://maps.google.com/maps?q=Crossbronx%20Beauty%20Salon/Barbershop,%201954%20Cross%20Bronx%20Expy,%20The%20Bronx,%20NY%2010472,%20Estados%20Unidos&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

 //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
