import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "U Got It BarberShop",

      homeDescription: 
        `One of the best barbershops in Arizona Giving the best service where you only come and we will attend you instantly with a spectacular price 
        and an impressive haircut so you can relax because our team will attend you and provide an unforgettable experience because 
        If you want an excellent haircut come to see me and you will get it. <br>
        The best you can get it, u got it barbershop.`,

      homeSlider:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fhome%2F1s_512x512.jpg?alt=media&token=43ded509-9224-4851-afb5-5a72b0eb2018",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fhome%2F2s_512x512.jpg?alt=media&token=1b1d4880-1edd-4110-9b4d-225decc0a601",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fhome%2F3s_512x512.jpg?alt=media&token=617aa410-f541-4824-8703-d9ac80d5fd02",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fhome%2F4s_512x512.jpg?alt=media&token=9e11b31a-e79d-4abd-8e4b-47b64bc155ac",
      ],

      homeServices: [
        {
          name:"Haircut",
          price: "$30"
        },
        {
          name:"Haircut & Beard",
          price: "$40"
        },
        {
          name:"Kids Cut",
          price: "$28"
        },
        {
          name:"Beard Trim",
          price: "$15"
        },
      ],
      homeSchedules: [
        {
          day: "Monday",
          hour: "10:00 AM - 6:00 PM",
        },
        {
          day: "Tuesday",
          hour: "10:00 AM - 6:00 PM",
        },
        {
          day: "Wednesday",
          hour: "10:00 AM - 6:00 PM",
        },
        {
          day: "Thursday",
          hour: "10:00 AM - 6:00 PM",
        },
        {
          day: "Friday",
          hour: "10:00 AM - 6:00 PM",
        },
        {
          day: "Saturday",
          hour: "09:00 AM – 05:00 PM",
        },
        {
          day: "Sunday",
          hour: "CLOSED",
        },
      ],
      homeVideos: [
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fhome%2F1.mp4?alt=media&token=db2826c8-28f3-42f3-b485-1ae684c2924a",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fhome%2F2.mp4?alt=media&token=c5356ab9-2c09-4179-a137-2d8ab96ad956",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fhome%2F3.mp4?alt=media&token=e33fce66-9039-4d7f-bb3a-bcc364561d78",
        ],

      f: [
        
      ],

      phoneNumbers: [
        {
          text: "U Got It",
          number: "520-744-9551",
        }       
      ],

      gallery:[
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F11_512x512.jpg?alt=media&token=cb7bb1e6-ed8e-4ebc-895b-e9469df1808d",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F12_512x512.jpg?alt=media&token=44176cc2-94d7-47f9-97b8-cd6e84a7f9d3",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F13_512x512.jpg?alt=media&token=9c4f400a-c1fc-4368-91bb-f98479c57a7a",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F14_512x512.jpg?alt=media&token=e5724245-1a25-4ebb-affc-d20c739dfb12",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F15_512x512.jpg?alt=media&token=593929ab-8477-4608-8ccd-51364aba0664",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F16_512x512.jpg?alt=media&token=1db374f5-1fc2-4e28-b307-139d27437e9a",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F17_512x512.jpg?alt=media&token=2b558bc9-5ac3-45e1-b8bb-100e57f9fe4c",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F18_512x512.jpg?alt=media&token=9df3a895-6714-45fb-828b-1dde22d60731",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F19_512x512.jpg?alt=media&token=b7ac8ce2-2210-4dbd-bd25-acf1b4dd162c",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F1_512x512.jpg?alt=media&token=263f536f-2238-43dd-9ff9-0354d45340d2",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F20_512x512.jpg?alt=media&token=cd9d5b7c-510d-4672-9d69-92b1b9f52a86",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F21_512x512.jpg?alt=media&token=3ef174bd-76df-458a-9e2f-2cfdca60c788",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F22_512x512.jpg?alt=media&token=2b81f5b6-8df8-4bbd-bff9-20afc5602da4",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F2_512x512.jpg?alt=media&token=dbc9c477-99a2-4716-8b8b-7c791a386a8b",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F3_512x512.jpg?alt=media&token=976ed1b5-0acd-4847-bcbe-4e9332c1ad56",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F4_512x512.jpg?alt=media&token=7a5653af-afda-439c-a044-8425e3e7179b",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F5_512x512.jpg?alt=media&token=5781ea08-1f5e-48e3-ab95-c4eea31bbf8c",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F6_512x512.jpg?alt=media&token=8260c7ac-e2c8-48e4-9322-50106972bf44",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F7_512x512.jpg?alt=media&token=dd621f76-489b-4d91-9503-149ddba81509",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F8_512x512.jpg?alt=media&token=f45d37c2-51ed-404a-aeb7-d2f4365b6136",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F9_512x512.jpg?alt=media&token=bbd1b8ab-d123-4df4-89c8-a624081b56a9",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fgallery%2F10_512x512.jpg?alt=media&token=204077c7-7120-4e85-9250-bc6662e82fbe",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FU%20Got%20It%20Barber%20Shop%2Fhome%2F1s_512x512.jpg?alt=media&token=43ded509-9224-4851-afb5-5a72b0eb2018",
      ],

      socialNetworks: [
        {
          text: "ugotit.barber",
          link: "https://www.instagram.com/ugotit.barber/?hl=es-la",
          icon: "logo-instagram",
        },
        {
          text: "U Got It Barber Shop",
          link: "https://www.facebook.com/U-Got-It-Barber-Shop-200396510020045/?ref=page_internal",
          icon: "logo-Facebook",
        },
        {
          text: "U Got It",
          link: "https://www.yelp.com/biz/u-got-it-tucson",
          icon: "logo-Yahoo",
        },
      ],

      address: "6741 N Thornydale Rd #159, Tucson, AZ 85741, United States",

      addressRoute: "6741 N Thornydale Rd #159, Tucson, AZ 85741, United States",

      mapSource: "https://maps.google.com/maps?q=U%20Got%20It%20Barber%20shop,&t=&z=15&ie=UTF8&iwloc=&output=embed",
    
    }

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

    //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
