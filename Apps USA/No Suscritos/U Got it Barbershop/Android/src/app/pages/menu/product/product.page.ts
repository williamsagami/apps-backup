import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { productsPage } from './products/products.page';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class productPage implements OnInit {

  d: any;

  images = [
    '../assets/images/p.jpg',
    '../assets/images/p1.jpg',
    '../assets/images/p2.jpg',
    '../assets/images/p3.jpg',
    '../assets/images/p4.jpg',
    '../assets/images/p5.jpg',
    '../assets/images/p6.jpg',
    '../assets/images/p7.jpg',
    '../assets/images/p8.jpg',
    '../assets/images/p9.jpg',
    '../assets/images/p10.jpg',
    '../assets/images/p11.jpg',
    '../assets/images/p12.jpg',
    '../assets/images/p13.jpg',
    '../assets/images/p14.jpg',
    '../assets/images/p15.jpg',
    '../assets/images/p16.jpg',
    '../assets/images/p17.jpg',
    '../assets/images/p18.jpg',
    '../assets/images/p19.jpg',
    '../assets/images/p20.jpg',
    '../assets/images/p21.jpg',
    '../assets/images/p22.jpg',
    '../assets/images/p23.jpg',
    '../assets/images/p24.jpg',
    '../assets/images/p25.jpg',
    '../assets/images/p26.jpg',
    '../assets/images/p27.jpg',
    '../assets/images/p28.jpg',
    '../assets/images/p29.jpg',
    '../assets/images/p30.jpg',
    '../assets/images/p31.jpg',
    '../assets/images/p32.jpg',
    '../assets/images/p33.jpg',
  ];

  constructor(private modalController: ModalController, public data: DataService) { 
    this.d = data.appData.data;
  }

  ngOnInit() {
    //Obtener Traduccion Dinamica
    this.data.getAppData().then(cloudTranslation => {
      this.d = this.data.appData.data;
       
      this.images=this.d.products;
    });
  }

  async openImage(i: number)
	{
    const modal = await this.modalController.create({
      component: productsPage,
      cssClass: 'modal-transparency',
      componentProps : {
        index: i+1,
        images: this.images,
      }
    });
    await modal.present();
	}

}
 