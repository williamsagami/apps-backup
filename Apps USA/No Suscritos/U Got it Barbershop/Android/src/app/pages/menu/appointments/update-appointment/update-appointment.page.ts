import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, ToastController, AlertController, LoadingController, PickerController } from '@ionic/angular';
import { FirebaseService, solicitud, usuario, state  } from 'src/app/services/firebase.service';
import * as moment from 'moment';
import { PickerOptions } from '@ionic/core';
import { ViewBarberPage } from './view-barber/view-barber.page';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-update-appointment',
  templateUrl: './update-appointment.page.html',
  styleUrls: ['./update-appointment.page.scss'],
})
export class UpdateAppointmentPage implements OnInit {
  d: any;
  userData: any;
  viewReady=false;
  appConfiguration: any;
  barbers: any;
  selectedBarber: any;
  currentBarber: any;

  solicitud = {} as solicitud;

 hours=[
    {
      text: "00:00 AM",
      value: moment().hours(0).minutes(0).seconds(0)
    },
    {
      text: "00:30 AM",
      value: moment().hours(0).minutes(30).seconds(0)
    },
    {
      text: "01:00 AM",
      value: moment().hours(1).minutes(0).seconds(0)
    },
    {
      text: "01:30 AM",
      value: moment().hours(1).minutes(30).seconds(0)
    },
    {
      text: "02:00 AM",
      value: moment().hours(2).minutes(0).seconds(0)
    },
    {
      text: "02:30 AM",
      value: moment().hours(2).minutes(30).seconds(0)
    },
    {
      text: "03:00 AM",
      value: moment().hours(3).minutes(0).seconds(0)
    },
    {
      text: "03:30 AM",
      value: moment().hours(3).minutes(30).seconds(0)
    },
    {
      text: "04:00 AM",
      value: moment().hours(4).minutes(0).seconds(0)
    },
    {
      text: "04:30 AM",
      value: moment().hours(4).minutes(30).seconds(0)
    },
    {
      text: "05:00 AM",
      value: moment().hours(5).minutes(0).seconds(0)
    },
    {
      text: "05:30 AM",
      value: moment().hours(5).minutes(30).seconds(0)
    },
    {
      text: "06:00 AM",
      value: moment().hours(6).minutes(0).seconds(0)
    },
    {
      text: "06:30 AM",
      value: moment().hours(6).minutes(30).seconds(0)
    },
    {
      text: "07:00 AM",
      value: moment().hours(7).minutes(0).seconds(0)
    },
    {
      text: "07:30 AM",
      value: moment().hours(7).minutes(30).seconds(0)
    },
    {
      text: "08:00 AM",
      value: moment().hours(8).minutes(0).seconds(0)
    },
    {
      text: "08:30 AM",
      value: moment().hours(8).minutes(30).seconds(0)
    },
    {
      text: "09:00 AM",
      value: moment().hours(9).minutes(0).seconds(0)
    },
    {
      text: "09:30 AM",
      value: moment().hours(9).minutes(30).seconds(0)
    },
    {
      text: "10:00 AM",
      value: moment().hours(10).minutes(0).seconds(0)
    },
    {
      text: "10:30 AM",
      value: moment().hours(10).minutes(30).seconds(0)
    },
    {
      text: "11:00 AM",
      value: moment().hours(11).minutes(0).seconds(0)
    },
    {
      text: "11:30 AM",
      value: moment().hours(11).minutes(30).seconds(0)
    },

    {
      text: "12:00 PM",
      value: moment().hours(12).minutes(0).seconds(0)
    },
    {
      text: "12:30 PM",
      value: moment().hours(12).minutes(30).seconds(0)
    },
    {
      text: "01:00 PM",
      value: moment().hours(13).minutes(0).seconds(0)
    },
    {
      text: "01:30 PM",
      value: moment().hours(13).minutes(30).seconds(0)
    },
    {
      text: "02:00 PM",
      value: moment().hours(14).minutes(0).seconds(0)
    },
    {
      text: "02:30 PM",
      value: moment().hours(14).minutes(30).seconds(0)
    },
    {
      text: "03:00 PM",
      value: moment().hours(15).minutes(0).seconds(0)
    },
    {
      text: "03:30 PM",
      value: moment().hours(15).minutes(30).seconds(0)
    },
    {
      text: "04:00 PM",
      value: moment().hours(16).minutes(0).seconds(0)
    },
    {
      text: "04:30 PM",
      value: moment().hours(16).minutes(30).seconds(0)
    },
    {
      text: "05:00 PM",
      value: moment().hours(17).minutes(0).seconds(0)
    },
    {
      text: "05:30 PM",
      value: moment().hours(17).minutes(30).seconds(0)
    },
    {
      text: "06:00 PM",
      value: moment().hours(18).minutes(0).seconds(0)
    },
    {
      text: "06:30 PM",
      value: moment().hours(18).minutes(30).seconds(0)
    },
    {
      text: "07:00 PM",
      value: moment().hours(19).minutes(0).seconds(0)
    },
    {
      text: "07:30 PM",
      value: moment().hours(19).minutes(30).seconds(0)
    },
    {
      text: "08:00 PM",
      value: moment().hours(20).minutes(0).seconds(0)
    },
    {
      text: "08:30 PM",
      value: moment().hours(20).minutes(30).seconds(0)
    },
    {
      text: "09:00 PM",
      value: moment().hours(21).minutes(0).seconds(0)
    },
    {
      text: "09:30 PM",
      value: moment().hours(21).minutes(30).seconds(0)
    },
    {
      text: "10:00 PM",
      value: moment().hours(22).minutes(0).seconds(0)
    },
    {
      text: "10:30 PM",
      value: moment().hours(22).minutes(30).seconds(0)
    },
    {
      text: "11:00 PM",
      value: moment().hours(23).minutes(0).seconds(0)
    },
    {
      text: "11:30 PM",
      value: moment().hours(23).minutes(30).seconds(0)
    },
  ]
  
  fecha: Date = moment().toDate();
  hora: Date =  this.hours[0].value.toDate();
  horarioFlexible = false;
  appointments: solicitud[] = [];
  mensaje: string;
  rangeLower: number = -1;
  rangeUpper: number = 1;
  minDate = this.fecha;
  maxDate = moment( moment().add(30, 'days') ).toDate();

  private loading: any;

  pickerStyle = {
    //cssClass: "datetime-picker",
  }


  

  constructor(
    public data: DataService,
    private pickerController: PickerController,
    private modalController: ModalController, 
    private navParams: NavParams, 
    private firebaseService: FirebaseService, 
    private toastController: ToastController,
    private alertController: AlertController,
    private loadingController: LoadingController,) { 

    this.userData = navParams.get('userData');
    this.solicitud = navParams.get('solicitud');
    this.appConfiguration = navParams.get('appConfiguration');

    this.maxDate = moment( moment().add(this.appConfiguration.dayRangeLimit, 'days') ).toDate();
   
  }

  ngOnInit() {
    if(this.solicitud) {
      this.fecha = this.solicitud.date;
      this.hora = this.solicitud.date;
      this.horarioFlexible = this.solicitud.flextime;
      this.mensaje = this.solicitud.message;
      this.rangeLower = this.solicitud.ftBefore;
      this.rangeUpper = this.solicitud.ftAfter;

    }

    this.data.getAppData().then(cloudTranslation => {
      this.d = this.data.appData.data;
      this.barbers = this.d.barbers;
        if(this.solicitud)
        {
          let solicitud=this.solicitud as any;
          this.currentBarber= solicitud.barber ? solicitud.barber: this.barbers[0];
          this.selectedBarber=this.currentBarber.name;
        }
        else 
        {
          this.currentBarber= this.barbers[0];
          this.selectedBarber=this.currentBarber.name;
        }
    });

    this.getAppointments();
  }

  

  closeView(){
	  this.modalController.dismiss();
  }

  setDay(event) {
    let date: Date = new Date(event.detail.value);
    this.fecha = date;
  }

  setHour(hour) {
    let date: Date = new Date(hour.toDate());
    this.hora = date;
    console.log(moment(this.hora).hours(), moment(hour).hours())
    console.log(moment(this.hora).minutes(), moment(hour).minutes())
    console.log(this.isHourSelected(hour))
  }

  isHourSelected(hour){
    if(moment(this.hora).hours() === moment(hour).hours() && moment(this.hora).minutes() === moment(hour).minutes()) return true;
    else return false;
  }

  getAppointments(){

    this.firebaseService.getRequests().then(appointments => {

      this.appointments = appointments as solicitud[];
      let apps = [] as solicitud[]
      this.appointments.forEach(appointment => {
        if(appointment.state === state.pending || appointment.state === state.accepted) apps.push(appointment);
      });
      this.appointments=apps;
      this.viewReady=true;
      console.log(this.appointments)
    })
  }

  isHourAvailable(hour){
    let choosenDate: Date = new Date(this.fecha);
    choosenDate.setHours(hour.toDate().getHours());
    choosenDate.setMinutes(hour.toDate().getMinutes());

    let result=true;
    this.appointments.forEach(appointment => {
     if((appointment as any).barber.name === this.selectedBarber && 
        moment(appointment.date).year() === moment(choosenDate).year() && 
        moment(appointment.date).month() === moment(choosenDate).month() &&
        moment(appointment.date).days() === moment(choosenDate).days() &&
        moment(appointment.date).hours() === moment(choosenDate).hours() &&
        moment(appointment.date).minutes() === moment(choosenDate).minutes()) result = false; 
    });
    return result;
  }

  rangeChange(event) {
    this.rangeLower = event.detail.value.lower;
    this.rangeUpper = event.detail.value.upper;
  }


  async showInfo(info: string) {
    const toast = await this.toastController.create({
      message: info,
      duration: 6000,
      color: 'secondary',
    });
    toast.present();
  }

  isNewRequest() {
    if(this.solicitud) return false;
    else return true;
  }

  createRequest() {
    var date: Date = new Date(this.fecha);
    date.setHours(this.hora.getHours());
    date.setMinutes(this.hora.getMinutes());
    var validDate = moment(date).isSameOrAfter(this.minDate);

    if(validDate) {
      this.loadingController.create({
        message: 'Creating Request...',
        cssClass: 'cool-loading',
      }).then(overlay => {
        this.loading = overlay;
        this.loading.present();

        var solicitud = {} as any;
        solicitud.id = '_'+this.userData.requestCount;
        solicitud.date = date;
        solicitud.creationDate = moment().toDate();
        solicitud.flextime = this.horarioFlexible;
        solicitud.message = (this.mensaje != undefined ? this.mensaje : "Undefined");
        solicitud.ftBefore = this.rangeLower;
        solicitud.ftAfter =  this.rangeUpper;
        solicitud.state = state.pending;
        solicitud.user = this.userData as usuario;
        solicitud.lastChange = this.userData as usuario;
        solicitud.valid = true;
        solicitud.barber= this.getBarber();

        this.firebaseService.createRequest(solicitud, this.userData).then(close => {
          this.loading.dismiss();
          this.closeView();
        });
      });
    } else {
      this.showInfo("You cannot assign a date before the current one.");
    }
  }

  updateRequest(state: number) {
    var date: Date = new Date(this.fecha);
    date.setHours(this.hora.getHours());
    date.setMinutes(this.hora.getMinutes());

    var validDate = moment(date).isSameOrAfter(this.minDate);

    if(validDate) {

      this.loadingController.create({
        message: 'Updating Request...',
        cssClass: 'cool-loading',
      }).then(overlay => {
        this.loading = overlay;
        this.loading.present();

        var solicitud = {} as any;
        solicitud.id = this.solicitud.id;
        solicitud.date = date;
        solicitud.creationDate = this.solicitud.creationDate;
        solicitud.flextime = this.horarioFlexible;
        solicitud.message = (this.mensaje != undefined ? this.mensaje : "Undefined");
        solicitud.ftBefore = this.rangeLower;
        solicitud.ftAfter =  this.rangeUpper;
        solicitud.state = state;
        solicitud.user = this.solicitud.user;
        solicitud.lastChange = this.userData as usuario;
        solicitud.valid = true;
        solicitud.barber= this.getBarber();

        this.firebaseService.updateRequest(solicitud).then(close => {
          this.loading.dismiss();
          this.closeView();
        });
      });
    } else {
      this.showInfo("You cannot assign a date before the current one.");
    }
  }

  updateRequestLastChange() {
    var solicitud = {} as any;
    solicitud.id = this.solicitud.id;
    solicitud.date = this.solicitud.date;
    solicitud.creationDate = this.solicitud.creationDate;
    solicitud.flextime = this.solicitud.flextime;
    solicitud.message = this.solicitud.message;
    solicitud.ftBefore = this.solicitud.ftBefore;
    solicitud.ftAfter =  this.solicitud.ftAfter;
    solicitud.state = this.solicitud.state;
    solicitud.user = this.solicitud.user;
    solicitud.lastChange = this.userData as usuario;
    solicitud.valid = true;
    solicitud.barber= this.getBarber();

    return this.firebaseService.updateRequest(solicitud);
  }

  deleteRequest() {
    this.loadingController.create({
      message: 'Deleting Request...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();
      
      this.updateRequestLastChange().then(lastChange => {
        this.firebaseService.deleteRequest(this.solicitud, true).then(close => {
          this.loading.dismiss();
          this.closeView();
        });
      });
    });
  }

  evaluateCreate() {
    if(this.userData.username === 'Guest' || this.userData.phone === 'Undefined') this.createConfirm();
    else this.createRequest();
  }

  async createConfirm() {
    var msg = this.userData.username === 'Guest' ? "username" : '' ;
    if(msg === '') msg = "phone number";
    else msg += this.userData.phone === 'Undefined' ? ' ' + "and phone number" : '';

    const alert = await this.alertController.create({
      cssClass: 'cool-alert',
      header: "User Data",
      message: "We recommend that you change your " + msg + " on the Account screen to schedule you more quickly.",
      buttons: [
        {
          text: "Cancel",
          role: 'cancel',
          cssClass: 'secondary',
        }, 
        {
          text: "Create",
          handler: () => {
            this.createRequest();
          }
        }
      ]
    });

    await alert.present();
  }

  async deleteConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'cool-alert',
      header: "Delete Request",
      message: "Are you sure you want to delete this request?",
      buttons: [
        {
          text: "Cancel",
          role: 'cancel',
          cssClass: 'secondary',
        }, 
        {
          text: "Delete",
          handler: () => {
            this.deleteRequest();
          }
        }
      ]
    });

    await alert.present();
  }

  async rejectConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'cool-alert',
      header: "Reject Request",
      message: "Are you sure you want to reject this request?",
      buttons: [
        {
          text: "Cancel",
          role: 'cancel',
          cssClass: 'secondary',
        }, 
        {
          text: "Reject",
          handler: () => {
            this.updateRequest(state.rejected);
          }
        }
      ]
    });

    await alert.present();
  }

  getBarbers()
  {
    var barbers = [];
    this.barbers.forEach(barber => {
      barbers .push({
        text: barber.name,
        value: barber.name,
      })
    });
    return barbers;
  }

  async selectBarber() {
    var options = this.getBarbers();
    var po: PickerOptions = {
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            this.selectedBarber=this.barbers[0].name;

          }
        },
        {
          text: 'Seleccionar',
          handler: () => {
            // this.selectBarber= null;
          }
        }
      ],
      columns: [
        {
          name: 'Barber',
          options: options,
        }
      ]
    }

    var picker = await this.pickerController.create(po);
    picker.present();
    picker.onWillDismiss().then(pickerValue => {
      if(pickerValue.data != undefined) 
      {
        this.selectedBarber = pickerValue.data.Barber.value;
        this.getBarber();

      }
    });
  }
  getBarber()
  {
    this.barbers.forEach(barber => {
      if (barber.name === this.selectedBarber)
      {
        this.currentBarber=barber;
      }
      
    });
    return this.currentBarber;
  }


  async viewBarber() {
    const va = await this.modalController.create({
      component: ViewBarberPage,
      componentProps : {
        barber: this.currentBarber,
      }
    });
    await va.present();
  }

}
