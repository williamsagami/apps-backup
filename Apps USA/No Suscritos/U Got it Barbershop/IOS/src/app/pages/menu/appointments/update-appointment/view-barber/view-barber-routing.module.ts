import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewBarberPage } from './view-barber.page';

const routes: Routes = [
  {
    path: '',
    component: ViewBarberPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewBarberPageRoutingModule {}
