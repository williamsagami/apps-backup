import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { ModalController } from '@ionic/angular';
import { UpdatePostPage } from './update-post/update-post.page';
import { post} from 'src/app/services/firebase.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  d: any; //App Data
  posts = [];

  postsReady = false;
  userData: any;
  adminPosts: any;

  private postsRef: any;
  private adminPostsRef: any;

  slider = [

  ];

  slideSales = {
    autoplay: {
      delay: 4000,
    }
  };

  slideOpts = {
    loop: true,
    autoplay: {
      delay: 2000,
    }
  };

  schedules = [
    {
      day: "Monday",
      hour: "Close",
    },
    {
      day: "Tuesday",
      hour: "10:00 am – 06:00 pm",
    },
    {
      day: "Wednesday",
      hour: "10:00 am – 06:00 pm",
    },
    {
      day: "Thursday",
      hour: "10:00 am – 06:00 pm",
    },
    {
      day: "Friday",
      hour: "10:00 am – 06:00 pm",
    },
    {
      day: "Saturday",
      hour: "10:00 am – 06:00 pm",
    },
    {
      day: "Sunday",
      hour: "12:00 am – 05:00 pm",
    },
  ];

  videos= [

  ];
  f= [

  ];

  constructor(
    public data: DataService, 
    private firebaseService: FirebaseService, 
    private modalController: ModalController,) {
    this.d = data.appData.data;
  }

  ngOnInit() {
    //Obtener Traduccion Dinamica
    this.data.getAppData().then(cloudTranslation => {
      this.d = this.data.appData.data;

      this.slider=this.d.homeSlider;
      this.schedules=this.d.homeSchedules;
      this.videos=this.d.homeVideos;
      this.f=this.d.f;


    });

    //Obtener Posts
    if(this.postsRef) this.postsRef.unsubscribe();
    this.postsRef = this.firebaseService.getPostsRealTime().subscribe(posts => {
      if(posts) this.posts = posts;
      this.postsReady = true;
    });

    if(this.adminPostsRef) this.adminPostsRef.unsubscribe();
    this.adminPostsRef = this.firebaseService.getPostDataRealTime().subscribe(adminPosts => {
      this.adminPosts = adminPosts;
    });
  }

  showPostEdit() {
    if(this.firebaseService.userData) return this.firebaseService.userData.admin; 
    else return false;
  }

  showPostCreate() {
    if(this.adminPosts) {
      return (this.showPostEdit() && this.adminPosts.postNumber < this.adminPosts.maxPostNumber);
    } 
    else return false;
  }

  async viewPost(post: post) {
    const va = await this.modalController.create({
      component: UpdatePostPage,
      componentProps : {
        post: post,
        create: false,
        adminPosts: this.adminPosts,
      }
    });
    await va.present();
  }

  async createPost() {
    const va = await this.modalController.create({
      component: UpdatePostPage,
      componentProps : {
        post: null,
        create: true,
        adminPosts: this.adminPosts,
      }
    });
    await va.present();
  }

}
