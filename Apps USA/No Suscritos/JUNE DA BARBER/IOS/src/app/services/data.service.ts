import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "JUNE DA BARBER",

      homeDescription: 
        `One of the best barbershops Atlanta with many years of experience. <br>
        large number of high quality services with amazing prices, looking for your cut to be the best cut you have ever made. <br>
        our professionals will assist you so that you just feel relaxed and in no time you will see a change in your haircut. <br>
        In addition, our family atmosphere means that you can bring the whole family to take a pleasant visit to our barber shop, do not forget to schedule your appointment now. <br> 
        It's not about what you take off But, what you leave on. That counts.`,

      homeServices: [
        {
          name:"Haircut & Face",
          price: "$50"
        },
        {
          name:"Haircut",
          price: "$40"
        },
        {
          name:"Line Up",
          price: "$25"
        },
        {
          name:"Razor Shave",
          price: "$60"
        },
        {
          name:"Beard Trim",
          price: "$20"
        },
        {
          name:"Kids Cut",
          price: "$25"
        },
        {
          name:"Eyebrows",
          price: "$10"
        },
        {
          name:"Adult design",
          price: "$60"
        },
        {
          name:"Kids design",
          price: "$35"
        },
        {
          name:"Haircut & Face",
          price: "$50"
        },
        {
          name:"Facial",
          price: "$30"
        },
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "770 527 5361",
        },
      ],

      socialNetworks: [
        {
          text: "June Da Barber",
          link: "https://booksy.com/en-us/159899_june-da-barber_barber-shop_134770_atlanta",
          icon: "calendar",
        },

        {
          text: "june_da_barber100",
          link: "https://www.instagram.com/june_da_barber100/",
          icon: "logo-instagram",
        },
        {
          text: "@junedabarber100",
          link: "https://twitter.com/junedabarber100",
          icon: "logo-twitter",
        }
      ],

      address: "2421 Piedmont Rd NE, Atlanta, GA 30324, Estados Unidos",

      addressRoute: "2421 Piedmont Rd NE, Atlanta, GA 30324, Estados Unidos",

      mapSource: "https://maps.google.com/maps?q=JUNE%20DA%20BARBER,%202421%20Piedmont%20Rd%20NE,%20Atlanta,%20GA%2030324,%20Estados%20Unidos&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

 //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
