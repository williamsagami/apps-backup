import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Straightedge BarberShop",

      homeDescription: 
        `It is one of the best Barbers in Kansas giving an excellent service with a very affordable price. <br>
        For us, your cut is an important part of you, that's why our professionals give the best effort and the best quality for you. <br>
        Our barber shop has a family atmosphere so that you bring your whole family and everyone has the desired cut at the best price.  <br>
        Do not hesitate to schedule your appointment TODAY! so you can get one of the best experiences in our barber shop.`,


        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2Fgallery%2F1.png?alt=media&token=2866c677-a3c5-4838-a4d8-b4455230e304",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2FHome%2F1.jpg?alt=media&token=8385564e-fcca-4874-99c5-32d60ff97bbb",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2FHome%2F1s.jpg?alt=media&token=437a1fc3-32ff-4a73-a9e4-2b4584352efb",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2FHome%2F4s.jpg?alt=media&token=0596f65a-9027-4e92-859c-ba41e408f9a3",
        ],


      homeServices: [
        {
          name:"Men's Haircut with Mustache Trim",
          price: "$35"
        },
        {
          name:"Specialty Haircut (Mohawk, Parts/Lines, etc)",
          price: "$35"
        },
        {
          name:"Men's Haircut",
          price: "$25"
        },
        {
          name:"Men's Haircut with Beard",
          price: "$35"
        },
        {
          name:"Haircut Corrections",
          price: "$35"
        },
        {
          name:"Women's Haircut",
          price: "$20"
        },
        {
          name:"Children's Haircut (12 and under)",
          price: "$15"
        },
        {
          name:"Children's Haircut (13 and over)",
          price: "$20"
        },
        {
          name:"Eye Arch",
          price: "$10"
        }
      ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "Close",
        },
        {
          day: "Tuesday",
          hour: "09:00 am – 07:00 pm",
        },
        {
          day: "Wednesday",
          hour: "09:00 am – 07:00 pm",
        },
        {
          day: "Thursday",
          hour: "09:00 am – 07:00 pm",
        },
        {
          day: "Friday",
          hour: "09:00 am – 07:00 pm",
        },
        {
          day: "Saturday",
          hour: "09:00 am – 07:00 pm",
        },
        {
          day: "Sunday",
          hour: "Close",
        },
      ],

      
      homeVideos: [

      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "316 390 1051",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2Fgallery%2F1.png?alt=media&token=2866c677-a3c5-4838-a4d8-b4455230e304",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2Fgallery%2F10.jpg?alt=media&token=6a08399e-cbb6-4c71-84c8-48d9d34f6520",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2Fgallery%2F11.jpg?alt=media&token=fdee1a35-b26d-4606-9dbd-eb7f2ae03cbc",        
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2Fgallery%2F12.jpg?alt=media&token=b814d9dc-4a3b-4070-bb72-bc770888b278",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2Fgallery%2F13.jpg?alt=media&token=d1d250a7-5c55-4ec3-836a-d3e312f6785b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2Fgallery%2F14.jpg?alt=media&token=fe136df6-3cfa-416b-a912-ee4956c3ceb9",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2Fgallery%2F2.jpg?alt=media&token=6c004652-7ffc-4d0e-b50f-44ee9b55330b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2Fgallery%2F3.png?alt=media&token=519d2f4a-d955-4e49-9977-f5218f332a4a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2Fgallery%2F4.jpg?alt=media&token=10d59b76-20ac-4da2-88da-4d50a68af7b5",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2Fgallery%2F5.jpg?alt=media&token=9dc9eb57-c0c4-4d87-bbc2-ff395bbbf9c2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2Fgallery%2F6.jpg?alt=media&token=3d1fa7fc-6b0d-41be-a40e-a331ef1f61dd",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2Fgallery%2F7.jpg?alt=media&token=b52754af-c243-4e8c-8ec0-fd7dbbfeb94d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2Fgallery%2F8.jpg?alt=media&token=3d4d7a05-0969-4ec9-b594-8990a388c67e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FStraightedge%20BarberShop%2Fgallery%2F9.jpg?alt=media&token=ff36d6eb-5579-4ee8-9817-88848a80c017",
      ],

      socialNetworks: [
        {
          text: "Straight Edge Barber Shop",
          link: "https://www.facebook.com/SEBSWK/about/?ref=page_internal",
          icon: "logo-facebook",
        },
        {
          text: "Straight Edge Barber Shop",
          link: "https://yellow.place/en/straight-edge-barber-shop-wichita-usa",
          icon: "logo-yahoo",
        },
        {
          text: "Straightedge Barber Shop",
          link: "https://www.google.com.mx/maps/place/Straightedge+Barber+Shop/@37.6641503,-97.307007,15z/data=!4m5!3m4!1s0x0:0x34eda43696072937!8m2!3d37.6641503!4d-97.307007?hl=es-419",
          icon: "logo-google",
        },
      ],

      address: "2507 E Harry St, Wichita, KS 67211, USA",

      mapSource: "https://maps.google.com/maps?q=Straightedge%20Barber%20Shop,%202507%20E%20Harry%20St,%20Wichita,%20KS%2067211,%20Estados%20Unidos&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

 //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
