import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Barbers Town",

      homeDescription: 
        `We provide the best haircuts in the DFW area,
         Barbers Town have the most professional barbers in a great atmosphere and awesome customer service with the best prices,
         so please stop by BARBERS TOWN and get your haircut. <br>
         Being in business for 5 years, we have the tools and the experience to give you a great quality haircut and shave.`,

      homeSlider:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fhome%2F1s.jpg?alt=media&token=834d467b-f881-4fab-af6c-a47fc3e51e31",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fhome%2F2s.jpg?alt=media&token=273ac1a0-3aad-44b8-87f0-4f428902672d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fhome%2F3s.jpg?alt=media&token=a3b1c8ea-606e-45c1-93f6-e66de22fc57e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fhome%2F4s.jpg?alt=media&token=d48c3b69-d008-481e-be2b-beabcf198ffc",
      ],

      homeServices: [
        {
          name:"HAIRCUT",
          price: "$25"
        },
        {
          name:"HAIRCUT PLUS",
          price: "$40"
        },
        {
          name:"HAIRCUT PREMIUM",
          price: "$50"
        },
        {
          name:"HAIRCUT GOLD",
          price: "$80"
        },
        {
          name:"HOT TOWEL SHAVE ",
          price: "$20"
        },
        {
          name:"EYEBROWS",
          price: "$8"
        },
        {
          name:"BLACK MASK",
          price: "$10"
        },
        {
          name:"FULL FACIAL",
          price: "$40"
        },
        {
          name:"KIDS (10 year under)",
          price: "$20"
        },
      ],
      homeSchedules: [
        {
          day: "Monday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Tuesday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Wednesday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Thursday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Friday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Saturday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Sunday",
          hour: "08:00 am – 06:00 pm",
        },
      ],
      homeVideos: [
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fhome%2F1.mov?alt=media&token=350ec8ff-a7e4-4b29-8273-e6020717a69c",        
      ],

      f: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fhome%2Ff.jpeg?alt=media&token=3389af8e-5da6-4d70-8261-65feb3775c40",
      ],

      phoneNumbers: [
        {
          text: "Tony",
          number: "817 204 8246",
        },
        {
          text: "Rob",
          number: "386 274 9260",
        },
        {
          text: "Saman",
          number: "615 397 0916",
        },
        {
          text: "Orlando",
          number: "682 249 3539",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F1.jpg?alt=media&token=ad4596b4-7918-4cdb-95c1-8d1104721608",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F10.jpg?alt=media&token=b9f33cb6-0843-440c-b4c1-75a87427b2cf",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F11%60.jpg?alt=media&token=bff44aeb-fab1-4a98-9736-f4b2a10cea44",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F12.jpg?alt=media&token=4f7382e7-e845-4d53-82de-a3b9e2a4c73b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F13.jpg?alt=media&token=3380d8cf-a6cc-4d42-a0f3-3ca17c34d839",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F14.jpg?alt=media&token=a9118f9d-a8aa-43cf-bff2-2741068c835f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F15.jpg?alt=media&token=1e64eb7c-56b1-4414-9867-0a9b629548eb",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F17.jpg?alt=media&token=d3cff80a-c30c-4047-9f5b-eaf88a5c3a4e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F18.jpg?alt=media&token=cb55e982-f363-4a44-a8cd-f4dd626d97ad",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F19.jpg?alt=media&token=3f2ae3cd-7127-48df-8b38-b6ee5375d61f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F1s.jpg?alt=media&token=fb2edb0b-c4ae-452f-8385-3a71698c06ea",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F2.jpg?alt=media&token=02267eac-2332-47ee-b6ef-a9c9adc4028b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F20.jpg?alt=media&token=32b06e63-c0e9-420e-8821-3c5702e619bd",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F21.jpg?alt=media&token=8dbce5ca-8f69-4e86-8738-5ee7fc1ee75f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F22.jpg?alt=media&token=aafb6820-ffc1-4e5e-b60a-4b68c19bfc8f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F2s.jpg?alt=media&token=49294978-73aa-4246-ae56-0ac135452308",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F3.jpg?alt=media&token=dfbc6451-66a2-422a-9ccb-a326f7166511",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F3s.jpg?alt=media&token=a81320bd-7e76-49e0-b6f5-68ed37b331a7",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F4.jpg?alt=media&token=f3afa8be-f1e7-4a92-bff5-4e83ca7386b3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F4s.jpg?alt=media&token=8e283b21-9c3b-4408-8548-df895067a9b4",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F5.jpg?alt=media&token=5345260c-a82b-4c56-98ff-02c0a07f5607",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F6.jpg?alt=media&token=823edd21-a1fc-48eb-ba04-9255cfdb5337",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F7.jpg?alt=media&token=98c6640a-2ee3-4f33-a7ac-ae06e7264c12",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F8.jpg?alt=media&token=1cd44b07-85c6-46f9-a2c0-d916dc3469e7",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fgallery%2F9.jpg?alt=media&token=cdce2354-15ca-4f5a-8025-29753c3b6e0b",
      ],

      socialNetworks: [
        {
          text: "barbers.town",
          link: "https://www.instagram.com/barbers.town/",
          icon: "logo-instagram",
        },
        {
          text: "Barbers Town",
          link: "https://www.facebook.com/barberstown/",
          icon: "logo-Facebook",
        },
      ],

      address: "2607 S Cooper St, Arlington, TX 76015, United States",

      addressRoute: "2607 S Cooper St, Arlington, TX 76015, United States",

      mapSource: "https://maps.google.com/maps?q=Barbers%20Town&t=&z=15&ie=UTF8&iwloc=&output=embed",
    
      barbers:[
        {
          name: "Tony",
          description: "Monday - Friday (10am-7pm) Saturday (8am - 6pm) <br> cashap: $barberstown <br> zelle: 81742048246",
          socialNetworks:[
            {
              text: "barbers.town",
              link: "https://www.instagram.com/barbers.town/",
              icon: "logo-instagram",
            },
            {
              text: "Barbers Town",
              link: "https://www.facebook.com/barberstown/",
              icon: "logo-Facebook",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fappoiments%2Fbarbers1.jpg?alt=media&token=9c236200-cc34-4870-aa87-fdaefaceb57d",

        },

        {
          name: "Rob",
          description: "Monday - Friday 10:00 am to 7:00 pm, saturday 9:00 am to 6:00 pm Appt only appointments are in 45 min. intervals same day cancellation <br> Cash app: $RM0517",
          socialNetworks:[],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fappoiments%2Fbarber%20sin%20imagenes.jpg?alt=media&token=8f3d2b33-7ffe-489e-a45c-b5baf48227d0",

        },
        {
          name: "Saman",
          description: "Hairstyles",
          socialNetworks:[
            {
              text: "saman_07_25",
              link: "https://www.instagram.com/saman_07_25/",
              icon: "logo-instagram",
            }
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fappoiments%2Fbarber3.jpg?alt=media&token=95da0641-10a2-42c7-8a43-c96e20efe4d9",

        },
        {
          name: "Orlando",
          description: "Expert Barber and good prices",
          socialNetworks:[
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FBarbers%20Town%2Fappoiments%2Fbarber%20sin%20imagenes.jpg?alt=media&token=8f3d2b33-7ffe-489e-a45c-b5baf48227d0",

        },
      ],
    }

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

   // this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
