import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Stay Sharp Barbershop",

      homeDescription: 
        `Stay Sharp Barbershop is one of the best barbershops in California. <br>
        having a spectacular customer service and with many years of experience offering style and quality and one of the good prices,
        with a huge variety of services and also offers products such as clothing or accessories with high quality in each of them products,
        We seek to be better day by day to give you an incredible haircut.  <br>
        Make your appointment now and you will have an amazing visit.`,

      homeServices: [
        "HAIRCUT", 
        "BEARD TRIM", 
        "DESIGN", 
        "EYEBROW", 
        "KID'S HAIRCUT", 
        "LINE UP",
        "HOT TOWEL SHAVE", 
        "COLOR ENHANCEMENT", 
        "RAZOR FADE",
        "BLACK MASK", 
        "HAIR WASH",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "209 810 0890",
        }
      ],

      socialNetworks: [
        {
          text: "Stay Sharp Barbershop",
          link: "https://www.instagram.com/stay_sharp_barbershop_/",
          icon: "logo-instagram",
        },
        {
          text: "Stay Sharp Barbershop",
          link: "https://www.facebook.com/staysharpbarbershop209/",
          icon: "logo-facebook",
        },
        {
          text: "Stay Sharp Barbershop",
          link: "https://www.yelp.com/biz/stay-sharp-barbershop-lodi",
          icon: "logo-yahoo",
        }
      ],

      address: "218 W Pine St, Lodi, CA 95240, USA",

      mapSource: "https://maps.google.com/maps?q=stay%20sharp%20barbershop%20218%20W%20Pine%20St,%20Lodi,%20CA%2095240&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

  //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
