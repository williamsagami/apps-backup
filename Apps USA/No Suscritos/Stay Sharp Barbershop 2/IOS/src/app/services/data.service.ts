import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Majesty Barber Hair Studio",

      homeDescription: 
        `Looking for a detailed precision haircut, braid, or style? We got you covered! Majesty Barber Hair Shop is your one-stop-shop for beauty accessories and gifts and professional 
        haircuts that we will make you look and feel good. <br>
        From old school styles to modern ​makeup trends, we can do it all. <br>
        We also offer natural beauty products. <br>
        Visit us once or twice a week for a full cut or trim, a fade, and even a Mohawk! Our talented hairdressers offer a light shave service with no razor blades, 
        as well as hair braiding. <br>
        Come and enjoy the majestic experience here at Majesty Barber Hair Studio!`,

      homeServices: [
        {
          name:"Men's Haircut with Mustache Trim",
          price: "$35"
        },
        {
          name:"Specialty Haircut (Mohawk, Parts/Lines, etc)",
          price: "$35"
        },
        {
          name:"Men's Haircut",
          price: "$25"
        },
        {
          name:"Men's Haircut with Beard",
          price: "$35"
        },
        {
          name:"Haircut Corrections",
          price: "$35"
        },
        {
          name:"Women's Haircut",
          price: "$20"
        },
        {
          name:"Children's Haircut (12 and under)",
          price: "$15"
        },
        {
          name:"Children's Haircut (13 and over)",
          price: "$20"
        },
        {
          name:"Eye Arch",
          price: "$10"
        }
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "240 678 1208",
        },
      ],

      socialNetworks: [
        {
          text: "majestybarbershop",
          link: "https://www.majestybarbershop.com/services",
          icon: "globe",
        },
        {
          text: "Appointments",
          link: "https://www.mytime.com/express_checkout/54850/54352",
          icon: "calendar",
        },
        {
          text: "majestybarbershop",
          link: "https://www.instagram.com/majestybarbershop/",
          icon: "logo-instagram",
        },
        {
          text: "Majesty VIP Barber",
          link: "https://www.facebook.com/majestyviphairstudio1/?ref=page_internal",
          icon: "logo-facebook",
        },
        {
          text: "@majestyviphair",
          link: "https://twitter.com/majestyviphair",
          icon: "logo-twitter",
        },
        {
          text: "Majesty Barber Hair Studio",
          link: "https://www.yelp.com/biz/majesty-barber-hair-studio-hyattsville",
          icon: "logo-facebook",
        },
        {
          text: "Howard Brown",
          link: "https://www.linkedin.com/public-profile/in/howard-brown-699490105?challengeId=AQHKNIeGn1wbmAAAAXiLa8i29NCQrZeEidV-HuJWmMSPpUvu2gnHYcb3BmrsMS7ToNG0dLIONVnp5FKWqchK9Uu_oKf4An2waQ&submissionId=4f2e6c59-659d-7116-ce2b-5998ec0d6678",
          icon: "logo-linkedin",
        }
      ],

      address: "3321 Toledo Terrace Suite 102-103, Hyattsville, MD 20782, USA",

      addressRoute:"3321 Toledo Terrace Suite 102-103, Hyattsville, MD 20782, USA",

      mapSource: "https://maps.google.com/maps?q=Majesty%20Barber%20Hair%20Studio&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

 //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
