import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Chosen Barber shop",
      homeDescription: 
        `One of the best barbershops in New York with. many years of experience. <br>
        having the customer service since our specialists will attend them quickly and without problem you will just sit down and relax, 
        our professionals will attend them, our service is fast and with an incredible price. <br>
        We want you to have the amazing experiences in your haircut and that it be the best cut you will have because for us It’s more than a haircut. <br>
        Do not hesitate to schedule an appointment and you will have a unique experience in barber shop. <br>`,
      homeServices: [
        {
          name:"Signature Haircut",
          price: "$30"
        },
        {
          name:"Haircut",
          price: "$25"
        },
        {
          name:"Beard Shaving/Hot Towel",
          price: "$18"
        },
        {
          name:"Beard Shaving/Hot Towel",
          price: "$18"
        },
        {
          name:"Beard Trim",
          price: "$15"
        },
        {
          name:"Kids Haircut",
          price: "$20"
        },
        {
          name:"FULL SERVICE",
          price: "$35"
        }
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: " 951 406 1017",
        },
      ],

      socialNetworks: [
        {
          text: "chosenbarbershopinc",
          link: "https://www.instagram.com/chosenbarbershopinc/",
          icon: "logo-instagram",
        },
        {
          text: "Chosen Barber Shop",
          link: "https://www.yelp.com/biz/chosen-barber-shop-the-bronx",
          icon: "logo-yahoo",
        },
      ],

      address: "2401B Westchester Ave, The Bronx, NY 10461, Estados Unidos",

      addressRoute: "2401B Westchester Ave, The Bronx, NY 10461, Estados Unidos",

      mapSource: "https://maps.google.com/maps?q=Chosen%20Barber%20Shop,%202401B%20Westchester%20Ave,%20The%20Bronx,%20NY%2010461,%20Estados%20Unidos&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

 //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
