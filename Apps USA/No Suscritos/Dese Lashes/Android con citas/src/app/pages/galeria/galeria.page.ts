import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { ImagePage } from '../image/image.page';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.page.html',
  styleUrls: ['./galeria.page.scss'],
})
export class GaleriaPage implements OnInit {

  t: any;
  d: any;

  images = [
    'assets/images/1.jpg',
    'assets/images/2.jpg',
    'assets/images/3.jpg',
    'assets/images/4.jpg',
    'assets/images/5.jpg',
    'assets/images/6.jpg',
    'assets/images/7.jpg',
     'assets/images/8.jpg',
     'assets/images/9.jpg',
    'assets/images/10.jpg',
    'assets/images/11.jpg',
    'assets/images/12.jpg',
    'assets/images/13.jpg',
    'assets/images/14.jpg',
    'assets/images/15.jpg',
    'assets/images/16.jpg',
    'assets/images/17.jpg',
    'assets/images/18.jpg',
    'assets/images/19.jpg',
    'assets/images/20.jpg',
    'assets/images/21.JPG',
    'assets/images/22.JPG',
    'assets/images/23.JPG',
    'assets/images/24.JPG',
    'assets/images/25.JPG',
    'assets/images/26.JPG',
   'assets/images/call.jpg',
  ];

  constructor(private modalController: ModalController, private data: DataService) { 
    this.t = data.appDataLocal.translate;
    this.d = data.appDataLocal.data;
  }

  ngOnInit() {
  }

  async openImage(i: number)
	{
    const modal = await this.modalController.create({
      component: ImagePage,
      cssClass: 'modal-transparency',
      componentProps : {
        index: i+1,
        images: this.images,
        translate: this.t,
      }
    });
    await modal.present();
	}

}
