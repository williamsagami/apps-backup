import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'Dese Lashes',
      en: 'Dese Lashes'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'School Girl (Todo el Conjunto)',
    'Top Model (Todo el Conjunto)',
    "DA' LASH (Todo el Conjunto)",
    '-----Cosmética',
    'Ortodoncia',
    'Endodoncia',
  ];
  

  translateIndex = [
    {
    es:`Dese Lashes, especialista en extensión de pestañas, con una Licenciatura en Cosmetología.
    Siendo una de las mejores en su profesion en todo California, donde contamos con diferentes extensiones en diferentes precios, 
    buscando que tu belleza se trasmita en tus ojos.`,
    en:`Dese Lashes, an eyelash extension specialist, with a Bachelor of Cosmetology.
    Being one of the best in her profession in California, where we have different extensions at different prices,
    looking for your beauty to be transmitted in your eyes.`
    },
    {
      es: 'Procedimientos',
      en: 'Procedures'
    },
  ];

  translateProcedures = [
    {
      es: 'School Girl (Todo el Conjunto)',
      en: 'School Girl (Full Set)'
    },
    {
      es: 'Top Model (Todo el Conjunto)',
      en: 'Top Model (Full Set)'
    },
    {
      es: "DA' LASH (Todo el Conjunto)",
      en: "DA' LASH (Full Set)"
    },
    {
      es: 'School Girl',
      en: 'School Girl (Fill)'
    },
    {
      es: 'Top Model',
      en: 'Top Model (Fill)'
    }, 
    {
      es: "DA' LASH",
      en: "DA' LASH (Fill)"
    },   
    
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = true;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });


      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
