import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Nomenclatura
  /*
  t_ = Titulo
  txt_ = Texto
  b_ = Boton
  msg_ = Mensaje
  al_ = Alerta
  alb_ = Boton de Alerta
  */

  //Datos de Aplicacion locales utilizados para mostrar texto en el app
  appDataLocal = {
    translate: { 
      //General
      currentTranslation: this.firebaseService.defaultLanguage,
      translationNumber: 2,
      appTitle: "Dese Lashes",
      al_Cargando: [
        "Cargando", 
        "Loading"
      ],
      al_Guardando: [
        "Guardando",
        "Saving"
      ],
      alb_Cancelar: [
        "Cancelar", 
        "Cancel"
      ],
      alb_Seleccionar: [
        "Seleccionar", 
        "Select"
      ],
      txt_Indefinido: [
        "Indefinido", 
        "Undefined"
      ],
  
      //Menu
      b_Menu: [
        ["INICIO", "HOME"],
        ["CITAS", "APPOINTMENTS"],
        ["GALERIA", "GALLERY"],
        ["CONTACTO", "CONTACT"],
      ],
      b_Lenguaje: [
        "ES", 
        "EN"
      ],
      b_Cuenta: [
        "Cuenta", 
        "Account"
      ],
      b_Suscribirse: [
        "Suscribirse",
        "Subscribe"
      ],
  
      //Login
      t_IniciarSesion: [
        "INICIAR SESION", 
        "LOGIN"
      ],
      txt_Acceder: [
        "Acceder", 
        "Login"
      ],
      txt_Registarse: [
        "Registrarse", 
        "Register"
      ],
      txt_Correo: [
        "Correo",
        "Email"
      ],
      txt_Contrasena: [
        "Contraseña", 
        "Password"
      ],
      txt_RContrasena: [
        "Repetir Contraseña", 
        "Repeat Password"
      ],
      b_Entrar: [
        "Entrar", 
        "Login"
      ],
      b_Registrarse: [
        "Registrarse", 
        "Register"
      ],
      msg_Campos: [
        "Debes llenar todos los campos.", 
        "You must fill all the fields."
      ],
      msg_Correo: [
        "Correo inválido.", 
        "Invalid email."
      ],
      msg_Usuario: [
        "Este correo no esta registrado.", 
        "This email is not registered."
      ],
      msg_Contrasena: [
        "Contraseña incorrecta.", 
        "Incorrect password."
      ],
      msg_ContrasenaChica: [
        "La contraseña debe tener al menos 6 caracteres.", 
        "The password must be at least 6 characters."
      ],
      msg_ContrasenaDiferente: [
        "Las contraseñas no son iguales.", 
        "Passwords are not the same."
      ],
      msg_UsuarioExiste: [
        "Este email ya está en uso.", 
        "This email is already in use."
      ],
      msg_IniciarSesion: [
        `Para agendar citas o ver tu cuenta necesitas iniciar sesion o registrarte.`,
        `To schedule appointments or view your account you need to log in or register.`
      ],
      al_CerrandoSesion: [
        "Cerrando Sesion",
        "Loging Out"
      ],
  
  
      //Inicio
      t_Inicio: [
        "INICIO", 
        "HOME"
      ],
      txt_Descripcion: [
        `Dese Lashes, especialista en extensión de pestañas, con una Licenciatura en Cosmetología.
        Siendo una de las mejores en su profesion en todo California, donde contamos con diferentes extensiones en diferentes precios, 
        buscando que tu belleza se trasmita en tus ojos.`,

        `Dese Lashes, an eyelash extension specialist, with a Bachelor of Cosmetology.
        Being one of the best in her profession in California, where we have different extensions at different prices,
        looking for your beauty to be transmitted in your eyes.`
      ],

      homeSlider:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fhome%2F1s.jpg?alt=media&token=c298620c-f141-481c-a072-5738268ed9d3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fhome%2F2s.jpg?alt=media&token=d22495c6-5218-4745-8105-e3c3dbe6637c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fhome%2F3s.jpg?alt=media&token=de48f2a5-63e0-4a40-80cb-11b96fa3d2ec",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fhome%2F4s.jpg?alt=media&token=b5203a1a-1282-45c7-b722-d07d8e1e15ee",
       ],
       
      t_Servicios: [
        "Servicios", 
        "Services"
      ],
      txt_Servicios: [
        ["School Girl (Todo el Conjunto)", "School Girl (Full Set)"],
        ["Top Model (Todo el Conjunto)", "Top Model (Full Set)"],
        ["DA' LASH (Todo el Conjunto)", "DA' LASH (Full Set)"],
        ["School Girl", "School Girl (Fill"],
        ["Top Model", "Top Model (Fill)"],
      ],
      txt_Horario: [
        "Clientes Satisfechos",
        "Satisfied customers"
      ],
      txt_HorarioDias: [
        [ "Lunes", "Monday" ],
        [ "Martes", "Tuesday" ],
        [ "Miércoles", "Wednesday" ],
        [ "Jueves", "Thursday" ],
        [ "Viernes", "Friday" ],
        [ "Sabado", "Saturday" ],
        [ "Domingo", "Sunday" ],
      ],
      txt_Cerrado: [
        "Cerrado",
        "Closed"
      ],

      //Blog
      t_Publicar: [
        "PUBLICAR",
        "POST"
      ],
      txt_EditarPublicacion: [
        "Editar Publicacion",
        "Edit Post"
      ],
      txt_TituloPublicacion: [
        "Titulo de Publicacion",
        "Post Title"
      ],
      txt_EscribeTitulo: [
        "Escribe Titulo",
        "Write Title"
      ],
      txt_ContenidoPublicacion: [
        "Contenido de Publicacion",
        "Post Content"
      ],
      txt_EscribeContenido: [
        "Escribe Contenido",
        "Write Content"
      ],
      txt_PorcentajeOferta: [
        "Porcentaje de Oferta",
        "Sale Percentage"
      ],
      txt_EscribeOferta: [
        "Escribe Oferta de 1 a 100",
        "Offer Percentage from 1 to 100"
      ],
      b_CrearPublicacion: [
        "Crear Publicacion",
        "Create Post"
      ],
      b_Publicar: [
        "Publicar",
        "Post"
      ],
      al_BorrarPublicacion: [
        "Borrar Publicacion",
        "Delete Post"
      ],
      al_BorrarPublicacionSeguro: [
        "Estas seguro que deseas Borrar esta Publicacion?",
        "Are you sure you want to Delete this Post?"
      ],
      msg_PublicacionCreada: [
        "Publicación Creada Correctamente.",
        "Post Created Successfully."
      ],
      msg_PublicacionActualizada: [
        "Publicación ha sido Actualizada.",
        "Post has been Updated."
      ],
      msg_PublicacionBorrada: [
        "La Publicación ha sido Borrada.",
        "Post has been Deleted."
      ],

      //Galeria
      t_Galeria: [
        "GALERIA", 
        "GALLERY"
      ],
  
      //Contacto
      t_Contacto: [
        "CONTACTO", 
        "CONTACT"
      ],
      txt_Llamar: [
        "LLAMAR",
        "CALL"
      ],
      txt_Telefonos: [
        "Telefonos", 
        "Phones"
      ],
      txt_Direccion: [
        "Direccion", 
        "Address"
      ],
      txt_Ubicacion: [
        "Ubicacion", 
        "Location"
      ],
      txt_RedesSociales: [
        "Redes Sociales", 
        "Social Networks"
      ],
  
      //Citas
      t_Citas: [
        "CITAS", 
        "APPOINTMENTS"
      ],
      txt_SolicitarCita: [
        "Solicitar Cita", 
        "Request Appointments"
      ],
      b_CrearSolicitud: [
        "Crear Solicitud", 
        "Create Request"
      ],
      txt_Solicitudes: [
        "Solicitudes", 
        "Requests"
      ],
      txt_Pendientes: [
        "Pendientes", 
        "Pending"
      ],
      txt_Aceptadas: [
        "Aceptadas", 
        "Accepted"
      ],
      txt_Rechazadas: [
        "Rechazadas", 
        "Rejected"
      ],
      txt_SolicitudesEnviadas: [
        "Solicitudes Enviadas", 
        "Requests Sent"
      ],
      txt_SolicitudesVacias: [
        "No has enviado ninguna solicitud",
        "You have not sent any request"
      ],
      txt_PendientesVacias: [
        "No hay solicitudes pendientes",
        "No pending requests"
      ],
      txt_AceptadasVacias: [
        "No hay solicitudes aceptadas",
        "No accepted requests"
      ],
      txt_RechazadasVacias: [
        "No hay solicitudes rechazadas",
        "No rejected requests"
      ],
  
      //Solicitud
      txt_CitaPendiente: [
        "Pendiente", 
        "Pending"
      ],
      txt_CitaAceptada: [
        "Aceptada", 
        "Accepted"
      ],
      txt_CitaRechazada: [
        "Rechazada", 
        "Rejected"
      ],
      txt_Fecha: [
        "Fecha", 
        "Date"
      ],
      txt_Semana: [
        ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
        ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      ],
      txt_Mes: [
        ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      ],
      txt_HorarioFlexible: [
        "Horario Flexible", 
        "Flextime"
      ],
      txt_Opcional: [
        "Opcional", 
        "Optional"
      ],
      txt_RangoTiempo: [
        "Rango de Tiempo", 
        "Time Range"
      ],
      txt_Servicio: [
        "Servicio", 
        "Service"
      ],
  
      //Crear Cita
      t_SolicitarCita: [
        "SOLICITAR CITA", 
        "REQUEST APPOINTMENT"
      ],
      txt_CrearSolicitud: [
        "Crear Solicitud", 
        "Create Request"
      ],
      txt_SeleccionarDia: [
        "Seleccionar Dia", 
        "Choose Day"
      ],
      txt_SeleccionarHora: [
        "Seleccionar Hora", 
        "Choose Hour"
      ],
      msg_FechaAnterior: [
        "No puedes asignar una fecha anterior a la actual.", 
        "You cannot assign a date before the current one."
      ],
      msg_RangoTiempo: [
        "Elige el rango antes y despues de la hora elegida que estes disponible.", 
        "Choose the range before and after the chosen time that you are available."
      ],
      msg_Servicio: [
        "Indica el servicio que deseas para agendarte fácilmente.", 
        "Indicate the service you want to easily schedule your appointment."
      ],
      al_NombreUsuario: [
        "nombre de usuario", 
        "username"
      ],
      al_NumeroTelefono: [
        "numero de telefono", 
        "phone number"
      ],
      al_YNumeroTelefono: [
        "y numero de telefono", 
        "and phone number"
      ],
      al_DatosUsuario: [
        "Datos de Usuario", 
        "User Data"
      ],
      al_Recomendamos: [
        "Recomendamos que cambies tu", 
        "We recommend that you change your"
      ],
      al_Agendarte: [
        "en la pantalla de Cuenta para agendarte de manera mas rapida.", 
        "on the Account screen to schedule you more quickly."
      ],
      al_CreandoSolicitud: [
        "Creando Solicitud",
        "Creating Request"
      ],
      alb_Crear: [
        "Crear", 
        "Create"
      ],
  
      //Modificar Cita
      t_ModificarCita: [
        "MODIFICAR CITA", 
        "MODIFY APPOINTMENT"
      ],
      txt_ActualizarSolicitud: [
        "Actualizar Cita", 
        "Update Request"
      ],
      al_RechazarSolicitud: [
        "Rechazar Solicitud", 
        "Reject Request"
      ],
      al_RechazarSeguro: [
        "Seguro que deseas rechazar esta solicitud?", 
        "Are you sure you want to reject this request?"
      ],
      alb_Rechazar: [
        "Rechazar", 
        "Reject"
      ],
      al_BorrarSolicitud: [
        "Borrar Solicitud", 
        "Delete Request"
      ],
      al_BorrarSeguro: [
        "Seguro que deseas borrar esta solicitud?", 
        "Are you sure you want to delete this request?"
      ],
      alb_Borrar: [
        "Borrar", 
        "Delete"
      ],
      b_Borrar: [
        "Borrar", 
        "Delete"
      ],
      al_BorrandoSolicitud: [
        "Borrando Solicitud",
        "Deleting Request"
      ],
      b_Actualizar: [
        "Actualizar", 
        "Update"
      ],
      al_ActualizandoSolicitud: [
        "Actualizando Solicitud",
        "Updating Request"
      ],
  
      //Modificar Usuario / Cuenta
      t_ModificarUsuario: [
        "MODIFICAR USUARIO", 
        "MODIFY USER"
      ],
      t_Cuenta: [
        "CUENTA",
        "ACCOUNT"
      ],
      txt_DatosCuenta: [
        "Datos de Cuenta", 
        "Account Data"
      ],
      txt_Rol: [
        "Rol", 
        "Role"
      ],
      txt_Administrador: [
        "Administrador",
        "Administrator"
      ],
      txt_Cliente: [
        "Usuario",
        "User"
      ],
      txt_Nivel: [
        "Nivel", 
        "Level"
      ],
      txt_Notificaciones: [
        "Notificaciones", 
        "Notifications"
      ],
      txt_Prestigio: [
        "Prestigio", 
        "Ranking"
      ],
      txt_Cancelaciones: [
        "Cancelaciones", 
        "Cancellations"
      ],
      txt_SolicitudesActivas: [
        "Solicitudes Activas", 
        "Active Requests"
      ],
      txt_SolicitudesCreadas: [
        "Solicitudes Creadas", 
        "Requests Created"
      ],
      txt_PermitirSolicitudes: [
        "Permitir Solicitudes", 
        "Allow Requests"
      ],
      b_CerrarSesion: [
        "Cerrar Sesion", 
        "Log Out"
      ],
      txt_OpcionesAplicacion: [
        "Opciones de Aplicacion", 
        "Application Options"
      ],
      txt_LimiteSolicitudes: [
        "Límite de Solicitudes por Usuario", 
        "Limit of Requests per User"
      ],
      txt_RangoDiasSolicitudes: [
        "Rango de Días de Creación de Solicitudes", 
        "Range of Request Creation Days"
      ],
      b_GuardarCambios: [
        "Guardar Cambios", 
        "Save Changes"
      ],
      msg_CambiosGuardados: [
        "Los cambios se han guardado correctamente.",
        "The changes have been saved successfully."
      ],
      msg_NotificacionesDesactivadas: [
        "Notificaciones Desactivadas.",
        "Notifications Off."
      ],
      msg_NotificacionesActivadas: [
        "Notificaciones Activadas.",
        "Notifications On."
      ],
    },

    data: {
      phoneNumbers: [
        {
          text: "Telefono",
          number: "661 912 8487",
        }
      ],

      socialNetworks: [
        {
          text: "deselashes",
          link: "https://www.deselashes.com/",
          icon: "globe",
        },   
        {
          text: "Dese Lashes",
          link: "https://www.facebook.com/deselashes",
          icon: "logo-facebook",
        },
        {
          text: "dese_lashes",
          link: "https://www.instagram.com/dese_lashes/",
          icon: "logo-instagram",
        },
      ],

      address: `2235 H st. suite D Bakersfield 93301`,

      mapSource: "https://maps.google.com/maps?q=2235%20H%20st.%20suite%20D%20Bakersfield%2093301&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = { 

    //Todas las traducciones que posiblemente cambien
    translate: {
      appTitle: "Dese Lashes",
      //Contenido de Inicio
      txt_Descripcion: [
        `Dese Lashes, especialista en extensión de pestañas, con una Licenciatura en Cosmetología.
        Siendo una de las mejores en su profesion en todo California, donde contamos con diferentes extensiones en diferentes precios, 
        buscando que tu belleza se trasmita en tus ojos.`,

        `Dese Lashes, an eyelash extension specialist, with a Bachelor of Cosmetology.
        Being one of the best in her profession in California, where we have different extensions at different prices,
        looking for your beauty to be transmitted in your eyes.`
      ],


     
      txt_ServiciosContenidoES: 
      ["School Girl (Todo el Conjunto)", "Top Model (Todo el Conjunto)", "DA' LASH (Todo el Conjunto)","School Girl", "Top Model"],
      txt_ServiciosContenidoEN: 
      ["School Girl (Full Set)", "Top Model (Full Set)", "DA' LASH (Full Set)","School Girl (Fill)","Top Model (Fill)"],
    },

    //Datos de Contacto
    data: {

      homeSlider:[
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fhome%2F1s_512x512.jpg?alt=media&token=afcb4f17-7358-4eb6-adfc-12f5fbff4cc5",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fhome%2F2s_512x512.png?alt=media&token=7d84b399-a71b-4cab-9fe2-5374e8a2dedb",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fhome%2F3s_512x512.png?alt=media&token=971cd0d6-3dae-45f5-b43a-5a8927db806e",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fhome%2F4s_512x512.png?alt=media&token=b420878a-d019-43bb-bb60-5dd2f291fd29",
       ],

      phoneNumbers: [
        {
          text: "Telefono",
          number: "661 912 8487",
        },
      ],

      socialNetworks: [
        {
          text: "deselashes",
          link: "https://www.deselashes.com/",
          icon: "globe",
        },   
        {
          text: "Dese Lashes",
          link: "https://www.facebook.com/deselashes",
          icon: "logo-facebook",
        },
        {
          text: "dese_lashes",
          link: "https://www.instagram.com/dese_lashes/",
          icon: "logo-instagram",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F10_512x512.jpg?alt=media&token=f7e78668-6723-4e70-aefd-f46f39807ae0",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F11_512x512.jpg?alt=media&token=5112b467-985f-4e36-8e4f-e0014834ab7e",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F12_512x512.jpg?alt=media&token=79931105-0a1e-4ee4-8b43-fd6fe87dc9d7",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F13_512x512.jpg?alt=media&token=b89bf900-3ed8-449f-80a2-b359436c6379",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F14_512x512.jpg?alt=media&token=064141d0-60e5-4a61-bca1-0d9e091e9849",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F15_512x512.jpg?alt=media&token=c3cf9fec-61eb-469b-9477-30923e1fb958",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F16_512x512.jpg?alt=media&token=cfc73839-18f8-4a6a-b32e-ac5f151945e4",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F17_512x512.jpg?alt=media&token=647b23f5-1b8a-4999-b491-6bdd462c2311",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F18_512x512.jpg?alt=media&token=1785a398-b20c-499a-b271-90442d5c10a2",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F19_512x512.jpg?alt=media&token=40df017a-b3a1-45dc-9239-fb0921892149",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F1_512x512.jpg?alt=media&token=5a9ab465-81a7-4966-87a7-01e0009f712e",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F20_512x512.jpg?alt=media&token=3f322cec-46d6-4f6c-a027-22818a67d01a",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F21_512x512.JPG?alt=media&token=07f018c6-3299-442b-9946-b141d22b4c0d",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F22_512x512.JPG?alt=media&token=9094c509-0519-4c2c-ab6a-7a7146025344",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F23_512x512.JPG?alt=media&token=d91e418c-4942-4a6c-9b20-3df07d6a2567",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F24_512x512.JPG?alt=media&token=f8078056-c7be-4615-8fd6-f25ea35d7ec4",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F25_512x512.JPG?alt=media&token=cbd57143-cc14-495f-b220-6219847b6666",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F26_512x512.JPG?alt=media&token=30f43556-f6aa-42bb-94e3-72b9d21f1aba",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F2_512x512.jpg?alt=media&token=9083e91a-311d-474e-82ed-d08907ed1da6",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F3_512x512.jpg?alt=media&token=a8b733ad-def9-4e2e-86f6-4f8f417378ca",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F4_512x512.jpg?alt=media&token=853467ce-f996-4474-b262-177960c7d526",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F5_512x512.jpg?alt=media&token=a6ecf5d1-df70-4d25-82a6-74b4ad5ca840",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F6_512x512.jpg?alt=media&token=dd5cf436-facd-45ff-8eef-6912c1624b0e",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F7_512x512.jpg?alt=media&token=b5dd053e-5170-4264-9f30-e0ea91e379cf",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F8_512x512.jpg?alt=media&token=771484e1-b786-4fbd-97a0-2fdaea5a7145",
        //"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FDese%20Lashes%2Fgallery%2F9_512x512.jpg?alt=media&token=35d9e68d-7e37-4030-ae13-2d7437ae9698",
       ],

      address: `2235 H st. suite D Bakersfield 93301`,

      addressRoute: "2235 H st. suite D Bakersfield 93301",

      mapSource: "https://maps.google.com/maps?q=2235%20H%20st.%20suite%20D%20Bakersfield%2093301&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
    //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
        data: this.appDataLocal.translate,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  changeLanguage() {
    this.appDataLocal.translate.currentTranslation++;
    if(this.appDataLocal.translate.currentTranslation >= this.appDataLocal.translate.translationNumber)
    {
      this.appDataLocal.translate.currentTranslation = 0;
    }

    if(this.firebaseService.userData) {
      var coolUser = this.firebaseService.userData;
      coolUser.language = this.appDataLocal.translate.currentTranslation;
      this.firebaseService.updateUser(coolUser);
    }
  }

  setLanguage(language: number) {
    if(language >= 0 && language < this.appDataLocal.translate.translationNumber) { this.appDataLocal.translate.currentTranslation = language }
    else this.appDataLocal.translate.currentTranslation = this.firebaseService.defaultLanguage;
  }

  getServices() {
    var servicios = [];
    this.appDataLocal.translate.txt_Servicios.forEach((service, i) => {
      var servicio = [this.appData.translate.txt_ServiciosContenidoES[i], this.appData.translate.txt_ServiciosContenidoEN[i]]
      servicios.push(servicio);
    });
    return servicios;
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        this.appDataLocal.translate.appTitle = this.appData.translate.appTitle;
        this.appDataLocal.translate.txt_Descripcion = this.appData.translate.txt_Descripcion;
        this.appDataLocal.translate.txt_Servicios = this.getServices();
        this.appDataLocal.data = this.appData.data;
        //console.log(appData);
      }
    });
  }
}
