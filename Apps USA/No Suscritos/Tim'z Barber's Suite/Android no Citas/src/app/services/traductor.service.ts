import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: "Tim'z barber suite",
      en: "Tim'z Barber Suite"
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    '11:30am to 7:30pm',
    '2:00pm to 7:00pm on Sunday',
    'Servicio a Domicilio',
  ];
  

  translateIndex = [
    {
    es:`Una de las mejores Barberias de todo San Jose, buscando el mejor estilo para ti y tu cabello, 
    dando el mejor servicio, ofrecemos servicio a domicilio para que tu no tengas ni que viajar nosotros vamos a por ti.
    Logrando cuidar tu cabello mostrando tu mejor corte en cualquier lugar. Haz tu reserva Ahora!`,
    en:`One of the best Barbers in San Jose, looking for the best style for you and your hair,
    Giving the best service, we offer Home Service so that you do not even have to travel we will go to you.
    Being able to take care of your hair showing your best cut anywhere. Make your reservation Now!`
    },
    {
      es: 'Schedule',
      en: 'Schedule'
    },
  ];

  translateProcedures = [
    {
      es: '11:30am to 7:30pm',
      en: '11:30am to 7:30pm'
    },
    {
      es: '2:00pm to 7:00pm on Sunday',
      en: '2:00pm to 7:00pm on Sunday'
    },
    {
      es: 'Servicio a Domicilio',
      en: 'Home Service'
    },
  
    
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = true;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });


      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
