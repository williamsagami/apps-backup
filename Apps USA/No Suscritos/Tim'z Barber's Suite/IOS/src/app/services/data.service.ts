import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Tim'z Barber Suite",

      homeDescription: 
      `One of the best Barbers in San Jose, looking for the best style for you and your hair,
      Giving the best service, we offer Home Service so that you do not even have to travel we will go to you.
      Being able to take care of your hair showing your best cut anywhere. Make your reservation Now!`,

      homeServices: [
        {
          name:"Mens Cuts",
          price: "$25"
        },
        {
          name:"18-14",
          price: "$20"
        },
        {
          name:"Womens",
          price: "$20"
        },
        {
          name:"Edge Ups, Eyebrows, Beards",
          price: "$15"
        },
        {
          name:"Designs + Extrase",
          price: "$15"
        },
        {
          name:"Shampoo",
          price: "$15"
        },
        {
          name:"The FakeOut",
          price: "$25"
        },
        {
          name:"13and Under",
          price: "$15"
        },
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "408 509 3891",
        }
      ],

      socialNetworks: [
        {
          text: "Tim'z Barber's Suite",
          link: "https://www.facebook.com/Timz-Barbers-Suite-104237581478803/reviews/?ref=page_internal",
          icon: "logo-facebook",
        },
        {
          text: "timmylightning_barber",
          link: "https://www.instagram.com/timmylightning_barber/",
          icon: "logo-instagram",
        },
        {
          text: "Booksy tim'z barber suite",
          link: "https://booksy.com/en-us/12897_tim-z-barber-suite_barber-shop_134690_san-jose",
          icon: "globe",
        }
      ],

      address: "1773 Capital Expy Suite 305 San Jose CA 95121",

      addressRoute: "1773 Capital Expy Suite 305 San Jose CA 95121",

      mapSource: "https://maps.google.com/maps?q=Tim'z%20Barber%20Suite,%201773%20Capital%20Expy%20Suite%20305%20San%20Jose%20CA%2095121&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

    this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
