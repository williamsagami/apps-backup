import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  hours=[
    // {
    //   text: "00:00 AM",
    //   value: moment().hours(0).minutes(0).seconds(0)
    // },
    // {
    //   text: "00:30 AM",
    //   value: moment().hours(0).minutes(30).seconds(0)
    // },
    // {
    //   text: "01:00 AM",
    //   value: moment().hours(1).minutes(0).seconds(0)
    // },
    // {
    //   text: "01:30 AM",
    //   value: moment().hours(1).minutes(30).seconds(0)
    // },
    // {
    //   text: "02:00 AM",
    //   value: moment().hours(2).minutes(0).seconds(0)
    // },
    // {
    //   text: "02:30 AM",
    //   value: moment().hours(2).minutes(30).seconds(0)
    // },
    // {
    //   text: "03:00 AM",
    //   value: moment().hours(3).minutes(0).seconds(0)
    // },
    // {
    //   text: "03:30 AM",
    //   value: moment().hours(3).minutes(30).seconds(0)
    // },
    // {
    //   text: "04:00 AM",
    //   value: moment().hours(4).minutes(0).seconds(0)
    // },
    // {
    //   text: "04:30 AM",
    //   value: moment().hours(4).minutes(30).seconds(0)
    // },
    // {
    //   text: "05:00 AM",
    //   value: moment().hours(5).minutes(0).seconds(0)
    // },
    // {
    //   text: "05:30 AM",
    //   value: moment().hours(5).minutes(30).seconds(0)
    // },
    // {
    //   text: "06:00 AM",
    //   hours: 6,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "06:30 AM",
    //   hours: 6,
    //   minutes:30,
    //   seconds: 0
    // },
    // {
    //   text: "07:00 AM",
    //   hours: 7,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "07:30 AM",
    //   hours: 7,
    //   minutes:30,
    //   seconds: 0
    // },
    {
      text: "08:00 AM",
      hours: 8,
      minutes:0,
      seconds: 0
    },
    {
      text: "08:30 AM",
      hours: 8,
      minutes:30,
      seconds: 0
    },
    {
      text: "09:00 AM",
      hours: 9,
      minutes:0,
      seconds: 0
    },
    {
      text: "09:30 AM",
      hours: 9,
      minutes:30,
      seconds: 0
    },
    {
      text: "10:00 AM",
      hours: 10,
      minutes:0,
      seconds: 0
    },
    {
      text: "10:30 AM",
      hours: 10,
      minutes:30,
      seconds: 0
    },
    {
      text: "11:00 AM",
      hours: 11,
      minutes:0,
      seconds: 0
    },
    {
      text: "11:30 AM",
      hours: 11,
      minutes:30,
      seconds: 0
    },

    {
      text: "12:00 AM",
      hours: 12,
      minutes:0,
      seconds: 0
    },
    {
      text: "12:30 AM",
      hours: 12,
      minutes:30,
      seconds: 0
    },
    {
      text: "13:00 AM",
      hours: 13,
      minutes:0,
      seconds: 0
    },
    {
      text: "01:30 PM",
      hours: 13,
      minutes:30,
      seconds: 0
    },
    {
      text: "02:00 PM",
      hours: 14,
      minutes:0,
      seconds: 0
    },
    {
      text: "02:30 PM",
      hours: 14,
      minutes:30,
      seconds: 0
    },
    {
      text: "03:00 PM",
      hours: 15,
      minutes:0,
      seconds: 0
    },
    {
      text: "03:30 PM",
      hours: 15,
      minutes:30,
      seconds: 0
    },
    {
      text: "04:00 PM",
      hours: 16,
      minutes:0,
      seconds: 0
    },
    {
      text: "04:30 PM",
      hours: 16,
      minutes:30,
      seconds: 0
    },
    {
      text: "05:00 PM",
      hours: 17,
      minutes:0,
      seconds: 0
    },
    {
      text: "05:30 PM",
      hours: 17,
      minutes:30,
      seconds: 0
    },
    {
      text: "06:00 PM",
      hours: 18,
      minutes:0,
      seconds: 0
    },
    {
      text: "06:30 PM",
      hours: 18,
      minutes:30,
      seconds: 0
    },
    {
      text: "07:00 PM",
      hours: 19,
      minutes:0,
      seconds: 0
    },
    {
      text: "07:30 PM",
      hours: 19,
      minutes:30,
      seconds: 0
    },
    // {
    //   text: "08:00 PM",
    //   value: moment().hours(20).minutes(0).seconds(0)
    // },
    // {
    //   text: "08:30 PM",
    //   value: moment().hours(20).minutes(30).seconds(0)
    // },
    // {
    //   text: "09:00 PM",
    //   value: moment().hours(21).minutes(0).seconds(0)
    // },
    // {
    //   text: "09:30 PM",
    //   value: moment().hours(21).minutes(30).seconds(0)
    // },
    // {
    //   text: "10:00 PM",
    //   value: moment().hours(22).minutes(0).seconds(0)
    // },
    // {
    //   text: "10:30 PM",
    //   value: moment().hours(22).minutes(30).seconds(0)
    // },
    // {
    //   text: "11:00 PM",
    //   value: moment().hours(23).minutes(0).seconds(0)
    // },
    // {
    //   text: "11:30 PM",
    //   value: moment().hours(23).minutes(30).seconds(0)
    // },
  ]
  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {

    //Datos de Contacto
    data: {

      appTitle: "Straight up barbers",

      homeDescription: 
        `Gentleman Haircuts $20 Kids cuts $15 Haircuts and Beard w/hot towel $35.00 Our work is Guaranteed to make you feel 10 yrs. Younger. Set up appointments.
        WE NOW HAVE 4 BARBERS AVAILABLE IN THE AFTERNOONS. OUR SERVICES INCLUDE RAZOR FADES, RAZOR SHAVES, HIGH FADES, MID FADES, LOW FADES, HIGH TAPERS, LOW TAPERS, WE HAVE HOT TOWELS ON REQUEST. WE ENJOY KIDS. GIRL HAIR CUTS.
        We're taking walkins from 1pm-6pm Tuesday-Saturday call and let us know if you getting out of work late to accommodate you. We do Razor Fades with hot towel. Gentleman haircuts.`,

        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F25_512x512.jpg?alt=media&token=e321fbea-ed1f-4bb9-a4a6-a25562388e00",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F3_512x512.jpg?alt=media&token=46073901-9da5-47a7-9c7c-cdb87a619bf1",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F7_512x512.jpg?alt=media&token=8c6662ac-74d7-4c15-8cdf-459edc5168ef",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F9_512x512.jpg?alt=media&token=c7f36d6c-5e1d-451f-8fc5-7d9ba34e94f6",
        ],


      homeServices: [
        {
          name:"Beard maintenance",
          time:"15 minutes",
          //description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
          price: "$25"
        },
        {
          name:"Regular Haircut",
          time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$20"
        },
        {
          name:"Kids cut under 10yrs old",
          time:"45 minutes",
          //description:"High fade low fade",
          price: "$35"
        },
        {
          name:"Shave Fade",
          time:"35 minutes",
         // description:"High fade low fade",
          price: "$20"
        },
        {
          name:"EdgeUp",
          time:"10 minutes",
          //description:"High fade low fade",
          price: "$10"
        },
        {
          name:"Double Shave hot towel",
          time:"35 minutes",
         // description:"High fade low fade",
          price: "$25"
        },
        {
          name:"Hair color",
          time:"30 minutes",
          //description:"High fade low fade",
          price: "$30"
        },
        {
          name:"Men Color & haircut",
          time:"60 minutes",
          //description:"High fade low fade",
          price: "$50"
        },
        {
          name:"Men Bleaching and haircut and color",
          time:"2 Hours 30 Minutes",
          //description:"High fade low fade",
          price: "$60"
        },
        {
          name:"Women haircuts",
          time:"40 Minutes",
          //description:"High fade low fade",
          price: "$25"
        },
        {
          name:"Women color short hair",
          time:"60 Minutes",
         // description:"High fade low fade",
          price: "$50"
        },
        {
          name:"Women color long length",
          time:"1 Hour 45 Minutes",
          //description:"High fade low fade",
          price: "$125"
        },
        {
          name:"Women bleach and color short hair",
          time:"2 Hours 30 Minutes",
          //description:"High fade low fade",
          price: "$125"
        },
        {
          name:"Women color and bleach highlights long hair",
          time:"4 Hours 30 Minutes",
          //description:"High fade low fade",
          price: "$125"
        },
      ],
      
      homeSchedules: [
        {
          day: "Monday",
          hour: "Closed",
        },
        {
          day: "Tuesday",
          hour: "09:30 am – 08:00 pm",
        },
        {
          day: "Wednesday",
          hour: "09:30 am – 08:00 pm",
        },
        {
          day: "Thursday",
          hour: "09:30 am – 08:00 pm",
        },
        {
          day: "Friday",
          hour: "09:30 am – 08:00 pm",
        },
        {
          day: "Saturday",
          hour: "08:00 am – 08:00 pm",
        },
        {
          day: "Sunday",
          hour: "09:00 am – 03:00 pm",
        },
      ],

      // homeBreaktime: [
      //   {
      //     day: "Monday",
      //     hour: "Close",
      //   },
      //   {
      //     day: "Tuesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Wednesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Thursday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Friday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Saturday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Sunday",
      //     hour: "Close",
      //   },
      // ],

      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fhome%2F1.mp4?alt=media&token=df4a2ee9-8c97-4634-bd5b-fdb0f5589ee1",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fhome%2F2.mp4?alt=media&token=9a293e78-8508-437d-ae2c-9f77a98318d8",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fhome%2F3.mp4?alt=media&token=487dc77d-99af-4161-8104-bda062a02b0c",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fhome%2F4.mp4?alt=media&token=230e0b88-6de1-4574-93a0-9338706d31e7",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fhome%2F5.mp4?alt=media&token=c96635fa-e9bb-4df6-a071-be3957c8b63b",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "310 999 9466",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F9_512x512.jpg?alt=media&token=c7f36d6c-5e1d-451f-8fc5-7d9ba34e94f6",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F10_512x512.jpg?alt=media&token=285c74de-6d46-4a9e-8c48-6c79a1d71ce7",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F11_512x512.jpg?alt=media&token=b1cf80ef-4fe9-405d-a33f-af0a1a042138",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F12_512x512.jpg?alt=media&token=41d31552-937a-42c3-826b-b5e6d6861028",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F13_512x512.jpg?alt=media&token=dd66b482-d1d6-4c16-bbd9-681b0679dd78",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F14_512x512.jpg?alt=media&token=f5b4524a-4aef-41c0-a5bf-c71844878963",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F15_512x512.jpg?alt=media&token=96bbad2c-f87a-4ef8-af93-ed3e7befbd5a",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F16_512x512.jpg?alt=media&token=e3bb1946-4c75-4d11-9ece-f43dfec31337",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F17_512x512.jpg?alt=media&token=03e9e0c3-5cf2-40a1-b5be-7c70da330caa",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F18_512x512.jpg?alt=media&token=46e4e42b-6c93-4e34-9ce5-a474c85d6b96",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F19_512x512.jpg?alt=media&token=66baae7d-8202-46b6-9759-0931004d09b1",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F1_512x512.jpg?alt=media&token=e9a8add2-2bca-49fc-bf0f-1f9cdc042cf1",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F20_512x512.jpg?alt=media&token=6fed3fe3-982c-46dd-9408-2a160e3c3910",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F21_512x512.jpg?alt=media&token=d198f169-854a-4b0c-92d4-314a1f3df39d",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F22_512x512.jpg?alt=media&token=55458caa-7e2d-493b-81d9-aed444089a09",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F23_512x512.jpg?alt=media&token=8f1f930f-2d07-486e-a436-1c80a2f62cf3",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F24_512x512.jpg?alt=media&token=69d13adb-85a5-45e8-a3e7-99714417e45c",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F25_512x512.jpg?alt=media&token=e321fbea-ed1f-4bb9-a4a6-a25562388e00",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F26_512x512.jpg?alt=media&token=1b470c47-859a-40dc-b263-7c6f90b37e8a",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F27_512x512.jpg?alt=media&token=76f0c10e-263b-453a-a364-a40daabd759c",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F28_512x512.jpg?alt=media&token=5969f7d1-cbb3-4ab1-9469-314e265fcc6b",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F29_512x512.jpg?alt=media&token=2d4daad8-e71b-4de3-8430-7a66b1a55d5e",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F2_512x512.jpg?alt=media&token=c1a83f33-a015-4322-ac79-1f8f5ed895a7",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F30_512x512.jpg?alt=media&token=2e43b78f-90d3-4819-bb2d-a21747840a3a",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F31_512x512.jpg?alt=media&token=d8cfb175-0e16-49d3-b6ad-790f276d8022",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F32_512x512.jpg?alt=media&token=5e6cf4dc-dbcc-484c-ad85-484733de15b3",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F34_512x512.jpg?alt=media&token=fa271c4b-eba8-4407-b9c9-ccc1526e88d6",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F35_512x512.jpg?alt=media&token=1a9cf8e9-30ee-40d5-9f13-8e19e458c544",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F36_512x512.jpg?alt=media&token=24806a3b-6423-4369-b4aa-6ea02187075b",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F37_512x512.jpg?alt=media&token=6086b886-bd4e-4fcd-bdf0-6a75aacd43b8",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F3_512x512.jpg?alt=media&token=46073901-9da5-47a7-9c7c-cdb87a619bf1",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F4_512x512.jpg?alt=media&token=71e254f5-06b7-43fd-8a0b-8c0943e86a0b",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F5_512x512.jpg?alt=media&token=77fea785-bd79-437c-a851-5d436c5071bd",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F6_512x512.jpg?alt=media&token=c81347d8-5e80-44eb-8e99-11a527ef1bc1",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F7_512x512.jpg?alt=media&token=8c6662ac-74d7-4c15-8cdf-459edc5168ef",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2F8_512x512.jpg?alt=media&token=7eafc458-9162-4a06-b01b-c49df4a675c9",

      ],

      socialNetworks: [
        {
          text: "straightupbarbers",
          link: "https://www.instagram.com/straightupbarbers/",
          icon: "logo-instagram",
        },
        {
          text: "Straightupbarbers",
          link: "https://booksy.com/en-us/659011_straightupbarbers_barber-shop_103721_long-beach?do=invite&_branch_match_id=1008476610453898129&utm_medium=merchant_customer_invite&_branch_referrer=H4sIAAAAAAAAA8soKSkottLXT07J0UvKz88urtRLzs%2FVz0tNDDBNrixzyk0CAOPXazEiAAAA",
          icon: "book",
        },
        {
          text: "straight up barber's",
          link: "https://www.yelp.com/biz/straight-up-barbers-long-beach",
          icon: "logo-yahoo",
        },
      ],

      address: "5307 Atlantic Ave Long Beach, CA 90805",
      addressRoute: "5307 Atlantic Ave Long Beach, CA 90805",
      mapSource: "https://maps.google.com/maps?q=Straight%20up%20barber's%205307&t=&z=15&ie=UTF8&iwloc=&output=embed", 
    
      barbers:[
        {
          name: "Cindy C",
          description: "Business Owner",
          socialNetworks:[
            {
              text: "straightupbarbers",
              link: "https://www.instagram.com/straightupbarbers/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FStraight%20up%20barber's%2Fappoiments%2Fb1_512x512.jpg?alt=media&token=770c3d44-4a57-462e-bcaf-24811f8b14a3",
        },
      ]
    },

    appointmentsDays:[
      {
        day: 1,
        hours: this.hours,
        closed: true
      },
      {
        day: 2,
        hours: this.hours,
        closed: false
      },
      {
        day: 3,
        hours: this.hours,
        closed: false
      },
      {
        day: 4,
        hours: this.hours,
        closed: false
      },
      {
        day: 5,
        hours: this.hours,
        closed: false
      },
      {
        day: 6,
        hours: this.hours,
        closed: false
      },
      {
        day: 0,
        hours: this.hours,
        closed: false
      },
    ],
    
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
    //this.firebaseService.updateAppData(this.appData).then(t => {console.log(this.appData)}); 
  }


  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
     return this.firebaseService.getAppData().then(appData => {
        if(appData) {
          this.appData = appData as any;
          console.log(appData);
        }
      });
  }
}
