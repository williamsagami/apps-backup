import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "JimmyCutz Barbershop",

      homeDescription: 
        `JimmyCutz Barbershop and JimmyCutz Hair Studio are one of the best Barbers in California. <br>
        We have high quality and specialty in any type cut, with many years of experience giving the best designs and haircuts, so that you sit down and relax. <br>
        Our professionals will do the best job and amazing customer service, thus having the best experiences you will have in your day. <br>
        With the services we provide today, customers will only sit down and you will see that our work is impeccable, because for us the most important thing for you is to have a good design or haircut <br>
        Do not hesitate to call us and make an appointment.`,

      homeServices: [

      ],

      phoneNumbers: [
        {
          text: "JimmyCutz Barbershop",
          number: "951 807 1411",
        }
      ],

      phoneNumbers1: [
        {
          text: "Jimmycutz Hair Studio",
          number: " 951 807 6133",
        }
      ],

      socialNetworks: [
        {
          text: "jimmycutzbarbershop",
          link: "https://www.instagram.com/jimmycutzbarbershop/",
          icon: "logo-instagram",
        },
        {
          text: "JimmyCutz Barbershop",
          link: "https://www.facebook.com/JimmycutzBarbershop",
          icon: "logo-facebook",
        },
        {
          text: "Jimmycutz Hair Studio",
          link: "https://www.facebook.com/Jimmycutz-Hair-Studio-110706423816797/?ref=page_internal",
          icon: "logo-facebook",
        },
      ],

      address: "24907 Sunnymead Boulevard a, Moreno Valley, CA 92553, Estados Unidos",
      address1: "3977 N Perris Blvd, Perris, CA 92571, Estados Unidos",

      mapSource: "https://maps.google.com/maps?q=JimmyCutz%20Barbershop,%20Sunnymead%20Boulevard&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

 this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
