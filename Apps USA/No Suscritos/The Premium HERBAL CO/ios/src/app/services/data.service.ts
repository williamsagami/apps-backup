import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "The Premium HERBAL CO",

      homeDescription: 
        `One of the best stores in Michigan with excellent service. <br>
        We give any information the customer needs about our products. <br>
        Do not hesitate to leave your request so we can get in contact with you.`,

        homeSlider:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fhome%2F1_512x512.png?alt=media&token=3c5c5ed1-b2c0-4692-9afd-3f7d6d0d1e67",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fhome%2F1s_512x512.jpg?alt=media&token=85e9a783-c58a-4361-9081-65baca315f27",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fhome%2F2_512x512.png?alt=media&token=2d5d3527-629b-41ec-967c-2f44d462be6b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fhome%2F3_512x512.png?alt=media&token=5d077e74-c9cd-44f9-b0e7-82fff6666479",

        ],


      homeServices: [

      ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "09:00 am – 09:00 pm",
        },
        {
          day: "Tuesday",
          hour: "09:00 am – 09:00 pm",
        },
        {
          day: "Wednesday",
          hour: "09:00 am – 09:00 pm",
        },
        {
          day: "Thursday",
          hour: "09:00 am – 09:00 pm",
        },
        {
          day: "Friday",
          hour: "09:00 am – 09:00 pm",
        },
        {
          day: "Saturday",
          hour: "09:00 am – 09:00 pm",
        },
        {
          day: "Sunday",
          hour: "09:00 am – 09:00 pm",
        },
      ],

      
      homeVideos: [
       
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "313 367 8349",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F6_512x512.png?alt=media&token=20817bb9-bf78-465b-b283-a5d67fe00909",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F5_512x512.png?alt=media&token=55b27def-4a64-4c68-b549-3d01b4af1e6d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F4_512x512.png?alt=media&token=fe4984b8-49d0-4489-aa99-89c0610869ff",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F8_512x512.png?alt=media&token=dcd647f1-8cfd-4827-b241-63744bafda3f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F12_512x512.jpg?alt=media&token=eb5b00f8-cf87-4fc9-b8e7-160e38ba9602",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F16_512x512.jpg?alt=media&token=dd54136f-20f6-4c8e-8ea1-8cb78471cf58",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F24_512x512.jpg?alt=media&token=5f1c7f09-1fa9-4e1f-89a0-261c201d5549",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F11_512x512.jpg?alt=media&token=e97e5920-ee98-4427-b801-737500e4f6a3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F26_512x512.jpg?alt=media&token=83cfa50e-481c-4cfc-ab49-91c87eb41238",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F13_512x512.jpg?alt=media&token=5087b8fd-9076-4c1d-bce4-322254f5fcdf",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F20_512x512.jpg?alt=media&token=2e6a2535-cb8b-4ccf-896b-d02a28c818fd",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F21_512x512.jpg?alt=media&token=6375b94f-ac88-4e91-b11f-bdd83205affc",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F25_512x512.jpg?alt=media&token=b9911caa-d558-47df-aa03-0c795727cb19",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F14_512x512.jpg?alt=media&token=4c61f0ca-8d97-4452-bada-649b4667a6ba",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F18_512x512.jpg?alt=media&token=b8a47fed-b821-4fac-a740-21517000fd07",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F30_512x512.jpg?alt=media&token=22722f9c-f360-4a79-91fb-1f8163c919ef",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F22_512x512.jpg?alt=media&token=ac9756cc-ac01-494c-bf24-f096eb13d535",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F17_512x512.jpg?alt=media&token=78a5ab4d-c89e-4888-a192-324fde3b2f4a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F15_512x512.jpg?alt=media&token=770f45eb-e570-4c7c-89c0-a7e2edbd4370",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F28_512x512.jpg?alt=media&token=37acae83-e4ae-40db-9350-bfdd33d4d08c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F29_512x512.jpg?alt=media&token=478df12e-2d7a-40cb-bbcf-037813df2d1a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F27_512x512.jpg?alt=media&token=6bfbd7b9-eeb4-4001-9bcc-129109682772",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F23_512x512.jpg?alt=media&token=4daca611-ffcb-4801-99aa-41c5ab7a4354",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F10_512x512.jpg?alt=media&token=0f3c7b39-2a4f-42b4-a9b4-3158605fea33",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FThe%20Premium%20HERBAL%20CO%2Fgallery%2F9_512x512.jpg?alt=media&token=b38096c2-28a1-4c4f-9984-1ab1b6103cb6",
      ],

      products:[
        
      ],

      productsVideos: [
             
      ],
      socialNetworks: [
        {
          text: "Stan Jewell",
          link: "https://www.facebook.com/stan.jewell.7",
          icon: "logo-facebook",
        },
        {
          text: "dcldsthairnnails",
          link: "https://www.instagram.com/dcldsthairnnails/",
          icon: "logo-instagram",
        },
        {
          text: "DETROIT COLDEST CUTS",
          link: "https://www.facebook.com/DETROIT-COLDEST-CUTS-261196701922/",
          icon: "logo-facebook",
        },
        {
          text: "dcoldest_jefe",
          link: "https://www.instagram.com/dcoldest_jefe/",
          icon: "logo-instagram",
        },
      ],

      address: "Detroit, Míchigan, USA",

      addressRoute:"Detroit, Míchigan, USA",

      mapSource: "https://maps.google.com/maps?q=Detroit,%20M%C3%ADchigan,&t=&z=13&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

  // this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
