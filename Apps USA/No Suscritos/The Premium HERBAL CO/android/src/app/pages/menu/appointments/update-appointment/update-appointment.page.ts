import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { FirebaseService, solicitud, usuario, state  } from 'src/app/services/firebase.service';
import * as moment from 'moment';

@Component({
  selector: 'app-update-appointment',
  templateUrl: './update-appointment.page.html',
  styleUrls: ['./update-appointment.page.scss'],
})
export class UpdateAppointmentPage implements OnInit {

  userData: any;
  appConfiguration: any;
  solicitud = {} as solicitud;

  fecha: Date = new Date();
  hora: Date = this.fecha;
  horarioFlexible = false;
  mensaje: string;
  rangeLower: number = -1;
  rangeUpper: number = 1;
  minDate = this.fecha;
  maxDate = moment( moment().add(30, 'days') ).toDate();

  private loading: any;

  pickerStyle = {
    //cssClass: "datetime-picker",
  }

  constructor(
    private modalController: ModalController, 
    private navParams: NavParams, 
    private firebaseService: FirebaseService, 
    private toastController: ToastController,
    private alertController: AlertController,
    private loadingController: LoadingController,) { 

    this.userData = navParams.get('userData');
    this.solicitud = navParams.get('solicitud');
    this.appConfiguration = navParams.get('appConfiguration');

    this.maxDate = moment( moment().add(this.appConfiguration.dayRangeLimit, 'days') ).toDate();
  }

  ngOnInit() {
    if(this.solicitud) {
      this.fecha = this.solicitud.date;
      this.hora = this.solicitud.date;
      this.horarioFlexible = this.solicitud.flextime;
      this.mensaje = this.solicitud.message;
      this.rangeLower = this.solicitud.ftBefore;
      this.rangeUpper = this.solicitud.ftAfter;
    }
  }

  closeView(){
	  this.modalController.dismiss();
  }

  setDay(event) {
    let date: Date = new Date(event.detail.value);
    this.fecha = date;
  }

  setHour(event) {
    let date: Date = new Date(event.detail.value);
    this.hora = date;
  }

  rangeChange(event) {
    this.rangeLower = event.detail.value.lower;
    this.rangeUpper = event.detail.value.upper;
  }

  async showInfo(info: string) {
    const toast = await this.toastController.create({
      message: info,
      duration: 6000,
      color: 'secondary',
    });
    toast.present();
  }

  isNewRequest() {
    if(this.solicitud) return false;
    else return true;
  }

  createRequest() {
    var date: Date = this.fecha;
    date.setHours(this.hora.getHours());
    date.setMinutes(this.hora.getMinutes());
    var validDate = moment(date).isSameOrAfter(this.minDate);

    if(true) {
      this.loadingController.create({
        message: 'Creating Request...',
        cssClass: 'cool-loading',
      }).then(overlay => {
        this.loading = overlay;
        this.loading.present();

        var solicitud = {} as any;
        solicitud.id = '_'+this.userData.requestCount;
        solicitud.date = date;
        solicitud.creationDate = moment().toDate();
        solicitud.flextime = this.horarioFlexible;
        solicitud.message = (this.mensaje != undefined ? this.mensaje : "Undefined");
        solicitud.ftBefore = this.rangeLower;
        solicitud.ftAfter =  this.rangeUpper;
        solicitud.state = state.pending;
        solicitud.user = this.userData as usuario;
        solicitud.lastChange = this.userData as usuario;
        solicitud.valid = true;

        this.firebaseService.createRequest(solicitud, this.userData).then(close => {
          this.loading.dismiss();
          this.closeView();
        });
      });
    } else {
      this.showInfo("You cannot assign a date before the current one.");
    }
  }

  updateRequest(state: number) {
    var date: Date = this.fecha;
    date.setHours(this.hora.getHours());
    date.setMinutes(this.hora.getMinutes());

    var validDate = moment(date).isSameOrAfter(this.minDate);

    if(true) {

      this.loadingController.create({
        message: 'Updating Request...',
        cssClass: 'cool-loading',
      }).then(overlay => {
        this.loading = overlay;
        this.loading.present();

        var solicitud = {} as any;
        solicitud.id = this.solicitud.id;
        solicitud.date = date;
        solicitud.creationDate = this.solicitud.creationDate;
        solicitud.flextime = this.horarioFlexible;
        solicitud.message = (this.mensaje != undefined ? this.mensaje : "Undefined");
        solicitud.ftBefore = this.rangeLower;
        solicitud.ftAfter =  this.rangeUpper;
        solicitud.state = state;
        solicitud.user = this.solicitud.user;
        solicitud.lastChange = this.userData as usuario;
        solicitud.valid = true;

        this.firebaseService.updateRequest(solicitud).then(close => {
          this.loading.dismiss();
          this.closeView();
        });
      });
    } else {
      this.showInfo("You cannot assign a date before the current one.");
    }
  }

  updateRequestLastChange() {
    var solicitud = {} as any;
    solicitud.id = this.solicitud.id;
    solicitud.date = this.solicitud.date;
    solicitud.creationDate = this.solicitud.creationDate;
    solicitud.flextime = this.solicitud.flextime;
    solicitud.message = this.solicitud.message;
    solicitud.ftBefore = this.solicitud.ftBefore;
    solicitud.ftAfter =  this.solicitud.ftAfter;
    solicitud.state = this.solicitud.state;
    solicitud.user = this.solicitud.user;
    solicitud.lastChange = this.userData as usuario;
    solicitud.valid = true;

    return this.firebaseService.updateRequest(solicitud);
  }

  deleteRequest() {
    this.loadingController.create({
      message: 'Deleting Request...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();
      
      this.updateRequestLastChange().then(lastChange => {
        this.firebaseService.deleteRequest(this.solicitud, true).then(close => {
          this.loading.dismiss();
          this.closeView();
        });
      });
    });
  }

  evaluateCreate() {
    if(this.userData.username === 'Guest' || this.userData.phone === 'Undefined') this.createConfirm();
    else this.createRequest();
  }

  async createConfirm() {
    var msg = this.userData.username === 'Guest' ? "username" : '' ;
    if(msg === '') msg = "phone number";
    else msg += this.userData.phone === 'Undefined' ? ' ' + "and phone number" : '';

    const alert = await this.alertController.create({
      cssClass: 'cool-alert',
      header: "User Data",
      message: "We recommend that you change your " + msg + " on the Account screen to schedule you more quickly.",
      buttons: [
        {
          text: "Cancel",
          role: 'cancel',
          cssClass: 'secondary',
        }, 
        {
          text: "Create",
          handler: () => {
            this.createRequest();
          }
        }
      ]
    });

    await alert.present();
  }

  async deleteConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'cool-alert',
      header: "Delete Request",
      message: "Are you sure you want to delete this request?",
      buttons: [
        {
          text: "Cancel",
          role: 'cancel',
          cssClass: 'secondary',
        }, 
        {
          text: "Delete",
          handler: () => {
            this.deleteRequest();
          }
        }
      ]
    });

    await alert.present();
  }

  async rejectConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'cool-alert',
      header: "Reject Request",
      message: "Are you sure you want to reject this request?",
      buttons: [
        {
          text: "Cancel",
          role: 'cancel',
          cssClass: 'secondary',
        }, 
        {
          text: "Reject",
          handler: () => {
            this.updateRequest(state.rejected);
          }
        }
      ]
    });

    await alert.present();
  }

}
