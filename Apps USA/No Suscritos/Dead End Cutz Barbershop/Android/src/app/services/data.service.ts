import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Dead End Cutz Barbershop",

      homeDescription: 
        `Dead End Cutz Barbershop, one of the best barbershop in Los Angeles. <br> 
        with many years of experience, providing an impeccable service for all the clients, where they can get to sit back, relax, and have an excellent hair cuts. <br> 
        For us, keeping the client pleased is the most important thing. <br> 
        Do not hesitate to schedule the appointment with us, our professionals will be waiting for you.`,


        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fhome%2F21_512x512.jpg?alt=media&token=5d6b3c2c-e20b-4930-9a9c-ecf20b0cf116",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fhome%2F23_512x512.jpg?alt=media&token=70bc34b1-31e4-4872-95e7-897f1d8b80f5",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fhome%2F24_512x512.jpg?alt=media&token=d15d8134-feff-4631-97f0-dcdb650074b2",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fhome%2F25_512x512.jpg?alt=media&token=0c061f95-bd60-46ba-ae88-6fcbcbb8d333",
        ],


      homeServices: [
      ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Tuesday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Wednesday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Thursday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Friday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Saturday",
          hour: "09:00 am – 06:30 pm",
        },
        {
          day: "Sunday",
          hour: "09:00 am - 03:30 pm",
        },
      ],
      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fhome%2F3.mp4?alt=media&token=41d7eb37-3b2c-4e37-abdf-1269e136472a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fhome%2F1.mp4?alt=media&token=84bb9096-9577-446e-bc57-1f4c9c66acf1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fhome%2F2.mp4?alt=media&token=de1873ec-9cb9-4094-b332-8b7560a96c5b",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "818 810 0083",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F25_512x512.jpg?alt=media&token=5273ec7b-18b1-4269-bd3b-21f7bcf9f91f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F1_512x512.jpg?alt=media&token=f58ff8ea-0160-4319-98ba-f97546bde8ed",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F14_512x512.jpg?alt=media&token=7b49a3e4-ae10-4db4-bf3e-1e7a6f344dfe",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F18_512x512.jpg?alt=media&token=0f792c8d-fd26-4cb8-8caf-9f560638e91a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F3_512x512.jpg?alt=media&token=ced6f4c0-e210-4754-afa5-37e4198dc228",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F9_512x512.jpg?alt=media&token=16033aa9-5021-427f-b938-0ecf4875d501",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F17_512x512.jpg?alt=media&token=4efcc2f8-6e2e-44d2-b6ae-c831d3aacc8e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F13_512x512.jpg?alt=media&token=b0461432-9dc1-48c8-b202-ebdb40cfaaa2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F11_512x512.jpg?alt=media&token=fe26eeb6-3e4b-447d-b578-7567625fdd42",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F16_512x512.jpg?alt=media&token=5d0ff3d0-46bf-4353-8062-2910688c79b0",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F2_512x512.jpg?alt=media&token=55d31f17-ab20-443c-a373-5dc70b6e29b0",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F24_512x512.jpg?alt=media&token=e24a59c3-5872-4954-bb7f-9379d25db460",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F8_512x512.jpg?alt=media&token=2ec45684-f893-4aa3-b28e-5b1c1f6f834b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F4_512x512.jpg?alt=media&token=d9232f19-c399-4972-886c-593e2ec33dd9",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F22_512x512.jpg?alt=media&token=61d263a5-ec68-415e-928e-05b5d82eaa1a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F12_512x512.jpg?alt=media&token=129e33b9-4677-4d44-a193-66e8cbdc71d8",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F21_512x512.jpg?alt=media&token=eb7b896c-c99a-48f4-b1c2-7755cf907b53",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F10_512x512.jpg?alt=media&token=4cd04cd7-36e8-467a-9cc3-53220212a9c6",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F6_512x512.jpg?alt=media&token=c49baa67-9a52-4487-b31f-af83fd4b5b54",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F23_512x512.jpg?alt=media&token=0937f8e3-bc7f-4bac-9ce6-071647b0315d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F5_512x512.jpg?alt=media&token=96c68084-b4bd-4542-a94d-e25a052b02c6",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F7_512x512.jpg?alt=media&token=c804253c-35f6-4856-be82-3301083c6aec",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F19_512x512.jpg?alt=media&token=c4f88b3c-4c04-4526-b2f3-3ad87afb50c9",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fgallery%2F20_512x512.jpg?alt=media&token=b5208938-1ddc-4e17-9d69-032521687132",

      ],

      socialNetworks: [
        {
          text: "Dead End Cutz BarberShop",
          link: "https://www.facebook.com/Dead-End-Cutz-BarberShop-375426116291066",
          icon: "logo-facebook",
        },
        {
          text: "deadendcutz_official",
          link: "https://www.instagram.com/deadendcutz_official/",
          icon: "logo-instagram",
        },
        {
          text: "Dead End Cutz Barbershop",
          link: "https://www.yelp.com/biz/dead-end-cutz-barbershop-arleta",
          icon: "logo-yahoo",
        },
        {
          text: "emilioalmanza",
          link: "https://www.instagram.com/emilioalmanza/",
          icon: "logo-instagram",
        },
      ],

      address: "8863 Woodman Ave, Arleta, CA 91331, USA",

      addressRoute: " 8863 Woodman Ave, Arleta, CA 91331, USA",

      mapSource: "https://maps.google.com/maps?q=Dead%20End%20Cutz%20Barbershop&t=&z=15&ie=UTF8&iwloc=&output=embed",
      barbers:[
        {
          name: "Emilio Almanza",
          description: "Boss Barber",
          socialNetworks:[
            {
              text: "emilioalmanza",
              link: "https://www.instagram.com/emilioalmanza/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fappointments%2Fbarbers%2Fb0_512x512.jpg?alt=media&token=81eb56ed-430b-4c95-b46f-fabfa907fc48",
        },
        {
          name: "Francisco Rameño",
          description: "Veteran barber",
          socialNetworks:[
  
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fappointments%2Fbarbers%2Fb1_512x512.jpeg?alt=media&token=81083936-8c7a-4d80-b7be-08484a476941",
  
        },
        {
          name: "Gio Díaz",
          description: "Expert barber",
          socialNetworks:[
  
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fappointments%2Fbarbers%2Fb2_512x512.jpeg?alt=media&token=22a41625-62f8-4e7d-81d7-72c5bf08aaf9",
  
        },
  
        {
          name: "Peter Garay ",
          description: "Master Barber",
          socialNetworks:[
  
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fappointments%2Fbarbers%2Fb3_512x512.jpeg?alt=media&token=c191ad6b-ed86-4dbf-8c13-d915e1bf78a2",
  
        },
  
        {
          name: "Leslie Gomez",
          description: "haircut specialist",
          socialNetworks:[
  
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDead%20End%20Cutz%20Barbershop%2Fappointments%2Fbarbers%2Fb4_512x512.jpeg?alt=media&token=69c8f59d-b1c5-4448-b63a-0a2a1165748f",
  
        },
      ],
    }

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

   // this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)}); 
  }



  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
