import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Majesty Barber Hair Studio",

      homeDescription: 
        `Looking for a detailed precision haircut, braid, or style? We got you covered! Majesty Barber Hair Shop is your one-stop-shop for beauty accessories and gifts and professional 
        haircuts that we will make you look and feel good. <br>
        From old school styles to modern ​makeup trends, we can do it all. <br>
        We also offer natural beauty products. <br>
        Visit us once or twice a week for a full cut or trim, a fade, and even a Mohawk! Our talented hairdressers offer a light shave service with no razor blades, 
        as well as hair braiding. <br>
        Come and enjoy the majestic experience here at Majesty Barber Hair Studio!`,

       
        homeSlider:[
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fhome%2F1s.jpg?alt=media&token=c298620c-f141-481c-a072-5738268ed9d3",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fhome%2F2s.jpg?alt=media&token=d22495c6-5218-4745-8105-e3c3dbe6637c",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fhome%2F3s.jpg?alt=media&token=de48f2a5-63e0-4a40-80cb-11b96fa3d2ec",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fhome%2F4s.jpg?alt=media&token=b5203a1a-1282-45c7-b722-d07d8e1e15ee",
        ],


      homeServices: [
        {
          name:"Men's Haircut with Mustache Trim",
          price: "$35"
        },
        {
          name:"Specialty Haircut (Mohawk, Parts/Lines, etc)",
          price: "$35"
        },
        {
          name:"Men's Haircut",
          price: "$25"
        },
        {
          name:"Men's Haircut with Beard",
          price: "$35"
        },
        {
          name:"Haircut Corrections",
          price: "$35"
        },
        {
          name:"Women's Haircut",
          price: "$25"
        },
        {
          name:"Children's Haircut (12 and under)",
          price: "$20"
        },
        {
          name:"Children's Haircut (13 and over)",
          price: "$25"
        },
        {
          name:"Eye Arch",
          price: "$10"
        }
      ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "09:00 am – 09:00 pm",
        },
        {
          day: "Tuesday",
          hour: "09:00 am – 09:00 pm",
        },
        {
          day: "Wednesday",
          hour: "09:00 am – 09:00 pm",
        },
        {
          day: "Thursday",
          hour: "09:00 am – 09:00 pm",
        },
        {
          day: "Friday",
          hour: "09:00 am – 09:00 pm",
        },
        {
          day: "Saturday",
          hour: "09:00 am – 08:00 pm",
        },
        {
          day: "Sunday",
          hour: "09:00 am – 08:00 pm",
        },
      ],

      homeVideos: [
        
      ],
      f: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fhome%2Ff.png?alt=media&token=1a994b0a-2863-4ce7-8dda-13e2e80f44b1",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "240 678 1208",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F1.jpg?alt=media&token=d0f2b053-e4ff-4f6b-9e79-c5b95073f3b7",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F10.jpg?alt=media&token=3e060000-f429-42e5-8699-c1b72c5357d2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F11.jpg?alt=media&token=b8169428-d3aa-41b5-9a1d-0af27f7980ec",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F12.jpg?alt=media&token=20297c53-3fe9-4022-ab44-8a92f9b76780",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F13.jpg?alt=media&token=e5a3c7dd-5f43-4792-ac2d-f09ab1c4fb3a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F14.jpg?alt=media&token=43d903d5-5024-4c1e-9b04-a4c66fe39cc9",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F15.jpg?alt=media&token=2010a397-3983-49a3-9a88-63c9654904bc",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F16.jpg?alt=media&token=ed42ec3b-a98c-475a-81bd-342798473521",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F17.jpg?alt=media&token=740df494-f782-4d64-95bf-3771592aa6c3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F18.jpg?alt=media&token=e5b6636d-7915-4a2b-b053-a0e2bd2ec60b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F19.jpg?alt=media&token=e6304257-8229-4150-850c-e1b81dc36612",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F1s.jpg?alt=media&token=ad59663a-0634-45b1-833c-e0bf77a75052",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F2.jpg?alt=media&token=7c1a8e2f-7eff-4ef3-988b-c4245653b872",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F20.jpg?alt=media&token=16d08708-e682-4dba-850d-9a07f3b3047d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F21.jpg?alt=media&token=bef2d38e-eaea-463d-b582-12afe663573d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F22.jpg?alt=media&token=c8be5534-06ed-4841-bcdc-f2a880ed0fbd",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F23.jpg?alt=media&token=7ef218d4-a433-46f8-8146-751a68a935ab",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F24.jpg?alt=media&token=c233874f-8ab8-46bf-8ff7-72ed52f4b713",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F25.jpg?alt=media&token=974ad315-e7e7-4a13-9623-b17cffcc19d9",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F26.jpg?alt=media&token=1dcda194-0ae2-4be5-9766-838105394cd0",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F27.jpg?alt=media&token=d49344e3-799a-49c6-bcef-f70b841481b8",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F28.jpg?alt=media&token=dfd9d0f4-c82b-431f-a55d-ddba4c55ddd5",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F29.jpg?alt=media&token=4d22d50a-efb5-4a12-a2c3-4c25aece62dd",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F2s.jpg?alt=media&token=1063a366-ae9c-4171-968c-c69e24b26013",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F3.jpg?alt=media&token=253a3c3f-d4aa-458b-8957-d3570b53f0e0",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F30.jpg?alt=media&token=362988cb-98e6-427f-85f4-ecac11dbb902",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F31.jpg?alt=media&token=6be0f848-8873-4c9e-af24-0ad8905c52ef",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F3s.jpg?alt=media&token=bbf8736e-eac6-4af0-9dda-ff15964dc951",
        //"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F4.jpg?alt=media&token=12ebcd2f-b8cb-4fe1-ae9d-1f399dbe19c3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F4s.jpg?alt=media&token=31ff5181-e84b-411a-b435-2610ef6b7839",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F5.jpg?alt=media&token=1b47da8d-9ecb-45ea-a787-a4971dc50024",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F6.jpg?alt=media&token=3602d0aa-1ba6-4563-9512-3a5a5e1940c4",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F7.jpg?alt=media&token=a10e7720-e30d-4d19-9790-552489dacfd8",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F8.jpg?alt=media&token=c5c38078-db6d-4f15-8e66-76c589035d37",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMajesty%20Barber%20Hair%20Studio%2Fgallery%2F9.jpg?alt=media&token=8662f5cd-efb4-4d5a-b34e-a1acd2720913",
       ],
 

      socialNetworks: [
        {
          text: "majestybarbershop",
          link: "https://www.majestybarbershop.com/services",
          icon: "globe",
        },
        {
          text: "Appointments",
          link: "https://www.mytime.com/express_checkout/54850/54352",
          icon: "calendar",
        },
        {
          text: "majestybarbershop",
          link: "https://www.instagram.com/majestybarbershop/",
          icon: "logo-instagram",
        },
        {
          text: "Majesty VIP Barber",
          link: "https://www.facebook.com/majestyviphairstudio1/?ref=page_internal",
          icon: "logo-facebook",
        },
        {
          text: "@majestyviphair",
          link: "https://twitter.com/majestyviphair",
          icon: "logo-twitter",
        },
        {
          text: "Majesty Barber Hair Studio",
          link: "https://www.yelp.com/biz/majesty-barber-hair-studio-hyattsville",
          icon: "logo-facebook",
        },
        {
          text: "Howard Brown",
          link: "https://www.linkedin.com/public-profile/in/howard-brown-699490105?challengeId=AQHKNIeGn1wbmAAAAXiLa8i29NCQrZeEidV-HuJWmMSPpUvu2gnHYcb3BmrsMS7ToNG0dLIONVnp5FKWqchK9Uu_oKf4An2waQ&submissionId=4f2e6c59-659d-7116-ce2b-5998ec0d6678",
          icon: "logo-linkedin",
        }
      ],

      address: "3321 Toledo Terrace Suite 102-103, Hyattsville, MD 20782, USA",

      addressRoute:"3321 Toledo Terrace Suite 102-103, Hyattsville, MD 20782, USA",

      mapSource: "https://maps.google.com/maps?q=Majesty%20Barber%20Hair%20Studio&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

 //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
