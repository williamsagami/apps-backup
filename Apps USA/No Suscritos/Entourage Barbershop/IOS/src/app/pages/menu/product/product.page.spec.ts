import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { productPage } from './product.page';

describe('productPage', () => {
  let component: productPage;
  let fixture: ComponentFixture<productPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ productPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(productPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
