import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {

    //Datos de Contacto
    data: {
      appTitle: "Entourage Barbershop",

      appointments: true,

      homeDescription: 
        `Entourage Barbershop is a Barber Shop located in West Hollywood, CA. <br>
        We specialize in Hair Cuts, Men's Hair Cuts, Bread Trimming, Fades, lineups, Hot Towel Shave, Nose Wax, Straight Razor Shave & more. <br>
        Here at Entourage Barbershop, our success is due to our dedication to delivering great barber service & exceptional customer service to all of our customers. <br>
        We have only the best Barbers & Hair Stylists in the business. <br>
        Call to schedule an appointment today!`,

      homeSlider:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F1s_512x512.jpg?alt=media&token=5fcd4876-3b7e-4dc0-a6e8-11a68578338b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F2s_512x512.jpg?alt=media&token=cdfce1f5-d3dd-4609-a177-66dcb570fc6b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F3s_512x512.jpg?alt=media&token=8a1b7aec-deaa-4b00-b6b4-1bee20e45bcb",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F4s_512x512.jpg?alt=media&token=71600206-0846-426f-8478-8fbe4309039c",
      ],
      barbersDescription:
      `We have the best barbers in Hollywood, with excellence in their work area, their professionalism seems like magic. <br>
       Meet them now!!`,

      homeSlider2:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F26_512x512.png?alt=media&token=f95ba26e-b1e4-4e70-a4a4-bf5ff7f9d984",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F27_512x512.jpeg?alt=media&token=151ba6fa-b786-4977-aad6-c9af248c25fc",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F28_512x512.jpeg?alt=media&token=bf607257-2276-4268-810c-dd94c509d79c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F29_512x512.jpeg?alt=media&token=920b1407-86b5-4721-91a4-be683c753f07",
      ],

      homeServices: [
        {
          name:"Men's Haircut",
          price: "$45.00"
        },
        {
          name:"Haircut + Shave",
          price: "$60"
        },
        {
          name:"Kids haircut",
          price: "$35"
        },
        {
          name:"Long haircut",
          price: "$50"
        },
        {
          name:"Hot towel shave",
          price: "$30"
        },
        {
          name:"Design",
          price: "$50"
        },
        {
          name:"Line-up",
          price: "$25"
        },
        {
          name:"Wash",
          price: "$15"
        },
        {
          name:"Wash + Style",
          price: "$25"
        },
        {
          name:"Hair color",
          price: "$100"
        },
        {
          name:"Beard color",
          price: "$40"
        },
        {
          name:"Relaxer",
          price: "$40"
        },
        {
          name:"Eyebrow (Straight razor)",
          price: "$15"
        },
        {
          name:"Eyebrow Wax",
          price: "$25"
        },
        {
          name:"Nose wax",
          price: "$20"
        },
        {
          name:"Black Charcoal Mask",
          price: "$25"
        },
        {
          name:"Recharge Facial (60 min)",
          price: "$110"
        },
        {
          name:"Detox Facial (60 min)",
          price: "$120"
        },
        {
          name:"Age Defying Facial (60 min)",
          price: "$120"
        },
        {
          name:"CBD Massage (90 min)",
          price: "$50"
        },
        {
          name:"CBD N Chill Treatment (80 min)",
          price: "$130"
        },
        {
          name:"Back Facial (60 min)",
          price: "$120"
        },
        {
          name:"LED (30 min ADD ON)",
          price: "$70"
        },
        {
          name:"Swedish Massage (60 min)",
          price: "$110"
        },
        {
          name:"Swedish Massage (90 min)",
          price: "$130"
        },
        {
          name:"Deep Tissue Massage (60 min)",
          price: "$130"
        },
        {
          name:"Deep Tissue Massage (90 min)",
          price: "$150"
        },
        {
          name:"Sports Massage (60 min)",
          price: "120"
        },
        {
          name:"Sports Massage (90 min)",
          price: "$140"
        },
        {
          name:"CBD Massage (60 min)",
          price: "$130"
        },
      ],
      homeSchedules: [
        {
          day: "Monday",
          hour: "09:00 AM - 7:00 PM",
        },
        {
          day: "Tuesday",
          hour: "09:00 AM - 7:00 PM",
        },
        {
          day: "Wednesday",
          hour: "09:00 AM - 7:00 PM",
        },
        {
          day: "Thursday",
          hour: "09:00 AM - 7:00 PM",
        },
        {
          day: "Friday",
          hour: "09:00 AM - 7:00 PM",
        },
        {
          day: "Saturday",
          hour: "09:00 AM - 7:00 PM",
        },
        {
          day: "Sunday",
          hour: "10:00 AM - 4:00 PM",
        },
      ],
      homeVideos: [
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F1.mov?alt=media&token=fd8f8eb9-f4d4-4165-a25f-0da1d211e21d",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F2.mov?alt=media&token=8431bcd5-69e0-467f-a171-18dc0db66ff7",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F3.mp4?alt=media&token=201860b0-3f7f-4d77-bb83-3d27450a498c",
        ],

      f: [
        
      ],
      phoneNumbers: [
        {
          text: "Entourage Barbershop",
          number: "310 855 8044",
        }       
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F22_512x512.png?alt=media&token=2894dafb-8bd0-4dd9-af27-fdd4d0ec8a59",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F23_512x512.jpeg?alt=media&token=9fe82f30-ecfc-4946-a951-3acd48f475eb",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F24_512x512.jpeg?alt=media&token=663729ff-6394-4ead-837c-20b89629e1b0",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F25_512x512.jpeg?alt=media&token=17a95c4b-8b6f-489a-b2cb-a3e2f8307bd4",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F4s_512x512.jpg?alt=media&token=71600206-0846-426f-8478-8fbe4309039c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F9_512x512.jpg?alt=media&token=8d51b5f2-6d76-4a65-b6b2-7951d0a61d99",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F10_512x512.jpg?alt=media&token=1529b650-34cc-4b4e-a17c-11d4009c6768",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F11_512x512.jpg?alt=media&token=cf0da7c5-e251-4a85-ba92-16adaee0c711",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F12_512x512.jpg?alt=media&token=c91d7da4-a9c1-4f0c-90a2-b52f3c1bb0c5",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F13_512x512.jpg?alt=media&token=cc424f6c-8bf6-4498-a8ea-4212a3e2ec65",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F14_512x512.jpg?alt=media&token=2af654c6-e250-4209-82e6-5bdbe98c75b8",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F15_512x512.jpg?alt=media&token=0670307b-7441-41df-8673-e6c5fdb4cccc",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F16_512x512.jpg?alt=media&token=523ffb27-7ed6-4d2d-ae9d-ccb2003f7633",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F17_512x512.jpg?alt=media&token=b1219124-11b4-4bbe-bf44-dd0989566e04",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F18_512x512.jpg?alt=media&token=585878fa-f2cb-4bb8-9184-89ccaa35cb3a",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F19_512x512.jpg?alt=media&token=28be3662-150c-4d15-8443-16921d9f3170",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F1_512x512.jpg?alt=media&token=b54844d2-4dad-4d59-aac3-cb65d25b4300",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F20_512x512.jpg?alt=media&token=086f89b3-2632-4cac-99a5-fdeddea438bc",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F21_512x512.jpg?alt=media&token=776fa539-eee8-460b-9969-4a48303266e4",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F2_512x512.jpg?alt=media&token=dd22a4ca-d968-48d1-b8c7-16089406347c",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F3_512x512.jpg?alt=media&token=bf03c893-f583-4b5d-94fa-543e403141eb",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F4_512x512.jpg?alt=media&token=e18870e4-8e14-4305-894d-3c69291871e5",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F5_512x512.jpg?alt=media&token=1ef24d0a-3144-435f-8dd9-e37f4a50dd04",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F6_512x512.jpg?alt=media&token=6eb7c997-793e-4719-84a3-a20ee6330ad1",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F7_512x512.jpg?alt=media&token=ea538502-5aeb-4fd8-9cb9-ad8837f77748",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F8_512x512.jpg?alt=media&token=aa1d01db-4720-4a3b-9b07-1f8eb62dc8c3",
      
      ],

      socialNetworks: [
        {
          text: "entouragebarbershop",
          link: "https://www.instagram.com/entouragebarbershop/",
          icon: "logo-instagram",
        },
        {
          text: "Entourage Barbershop",
          link: "https://www.facebook.com/Entourage-Barbershop-116870816818062/",
          icon: "logo-Facebook",
        },
        {
          text: "Entourage Barbershop",
          link: "https://www.yelp.com/biz/entourage-barbershop-west-hollywood",
          icon: "logo-Yahoo",
        },
      ],

      address: "8601 Sunset Blvd, West Hollywood, CA 90069, United States",

      addressRoute: "8601 Sunset Blvd, West Hollywood, CA 90069, United States",

      mapSource: "https://maps.google.com/maps?q=Entourage%20Barbershop&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

   //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return new Promise<any>((resolve, reject) => {
      
      this.firebaseService.getAppData().then(appData => {
        if(appData) {
          this.appData = appData as any;
          //console.log(appData);
          resolve(appData);
        }
        else resolve(null);
      });
    })
  }
}
