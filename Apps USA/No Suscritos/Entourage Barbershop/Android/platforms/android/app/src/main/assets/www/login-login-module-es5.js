(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/login/login.page.html":
    /*!****************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/login/login.page.html ***!
      \****************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesMenuLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- HEADER -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: #fff;\"></ion-menu-button>\n    </ion-buttons>\n    <!-- <ion-title>{{data.t_Inicio[data.currentTranslation]}}</ion-title> -->\n    <ion-title>LOGIN</ion-title>\n  </ion-toolbar>\n  <div class=\"cool-line\"></div>\n</ion-header>\n\n<ion-content class=\"cool-content\">\n  <!-- PORTADA -->\n  <ion-card class=\"animate__animated animate__fadeInLeft cool-card\" style=\"border-style: none; height: 25%;\">\n    <img src=\"../assets/images/Logo.png\" class=\"logo\">\n  </ion-card>\n  <div class=\"cool-line\"></div>\n\n  <ion-segment color=\"secondary\" [(ngModel)]=\"selectedTab\">\n    <ion-segment-button class=\"login-off\" value=\"login\">\n      <ion-label>Login</ion-label>\n    </ion-segment-button>\n    <ion-segment-button class=\"login-off\" value=\"register\">\n      <ion-label>Register</ion-label>\n    </ion-segment-button>\n  </ion-segment>\n\n  <ion-grid class=\"grid\">\n    <form>\n      <ion-row class=\"ion-align-items-center\" *ngIf=\"selectedTab == 'login'\">\n        <ion-col size=\"12\">\n          <ion-item lines=\"none\" color=\"primary\" class=\"ion-text-center input\">\n            <ion-input type=\"email\" name=\"email\" placeholder=\"Email\" [(ngModel)]=\"email\"></ion-input>\n            <ion-icon slot=\"end\" name=\"mail\" color=\"secondary\"></ion-icon>\n          </ion-item>\n        </ion-col>\n        <ion-col size=\"12\">\n          <ion-item lines=\"none\" color=\"primary\" class=\"ion-text-center input\">\n            <ion-input [type]=\"showPassword ? 'text' : 'password'\" name=\"password\" placeholder=\"Password\" [(ngModel)]=\"password\"></ion-input>\n            <ion-icon slot=\"end\" name=\"eye\" color=\"secondary\" (click)=\"togglePassword()\"></ion-icon>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row class=\"ion-align-items-center\" *ngIf=\"selectedTab == 'register'\">\n        <ion-col size=\"12\">\n          <ion-item lines=\"none\" color=\"primary\" class=\"ion-text-center input\">\n            <ion-input type=\"email\" name=\"email\" placeholder=\"Email\" [(ngModel)]=\"email\"></ion-input>\n            <ion-icon slot=\"end\" name=\"mail\" color=\"secondary\"></ion-icon>\n          </ion-item>\n        </ion-col>\n        <ion-col size=\"12\">\n          <ion-item lines=\"none\" color=\"primary\" class=\"ion-text-center input\">\n            <ion-input [type]=\"showPassword ? 'text' : 'password'\" name=\"password\" placeholder=\"Password\" [(ngModel)]=\"password\"></ion-input>\n            <ion-icon slot=\"end\" name=\"eye\" color=\"secondary\" (click)=\"togglePassword()\"></ion-icon>\n          </ion-item>\n        </ion-col>\n        <ion-col size=\"12\">\n          <ion-item lines=\"none\" color=\"primary\" class=\"ion-text-center input\">\n            <ion-input [type]=\"showPassword ? 'text' : 'password'\" name=\"password\" placeholder=\"Repeat Password\" [(ngModel)]=\"rpassword\"></ion-input>\n            <ion-icon slot=\"end\" name=\"key\" color=\"secondary\"></ion-icon>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row class=\"ion-align-items-center\" *ngIf=\"selectedTab == 'login'\">\n        <ion-col size=\"12\" class=\"ion-text-center\" style=\"padding: 0 25% 0 25%;\">\n          <ion-button class=\"cool-button\" shape=\"round\" color=\"secondary\" (click)=\"login()\">Login</ion-button>\n        </ion-col>\n      </ion-row>\n\n      <ion-row class=\"ion-align-items-center\" *ngIf=\"selectedTab == 'register'\">\n        <ion-col size=\"12\" class=\"ion-text-center\" style=\"padding: 0 25% 0 25%;\">\n          <ion-button class=\"cool-button\" shape=\"round\" color=\"secondary\" (click)=\"register()\">Register</ion-button>\n        </ion-col>\n      </ion-row>\n    </form>\n  </ion-grid>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/menu/login/login-routing.module.ts":
    /*!**********************************************************!*\
      !*** ./src/app/pages/menu/login/login-routing.module.ts ***!
      \**********************************************************/

    /*! exports provided: LoginPageRoutingModule */

    /***/
    function srcAppPagesMenuLoginLoginRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function () {
        return LoginPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/pages/menu/login/login.page.ts");

      var routes = [{
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
      }];

      var LoginPageRoutingModule = function LoginPageRoutingModule() {
        _classCallCheck(this, LoginPageRoutingModule);
      };

      LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/menu/login/login.module.ts":
    /*!**************************************************!*\
      !*** ./src/app/pages/menu/login/login.module.ts ***!
      \**************************************************/

    /*! exports provided: LoginPageModule */

    /***/
    function srcAppPagesMenuLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
        return LoginPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-routing.module */
      "./src/app/pages/menu/login/login-routing.module.ts");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/pages/menu/login/login.page.ts");

      var LoginPageModule = function LoginPageModule() {
        _classCallCheck(this, LoginPageModule);
      };

      LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
      })], LoginPageModule);
      /***/
    },

    /***/
    "./src/app/pages/menu/login/login.page.scss":
    /*!**************************************************!*\
      !*** ./src/app/pages/menu/login/login.page.scss ***!
      \**************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesMenuLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".logo {\n  position: absolute;\n  left: 20%;\n  width: 60%;\n  top: -25%;\n}\n\n.login-off {\n  color: #a1a1a1;\n}\n\n.input {\n  color: #fff;\n  border-color: var(--ion-color-secondary);\n  border-style: solid;\n  border-radius: 32px;\n}\n\n.grid {\n  padding: 10% 10% 0 10%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWVudS9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtBQUNKOztBQUVBO0VBQ0ksY0FBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLHdDQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksc0JBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21lbnUvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvZ28ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiAyMCU7XG4gICAgd2lkdGg6IDYwJTtcbiAgICB0b3A6IC0yNSU7XG59XG5cbi5sb2dpbi1vZmYge1xuICAgIGNvbG9yOiAjYTFhMWExO1xufVxuXG4uaW5wdXQge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgICBib3JkZXItcmFkaXVzOiAzMnB4O1xufVxuXG4uZ3JpZCB7XG4gICAgcGFkZGluZzogMTAlIDEwJSAwIDEwJTtcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/menu/login/login.page.ts":
    /*!************************************************!*\
      !*** ./src/app/pages/menu/login/login.page.ts ***!
      \************************************************/

    /*! exports provided: LoginPage */

    /***/
    function srcAppPagesMenuLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
        return LoginPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/firebase.service */
      "./src/app/services/firebase.service.ts");

      var LoginPage = /*#__PURE__*/function () {
        function LoginPage(firebaseService, router, toastController, loadingController) {
          _classCallCheck(this, LoginPage);

          this.firebaseService = firebaseService;
          this.router = router;
          this.toastController = toastController;
          this.loadingController = loadingController;
          this.selectedTab = "login";
          this.showPassword = false;
          this.showInfo("To schedule appointments or view your account you need to login or register.");
        }

        _createClass(LoginPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "togglePassword",
          value: function togglePassword() {
            this.showPassword = !this.showPassword;
          }
        }, {
          key: "login",
          value: function login() {
            var _this = this;

            this.loadingController.create({
              message: 'Loading...',
              cssClass: 'cool-loading'
            }).then(function (overlay) {
              _this.loading = overlay;

              _this.loading.present();

              _this.firebaseService.login(_this.email, _this.password).then(function (res) {
                _this.loading.dismiss();

                _this.router.navigate(['/menu/home']);
              })["catch"](function (err) {
                switch (err.code) {
                  case 'auth/argument-error':
                    {
                      _this.showInfo("You must fill all the fields.");
                    }
                    break;

                  case 'auth/invalid-email':
                    {
                      _this.showInfo("Invalid email.");
                    }
                    break;

                  case 'auth/user-not-found':
                    {
                      _this.showInfo("This email is not registered.");
                    }
                    break;

                  case 'auth/wrong-password':
                    {
                      _this.showInfo("Incorrect password.");
                    }
                    break;
                }

                _this.password = null;

                _this.loading.dismiss();
              });
            });
          }
        }, {
          key: "register",
          value: function register() {
            var _this2 = this;

            this.loadingController.create({
              message: 'Loading...',
              cssClass: 'cool-loading'
            }).then(function (overlay) {
              _this2.loading = overlay;

              _this2.loading.present();

              if (_this2.password === _this2.rpassword) {
                _this2.firebaseService.register(_this2.email, _this2.password).then(function (res) {
                  _this2.loading.dismiss();

                  _this2.router.navigate(['/menu/home']);
                })["catch"](function (err) {
                  console.log(err.code);

                  switch (err.code) {
                    case 'auth/argument-error':
                      {
                        _this2.showInfo("You must fill all the fields.");
                      }
                      break;

                    case 'auth/invalid-email':
                      {
                        _this2.showInfo("Invalid email.");
                      }
                      break;

                    case 'auth/weak-password':
                      {
                        _this2.showInfo("The password must be at least 6 characters.");
                      }
                      break;

                    case 'auth/email-already-in-use':
                      {
                        _this2.showInfo("This email is already in use.");
                      }
                      break;
                  }

                  _this2.password = null;
                  _this2.rpassword = null;

                  _this2.loading.dismiss();
                });
              } else {
                _this2.showInfo("Passwords are not the same.");

                _this2.password = null;
                _this2.rpassword = null;

                _this2.loading.dismiss();
              }
            });
          }
        }, {
          key: "showInfo",
          value: function showInfo(info) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastController.create({
                        message: info,
                        duration: 6000,
                        color: 'secondary'
                      });

                    case 2:
                      toast = _context.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }]);

        return LoginPage;
      }();

      LoginPage.ctorParameters = function () {
        return [{
          type: src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_4__["FirebaseService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
        }];
      };

      LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/login/login.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login.page.scss */
        "./src/app/pages/menu/login/login.page.scss"))["default"]]
      })], LoginPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=login-login-module-es5.js.map