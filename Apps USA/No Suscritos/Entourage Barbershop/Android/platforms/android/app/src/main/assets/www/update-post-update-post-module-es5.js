(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["update-post-update-post-module"], {
    /***/
    "./src/app/pages/menu/home/update-post/update-post-routing.module.ts":
    /*!***************************************************************************!*\
      !*** ./src/app/pages/menu/home/update-post/update-post-routing.module.ts ***!
      \***************************************************************************/

    /*! exports provided: UpdatePostPageRoutingModule */

    /***/
    function srcAppPagesMenuHomeUpdatePostUpdatePostRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UpdatePostPageRoutingModule", function () {
        return UpdatePostPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _update_post_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./update-post.page */
      "./src/app/pages/menu/home/update-post/update-post.page.ts");

      var routes = [{
        path: '',
        component: _update_post_page__WEBPACK_IMPORTED_MODULE_3__["UpdatePostPage"]
      }];

      var UpdatePostPageRoutingModule = function UpdatePostPageRoutingModule() {
        _classCallCheck(this, UpdatePostPageRoutingModule);
      };

      UpdatePostPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], UpdatePostPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/menu/home/update-post/update-post.module.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/menu/home/update-post/update-post.module.ts ***!
      \*******************************************************************/

    /*! exports provided: UpdatePostPageModule */

    /***/
    function srcAppPagesMenuHomeUpdatePostUpdatePostModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UpdatePostPageModule", function () {
        return UpdatePostPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _update_post_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./update-post-routing.module */
      "./src/app/pages/menu/home/update-post/update-post-routing.module.ts");
      /* harmony import */


      var _update_post_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./update-post.page */
      "./src/app/pages/menu/home/update-post/update-post.page.ts");

      var UpdatePostPageModule = function UpdatePostPageModule() {
        _classCallCheck(this, UpdatePostPageModule);
      };

      UpdatePostPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _update_post_routing_module__WEBPACK_IMPORTED_MODULE_5__["UpdatePostPageRoutingModule"]],
        declarations: [_update_post_page__WEBPACK_IMPORTED_MODULE_6__["UpdatePostPage"]]
      })], UpdatePostPageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=update-post-update-post-module-es5.js.map