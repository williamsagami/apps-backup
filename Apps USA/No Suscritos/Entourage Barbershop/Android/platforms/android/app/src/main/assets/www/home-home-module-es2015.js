(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/home/home.page.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/home/home.page.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- HEADER -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: white;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title style=\"color: #fff;\">HOME</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"cool-content\" [scrollEvents]=\"true\" (ionScroll)=\"didScroll($event)\">\n\n  <!-- BOTON CONTACTO -->\n  <ion-fab vertical=\"top\" horizontal=\"end\" edge slot=\"fixed\" class=\"fab-button\">\n    <ion-fab-button>\n      <ion-icon name=\"person\"></ion-icon>\n    </ion-fab-button>\n    <ion-fab-list side=\"bottom\">\n      <ion-fab-button *ngFor=\"let numero of d.phoneNumbers\" (click)=\"data.phoneCall(numero.number)\" color=\"secondary\">\n        <ion-icon name=\"call\"></ion-icon>\n      </ion-fab-button>\n    </ion-fab-list>\n    <ion-fab-list side=\"start\">\n      <ion-fab-button *ngFor=\"let red of d.socialNetworks\" (click)=\"data.openLink(red.link)\" color=\"secondary\">\n        <ion-icon [name]=\"red.icon\"></ion-icon>\n      </ion-fab-button>\n    </ion-fab-list>\n  </ion-fab>\n\n  <!-- PORTADA -->\n  <ion-card class=\"animate__animated animate__fadeInLeft cool-card\" style=\"border-style: none;\">\n      <img src=\"../assets/images/Logo.png\" class=\"full-image\" style=\"object-fit: cover; height: 240px;\">\n  </ion-card>  \n  <div class=\"cool-line\"></div> \n\n  <!-- BLOG -->\n  <ion-card class=\"cool-card sub-card\" *ngIf=\"!postsReady\">\n    <div class=\"pin\" style=\"top: -5px;\"></div>\n    <ion-item>\n      <ion-progress-bar type=\"indeterminate\" color=\"secondary\"></ion-progress-bar>\n    </ion-item>\n    <div class=\"pin\" style=\"bottom: -5px;\"></div>\n  </ion-card>\n\n  <div *ngIf=\"postsReady\">\n    <ion-card class=\"animate__animated animate__fadeInUp cool-secondary-card\">\n      <ion-slides pager=\"true\" [options]=\"slideSales\" autoplay=\"true\">\n        <ion-slide *ngFor=\"let post of posts\" style=\"height: 100%;\">\n          <div class=\"post-button\" *ngIf=\"showPostEdit()\" (click)=\"viewPost(post)\">\n            <ion-icon color=\"primary\" name=\"ellipsis-horizontal\" style=\"font-size: 24px;\"></ion-icon>\n          </div>\n          <div style=\"margin-bottom: 48px;\">\n            <ion-item>\n              <ion-text class=\"cool-secondary-title\">{{post.title}}</ion-text>\n              <ion-badge *ngIf=\"post.sale !==''\" slot=\"end\" color=\"danger\" style=\"font-size: 18px;\">{{post.sale}}</ion-badge>\n            </ion-item>\n        \n            <ion-item lines=\"none\">\n              <ion-card-content [innerHtml]=\"post.content\">\n                <!-- {{post.content}} -->\n              </ion-card-content>\n            </ion-item>\n          </div>\n        </ion-slide>\n      </ion-slides>\n      <div class=\"create-post-button\" *ngIf=\"showPostCreate()\" (click)=\"createPost()\">\n        <ion-icon color=\"primary\" name=\"add\" style=\"font-size: 32px;\"></ion-icon>\n      </div>\n    </ion-card>\n\n    <div *ngIf=\"showPostCreate() && posts.length === 0\">\n      <ion-grid class=\"grid\">\n        <ion-row class=\"ion-align-items-center\">\n          <ion-col size=\"12\" class=\"ion-text-center\">\n            <ion-button class=\"cool-button\" shape=\"round\" color=\"secondary\" (click)=\"createPost()\">\n              Create Post\n              <ion-icon slot=\"end\" name=\"share\" color=\"primary\"></ion-icon>\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n  </div>\n\n  <!-- DESCRIPCION -->\n  <ion-card class=\"animate__animated animate__fadeInLeft cool-card\">\n    <ion-item color=\"tertiary\">\n      <ion-icon slot=\"end\" name=\"book\" color=\"light\"></ion-icon>\n      <ion-title>{{d.appTitle}}</ion-title>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n\n    <ion-item lines=\"none\">\n      <ion-card-content [innerHtml]=\"d.homeDescription\">\n        {{d.homeDescription}}\n      </ion-card-content>\n    </ion-item>\n  </ion-card>\n\n  <!-- SLIDER -->\n  <ion-card class=\"animate__animated animate__fadeInRight cool-card\">\n    <ion-slides pager=\"true\" [options]=\"slideOpts\" autoplay=\"true\" loop=\"true\" class=\"slider\">\n      <ion-slide *ngFor=\"let image of slider\">\n        <img [src]=\"image\" style=\"width: 100%; height: 100%; object-fit: fill;\">\n      </ion-slide>\n    </ion-slides>\n  </ion-card>\n\n\n\n    <!-- SLIDER 2-->\n    <ion-card class=\"animate__animated animate__fadeInRight cool-card\">\n\n      <ion-item color=\"tertiary\">\n        <ion-icon slot=\"end\" name=\"book\" color=\"light\"></ion-icon>\n        <ion-title>Barbers</ion-title>\n      </ion-item>\n  \n      <div class=\"cool-line\"></div>\n  \n      <ion-item lines=\"none\">\n        <ion-card-content [innerHtml]=\"d.barbersDescription\">\n        </ion-card-content>\n      </ion-item>\n\n      <ion-slides pager=\"true\" [options]=\"slideOpts\" autoplay=\"true\" loop=\"true\" class=\"slider1\">\n        <ion-slide *ngFor=\"let image of slider2\">\n          <img [src]=\"image\" style=\"width: 100%; height: 100%; object-fit: fill;\">\n        </ion-slide>\n      </ion-slides>\n    </ion-card>\n  \n\n  <!-- SERVICIOS -->\n  <ion-card class=\"cool-card\">\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"end\" name=\"list\" color=\"#fff\"></ion-icon>\n      <ion-title>Services</ion-title>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n\n    <ion-grid>\n      <ion-row *ngFor=\"let service of d.homeServices\" style=\"padding: 8px 0 8px 0;\">\n        <ion-col>\n          <div class=\"ion-text-start cool-text\">\n            <ion-text>{{service.name}}</ion-text> <br> \n            <ion-text style=\"color: grey;\">{{service.time}}</ion-text> <br>\n            <ion-text  class=\"ion-text-capitalize\" style=\"color: antiquewhite;\">{{service.description}}</ion-text>\n          </div>\n        </ion-col>\n        <ion-col>\n          <div class=\"ion-text-end cool-text\">\n            <ion-text>{{service.price}}</ion-text>\n          </div>\n        </ion-col>\n      </ion-row> \n    </ion-grid>\n  </ion-card> \n\n  <!-- HORARIO -->\n  <ion-card class=\"cool-card\">\n    <ion-item lines=\"none\" color=\"tertiary\">\n      <ion-icon slot=\"end\" name=\"calendar\" color=\"light\"></ion-icon>\n      <ion-title>Schedule</ion-title>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n\n    <ion-grid>\n      <ion-row *ngFor=\"let schedule of schedules index as i\">\n        <ion-col>\n          <div class=\"ion-text-start cool-text\">\n            <ion-text>{{schedule.day}}</ion-text>\n          </div>\n        </ion-col>\n        <ion-col>\n          <div class=\"ion-text-end cool-text\">\n            <ion-text>{{schedule.hour}}</ion-text>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n\n  <!-- VIDEO -->\n  <ion-card class=\"animate__animated animate__fadeInLeft cool-card\" *ngFor=\"let video of videos\">\n\n    <video #player playsinline preload=\"auto\" style=\"width: 100%; background-color: #1a1a1a;\" controls>\n      <source [src]=\"video\" type=\"video/mp4\">\n      Your browser does not support HTML5 video.\n    </video>\n  </ion-card>\n  <div style=\"margin-bottom: 96px;\"></div>\n\n   <!-- MUSICA -->\n   <!-- <ion-card *ngFor=\"let audio of audios\">\n   <audio controls>\n    <source [src]=\"audios\" type=\"audio/mpeg\">\n  </audio>\n</ion-card>    -->\n\n\n  <!-- FLYER -->\n  <!-- <ion-card class=\"cool-card\">\n    <img src=\"../assets/images/f.jpg\">\n  </ion-card> -->\n\n</ion-content>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/home/products/products.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/home/products/products.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar  lines=\"none\" class=\"ion-text-center\" color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-button (click)=\"closeImage()\" color=\"light\">\n        <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content class=\"cool-content\">\n  \n  <ion-grid style=\"height: 100%\">\n    <ion-row class=\"ion-align-items-center\" style=\"height: 100%\">\n      <ion-col size=\"12\" class=\"ion-text-center\">\n        <ion-slides #slides [options]=\"slideOpts\" loop=\"true\" zoom=\"true\" *ngIf=\"viewEntered\" (ionSlidesDidLoad)=\"slides.slideTo(productsIndex, 0)\">\n          <ion-slide *ngFor=\"let image1 of images1\">\n            <div class=\"swiper-zoom-container\">\n              <img [src]=\"image1\">\n            </div>\n          </ion-slide>\n        </ion-slides>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n    \n</ion-content> \n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/home/update-post/update-post.page.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/home/update-post/update-post.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- HEADER -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-button (click)=\"closeView()\">\n        <ion-icon slot=\"icon-only\" name='close' color='light' style=\"font-size: 25px;\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title>POST</ion-title>\n  </ion-toolbar>\n  <div class=\"cool-line\"></div>\n</ion-header>\n\n<ion-content class=\"cool-content\">\n\n  <ion-item lines=\"none\" color=\"primary\">\n    <ion-title class=\"cool-title\">Edit Post</ion-title>\n    <ion-icon slot=\"end\" name=\"document\" color=\"secondary\"></ion-icon>\n  </ion-item>\n\n  <div class=\"cool-line\"></div>\n\n  <ion-card class=\"cool-card sub-card\" style=\"padding: 32px 0 32px 0; margin-bottom: 32px; margin-top: 32px;\">\n    <div class=\"pin\" style=\"top: -5px;\"></div>\n\n    <!-- TITULO -->\n    <ion-item lines=\"none\" color=\"primary\">\n      <ion-label>Post Title</ion-label>\n      <ion-icon slot=\"end\" name=\"create\" color=\"secondary\"></ion-icon>\n    </ion-item>\n  \n    <ion-item lines=\"none\">\n      <ion-input placeholder=\"Write Title\" type=\"text\" [(ngModel)]=\"title\"></ion-input>\n    </ion-item>\n  \n    <div class=\"cool-line\"></div>\n\n    <!-- OFERTA -->\n    <ion-item lines=\"none\" color=\"primary\">\n      <ion-label>{{'Sale Percentage (Optional)'}}</ion-label>\n      <ion-icon slot=\"end\" name=\"create\" color=\"secondary\"></ion-icon>\n    </ion-item>\n  \n    <ion-item lines=\"none\">\n      <ion-input placeholder=\"Offer Percentage from 1 to 100\" maxlength=\"3\" type=\"text\" [(ngModel)]=\"sale\"></ion-input>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n  \n    <!-- CONTENIDO -->\n    <ion-item lines=\"none\" color=\"primary\">\n      <ion-label>Post Content</ion-label>\n      <ion-icon slot=\"end\" name=\"chatbox\" color=\"secondary\"></ion-icon>\n    </ion-item>\n  \n    <ion-card class=\"cool-card\" style=\"border-radius: 8px;\">\n      <ion-textarea \n      placeholder=\"Write Content\"\n      class=\"cool-text\" \n      style=\"white-space: pre-wrap;\"\n      [(ngModel)]=\"content\"\n      style=\"height: auto;\"></ion-textarea>\n    </ion-card>\n\n    <div class=\"pin\" style=\"bottom: -5px;\"></div>\n  </ion-card>\n\n  <div class=\"cool-line\"></div>\n\n\n  <ion-grid class=\"grid\" *ngIf=\"create\">\n    <ion-row class=\"ion-align-items-center\">\n      <ion-col size=\"12\" class=\"ion-text-center\">\n        <ion-button class=\"cool-button appointment-button\" shape=\"round\" color=\"secondary\" (click)=\"createPost()\">\n          Post\n          <ion-icon slot=\"end\" name=\"share\" color=\"primary\"></ion-icon>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid class=\"grid\" *ngIf=\"!create\">\n    <ion-row class=\"ion-align-items-center\">\n      <ion-col size=\"6\" class=\"ion-text-center\">\n        <ion-button class=\"cool-button appointment-button\" shape=\"round\" color=\"secondary\" (click)=\"deleteConfirm()\">\n          Delete\n          <ion-icon slot=\"end\" name=\"trash\" color=\"primary\"></ion-icon>\n        </ion-button>\n      </ion-col>\n      \n      <ion-col size=\"6\" class=\"ion-text-center\">\n        <ion-button class=\"cool-button appointment-button\" shape=\"round\" color=\"secondary\" (click)=\"updatePost()\">\n          Update\n          <ion-icon slot=\"end\" name=\"save\" color=\"primary\"></ion-icon>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/menu/home/home-routing.module.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/menu/home/home-routing.module.ts ***!
  \********************************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/menu/home/home.page.ts");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"]
    },
    {
        path: 'update-post',
        loadChildren: () => __webpack_require__.e(/*! import() | update-post-update-post-module */ "update-post-update-post-module").then(__webpack_require__.bind(null, /*! ./update-post/update-post.module */ "./src/app/pages/menu/home/update-post/update-post.module.ts")).then(m => m.UpdatePostPageModule)
    },
    {
        path: 'image',
        loadChildren: () => Promise.all(/*! import() | image-image-module */[__webpack_require__.e("common"), __webpack_require__.e("image-image-module")]).then(__webpack_require__.bind(null, /*! ./image/image.module */ "./src/app/pages/menu/home/image/image.module.ts")).then(m => m.ImagePageModule)
    },
    {
        path: 'image1',
        loadChildren: () => __webpack_require__.e(/*! import() | products-products-module */ "products-products-module").then(__webpack_require__.bind(null, /*! ./products/products.module */ "./src/app/pages/menu/home/products/products.module.ts")).then(m => m.productsPageModule)
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/menu/home/home.module.ts":
/*!************************************************!*\
  !*** ./src/app/pages/menu/home/home.module.ts ***!
  \************************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/pages/menu/home/home-routing.module.ts");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/menu/home/home.page.ts");







let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomePageRoutingModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/pages/menu/home/home.page.scss":
/*!************************************************!*\
  !*** ./src/app/pages/menu/home/home.page.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".post-button {\n  position: absolute;\n  left: 32px;\n  bottom: 24px;\n  z-index: 10;\n  height: 24px;\n  font-size: medium;\n}\n\n.create-post-button {\n  position: absolute;\n  right: 24px;\n  bottom: 24px;\n  z-index: 10;\n  height: 24px;\n}\n\n.gallery {\n  margin: 4px;\n  -moz-column-count: 2;\n       column-count: 2;\n  -moz-column-gap: 4px;\n       column-gap: 4px;\n}\n\n.image-container {\n  margin: 0px;\n}\n\n.image {\n  -o-object-fit: fill;\n     object-fit: fill;\n  height: 100%;\n  width: 100%;\n  background: var(--ion-color-primary);\n}\n\n.segment-container {\n  background: var(--ion-color-primary);\n}\n\n.slider1 {\n  background-color: var(--ion-color-primary);\n  --bullet-background: #383a3e;\n  --bullet-background-active: var(--ion-color-secondary);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWVudS9ob21lL2hvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLG9CQUFBO09BQUEsZUFBQTtFQUNBLG9CQUFBO09BQUEsZUFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7S0FBQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esb0NBQUE7QUFDSjs7QUFDQTtFQUNJLG9DQUFBO0FBRUo7O0FBQ0E7RUFDSSwwQ0FBQTtFQUNBLDRCQUFBO0VBQ0Esc0RBQUE7QUFFSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21lbnUvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wb3N0LWJ1dHRvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlOyBcbiAgICBsZWZ0OiAzMnB4OyBcbiAgICBib3R0b206IDI0cHg7IFxuICAgIHotaW5kZXg6IDEwOyBcbiAgICBoZWlnaHQ6IDI0cHg7XG4gICAgZm9udC1zaXplOiBtZWRpdW07XG59XG5cbi5jcmVhdGUtcG9zdC1idXR0b24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTsgXG4gICAgcmlnaHQ6IDI0cHg7IFxuICAgIGJvdHRvbTogMjRweDsgXG4gICAgei1pbmRleDogMTA7IFxuICAgIGhlaWdodDogMjRweDtcbn1cblxuLmdhbGxlcnkge1xuICAgIG1hcmdpbjogNHB4O1xuICAgIGNvbHVtbi1jb3VudDogMjtcbiAgICBjb2x1bW4tZ2FwOiA0cHg7XG59XG5cbi5pbWFnZS1jb250YWluZXIge1xuICAgIG1hcmdpbjogMHB4O1xufVxuXG4uaW1hZ2Uge1xuICAgIG9iamVjdC1maXQ6IGZpbGw7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn1cbi5zZWdtZW50LWNvbnRhaW5lcntcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG5cbi5zbGlkZXIxIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7IFxuICAgIC0tYnVsbGV0LWJhY2tncm91bmQ6ICMzODNhM2U7IFxuICAgIC0tYnVsbGV0LWJhY2tncm91bmQtYWN0aXZlOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTsgXG59Il19 */");

/***/ }),

/***/ "./src/app/pages/menu/home/home.page.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/menu/home/home.page.ts ***!
  \**********************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/data.service */ "./src/app/services/data.service.ts");
/* harmony import */ var src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/firebase.service */ "./src/app/services/firebase.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _update_post_update_post_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./update-post/update-post.page */ "./src/app/pages/menu/home/update-post/update-post.page.ts");
/* harmony import */ var _image_image_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./image/image.page */ "./src/app/pages/menu/home/image/image.page.ts");
/* harmony import */ var _products_products_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./products/products.page */ "./src/app/pages/menu/home/products/products.page.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");









let HomePage = class HomePage {
    constructor(ds, data, firebaseService, modalController) {
        this.ds = ds;
        this.data = data;
        this.firebaseService = firebaseService;
        this.modalController = modalController;
        this.posts = [];
        this.postsReady = false;
        this.selectedTab = "home";
        this.currentPlaying = null;
        this.slider = [];
        this.slider2 = [];
        this.slideSales = {
            autoplay: {
                delay: 4000,
            }
        };
        this.slideOpts = {
            loop: true,
            autoplay: {
                delay: 2000,
            }
        };
        this.schedules = [
            {
                day: "Monday",
                hour: "Close",
            },
            {
                day: "Tuesday",
                hour: "10:00 am – 06:00 pm",
            },
            {
                day: "Wednesday",
                hour: "10:00 am – 06:00 pm",
            },
            {
                day: "Thursday",
                hour: "10:00 am – 06:00 pm",
            },
            {
                day: "Friday",
                hour: "10:00 am – 06:00 pm",
            },
            {
                day: "Saturday",
                hour: "10:00 am – 06:00 pm",
            },
            {
                day: "Sunday",
                hour: "12:00 am – 05:00 pm",
            },
        ];
        this.videos1 = [];
        this.videos = [];
        this.audios = [];
        this.audios1 = [];
        this.images = [];
        this.images1 = [];
        this.d = data.appData.data;
    }
    ngOnInit() {
        //Obtener Traduccion Dinamica
        this.data.getAppData().then(cloudTranslation => {
            this.d = this.data.appData.data;
            this.slider = this.d.homeSlider;
            this.slider2 = this.d.homeSlider2;
            this.schedules = this.d.homeSchedules;
            this.videos1 = this.d.homeVideos1;
            this.videos = this.d.homeVideos;
            this.audios = this.d.audios;
            this.audios1 = this.d.audios1;
            this.images = this.d.gallery;
            this.images1 = this.d.products;
        });
        //Obtener Posts
        if (this.postsRef)
            this.postsRef.unsubscribe();
        this.postsRef = this.firebaseService.getPostsRealTime().subscribe(posts => {
            if (posts)
                this.posts = posts;
            this.postsReady = true;
        });
        if (this.adminPostsRef)
            this.adminPostsRef.unsubscribe();
        this.adminPostsRef = this.firebaseService.getPostDataRealTime().subscribe(adminPosts => {
            this.adminPosts = adminPosts;
        });
    }
    ionViewDidEnter() {
        this.didScroll();
    }
    didScroll() {
        if (this.currentPlaying && this.isElementInViewport(this.currentPlaying)) {
            return;
        }
        else if (this.currentPlaying && !this.isElementInViewport(this.currentPlaying)) {
            this.currentPlaying.pause();
            this.currentPlaying = null;
        }
        this.videoPlayers.forEach(player => {
            if (this.currentPlaying) {
                return;
            }
            const nativeElement = player.nativeElement;
            const inView = this.isElementInViewport(nativeElement);
            if (inView) {
                this.currentPlaying = nativeElement;
                this.currentPlaying.muted = true;
                this.currentPlaying.play();
            }
        });
    }
    isElementInViewport(el) {
        const rect = el.getBoundingClientRect();
        return (rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <= (window.innerWidth || document.documentElement.clientWidth));
    }
    showPostEdit() {
        if (this.firebaseService.userData)
            return this.firebaseService.userData.admin;
        else
            return false;
    }
    showPostCreate() {
        if (this.adminPosts) {
            return (this.showPostEdit() && this.adminPosts.postNumber < this.adminPosts.maxPostNumber);
        }
        else
            return false;
    }
    viewPost(post) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const va = yield this.modalController.create({
                component: _update_post_update_post_page__WEBPACK_IMPORTED_MODULE_5__["UpdatePostPage"],
                componentProps: {
                    post: post,
                    create: false,
                    adminPosts: this.adminPosts,
                }
            });
            yield va.present();
        });
    }
    createPost() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const va = yield this.modalController.create({
                component: _update_post_update_post_page__WEBPACK_IMPORTED_MODULE_5__["UpdatePostPage"],
                componentProps: {
                    post: null,
                    create: true,
                    adminPosts: this.adminPosts,
                }
            });
            yield va.present();
        });
    }
    openImage(i) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _image_image_page__WEBPACK_IMPORTED_MODULE_6__["ImagePage"],
                cssClass: 'modal-transparency',
                componentProps: {
                    index: i + 1,
                    images: this.images,
                }
            });
            yield modal.present();
        });
    }
    openImage1(i) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _products_products_page__WEBPACK_IMPORTED_MODULE_7__["productsPage"],
                cssClass: 'modal-transparency',
                componentProps: {
                    index: i + 1,
                    images1: this.images1,
                }
            });
            yield modal.present();
        });
    }
    getMapUrl() {
        return this.ds.bypassSecurityTrustResourceUrl(this.d.mapSource);
    }
};
HomePage.ctorParameters = () => [
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_8__["DomSanitizer"] },
    { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_2__["DataService"] },
    { type: src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_3__["FirebaseService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] }
];
HomePage.propDecorators = {
    videoPlayers: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChildren"], args: ['player',] }]
};
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/home/home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/menu/home/home.page.scss")).default]
    })
], HomePage);



/***/ }),

/***/ "./src/app/pages/menu/home/products/products.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/menu/home/products/products.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21lbnUvaG9tZS9wcm9kdWN0cy9wcm9kdWN0cy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/menu/home/products/products.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/menu/home/products/products.page.ts ***!
  \***********************************************************/
/*! exports provided: productsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productsPage", function() { return productsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let productsPage = class productsPage {
    constructor(modalController, navParams, loadingController) {
        this.modalController = modalController;
        this.navParams = navParams;
        this.loadingController = loadingController;
        this.viewEntered = false;
        this.images = [];
        this.slideOpts = {
            loop: true,
            zoom: true,
            passiveListeners: false,
        };
        this.productsIndex = this.navParams.get('index');
        this.images = this.navParams.get('images');
    }
    ngOnInit() {
        this.loadingController.create({
            message: 'Loading...',
            cssClass: 'cool-loading',
        }).then(overlay => {
            this.loading = overlay;
            this.loading.present();
        });
    }
    ionViewDidEnter() {
        this.viewEntered = true;
        this.loading.dismiss();
    }
    closeImage() {
        this.modalController.dismiss();
    }
};
productsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
productsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-products',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./products.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/home/products/products.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./products.page.scss */ "./src/app/pages/menu/home/products/products.page.scss")).default]
    })
], productsPage);



/***/ }),

/***/ "./src/app/pages/menu/home/update-post/update-post.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/menu/home/update-post/update-post.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21lbnUvaG9tZS91cGRhdGUtcG9zdC91cGRhdGUtcG9zdC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/menu/home/update-post/update-post.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/menu/home/update-post/update-post.page.ts ***!
  \*****************************************************************/
/*! exports provided: UpdatePostPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatePostPage", function() { return UpdatePostPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/firebase.service */ "./src/app/services/firebase.service.ts");




let UpdatePostPage = class UpdatePostPage {
    constructor(firebaseService, modalController, navParams, toastController, alertController, loadingController) {
        this.firebaseService = firebaseService;
        this.modalController = modalController;
        this.navParams = navParams;
        this.toastController = toastController;
        this.alertController = alertController;
        this.loadingController = loadingController;
        this.create = false;
        this.title = "";
        this.sale = "";
        this.content = "";
        this.post = navParams.get('post');
        this.create = navParams.get('create');
        this.adminPosts = navParams.get('adminPosts');
        //console.log(this.post);
    }
    ngOnInit() {
        if (!this.create) {
            this.title = this.post.title;
            this.sale = this.post.sale;
            this.content = this.post.content;
        }
    }
    closeView() {
        this.modalController.dismiss();
    }
    showInfo(info) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: info,
                duration: 6000,
                color: 'secondary',
            });
            toast.present();
        });
    }
    allowPost() {
        return (this.title !== "" && this.content !== "");
    }
    createPost() {
        this.loadingController.create({
            message: 'Loading...',
            cssClass: 'cool-loading',
        }).then(overlay => {
            this.loading = overlay;
            this.loading.present();
            if (this.allowPost()) {
                var post = {};
                post.id = 'Undefined';
                post.title = this.title;
                post.sale = this.sale;
                post.content = this.content;
                post.postNumber = this.adminPosts.postCount;
                this.firebaseService.createPost(post, this.adminPosts).then(close => {
                    this.loading.dismiss();
                    this.closeView();
                    this.showInfo("Post Created Successfully.");
                });
            }
            else {
                this.showInfo("You must fill all the fields.");
                this.loading.dismiss();
            }
        });
    }
    updatePost() {
        this.loadingController.create({
            message: 'Loading...',
            cssClass: 'cool-loading',
        }).then(overlay => {
            this.loading = overlay;
            this.loading.present();
            if (this.allowPost()) {
                this.post.title = this.title;
                this.post.sale = this.sale;
                this.post.content = this.content;
                this.firebaseService.updatePost(this.post).then(close => {
                    this.loading.dismiss();
                    this.closeView();
                    this.showInfo("Post has been Updated.");
                });
            }
            else {
                this.showInfo("You must fill all the fields.");
                this.loading.dismiss();
            }
        });
    }
    deleteConfirm() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'cool-alert',
                header: "Delete Post",
                message: "Are you sure you want to Delete this Post?",
                buttons: [
                    {
                        text: "Cancel",
                        role: 'cancel',
                        cssClass: 'secondary',
                    },
                    {
                        text: "Delete",
                        handler: () => {
                            this.deletePost();
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    deletePost() {
        this.loadingController.create({
            message: 'Loading...',
            cssClass: 'cool-loading',
        }).then(overlay => {
            this.loading = overlay;
            this.loading.present();
            this.firebaseService.deletePost(this.post, this.adminPosts).then(close => {
                this.loading.dismiss();
                this.closeView();
                this.showInfo("Post has been Deleted.");
            });
        });
    }
};
UpdatePostPage.ctorParameters = () => [
    { type: src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_3__["FirebaseService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
UpdatePostPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-update-post',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./update-post.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/home/update-post/update-post.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./update-post.page.scss */ "./src/app/pages/menu/home/update-post/update-post.page.scss")).default]
    })
], UpdatePostPage);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map