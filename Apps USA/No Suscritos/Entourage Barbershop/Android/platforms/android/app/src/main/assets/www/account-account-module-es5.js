(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["account-account-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/account/account.page.html":
    /*!********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/account/account.page.html ***!
      \********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesMenuAccountAccountPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- HEADER -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: #fff;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>ACCOUNT</ion-title>\n  </ion-toolbar>\n  <div class=\"cool-line\"></div>\n</ion-header>\n\n<ion-content class=\"cool-content\">\n\n  <div class=\"animate__animated animate__fadeInLeft\" *ngIf=\"userReady\">\n\n    <ion-item lines=\"none\" color=\"primary\">\n      <ion-icon slot=\"end\" name=\"book\" color=\"light\"></ion-icon>\n      <ion-title class=\"cool-title\">Account Data</ion-title>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n\n    <ion-grid style=\"margin-top: 32px;\">\n      <ion-row class=\"ion-align-items-center\">\n        <ion-col size=\"12\" class=\"ion-text-center\">\n          <div class=\"avatar\">\n            <img src=\"../assets/images/user.png\">\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid> \n  \n    <ion-card class=\"cool-card sub-card\" style=\"padding: 32px 0 32px 0;\">\n\n      <div class=\"pin\" style=\"top: -5px;\"></div>\n\n      <!-- USERNAME -->\n      <ion-item lines=\"none\">\n        <ion-icon slot=\"start\" name=\"person\" color=\"#fff\"></ion-icon>\n        <ion-input type=\"text\" [(ngModel)]=\"firebaseService.userData.username\"></ion-input>\n      </ion-item>\n\n      <div class=\"cool-line\"></div>\n  \n      <ion-item lines=\"none\">\n        <ion-icon slot=\"start\" name=\"mail\" color=\"#fff\"></ion-icon>\n        <ion-label>{{firebaseService.userData.email}}</ion-label>\n      </ion-item>\n\n      <div class=\"cool-line\"></div>\n\n      <!-- PHONE -->\n      <ion-item lines=\"none\">\n        <ion-icon slot=\"start\" name=\"call\" color=\"#fff\"></ion-icon>\n        <ion-input type=\"text\" [(ngModel)]=\"firebaseService.userData.phone\"></ion-input>\n      </ion-item>\n\n      <div class=\"cool-line\"></div>\n\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"start\" name=\"flame\" color=\"light\"></ion-icon>\n      <ion-label>{{'Role: '+ getAdminText()}}</ion-label>\n    </ion-item>\n  \n      <div class=\"cool-line\"></div>\n  \n      <ion-item lines=\"none\">\n        <ion-icon slot=\"start\" name=\"ribbon\" color=\"#fff\"></ion-icon>\n        <ion-label>{{'Level: '+ firebaseService.userData.level}}</ion-label>\n      </ion-item>\n\n      <div class=\"cool-line\"></div>\n\n      <!-- NOTIFICATIONS -->\n      <ion-item lines=\"none\">\n        <ion-icon slot=\"start\" name=\"notifications\" color=\"#fff\"></ion-icon>\n        <ion-text>Notifications</ion-text>\n        <ion-toggle [(ngModel)]=\"activateNotifications\" slot=\"end\" color=\"secondary\"></ion-toggle>\n      </ion-item>\n  \n      <div class=\"pin\" style=\"bottom: -5px;\"></div>\n  \n    </ion-card>\n  \n    <ion-grid class=\"grid\" style=\"padding-top: 0px;\">\n      <ion-row class=\"ion-align-items-center\">\n        <ion-col size=\"12\" class=\"ion-text-center\">\n          <ion-button class=\"cool-button\" shape=\"round\" color=\"secondary\" (click)=\"logout()\">Logout</ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid> \n    \n    <div *ngIf=\"firebaseService.userData.admin\">\n      <div class=\"cool-line\" style=\"margin-top: 32px;\"></div>\n\n      <ion-item lines=\"none\" color=\"primary\">\n        <ion-icon slot=\"end\" name=\"options\" color=\"light\"></ion-icon>\n        <ion-title class=\"cool-title\">Application Options</ion-title>\n      </ion-item>\n\n      <div class=\"cool-line\"></div>\n\n      <ion-card class=\"cool-card sub-card\" style=\"padding: 32px 0 32px 0;\">\n        <div class=\"pin\" style=\"top: -5px;\"></div>\n    \n        <ion-item lines=\"none\">\n          <ion-icon slot=\"start\" name=\"chatbubbles\" color=\"light\"></ion-icon>\n          <ion-label class=\"ion-text-wrap\">Limit of Requests per User</ion-label>\n        </ion-item>\n\n        <ion-range class=\"cool-range\" min=\"1\" max=\"5\" step=\"1\" snaps=\"true\" pin=\"true\" [value]=\"appConfiguration.maxUserRequestNumber\" color=\"secondary\" (ionChange)=\"setMaxRequests($event)\">\n          <ion-text class=\"cool-text\" slot=\"end\">{{appConfiguration.maxUserRequestNumber}}</ion-text>\n        </ion-range>\n\n        <div class=\"cool-line\"></div>\n\n        <ion-item lines=\"none\">\n          <ion-icon slot=\"start\" name=\"calendar\" color=\"light\"></ion-icon>\n          <ion-label class=\"ion-text-wrap\">Range of Request Creation Days</ion-label>\n        </ion-item>\n\n        <ion-range class=\"cool-range\" min=\"5\" max=\"30\" step=\"1\" snaps=\"true\" pin=\"true\" [value]=\"appConfiguration.dayRangeLimit\" color=\"secondary\" (ionChange)=\"setDayRangeLimit($event)\">\n          <ion-text class=\"cool-text\" slot=\"end\">{{appConfiguration.dayRangeLimit}}</ion-text>\n        </ion-range>\n  \n        <div class=\"pin\" style=\"bottom: -5px;\"></div>\n      </ion-card>\n    </div>\n\n    <ion-grid class=\"grid\" style=\"padding-top: 0px; margin-bottom: 32px;\">\n      <ion-row class=\"ion-align-items-center\">\n        <ion-col size=\"12\" class=\"ion-text-center\">\n          <ion-button class=\"cool-button\" shape=\"round\" color=\"secondary\" (click)=\"saveAccountChanges()\">\n            Save Changes\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid> \n\n  </div>\n  \n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/menu/account/account-routing.module.ts":
    /*!**************************************************************!*\
      !*** ./src/app/pages/menu/account/account-routing.module.ts ***!
      \**************************************************************/

    /*! exports provided: AccountPageRoutingModule */

    /***/
    function srcAppPagesMenuAccountAccountRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AccountPageRoutingModule", function () {
        return AccountPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _account_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./account.page */
      "./src/app/pages/menu/account/account.page.ts");

      var routes = [{
        path: '',
        component: _account_page__WEBPACK_IMPORTED_MODULE_3__["AccountPage"]
      }];

      var AccountPageRoutingModule = function AccountPageRoutingModule() {
        _classCallCheck(this, AccountPageRoutingModule);
      };

      AccountPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AccountPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/menu/account/account.module.ts":
    /*!******************************************************!*\
      !*** ./src/app/pages/menu/account/account.module.ts ***!
      \******************************************************/

    /*! exports provided: AccountPageModule */

    /***/
    function srcAppPagesMenuAccountAccountModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AccountPageModule", function () {
        return AccountPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _account_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./account-routing.module */
      "./src/app/pages/menu/account/account-routing.module.ts");
      /* harmony import */


      var _account_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./account.page */
      "./src/app/pages/menu/account/account.page.ts");

      var AccountPageModule = function AccountPageModule() {
        _classCallCheck(this, AccountPageModule);
      };

      AccountPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _account_routing_module__WEBPACK_IMPORTED_MODULE_5__["AccountPageRoutingModule"]],
        declarations: [_account_page__WEBPACK_IMPORTED_MODULE_6__["AccountPage"]]
      })], AccountPageModule);
      /***/
    },

    /***/
    "./src/app/pages/menu/account/account.page.scss":
    /*!******************************************************!*\
      !*** ./src/app/pages/menu/account/account.page.scss ***!
      \******************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesMenuAccountAccountPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".grid {\n  padding: 10% 10% 0 10%;\n}\n\n.avatar {\n  margin-left: auto;\n  margin-right: auto;\n  width: 128px;\n  height: 128px;\n  border-radius: 50%;\n  background-color: var(--ion-color-secondary);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWVudS9hY2NvdW50L2FjY291bnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksc0JBQUE7QUFDSjs7QUFFQTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsNENBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21lbnUvYWNjb3VudC9hY2NvdW50LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ncmlkIHtcbiAgICBwYWRkaW5nOiAxMCUgMTAlIDAgMTAlO1xufVxuXG4uYXZhdGFyIHtcbiAgICBtYXJnaW4tbGVmdDogYXV0bzsgXG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvOyBcbiAgICB3aWR0aDogMTI4cHg7XG4gICAgaGVpZ2h0OiAxMjhweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/pages/menu/account/account.page.ts":
    /*!****************************************************!*\
      !*** ./src/app/pages/menu/account/account.page.ts ***!
      \****************************************************/

    /*! exports provided: AccountPage */

    /***/
    function srcAppPagesMenuAccountAccountPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AccountPage", function () {
        return AccountPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/firebase.service */
      "./src/app/services/firebase.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var AccountPage = /*#__PURE__*/function () {
        function AccountPage(firebaseService, router, loadingController, toastController) {
          _classCallCheck(this, AccountPage);

          this.firebaseService = firebaseService;
          this.router = router;
          this.loadingController = loadingController;
          this.toastController = toastController;
          this.userReady = false;
          this.activateNotifications = false;
        }

        _createClass(AccountPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.getAppData();
          }
        }, {
          key: "getAppData",
          value: function getAppData() {
            var _this = this;

            this.loadingController.create({
              message: 'Loading...',
              cssClass: 'cool-loading'
            }).then(function (overlay) {
              _this.loading = overlay;

              _this.loading.present(); //Tomar Configuracion


              _this.firebaseService.getAppConfiguration().then(function (appConfiguration) {
                _this.appConfiguration = appConfiguration; //Datos de Usuario

                _this.firebaseService.userSetup().then(function (userDataExist) {
                  _this.userReady = userDataExist;

                  if (userDataExist) {
                    _this.activateNotifications = _this.firebaseService.userData.notifications;

                    _this.loading.dismiss();
                  }
                });
              });
            });
          }
        }, {
          key: "showInfo",
          value: function showInfo(info) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastController.create({
                        message: info,
                        duration: 6000,
                        color: 'secondary'
                      });

                    case 2:
                      toast = _context.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "logout",
          value: function logout() {
            var _this2 = this;

            this.loadingController.create({
              message: 'Loging Out...',
              cssClass: 'cool-loading'
            }).then(function (overlay) {
              _this2.loading = overlay;

              _this2.loading.present();

              _this2.firebaseService.logout().then(function (redirect) {
                _this2.loading.dismiss();

                _this2.router.navigate(['/menu/login']);
              });
            });
          }
        }, {
          key: "setMaxRequests",
          value: function setMaxRequests(event) {
            this.appConfiguration.maxUserRequestNumber = event.detail.value;
          }
        }, {
          key: "setDayRangeLimit",
          value: function setDayRangeLimit(event) {
            this.appConfiguration.dayRangeLimit = event.detail.value;
          }
        }, {
          key: "getAdminText",
          value: function getAdminText() {
            return this.firebaseService.userData.admin ? "Admin" : "User";
          }
        }, {
          key: "saveAccountChanges",
          value: function saveAccountChanges() {
            var _this3 = this;

            this.loadingController.create({
              message: 'Saving...',
              cssClass: 'cool-loading'
            }).then(function (overlay) {
              _this3.loading = overlay;

              _this3.loading.present();

              _this3.userData = _this3.firebaseService.userData;
              _this3.userData.notifications = _this3.activateNotifications;

              if (_this3.userData.admin) {
                _this3.firebaseService.updateUser(_this3.userData).then(function (updateAppConfig) {
                  var appConfig = {
                    maxUserRequestNumber: _this3.appConfiguration.maxUserRequestNumber,
                    dayRangeLimit: _this3.appConfiguration.dayRangeLimit,
                    maxUserRating: 10
                  };

                  _this3.firebaseService.updateAppConfiguration(appConfig).then(function (showInfo) {
                    _this3.loading.dismiss();

                    _this3.showInfo("The changes have been saved successfully.");
                  });
                });
              } else {
                _this3.firebaseService.updateUser(_this3.userData).then(function (showInfo) {
                  _this3.loading.dismiss();

                  _this3.showInfo("The changes have been saved successfully.");
                });
              }

              _this3.toggleSalesNotifications();
            });
          }
        }, {
          key: "toggleSalesNotifications",
          value: function toggleSalesNotifications() {
            var _this4 = this;

            if (!this.activateNotifications) {
              this.firebaseService.fcm.unsubscribe(this.firebaseService.getDataBaseName() + "-sales").then(function (showInfo) {
                _this4.showInfo("Notifications Off");
              });
            } else {
              if (!this.firebaseService.userData.admin) {
                this.firebaseService.fcm.subscribe(this.firebaseService.getDataBaseName() + "-sales").then(function (showInfo) {
                  _this4.showInfo("Notifications On");
                });
              }
            }
          }
        }]);

        return AccountPage;
      }();

      AccountPage.ctorParameters = function () {
        return [{
          type: src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_3__["FirebaseService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
        }];
      };

      AccountPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-account',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./account.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/account/account.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./account.page.scss */
        "./src/app/pages/menu/account/account.page.scss"))["default"]]
      })], AccountPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=account-account-module-es5.js.map