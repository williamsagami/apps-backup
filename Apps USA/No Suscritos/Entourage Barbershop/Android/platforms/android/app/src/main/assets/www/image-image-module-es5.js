(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["image-image-module"], {
    /***/
    "./src/app/pages/menu/gallery/image/image-routing.module.ts":
    /*!******************************************************************!*\
      !*** ./src/app/pages/menu/gallery/image/image-routing.module.ts ***!
      \******************************************************************/

    /*! exports provided: ImagePageRoutingModule */

    /***/
    function srcAppPagesMenuGalleryImageImageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImagePageRoutingModule", function () {
        return ImagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _image_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./image.page */
      "./src/app/pages/menu/gallery/image/image.page.ts");

      var routes = [{
        path: '',
        component: _image_page__WEBPACK_IMPORTED_MODULE_3__["ImagePage"]
      }];

      var ImagePageRoutingModule = function ImagePageRoutingModule() {
        _classCallCheck(this, ImagePageRoutingModule);
      };

      ImagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ImagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/menu/gallery/image/image.module.ts":
    /*!**********************************************************!*\
      !*** ./src/app/pages/menu/gallery/image/image.module.ts ***!
      \**********************************************************/

    /*! exports provided: ImagePageModule */

    /***/
    function srcAppPagesMenuGalleryImageImageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImagePageModule", function () {
        return ImagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _image_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./image-routing.module */
      "./src/app/pages/menu/gallery/image/image-routing.module.ts");
      /* harmony import */


      var _image_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./image.page */
      "./src/app/pages/menu/gallery/image/image.page.ts");

      var ImagePageModule = function ImagePageModule() {
        _classCallCheck(this, ImagePageModule);
      };

      ImagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _image_routing_module__WEBPACK_IMPORTED_MODULE_5__["ImagePageRoutingModule"]],
        declarations: [_image_page__WEBPACK_IMPORTED_MODULE_6__["ImagePage"]]
      })], ImagePageModule);
      /***/
    },

    /***/
    "./src/app/pages/menu/home/image/image-routing.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/menu/home/image/image-routing.module.ts ***!
      \***************************************************************/

    /*! exports provided: ImagePageRoutingModule */

    /***/
    function srcAppPagesMenuHomeImageImageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImagePageRoutingModule", function () {
        return ImagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _image_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./image.page */
      "./src/app/pages/menu/home/image/image.page.ts");

      var routes = [{
        path: '',
        component: _image_page__WEBPACK_IMPORTED_MODULE_3__["ImagePage"]
      }];

      var ImagePageRoutingModule = function ImagePageRoutingModule() {
        _classCallCheck(this, ImagePageRoutingModule);
      };

      ImagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ImagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/menu/home/image/image.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/menu/home/image/image.module.ts ***!
      \*******************************************************/

    /*! exports provided: ImagePageModule */

    /***/
    function srcAppPagesMenuHomeImageImageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImagePageModule", function () {
        return ImagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _image_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./image-routing.module */
      "./src/app/pages/menu/home/image/image-routing.module.ts");
      /* harmony import */


      var _image_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./image.page */
      "./src/app/pages/menu/home/image/image.page.ts");

      var ImagePageModule = function ImagePageModule() {
        _classCallCheck(this, ImagePageModule);
      };

      ImagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _image_routing_module__WEBPACK_IMPORTED_MODULE_5__["ImagePageRoutingModule"]],
        declarations: [_image_page__WEBPACK_IMPORTED_MODULE_6__["ImagePage"]]
      })], ImagePageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=image-image-module-es5.js.map