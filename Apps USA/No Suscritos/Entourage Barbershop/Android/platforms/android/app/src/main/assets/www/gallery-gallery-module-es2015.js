(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["gallery-gallery-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/gallery/gallery.page.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/gallery/gallery.page.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- HEADER -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: #fff;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title style=\"color: #fff;\">Gallery</ion-title>\n  </ion-toolbar>\n  <div class=\"cool-line\"></div>\n</ion-header>\n\n<ion-content class=\"cool-content\">\n\n  <!-- BOTON CONTACTO -->\n  <ion-fab vertical=\"top\" horizontal=\"end\" edge slot=\"fixed\" class=\"fab-button\">\n    <ion-fab-button>\n      <ion-icon name=\"person\"></ion-icon>\n    </ion-fab-button>\n    <ion-fab-list side=\"bottom\">\n      <ion-fab-button *ngFor=\"let phoneNumber of d.phoneNumbers\" (click)=\"data.phoneCall(phoneNumber.number)\" color=\"primary\">\n        <ion-icon name=\"call\"></ion-icon>\n      </ion-fab-button>\n    </ion-fab-list>\n    <ion-fab-list side=\"start\">\n      <ion-fab-button *ngFor=\"let socialNetwork of d.socialNetworks\" (click)=\"data.openLink(socialNetwork.link)\" color=\"secondary\">\n        <ion-icon [name]=\"socialNetwork.icon\"></ion-icon>\n      </ion-fab-button>\n    </ion-fab-list>\n  </ion-fab>\n\n  <!-- GALERIA -->\n  <div  class=\"gallery\">\n    <div class=\"image\" *ngFor=\"let image of images; index as i\" button (click)=\"openImage(i)\">\n      <img [src]=\"image\">\n    </div>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/menu/gallery/gallery-routing.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/menu/gallery/gallery-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: GalleryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleryPageRoutingModule", function() { return GalleryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _gallery_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./gallery.page */ "./src/app/pages/menu/gallery/gallery.page.ts");




const routes = [
    {
        path: '',
        component: _gallery_page__WEBPACK_IMPORTED_MODULE_3__["GalleryPage"]
    },
    {
        path: 'image',
        loadChildren: () => Promise.all(/*! import() | image-image-module */[__webpack_require__.e("common"), __webpack_require__.e("image-image-module")]).then(__webpack_require__.bind(null, /*! ./image/image.module */ "./src/app/pages/menu/gallery/image/image.module.ts")).then(m => m.ImagePageModule)
    }
];
let GalleryPageRoutingModule = class GalleryPageRoutingModule {
};
GalleryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], GalleryPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/menu/gallery/gallery.module.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/menu/gallery/gallery.module.ts ***!
  \******************************************************/
/*! exports provided: GalleryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleryPageModule", function() { return GalleryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _gallery_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./gallery-routing.module */ "./src/app/pages/menu/gallery/gallery-routing.module.ts");
/* harmony import */ var _gallery_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./gallery.page */ "./src/app/pages/menu/gallery/gallery.page.ts");







let GalleryPageModule = class GalleryPageModule {
};
GalleryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _gallery_routing_module__WEBPACK_IMPORTED_MODULE_5__["GalleryPageRoutingModule"]
        ],
        declarations: [_gallery_page__WEBPACK_IMPORTED_MODULE_6__["GalleryPage"]]
    })
], GalleryPageModule);



/***/ }),

/***/ "./src/app/pages/menu/gallery/gallery.page.scss":
/*!******************************************************!*\
  !*** ./src/app/pages/menu/gallery/gallery.page.scss ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".gallery {\n  margin: 4px;\n  -moz-column-count: 2;\n       column-count: 2;\n  -moz-column-gap: 4px;\n       column-gap: 4px;\n}\n\n.image-container {\n  margin: 0px;\n}\n\n.image {\n  -o-object-fit: fill;\n     object-fit: fill;\n  height: 100%;\n  width: 100%;\n  background: var(--ion-color-primary);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWVudS9nYWxsZXJ5L2dhbGxlcnkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksV0FBQTtFQUNBLG9CQUFBO09BQUEsZUFBQTtFQUNBLG9CQUFBO09BQUEsZUFBQTtBQUFKOztBQUdBO0VBQ0ksV0FBQTtBQUFKOztBQUdBO0VBQ0ksbUJBQUE7S0FBQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esb0NBQUE7QUFBSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21lbnUvZ2FsbGVyeS9nYWxsZXJ5LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuLmdhbGxlcnkge1xuICAgIG1hcmdpbjogNHB4O1xuICAgIGNvbHVtbi1jb3VudDogMjtcbiAgICBjb2x1bW4tZ2FwOiA0cHg7XG59XG5cbi5pbWFnZS1jb250YWluZXIge1xuICAgIG1hcmdpbjogMHB4O1xufVxuXG4uaW1hZ2Uge1xuICAgIG9iamVjdC1maXQ6IGZpbGw7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/menu/gallery/gallery.page.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/menu/gallery/gallery.page.ts ***!
  \****************************************************/
/*! exports provided: GalleryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleryPage", function() { return GalleryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/data.service */ "./src/app/services/data.service.ts");
/* harmony import */ var _image_image_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./image/image.page */ "./src/app/pages/menu/gallery/image/image.page.ts");





let GalleryPage = class GalleryPage {
    constructor(modalController, data) {
        this.modalController = modalController;
        this.data = data;
        this.images = [
            '../assets/images/1.jpg',
            '../assets/images/2.jpg',
            '../assets/images/3.jpg',
            '../assets/images/4.jpg',
            '../assets/images/5.jpg',
            '../assets/images/6.jpg',
            '../assets/images/7.jpg',
            '../assets/images/8.jpg',
            '../assets/images/9.jpg',
            '../assets/images/10.jpg',
            '../assets/images/11.jpg',
            '../assets/images/12.jpg',
            '../assets/images/13.jpg',
            '../assets/images/14.jpg',
            '../assets/images/15.jpg',
            '../assets/images/16.jpg',
            '../assets/images/17.jpg',
            '../assets/images/18.jpg',
            '../assets/images/19.jpg',
            '../assets/images/21.png',
            '../assets/images/22.png',
            '../assets/images/23.png',
            '../assets/images/24.png',
            '../assets/images/25.png',
            '../assets/images/26.png',
            '../assets/images/27.png',
            '../assets/images/28.png',
            '../assets/images/29.png',
            '../assets/images/30.png',
            '../assets/images/31.png',
            '../assets/images/32.png',
            '../assets/images/33.png',
            '../assets/images/34.png',
            '../assets/images/35.png',
            '../assets/images/36.png',
            '../assets/images/37.png',
            '../assets/images/38.png',
            '../assets/images/39.png',
            '../assets/images/40.png',
            '../assets/images/41.png',
        ];
        this.d = data.appData.data;
    }
    ngOnInit() {
        //Obtener Traduccion Dinamica
        this.data.getAppData().then(cloudTranslation => {
            this.d = this.data.appData.data;
            this.images = this.d.gallery;
        });
    }
    openImage(i) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _image_image_page__WEBPACK_IMPORTED_MODULE_4__["ImagePage"],
                cssClass: 'modal-transparency',
                componentProps: {
                    index: i + 1,
                    images: this.images,
                }
            });
            yield modal.present();
        });
    }
};
GalleryPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"] }
];
GalleryPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-gallery',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./gallery.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/gallery/gallery.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./gallery.page.scss */ "./src/app/pages/menu/gallery/gallery.page.scss")).default]
    })
], GalleryPage);



/***/ })

}]);
//# sourceMappingURL=gallery-gallery-module-es2015.js.map