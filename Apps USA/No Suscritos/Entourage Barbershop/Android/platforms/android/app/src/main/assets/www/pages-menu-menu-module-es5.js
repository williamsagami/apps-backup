(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-menu-menu-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/call/call.page.html":
    /*!**************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/call/call.page.html ***!
      \**************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesMenuCallCallPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-button (click)=\"closeCall()\">\n        <ion-icon slot=\"icon-only\" name='close' color='light' style=\"font-size: 25px;\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"cover\">\n    <img src=\"../assets/images/call.jpg\" class=\"cover-image\">\n  </div>\n\n  <ion-grid class=\"container\">\n    <ion-row style=\"height: 100%;\">\n      <ion-col class=\"ion-text-center\" size=\"12\">\n        \n        <ion-card class=\"cool-card\" style=\"border-width: 0; box-shadow: none;\">\n          <ion-item lines=\"none\">\n            <ion-title>Call</ion-title>\n          </ion-item>\n        </ion-card>\n\n        <div class=\"cool-line\" style=\"margin-bottom: 16px;\"></div>\n\n        <ion-button class=\"cool-button\" style=\"height: 120px;\" shape=\"round\" fill=\"outline\" lines=\"none\" (click)=\"phoneCall(phoneNumber)\">\n          <ion-icon slot=\"start\" name=\"call\" color=\"#fff\"></ion-icon>\n          <div class=\"cool-text\">\n            <ion-text>{{phoneNumber}}</ion-text>\n          </div>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/menu.page.html":
    /*!*********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/menu.page.html ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesMenuMenuPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n<ion-menu contentId=\"sidemenu\" type=\"overlay\">\n\n  <ion-header class=\"ion-no-border\">\n    <ion-toolbar>\n      <ion-card class=\"animate__bounceIn container-menu-image\">\n        <img class=\"menu-image\" src=\"../assets/images/Logo.png\">\n      </ion-card>\n      <div class=\"tilt\">\n        <svg data-name=\"Layer 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 1200 120\" preserveAspectRatio=\"none\">\n          <path d=\"M1200 120L0 16.48 0 0 1200 0 1200 120z\" class=\"shape-fill\"></path>\n        </svg>\n      </div>\n    </ion-toolbar>\n  </ion-header>\n  \n  <ion-content class=\"menu-container\"> \n    <ion-grid style=\"padding-top: 8px;\">\n      <ion-row class=\"ion-align-items-center\">\n        \n\n        <ion-col size=\"12\" class=\"ion-text-center\" *ngFor=\"let page of pages; index as i\">\n          <ion-menu-toggle auto-hide=\"false\">\n            <ion-button class=\"animate__animated menu-button\" shape=\"round\" fill=\"outline\" lines=\"none\" (click)=\"changeSelectedUrl(page.url)\"\n              [routerLink]=\"page.url\" \n              routerDirection=\"root\"\n              [class.mb-active]=\"selectedUrl === page.url\"\n              [class.mb-deactive]=\"selectedUrl != page.url\"\n              [class.animate__heartBeat]=\"selectedUrl === page.url\">\n              <ion-icon [name]=\"page.icon\" slot=\"end\" color=\"light\"></ion-icon>\n              <span class=\"ion-text-left\" style=\"margin-right: auto;\">{{page.text}}</span>\n            </ion-button>\n          </ion-menu-toggle>\n        </ion-col>\n\n        <ion-col size=\"12\" class=\"ion-text-center\" *ngFor=\"let red of social1\">\n          <ion-menu-toggle auto-hide=\"false\">\n            <ion-button class=\"animate__animated menu-button\" shape=\"round\" fill=\"outline\" lines=\"none\"  (click)=\"gotoAppointments()\">\n              <ion-icon [name]=\"red.icon\" slot=\"end\" color=\"light\"></ion-icon>\n              <span class=\"ion-text-left\" style=\"margin-right: auto;\">{{red.text}}</span>\n            </ion-button>\n          </ion-menu-toggle>\n        </ion-col>\n\n        <ion-col size=\"12\" class=\"ion-text-center\" *ngFor=\"let page1 of pages1; index as i\">\n          <ion-menu-toggle auto-hide=\"false\">\n            <ion-button class=\"animate__animated menu-button\" shape=\"round\" fill=\"outline\" lines=\"none\"\n              [routerLink]=\"page1.url\" \n              routerDirection=\"root\"\n              [class.mb-active]=\"selectedUrl === page1.url\"\n              [class.mb-deactive]=\"selectedUrl != page1.url\"\n              [class.animate__heartBeat]=\"selectedUrl === page1.url\">\n              <ion-icon [name]=\"page1.icon\" slot=\"end\" color=\"light\"></ion-icon>\n              <span class=\"ion-text-left\" style=\"margin-right: auto;\">{{page1.text}}</span>\n            </ion-button>\n          </ion-menu-toggle>\n        </ion-col>\n\n        <ion-col size=\"12\" class=\"ion-text-center\">\n          <div *ngIf=\"showSubscriptionButton()\">\n            <ion-menu-toggle auto-hide=\"false\">\n              <ion-button class=\"animate__animated menu-button\" shape=\"round\" (click)=\"toggleSalesNotifications()\">\n                <ion-icon name=\"star\" slot=\"end\" color=\"light\"></ion-icon>\n                <span class=\"ion-text-left\" style=\"margin-right: auto;\">Join Us</span>\n              </ion-button>\n            </ion-menu-toggle>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-menu-toggle auto-hide=\"false\">\n      <ion-button class=\"account-button\" shape=\"round\" fill=\"clear\" lines=\"none\" routerLink=\"/menu/account\">Account</ion-button>\n    </ion-menu-toggle>\n\n    <div class=\"l-lines\"></div>\n    <div class=\"r-lines\"></div>\n    <div class=\"b-lines\"></div>\n\n  </ion-content>\n\n</ion-menu>\n\n<ion-router-outlet id=\"sidemenu\"></ion-router-outlet>\n";
      /***/
    },

    /***/
    "./src/app/guards/auth.guard.ts":
    /*!**************************************!*\
      !*** ./src/app/guards/auth.guard.ts ***!
      \**************************************/

    /*! exports provided: AuthGuard */

    /***/
    function srcAppGuardsAuthGuardTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthGuard", function () {
        return AuthGuard;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/fire/auth */
      "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-auth.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");

      var AuthGuard = /*#__PURE__*/function () {
        function AuthGuard(fbAuth, router) {
          _classCallCheck(this, AuthGuard);

          this.fbAuth = fbAuth;
          this.router = router;
        }

        _createClass(AuthGuard, [{
          key: "canActivate",
          value: function canActivate(next, state) {
            var _this = this;

            return this.fbAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (auth) {
              if (auth == null) {
                _this.router.navigate(['/menu/login']);

                return false;
              } else return true;
            }));
          }
        }]);

        return AuthGuard;
      }();

      AuthGuard.ctorParameters = function () {
        return [{
          type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }];
      };

      AuthGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], AuthGuard);
      /***/
    },

    /***/
    "./src/app/guards/login.guard.ts":
    /*!***************************************!*\
      !*** ./src/app/guards/login.guard.ts ***!
      \***************************************/

    /*! exports provided: LoginGuard */

    /***/
    function srcAppGuardsLoginGuardTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginGuard", function () {
        return LoginGuard;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/fire/auth */
      "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-auth.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");

      var LoginGuard = /*#__PURE__*/function () {
        function LoginGuard(fbAuth, router) {
          _classCallCheck(this, LoginGuard);

          this.fbAuth = fbAuth;
          this.router = router;
        }

        _createClass(LoginGuard, [{
          key: "canActivate",
          value: function canActivate(next, state) {
            var _this2 = this;

            return this.fbAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (auth) {
              if (auth == null) {
                return true;
              } else {
                _this2.router.navigate(['/menu/inicio']);

                return false;
              }
            }));
          }
        }]);

        return LoginGuard;
      }();

      LoginGuard.ctorParameters = function () {
        return [{
          type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }];
      };

      LoginGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LoginGuard);
      /***/
    },

    /***/
    "./src/app/pages/menu/call/call.page.scss":
    /*!************************************************!*\
      !*** ./src/app/pages/menu/call/call.page.scss ***!
      \************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesMenuCallCallPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".cover {\n  margin: 0;\n  height: 50%;\n  border-radius: 0px;\n  border-width: 0;\n}\n\n.cover-image {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.container {\n  height: 50%;\n  background-color: var(--ion-color-primary);\n}\n\n.header {\n  background: transparent;\n}\n\n.header ion-toolbar {\n  --background-color: transparent;\n  --ion-color-base: transparent !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWVudS9jYWxsL2NhbGwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksU0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLDBDQUFBO0FBQ0o7O0FBRUE7RUFDSSx1QkFBQTtBQUNKOztBQUFJO0VBQ0ksK0JBQUE7RUFDQSx3Q0FBQTtBQUVSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbWVudS9jYWxsL2NhbGwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvdmVyIHtcbiAgICBtYXJnaW46IDA7XG4gICAgaGVpZ2h0OiA1MCU7XG4gICAgYm9yZGVyLXJhZGl1czogMHB4O1xuICAgIGJvcmRlci13aWR0aDogMDtcbn1cblxuLmNvdmVyLWltYWdlIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgb2JqZWN0LWZpdDogY292ZXI7XG59XG5cbi5jb250YWluZXIge1xuICAgIGhlaWdodDogNTAlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn1cblxuLmhlYWRlciB7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgaW9uLXRvb2xiYXIge1xuICAgICAgICAtLWJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgICAgICAtLWlvbi1jb2xvci1iYXNlOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xuICAgIH1cbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/menu/call/call.page.ts":
    /*!**********************************************!*\
      !*** ./src/app/pages/menu/call/call.page.ts ***!
      \**********************************************/

    /*! exports provided: CallPage */

    /***/
    function srcAppPagesMenuCallCallPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CallPage", function () {
        return CallPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic-native/call-number/ngx */
      "./node_modules/@ionic-native/call-number/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var CallPage = /*#__PURE__*/function () {
        function CallPage(modalController, navParams, callNumber) {
          _classCallCheck(this, CallPage);

          this.modalController = modalController;
          this.navParams = navParams;
          this.callNumber = callNumber;
          this.phoneNumber = this.navParams.get('number');
        }

        _createClass(CallPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "phoneCall",
          value: function phoneCall() {
            this.callNumber.callNumber(this.phoneNumber, false).then(function (res) {
              return console.log('Launched dialer!', res);
            })["catch"](function (err) {
              return console.log('Error launching dialer', err);
            });
          }
        }, {
          key: "closeCall",
          value: function closeCall() {
            this.modalController.dismiss();
          }
        }]);

        return CallPage;
      }();

      CallPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"]
        }, {
          type: _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_2__["CallNumber"]
        }];
      };

      CallPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-call',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./call.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/call/call.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./call.page.scss */
        "./src/app/pages/menu/call/call.page.scss"))["default"]]
      })], CallPage);
      /***/
    },

    /***/
    "./src/app/pages/menu/menu-routing.module.ts":
    /*!***************************************************!*\
      !*** ./src/app/pages/menu/menu-routing.module.ts ***!
      \***************************************************/

    /*! exports provided: MenuPageRoutingModule */

    /***/
    function srcAppPagesMenuMenuRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MenuPageRoutingModule", function () {
        return MenuPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _menu_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./menu.page */
      "./src/app/pages/menu/menu.page.ts");
      /* harmony import */


      var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../guards/auth.guard */
      "./src/app/guards/auth.guard.ts");
      /* harmony import */


      var _guards_login_guard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../guards/login.guard */
      "./src/app/guards/login.guard.ts");

      var routes = [{
        path: 'menu',
        component: _menu_page__WEBPACK_IMPORTED_MODULE_3__["MenuPage"],
        children: [{
          path: 'home',
          loadChildren: function loadChildren() {
            return Promise.all(
            /*! import() | home-home-module */
            [__webpack_require__.e("common"), __webpack_require__.e("home-home-module")]).then(__webpack_require__.bind(null,
            /*! ./home/home.module */
            "./src/app/pages/menu/home/home.module.ts")).then(function (m) {
              return m.HomePageModule;
            });
          }
        }, {
          path: 'login',
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | login-login-module */
            "login-login-module").then(__webpack_require__.bind(null,
            /*! ./login/login.module */
            "./src/app/pages/menu/login/login.module.ts")).then(function (m) {
              return m.LoginPageModule;
            });
          },
          canActivate: [_guards_login_guard__WEBPACK_IMPORTED_MODULE_5__["LoginGuard"]]
        }, {
          path: 'account',
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | account-account-module */
            "account-account-module").then(__webpack_require__.bind(null,
            /*! ./account/account.module */
            "./src/app/pages/menu/account/account.module.ts")).then(function (m) {
              return m.AccountPageModule;
            });
          },
          canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]]
        }, {
          path: 'gallery',
          loadChildren: function loadChildren() {
            return Promise.all(
            /*! import() | gallery-gallery-module */
            [__webpack_require__.e("common"), __webpack_require__.e("gallery-gallery-module")]).then(__webpack_require__.bind(null,
            /*! ./gallery/gallery.module */
            "./src/app/pages/menu/gallery/gallery.module.ts")).then(function (m) {
              return m.GalleryPageModule;
            });
          }
        }, {
          path: 'contact',
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | contact-contact-module */
            "contact-contact-module").then(__webpack_require__.bind(null,
            /*! ./contact/contact.module */
            "./src/app/pages/menu/contact/contact.module.ts")).then(function (m) {
              return m.ContactPageModule;
            });
          }
        }, {
          path: 'appointments',
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | appointments-appointments-module */
            "appointments-appointments-module").then(__webpack_require__.bind(null,
            /*! ./appointments/appointments.module */
            "./src/app/pages/menu/appointments/appointments.module.ts")).then(function (m) {
              return m.AppointmentsPageModule;
            });
          },
          canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]]
        }]
      }, {
        path: 'call',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | call-call-module */
          "call-call-module").then(__webpack_require__.bind(null,
          /*! ./call/call.module */
          "./src/app/pages/menu/call/call.module.ts")).then(function (m) {
            return m.CallPageModule;
          });
        }
      }, {
        path: '',
        redirectTo: '/menu/home',
        pathMatch: 'full'
      }];

      var MenuPageRoutingModule = function MenuPageRoutingModule() {
        _classCallCheck(this, MenuPageRoutingModule);
      };

      MenuPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], MenuPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/menu/menu.module.ts":
    /*!*******************************************!*\
      !*** ./src/app/pages/menu/menu.module.ts ***!
      \*******************************************/

    /*! exports provided: MenuPageModule */

    /***/
    function srcAppPagesMenuMenuModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MenuPageModule", function () {
        return MenuPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _menu_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./menu-routing.module */
      "./src/app/pages/menu/menu-routing.module.ts");
      /* harmony import */


      var _menu_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./menu.page */
      "./src/app/pages/menu/menu.page.ts");

      var MenuPageModule = function MenuPageModule() {
        _classCallCheck(this, MenuPageModule);
      };

      MenuPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _menu_routing_module__WEBPACK_IMPORTED_MODULE_5__["MenuPageRoutingModule"]],
        declarations: [_menu_page__WEBPACK_IMPORTED_MODULE_6__["MenuPage"]]
      })], MenuPageModule);
      /***/
    },

    /***/
    "./src/app/pages/menu/menu.page.scss":
    /*!*******************************************!*\
      !*** ./src/app/pages/menu/menu.page.scss ***!
      \*******************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesMenuMenuPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-toolbar {\n  --background: var(--ion-color-primary);\n  box-shadow: none !important;\n}\nion-toolbar ion-title {\n  padding: 50px 0 40px 0;\n  font-weight: 900;\n  color: #ffffff;\n}\n.footer {\n  --background: #703BD5;\n}\n.container-menu-image {\n  margin: 0;\n  background: transparent;\n  width: 100%;\n  box-shadow: none !important;\n}\n.menu-image {\n  background: transparent;\n  width: 100%;\n  margin-top: 20px;\n  height: 200px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.menu-container {\n  --background: linear-gradient(to top, #252122, #524f51, #858385, #bcbbbc, #f6f6f6);\n}\n.mb-active {\n  width: 220px;\n  --background: var(--ion-color-primary);\n}\n.mb-deactive {\n  width: 200px;\n}\n.menu-button {\n  --border-color: white;\n  --box-shadow: none !important;\n  height: 30px;\n  font-weight: 600;\n  font-size: 14px;\n  color: white;\n}\n.account-button {\n  --border-color: white;\n  --box-shadow: none !important;\n  font-weight: 600;\n  font-size: 14px;\n  color: white;\n  position: absolute;\n  z-index: 10;\n  left: 3%;\n  bottom: 5%;\n}\n.h-lines {\n  position: absolute;\n  z-index: 10;\n  top: 12px;\n  left: 3%;\n  width: 94%;\n  height: 60px;\n  border-width: 2px;\n  border-style: solid;\n  border-color: white;\n  border-top: transparent;\n  border-bottom: transparent;\n}\n.l-lines {\n  position: absolute;\n  z-index: 10;\n  left: 3%;\n  bottom: 3%;\n  width: 2px;\n  height: 100%;\n  border-width: 2px;\n  border-style: solid;\n  border-color: white;\n  border-right: transparent;\n  border-top: transparent;\n  border-bottom: transparent;\n}\n.r-lines {\n  position: absolute;\n  z-index: 10;\n  right: 3%;\n  bottom: 3%;\n  width: 2px;\n  height: 100%;\n  border-width: 2px;\n  border-style: solid;\n  border-color: white;\n  border-left: transparent;\n  border-top: transparent;\n  border-bottom: transparent;\n}\n.b-lines {\n  position: absolute;\n  z-index: 10;\n  left: 3%;\n  bottom: 3%;\n  width: 94%;\n  height: 2px;\n  border-width: 2px;\n  border-style: solid;\n  border-color: white;\n  border-left: transparent;\n  border-right: transparent;\n  border-top: transparent;\n}\n.tilt {\n  position: relative;\n  bottom: 0;\n  left: 0;\n  width: 100%;\n  overflow: hidden;\n  line-height: 0;\n  transform: rotate(180deg);\n}\n.tilt svg {\n  position: relative;\n  display: block;\n  width: 100%;\n  height: 60px;\n  transform: rotateY(180deg);\n}\n.tilt .shape-fill {\n  fill: #f6f6f6;\n}\n.back-circle {\n  z-index: 10;\n  width: 80px;\n  height: 80px;\n  border-radius: 50%;\n  background: var(--ion-color-primary);\n  position: absolute;\n}\n.bc-m {\n  width: 40px;\n  height: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWVudS9tZW51LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHNDQUFBO0VBQ0EsMkJBQUE7QUFDSjtBQUNJO0VBQ0ksc0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUFDUjtBQUdBO0VBQVUscUJBQUE7QUFDVjtBQUNBO0VBQ0ksU0FBQTtFQUNBLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLDJCQUFBO0FBRUo7QUFDQTtFQUNJLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7QUFFSjtBQUNBO0VBQ0ksa0ZBQUE7QUFFSjtBQUVBO0VBQ0ksWUFBQTtFQUNBLHNDQUFBO0FBQ0o7QUFFQTtFQUNJLFlBQUE7QUFDSjtBQUVBO0VBRUkscUJBQUE7RUFDQSw2QkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FBQUo7QUFHQTtFQUNJLHFCQUFBO0VBQ0EsNkJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7QUFBSjtBQUdBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsMEJBQUE7QUFBSjtBQUdBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsdUJBQUE7RUFDQSwwQkFBQTtBQUFKO0FBR0E7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esd0JBQUE7RUFDQSx1QkFBQTtFQUNBLDBCQUFBO0FBQUo7QUFHQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSx3QkFBQTtFQUNBLHlCQUFBO0VBQ0EsdUJBQUE7QUFBSjtBQUlBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtBQURKO0FBSUE7RUFDSSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDBCQUFBO0FBREo7QUFJQTtFQUNJLGFBQUE7QUFESjtBQUlBO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQ0FBQTtFQUNBLGtCQUFBO0FBREo7QUFJQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FBREoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9tZW51L21lbnUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXIge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcblxuICAgIGlvbi10aXRsZSB7XG4gICAgICAgIHBhZGRpbmc6IDUwcHggMCA0MHB4IDA7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA5MDA7XG4gICAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIH1cbn1cblxuLmZvb3RlciB7IC0tYmFja2dyb3VuZDogIzcwM0JENTsgfSAvL3ZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTsgfVxuXG4uY29udGFpbmVyLW1lbnUtaW1hZ2Uge1xuICAgIG1hcmdpbjogMDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5tZW51LWltYWdlIHtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIGhlaWdodDogMjAwcHg7XG4gICAgb2JqZWN0LWZpdDogY292ZXI7XG59XG5cbi5tZW51LWNvbnRhaW5lciB7XG4gICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gdG9wLCAjMjUyMTIyLCAjNTI0ZjUxLCAjODU4Mzg1LCAjYmNiYmJjLCAjZjZmNmY2KTtcbn1cblxuLy9NZW51IEJ1dHRvbnNcbi5tYi1hY3RpdmUge1xuICAgIHdpZHRoOiAyMjBweDtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn1cblxuLm1iLWRlYWN0aXZlIHtcbiAgICB3aWR0aDogMjAwcHg7XG59XG5cbi5tZW51LWJ1dHRvbiB7XG4gICAgLy8tLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAtLWJvcmRlci1jb2xvcjogd2hpdGU7XG4gICAgLS1ib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiAzMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmFjY291bnQtYnV0dG9uIHtcbiAgICAtLWJvcmRlci1jb2xvcjogd2hpdGU7XG4gICAgLS1ib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB6LWluZGV4OiAxMDtcbiAgICBsZWZ0OiAzJTtcbiAgICBib3R0b206IDUlO1xufVxuXG4uaC1saW5lcyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHotaW5kZXg6IDEwO1xuICAgIHRvcDogMTJweDtcbiAgICBsZWZ0OiAzJTtcbiAgICB3aWR0aDogOTQlO1xuICAgIGhlaWdodDogNjBweDtcbiAgICBib3JkZXItd2lkdGg6IDJweDtcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICAgIGJvcmRlci1jb2xvcjogd2hpdGU7XG4gICAgYm9yZGVyLXRvcDogdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyLWJvdHRvbTogdHJhbnNwYXJlbnQ7XG59XG5cbi5sLWxpbmVzIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgei1pbmRleDogMTA7XG4gICAgbGVmdDogMyU7XG4gICAgYm90dG9tOiAzJTtcbiAgICB3aWR0aDogMnB4O1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBib3JkZXItd2lkdGg6IDJweDtcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICAgIGJvcmRlci1jb2xvcjogd2hpdGU7XG4gICAgYm9yZGVyLXJpZ2h0OiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXItdG9wOiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXItYm90dG9tOiB0cmFuc3BhcmVudDtcbn1cblxuLnItbGluZXMge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB6LWluZGV4OiAxMDtcbiAgICByaWdodDogMyU7XG4gICAgYm90dG9tOiAzJTtcbiAgICB3aWR0aDogMnB4O1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBib3JkZXItd2lkdGg6IDJweDtcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICAgIGJvcmRlci1jb2xvcjogd2hpdGU7XG4gICAgYm9yZGVyLWxlZnQ6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlci10b3A6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlci1ib3R0b206IHRyYW5zcGFyZW50O1xufVxuXG4uYi1saW5lcyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHotaW5kZXg6IDEwO1xuICAgIGxlZnQ6IDMlO1xuICAgIGJvdHRvbTogMyU7XG4gICAgd2lkdGg6IDk0JTtcbiAgICBoZWlnaHQ6IDJweDtcbiAgICBib3JkZXItd2lkdGg6IDJweDtcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICAgIGJvcmRlci1jb2xvcjogd2hpdGU7XG4gICAgYm9yZGVyLWxlZnQ6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlci1yaWdodDogdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyLXRvcDogdHJhbnNwYXJlbnQ7XG59XG5cbi8vU2hhcGVzXG4udGlsdCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGJvdHRvbTogMDtcbiAgICBsZWZ0OiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgbGluZS1oZWlnaHQ6IDA7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcbn1cblxuLnRpbHQgc3ZnIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIHRyYW5zZm9ybTogcm90YXRlWSgxODBkZWcpO1xufVxuXG4udGlsdCAuc2hhcGUtZmlsbCB7XG4gICAgZmlsbDogI2Y2ZjZmNjtcbn1cblxuLmJhY2stY2lyY2xlIHtcbiAgICB6LWluZGV4OiAxMDtcbiAgICB3aWR0aDogODBweDtcbiAgICBoZWlnaHQ6IDgwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG5cbi5iYy1tIHtcbiAgICB3aWR0aDogNDBweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/pages/menu/menu.page.ts":
    /*!*****************************************!*\
      !*** ./src/app/pages/menu/menu.page.ts ***!
      \*****************************************/

    /*! exports provided: MenuPage */

    /***/
    function srcAppPagesMenuMenuPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MenuPage", function () {
        return MenuPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/data.service */
      "./src/app/services/data.service.ts");
      /* harmony import */


      var src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/firebase.service */
      "./src/app/services/firebase.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var MenuPage = /*#__PURE__*/function () {
        function MenuPage(router, data, firebaseService, toastController, platform, modalController, menuController) {
          _classCallCheck(this, MenuPage);

          this.router = router;
          this.data = data;
          this.firebaseService = firebaseService;
          this.toastController = toastController;
          this.platform = platform;
          this.modalController = modalController;
          this.menuController = menuController;
          this.userReady = false;
          this.selectedUrl = '/menu/inicio';
          this.pages = [{
            text: "Home",
            url: '/menu/home',
            icon: "home"
          }];
          this.social1 = [{
            text: "Appointments",
            url: 'https://www.entouragebarbershop.com/',
            icon: "List"
          }];
          this.pages1 = [{
            text: "Gallery",
            url: '/menu/gallery',
            icon: "images"
          }, {
            text: "Contact",
            url: '/menu/contact',
            icon: "person"
          }];
          this.d = data.appData.data;
        }

        _createClass(MenuPage, [{
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            var _this3 = this;

            /*this.router.events.subscribe((event: RouterEvent) => {
              this.selectedUrl = event.url ? event.url : this.selectedUrl;
            }); */
            this.exit = this.platform.backButton.subscribe(function () {
              _this3.modalController.getTop().then(function (m) {
                if (!m) {
                  _this3.menuController.isOpen().then(function (a) {
                    if (!a) {
                      if (_this3.selectedUrl === '/menu/inicio') navigator['app'].exitApp();else {
                        _this3.router.navigate(['/menu/inicio']);

                        _this3.selectedUrl = '/menu/inicio';
                      }
                    }
                  });
                }
              });
            });
          }
        }, {
          key: "changeSelectedUrl",
          value: function changeSelectedUrl(url) {
            this.selectedUrl = url;
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this4 = this;

            this.firebaseService.userSetup().then(function (userDataExist) {
              _this4.userReady = userDataExist;

              _this4.firebaseService.getAppData().then(function (appData) {
                if (appData) _this4.d = appData.data;
              });
            });
          }
        }, {
          key: "gotoAppointments",
          value: function gotoAppointments() {
            if (this.d.appointments) this.router.navigate(["/menu/appointments"]);else this.data.openLink(this.social1[0].url);
          }
        }, {
          key: "showInfo",
          value: function showInfo(info) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastController.create({
                        message: info,
                        duration: 6000,
                        color: 'secondary'
                      });

                    case 2:
                      toast = _context.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "showSubscriptionButton",
          value: function showSubscriptionButton() {
            if (this.userReady) {
              if (this.firebaseService.userData) {
                if (!this.firebaseService.userData.admin) return !this.firebaseService.userData.notifications;else return false;
              }
            } else return false;
          }
        }, {
          key: "toggleSalesNotifications",
          value: function toggleSalesNotifications() {
            var _this5 = this;

            if (this.userReady) {
              if (!this.firebaseService.userData.notifications) {
                var userData = this.firebaseService.userData;
                userData.notifications = true;
                this.firebaseService.updateUser(userData).then(function (updateUser) {
                  _this5.firebaseService.fcm.subscribe(_this5.firebaseService.getDataBaseName() + "-sales").then(function (showInfo) {
                    _this5.showInfo("Notifications On.");
                  });
                });
              }
            } else {
              this.router.navigate(['/menu/login']);
            }
          }
        }]);

        return MenuPage;
      }();

      MenuPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"]
        }, {
          type: src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_4__["FirebaseService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"]
        }];
      };

      MenuPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-menu',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./menu.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/menu.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./menu.page.scss */
        "./src/app/pages/menu/menu.page.scss"))["default"]]
      })], MenuPage);
      /***/
    },

    /***/
    "./src/app/services/data.service.ts":
    /*!******************************************!*\
      !*** ./src/app/services/data.service.ts ***!
      \******************************************/

    /*! exports provided: DataService */

    /***/
    function srcAppServicesDataServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DataService", function () {
        return DataService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic-native/in-app-browser/ngx */
      "./node_modules/@ionic-native/in-app-browser/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _pages_menu_call_call_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../pages/menu/call/call.page */
      "./src/app/pages/menu/call/call.page.ts");
      /* harmony import */


      var _firebase_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./firebase.service */
      "./src/app/services/firebase.service.ts");

      var DataService = /*#__PURE__*/function () {
        function DataService(firebaseService, modalController, iab) {
          _classCallCheck(this, DataService);

          // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
          this.firebaseService = firebaseService;
          this.modalController = modalController;
          this.iab = iab; //Datos de Aplicacion--------------------------------------------------------------
          //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 

          this.appData = {
            //Datos de Contacto
            data: {
              appTitle: "Entourage Barbershop",
              appointments: true,
              homeDescription: "Entourage Barbershop is a Barber Shop located in West Hollywood, CA. <br>\n        We specialize in Hair Cuts, Men's Hair Cuts, Bread Trimming, Fades, lineups, Hot Towel Shave, Nose Wax, Straight Razor Shave & more. <br>\n        Here at Entourage Barbershop, our success is due to our dedication to delivering great barber service & exceptional customer service to all of our customers. <br>\n        We have only the best Barbers & Hair Stylists in the business. <br>\n        Call to schedule an appointment today!",
              homeSlider: ["https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F1s_512x512.jpg?alt=media&token=5fcd4876-3b7e-4dc0-a6e8-11a68578338b", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F2s_512x512.jpg?alt=media&token=cdfce1f5-d3dd-4609-a177-66dcb570fc6b", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F3s_512x512.jpg?alt=media&token=8a1b7aec-deaa-4b00-b6b4-1bee20e45bcb", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F4s_512x512.jpg?alt=media&token=71600206-0846-426f-8478-8fbe4309039c"],
              barbersDescription: "We have the best barbers in Hollywood, with excellence in their work area, their professionalism seems like magic. <br>\n       Meet them now!!",
              homeSlider2: ["https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F26_512x512.png?alt=media&token=f95ba26e-b1e4-4e70-a4a4-bf5ff7f9d984", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F27_512x512.jpeg?alt=media&token=151ba6fa-b786-4977-aad6-c9af248c25fc", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F28_512x512.jpeg?alt=media&token=bf607257-2276-4268-810c-dd94c509d79c", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F29_512x512.jpeg?alt=media&token=920b1407-86b5-4721-91a4-be683c753f07"],
              homeServices: [{
                name: "Men's Haircut",
                price: "$45.00"
              }, {
                name: "Haircut + Shave",
                price: "$60"
              }, {
                name: "Kids haircut",
                price: "$35"
              }, {
                name: "Long haircut",
                price: "$50"
              }, {
                name: "Hot towel shave",
                price: "$30"
              }, {
                name: "Design",
                price: "$50"
              }, {
                name: "Line-up",
                price: "$25"
              }, {
                name: "Wash",
                price: "$15"
              }, {
                name: "Wash + Style",
                price: "$25"
              }, {
                name: "Hair color",
                price: "$100"
              }, {
                name: "Beard color",
                price: "$40"
              }, {
                name: "Relaxer",
                price: "$40"
              }, {
                name: "Eyebrow (Straight razor)",
                price: "$15"
              }, {
                name: "Eyebrow Wax",
                price: "$25"
              }, {
                name: "Nose wax",
                price: "$20"
              }, {
                name: "Black Charcoal Mask",
                price: "$25"
              }, {
                name: "Recharge Facial (60 min)",
                price: "$110"
              }, {
                name: "Detox Facial (60 min)",
                price: "$120"
              }, {
                name: "Age Defying Facial (60 min)",
                price: "$120"
              }, {
                name: "CBD Massage (90 min)",
                price: "$50"
              }, {
                name: "CBD N Chill Treatment (80 min)",
                price: "$130"
              }, {
                name: "Back Facial (60 min)",
                price: "$120"
              }, {
                name: "LED (30 min ADD ON)",
                price: "$70"
              }, {
                name: "Swedish Massage (60 min)",
                price: "$110"
              }, {
                name: "Swedish Massage (90 min)",
                price: "$130"
              }, {
                name: "Deep Tissue Massage (60 min)",
                price: "$130"
              }, {
                name: "Deep Tissue Massage (90 min)",
                price: "$150"
              }, {
                name: "Sports Massage (60 min)",
                price: "120"
              }, {
                name: "Sports Massage (90 min)",
                price: "$140"
              }, {
                name: "CBD Massage (60 min)",
                price: "$130"
              }],
              homeSchedules: [{
                day: "Monday",
                hour: "09:00 AM - 7:00 PM"
              }, {
                day: "Tuesday",
                hour: "09:00 AM - 7:00 PM"
              }, {
                day: "Wednesday",
                hour: "09:00 AM - 7:00 PM"
              }, {
                day: "Thursday",
                hour: "09:00 AM - 7:00 PM"
              }, {
                day: "Friday",
                hour: "09:00 AM - 7:00 PM"
              }, {
                day: "Saturday",
                hour: "09:00 AM - 7:00 PM"
              }, {
                day: "Sunday",
                hour: "10:00 AM - 4:00 PM"
              }],
              homeVideos: ["https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F1.mov?alt=media&token=fd8f8eb9-f4d4-4165-a25f-0da1d211e21d", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F2.mov?alt=media&token=8431bcd5-69e0-467f-a171-18dc0db66ff7", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F3.mp4?alt=media&token=201860b0-3f7f-4d77-bb83-3d27450a498c"],
              f: [],
              phoneNumbers: [{
                text: "Entourage Barbershop",
                number: "310 855 8044"
              }],
              gallery: ["https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F22_512x512.png?alt=media&token=2894dafb-8bd0-4dd9-af27-fdd4d0ec8a59", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F23_512x512.jpeg?alt=media&token=9fe82f30-ecfc-4946-a951-3acd48f475eb", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F24_512x512.jpeg?alt=media&token=663729ff-6394-4ead-837c-20b89629e1b0", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F25_512x512.jpeg?alt=media&token=17a95c4b-8b6f-489a-b2cb-a3e2f8307bd4", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fhome%2F4s_512x512.jpg?alt=media&token=71600206-0846-426f-8478-8fbe4309039c", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F9_512x512.jpg?alt=media&token=8d51b5f2-6d76-4a65-b6b2-7951d0a61d99", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F10_512x512.jpg?alt=media&token=1529b650-34cc-4b4e-a17c-11d4009c6768", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F11_512x512.jpg?alt=media&token=cf0da7c5-e251-4a85-ba92-16adaee0c711", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F12_512x512.jpg?alt=media&token=c91d7da4-a9c1-4f0c-90a2-b52f3c1bb0c5", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F13_512x512.jpg?alt=media&token=cc424f6c-8bf6-4498-a8ea-4212a3e2ec65", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F14_512x512.jpg?alt=media&token=2af654c6-e250-4209-82e6-5bdbe98c75b8", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F15_512x512.jpg?alt=media&token=0670307b-7441-41df-8673-e6c5fdb4cccc", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F16_512x512.jpg?alt=media&token=523ffb27-7ed6-4d2d-ae9d-ccb2003f7633", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F17_512x512.jpg?alt=media&token=b1219124-11b4-4bbe-bf44-dd0989566e04", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F18_512x512.jpg?alt=media&token=585878fa-f2cb-4bb8-9184-89ccaa35cb3a", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F19_512x512.jpg?alt=media&token=28be3662-150c-4d15-8443-16921d9f3170", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F1_512x512.jpg?alt=media&token=b54844d2-4dad-4d59-aac3-cb65d25b4300", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F20_512x512.jpg?alt=media&token=086f89b3-2632-4cac-99a5-fdeddea438bc", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F21_512x512.jpg?alt=media&token=776fa539-eee8-460b-9969-4a48303266e4", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F2_512x512.jpg?alt=media&token=dd22a4ca-d968-48d1-b8c7-16089406347c", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F3_512x512.jpg?alt=media&token=bf03c893-f583-4b5d-94fa-543e403141eb", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F4_512x512.jpg?alt=media&token=e18870e4-8e14-4305-894d-3c69291871e5", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F5_512x512.jpg?alt=media&token=1ef24d0a-3144-435f-8dd9-e37f4a50dd04", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F6_512x512.jpg?alt=media&token=6eb7c997-793e-4719-84a3-a20ee6330ad1", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F7_512x512.jpg?alt=media&token=ea538502-5aeb-4fd8-9cb9-ad8837f77748", "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FEntourage%20Barbershop%2Fgallery%2F8_512x512.jpg?alt=media&token=aa1d01db-4720-4a3b-9b07-1f8eb62dc8c3"],
              socialNetworks: [{
                text: "entouragebarbershop",
                link: "https://www.instagram.com/entouragebarbershop/",
                icon: "logo-instagram"
              }, {
                text: "Entourage Barbershop",
                link: "https://www.facebook.com/Entourage-Barbershop-116870816818062/",
                icon: "logo-Facebook"
              }, {
                text: "Entourage Barbershop",
                link: "https://www.yelp.com/biz/entourage-barbershop-west-hollywood",
                icon: "logo-Yahoo"
              }],
              address: "8601 Sunset Blvd, West Hollywood, CA 90069, United States",
              addressRoute: "8601 Sunset Blvd, West Hollywood, CA 90069, United States",
              mapSource: "https://maps.google.com/maps?q=Entourage%20Barbershop&t=&z=15&ie=UTF8&iwloc=&output=embed"
            }
          }; //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
        }

        _createClass(DataService, [{
          key: "phoneCall",
          value: function phoneCall(phoneNumber) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var modal;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.modalController.create({
                        component: _pages_menu_call_call_page__WEBPACK_IMPORTED_MODULE_4__["CallPage"],
                        componentProps: {
                          number: phoneNumber
                        }
                      });

                    case 2:
                      modal = _context2.sent;
                      _context2.next = 5;
                      return modal.present();

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "openLink",
          value: function openLink(link) {
            this.iab.create(link);
          }
        }, {
          key: "getAppData",
          value: function getAppData() {
            var _this6 = this;

            return new Promise(function (resolve, reject) {
              _this6.firebaseService.getAppData().then(function (appData) {
                if (appData) {
                  _this6.appData = appData; //console.log(appData);

                  resolve(appData);
                } else resolve(null);
              });
            });
          }
        }]);

        return DataService;
      }();

      DataService.ctorParameters = function () {
        return [{
          type: _firebase_service__WEBPACK_IMPORTED_MODULE_5__["FirebaseService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }, {
          type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__["InAppBrowser"]
        }];
      };

      DataService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], DataService);
      /***/
    },

    /***/
    "./src/app/services/firebase.service.ts":
    /*!**********************************************!*\
      !*** ./src/app/services/firebase.service.ts ***!
      \**********************************************/

    /*! exports provided: state, language, FirebaseService */

    /***/
    function srcAppServicesFirebaseServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "state", function () {
        return state;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "language", function () {
        return language;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FirebaseService", function () {
        return FirebaseService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/fire/auth */
      "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-auth.js");
      /* harmony import */


      var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/fire/firestore */
      "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
      /* harmony import */


      var _angular_fire_functions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/fire/functions */
      "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-functions.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _ionic_native_firebase_x_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/firebase-x/ngx */
      "./node_modules/@ionic-native/firebase-x/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var state;

      (function (state) {
        state[state["pending"] = 0] = "pending";
        state[state["accepted"] = 1] = "accepted";
        state[state["rejected"] = 2] = "rejected";
      })(state || (state = {}));

      var language;

      (function (language) {
        language[language["ES"] = 0] = "ES";
        language[language["EN"] = 1] = "EN";
      })(language || (language = {}));

      var FirebaseService = /*#__PURE__*/function () {
        function FirebaseService(fbAuth, db, functions, fcm, platform) {
          var _this7 = this;

          _classCallCheck(this, FirebaseService);

          this.fbAuth = fbAuth;
          this.db = db;
          this.functions = functions;
          this.fcm = fcm;
          this.platform = platform;
          this.dbName = "entouragebarbershop";
          this.dbRoute = "appointments/entouragebarbershop/app";
          this.userReady = false;
          this.defaultLanguage = language.EN;
          this.getAppData().then(function (appData) {
            _this7.appData = appData;
          }); // this.createAppointmentsDataBase()
        } //SETUP DE BASE DE DATOS (USAR AL CREAR)
        // createAppointmentsDataBase() {
        //   const functionRef = this.functions.httpsCallable('createAppointmentsDataBase');
        //   return functionRef({dbRoute: this.dbRoute, dbName: this.dbName}).toPromise();
        // }


        _createClass(FirebaseService, [{
          key: "getDataBaseName",
          value: function getDataBaseName() {
            return this.dbName;
          } //SETUP APP --------------------------------------------------------------------

        }, {
          key: "userSetup",
          value: function userSetup() {
            var _this8 = this;

            return new Promise(function (resolve, rejected) {
              if (_this8.userReady) {
                var userDataExist = _this8.userData ? true : false;
                resolve(userDataExist);
              } else {
                if (_this8.authRef) _this8.authRef.unsubscribe(); //Limpia las referencias de suscripcion

                if (_this8.userRef) _this8.userRef.unsubscribe();
                _this8.authRef = _this8.fbAuth.authState.subscribe(function (user) {
                  _this8.userAuthState = user; //console.log(user);

                  if (user) {
                    _this8.userRef = _this8.getUserRealTime(user.uid).subscribe(function (userData) {
                      _this8.userData = userData; //console.log(userData);
                      //Si existe el usuario devuelve el true

                      if (userData) {
                        _this8.userReady = true;
                        resolve(true);
                      } //Si no existe se limpia la referencia de suscripcion y se crea un usuario nuevo, para despues tomar los datos en real time
                      else {
                          _this8.userRef.unsubscribe();

                          _this8.createUser(user.uid, user.email).then(function (createdUser) {
                            _this8.userData = createdUser;
                            _this8.userRef = _this8.getUserRealTime(user.uid).subscribe(function (userData) {
                              _this8.userData = userData;
                              console.log(userData);
                              _this8.userReady = true;
                              resolve(true);
                            });
                          });
                        }
                    });
                  } //Si no esta logeado
                  else {
                      _this8.userReady = true;
                      _this8.userData = null;
                      resolve(false);
                    }
                });
              }
            });
          }
        }, {
          key: "isUserReady",
          value: function isUserReady() {
            return this.userReady;
          } //AUTENTICACION----------------------------------------------------------

        }, {
          key: "login",
          value: function login(email, password) {
            var _this9 = this;

            return new Promise(function (resolve, rejected) {
              _this9.fbAuth.signInWithEmailAndPassword(email, password).then(function (res) {
                _this9.user = res.user;
                _this9.userReady = false;
                resolve(res);
              })["catch"](function (err) {
                return rejected(err);
              });
            }).then(function (setup) {
              _this9.userSetup().then(function (saveToken) {
                console.log(_this9.user.uid);

                _this9.saveToken(_this9.user.uid);
              });
            });
          }
        }, {
          key: "register",
          value: function register(email, password) {
            var _this10 = this;

            return new Promise(function (resolve, reject) {
              _this10.fbAuth.createUserWithEmailAndPassword(email, password).then(function (res) {
                _this10.user = res.user;
                resolve(res);
              })["catch"](function (err) {
                return reject(err);
              });
            }).then(function (setup) {
              _this10.createUser(_this10.user.uid, email).then(function (userSetup) {
                _this10.userReady = false;

                _this10.userSetup();
              });
            });
          }
        }, {
          key: "logout",
          value: function logout() {
            this.userReady = false;
            this.userAuthState = null;
            this.user = null;
            return this.fbAuth.signOut();
          } //GUARDAR TOKENS-------------------------------------------------------

        }, {
          key: "updateToken",
          value: function updateToken(userId, token) {
            return this.db.collection(this.dbRoute + '/userData/users').doc(userId).update({
              token: token
            });
          }
        }, {
          key: "saveToken",
          value: function saveToken(userId) {
            var _this11 = this;

            if (this.platform.is('android')) {
              return this.fcm.getToken().then(function (token) {
                return _this11.updateToken(userId, token);
              });
            } else return null;
          } //TRADUCCION----------------------------------------------------------

        }, {
          key: "updateAppData",
          value: function updateAppData(appData) {
            var functionRef = this.functions.httpsCallable('updateAppData');
            return functionRef({
              dbRoute: this.dbRoute,
              appData: appData
            }).toPromise();
          }
        }, {
          key: "getAppData",
          value: function getAppData() {
            var _this12 = this;

            return new Promise(function (resolve, reject) {
              if (_this12.appData) {
                resolve(_this12.appData);
              } else _this12.db.collection(_this12.dbRoute).doc('appData').ref.get().then(function (res) {
                resolve(res.data());
              })["catch"](function (err) {
                return reject(err);
              });
            });
          } //OPCIONES DE APLICACION-----------------------------------------

        }, {
          key: "updateAppConfiguration",
          value: function updateAppConfiguration(appConfig) {
            return this.db.collection(this.dbRoute).doc('appConfiguration').update({
              dayRangeLimit: appConfig.dayRangeLimit,
              maxUserRequestNumber: appConfig.maxUserRequestNumber
            });
          }
        }, {
          key: "getAppConfiguration",
          value: function getAppConfiguration() {
            var _this13 = this;

            return new Promise(function (resolve, reject) {
              _this13.db.collection(_this13.dbRoute).doc('appConfiguration').ref.get().then(function (res) {
                resolve(res.data());
              })["catch"](function (err) {
                return reject(err);
              });
            });
          } //BLOG----------------------------------------------------------

        }, {
          key: "getPostDataRealTime",
          value: function getPostDataRealTime() {
            return this.db.collection(this.dbRoute).doc('adminPosts').snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (postData) {
              return postData.payload.data();
            }));
          }
        }, {
          key: "getPostsRealTime",
          value: function getPostsRealTime() {
            return this.db.collection(this.dbRoute + '/adminPosts/posts', function (ref) {
              return ref.orderBy('postNumber', 'desc');
            }).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (posts) {
              return posts.map(function (post) {
                var postData = post.payload.doc.data();
                postData.id = post.payload.doc.id;
                return postData;
              });
            }));
          }
        }, {
          key: "createPost",
          value: function createPost(post, postsData) {
            var _this14 = this;

            return this.db.collection(this.dbRoute + '/adminPosts/posts').doc('p_' + post.postNumber).set(post).then(function (postNumber) {
              var pNumber = postsData.postNumber + 1;
              var pCount = postsData.postCount + 1;

              _this14.db.collection(_this14.dbRoute).doc('adminPosts').update({
                postNumber: pNumber,
                postCount: pCount
              });
            });
          }
        }, {
          key: "updatePost",
          value: function updatePost(post) {
            return this.db.collection(this.dbRoute + '/adminPosts/posts').doc(post.id).update(post);
          }
        }, {
          key: "deletePost",
          value: function deletePost(post, postsData) {
            var _this15 = this;

            return this.db.collection(this.dbRoute + '/adminPosts/posts').doc(post.id)["delete"]().then(function (postNumber) {
              var pNumber = postsData.postNumber - 1;

              _this15.db.collection(_this15.dbRoute).doc('adminPosts').update({
                postNumber: pNumber
              });
            });
          } //USUARIOS----------------------------------------------------------

        }, {
          key: "getUser",
          value: function getUser(user_id) {
            var _this16 = this;

            return new Promise(function (resolve, reject) {
              _this16.db.collection(_this16.dbRoute + '/userData/users').doc(user_id).ref.get().then(function (res) {
                resolve(res.data());
              })["catch"](function (err) {
                return reject(err);
              });
            });
          }
        }, {
          key: "getUserRealTime",
          value: function getUserRealTime(user_id) {
            return this.db.collection(this.dbRoute + '/userData/users').doc(user_id).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (user) {
              return user.payload.data();
            }));
          }
        }, {
          key: "updateUser",
          value: function updateUser(user) {
            return this.db.collection(this.dbRoute + '/userData/users').doc(user.id).update(user);
          } //CITAS----------------------------------------------------------

        }, {
          key: "getRequests",
          value: function getRequests() {
            var _this17 = this;

            return new Promise(function (resolve, reject) {
              _this17.db.collection(_this17.dbRoute + '/userRequests/requests', function (ref) {
                return ref.orderBy('creationDate');
              }).ref.get().then(function (res) {
                var requests = res.docs.map(function (request) {
                  var requestData = request.data();
                  requestData.date = requestData.date.toDate();
                  requestData.creationDate = requestData.creationDate.toDate();
                  return requestData;
                });
                resolve(requests);
              })["catch"](function (err) {
                return reject(err);
              });
            });
          }
        }, {
          key: "getRequestsRealTime",
          value: function getRequestsRealTime() {
            return this.db.collection(this.dbRoute + '/userRequests/requests', function (ref) {
              return ref.orderBy('creationDate');
            }).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (requests) {
              return requests.map(function (request) {
                var requestData = request.payload.doc.data();
                requestData.date = requestData.date.toDate();
                requestData.creationDate = requestData.creationDate.toDate();
                return requestData;
              });
            }));
          }
        }, {
          key: "getUserRequests",
          value: function getUserRequests(user_id) {
            var _this18 = this;

            return new Promise(function (resolve, reject) {
              _this18.db.collection(_this18.dbRoute + '/userRequests/requests', function (ref) {
                return ref.where("user.id", "==", user_id);
              }).ref.get().then(function (res) {
                var requests = res.docs.map(function (request) {
                  var requestData = request.data();
                  requestData.date = requestData.date.toDate();
                  requestData.creationDate = requestData.creationDate.toDate();
                  return requestData;
                });
                resolve(requests);
              })["catch"](function (err) {
                return reject(err);
              });
            });
          }
        }, {
          key: "getUserRequestsRealTime",
          value: function getUserRequestsRealTime(user_id) {
            return this.db.collection(this.dbRoute + '/userRequests/requests', function (ref) {
              return ref.where("user.id", "==", user_id);
            }).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (requests) {
              return requests.map(function (request) {
                var requestData = request.payload.doc.data();
                requestData.date = requestData.date.toDate();
                requestData.creationDate = requestData.creationDate.toDate();
                return requestData;
              });
            }));
          }
        }, {
          key: "acceptRequest",
          value: function acceptRequest(request) {
            request.state = state.accepted;
            this.updateRequest(request);
          }
        }, {
          key: "rejectRequest",
          value: function rejectRequest(request) {
            request.state = state.rejected;
            this.updateRequest(request);
          }
        }, {
          key: "updateRequest",
          value: function updateRequest(request) {
            var arId = request.user.id + request.id;
            return this.db.collection(this.dbRoute + '/userRequests/requests').doc(arId).update(request);
          }
        }, {
          key: "createRequest",
          value: function createRequest(request, userData) {
            var _this19 = this;

            var arId = request.user.id + request.id;
            var rCount = userData.requestCount + 1;
            var rNumber = userData.requestNumber + 1;
            return this.db.collection(this.dbRoute + '/userRequests/requests').doc(arId).set(request).then(function (requestCount) {
              _this19.db.collection(_this19.dbRoute + '/userData/users').doc(request.user.id).update({
                requestCount: rCount,
                requestNumber: rNumber
              });
            });
          }
        }, {
          key: "deleteRequest",
          value: function deleteRequest(request, validRequest) {
            var _this20 = this;

            var arId = request.user.id + request.id;
            return this.db.collection(this.dbRoute + '/userRequests/requests').doc(arId)["delete"]().then(function (requestCount) {
              if (validRequest) {
                _this20.getUser(request.user.id).then(function (tempUser) {
                  var tu = tempUser;
                  var cancel = tu.cancel + 1;

                  if (request.state === state.accepted) {
                    return _this20.db.collection(_this20.dbRoute + '/userData/users').doc(tu.id).update({
                      cancel: cancel
                    });
                  }
                });
              }
            });
          }
        }, {
          key: "createUser",
          value: function createUser(user_id, email) {
            var _this21 = this;

            var user = {};
            user.id = user_id;
            user.username = "Guest";
            user.admin = false;
            user.phone = "Undefined";
            user.email = email;
            user.rating = 0;
            user.cancel = 0;
            user.level = 1;
            user.requestCount = 0;
            user.requestNumber = 0;
            user.allowRequests = true;
            var coolUser = user;
            coolUser.language = language.EN;
            coolUser.notifications = false; //SE NECESITA REVISION AQUI

            return new Promise(function (resolve, reject) {
              _this21.fcm.getToken().then(function (token) {
                coolUser.token = token;
                return _this21.db.collection(_this21.dbRoute + '/userData/users').doc(user.id).set(coolUser).then(function (createdUser) {
                  resolve(coolUser);
                });
              })["catch"](function (err) {
                coolUser.token = '';
                return _this21.db.collection(_this21.dbRoute + '/userData/users').doc(user.id).set(coolUser).then(function (createdUser) {
                  reject(err);
                });
              });
            });
          }
        }]);

        return FirebaseService;
      }();

      FirebaseService.ctorParameters = function () {
        return [{
          type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"]
        }, {
          type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"]
        }, {
          type: _angular_fire_functions__WEBPACK_IMPORTED_MODULE_4__["AngularFireFunctions"]
        }, {
          type: _ionic_native_firebase_x_ngx__WEBPACK_IMPORTED_MODULE_6__["FirebaseX"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"]
        }];
      };

      FirebaseService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], FirebaseService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-menu-menu-module-es5.js.map