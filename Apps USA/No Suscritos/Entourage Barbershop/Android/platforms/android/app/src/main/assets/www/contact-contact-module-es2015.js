(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contact-contact-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/contact/contact.page.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/contact/contact.page.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- HEADER -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: #fff;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>CONTACT</ion-title>\n  </ion-toolbar>\n  <div class=\"cool-line\"></div>\n</ion-header>\n\n<ion-content class=\"cool-content\">\n\n  <div class=\"flex-container\">\n  <div class=\"logo\">\n    <img src=\"../assets/images/Logo.png\">\n  </div>\n  <br>\n  <br>\n  <div>\n    <ion-text class=\"cool-text\"> {{d.address}} </ion-text>\n  </div>\n  <div *ngFor=\"let phoneNumber of d.phoneNumbers\">\n    <br>\n    <ion-icon name=\"call\" color=\"light\"></ion-icon>\n    <ion-text class=\"cool-text\"> {{phoneNumber.number}} </ion-text>\n  </div>\n  </div>\n\n  <!-- SOCIAL NETWORKS -->\n  <ion-fab vertical=\"bottom\" horizontal=\"center\" slot=\"fixed\">\n    <ion-fab-button color=\"secondary\">\n        <ion-icon name=\"share\" color=\"primary\"></ion-icon>\n    </ion-fab-button>\n\n    <ion-fab-list side=\"top\">\n        <ion-fab-button color=\"secondary\" *ngFor=\"let sn of d.socialNetworks\" (click)=\"data.openLink(sn.link)\">\n            <ion-icon [name]=\"sn.icon\" color=\"primary\"></ion-icon>\n        </ion-fab-button>\n    </ion-fab-list>\n</ion-fab>\n\n<div style=\"position: fixed; bottom: 12px; left: 25vw; font-size: 32px;\">\n    <ion-icon name=\"call\" color=\"light\" (click)=\"data.phoneCall(d.phoneNumbers[0].number)\"></ion-icon>\n</div>\n\n<div style=\"position: fixed; bottom: 12px; right: 25vw; font-size: 32px;\">\n    <ion-icon name=\"map\" color=\"light\" (click)=\"openRoute()\"> </ion-icon>\n</div>\n\n <!-- TELEFONOS\n <ion-card class=\"animate__animated animate__fadeInLeft cool-card\">\n  <ion-item lines=\"none\">\n    <ion-icon slot=\"end\" name=\"call\" color=\"#fff\"></ion-icon>\n    <ion-title>Phones</ion-title>\n  </ion-item>\n\n  <div class=\"cool-line\"></div>\n\n  <ion-grid>\n    <ion-row class=\"ion-align-items-center\">\n       NUMEROS \n      <ion-col class=\"ion-text-center\" size=\"12\" *ngFor=\"let phoneNumber of d.phoneNumbers\">\n        <ion-button class=\"cool-button\" shape=\"round\" fill=\"outline\" lines=\"none\" (click)=\"data.phoneCall(phoneNumber.number)\">\n          <div class=\"cool-text\">\n            <ion-text>{{phoneNumber.number}}</ion-text>\n          </div>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-card>\n\n   DIRECCION \n  <ion-card class=\"animate__animated animate__fadeInRight cool-card\">\n    <ion-item>\n      <ion-icon slot=\"end\" name=\"locate\" color=\"#fff\"></ion-icon>\n      <ion-title>Address</ion-title>\n    </ion-item>\n    <div class=\"cool-line\"></div>\n    <ion-item lines=\"none\">\n      <ion-card-content>\n        {{d.address}}\n      </ion-card-content>\n    </ion-item>\n  </ion-card>\n\n   MAPA \n  <ion-card  class=\"animate__animated animate__fadeInLeft cool-card\">\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"end\" name=\"map\" color=\"#fff\"></ion-icon>\n      <ion-title>Location</ion-title>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n    \n    <iframe style=\"width: 100%; height: 300px; background-color: #ffffff;\"\n      [src]=\"getMapUrl()\"\n      frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\"></iframe>\n  </ion-card >\n   Embed code\n  <script type=\"text/javascript\">(new Image).src = \"//googlemapsembed.net/get?r\" + escape(document.referrer);</script>\n  <script type=\"text/javascript\" src=\"https://googlemapsembed.net/embed\"></script>\n   END CODE \n\n   REDES SOCIALES \n  <ion-card class=\"animate__animated animate__fadeInRight cool-card\">\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"end\" name=\"list\" color=\"#fff\"></ion-icon>\n      <ion-title>Social Networks</ion-title>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n\n    AQUI AGREGAR ITEM DE CORREO \n\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"start\" name=\"mail\" color=\"ligth\"></ion-icon>\n      <ion-label>entouragebarber@gmail.com</ion-label>\n    </ion-item> \n\n    <ion-grid>\n      <ion-row class=\"ion-align-items-center\">\n        <ion-col class=\"ion-text-center\" size=\"12\" *ngFor=\"let socialNetwork of d.socialNetworks\">\n          <ion-button class=\"cool-button\" shape=\"round\" fill=\"outline\" lines=\"none\" (click)=\"data.openLink(socialNetwork.link)\">\n            <ion-icon slot=\"start\" [name]=\"socialNetwork.icon\" color=\"#fff\"></ion-icon>\n            <div class=\"cool-text\">\n              <ion-text>{{socialNetwork.text}}</ion-text>\n            </div>\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card> -->\n\n\n</ion-content>\n\n");

/***/ }),

/***/ "./src/app/pages/menu/contact/contact-routing.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/menu/contact/contact-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: ContactPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactPageRoutingModule", function() { return ContactPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _contact_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contact.page */ "./src/app/pages/menu/contact/contact.page.ts");




const routes = [
    {
        path: '',
        component: _contact_page__WEBPACK_IMPORTED_MODULE_3__["ContactPage"]
    }
];
let ContactPageRoutingModule = class ContactPageRoutingModule {
};
ContactPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ContactPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/menu/contact/contact.module.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/menu/contact/contact.module.ts ***!
  \******************************************************/
/*! exports provided: ContactPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactPageModule", function() { return ContactPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _contact_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contact-routing.module */ "./src/app/pages/menu/contact/contact-routing.module.ts");
/* harmony import */ var _contact_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contact.page */ "./src/app/pages/menu/contact/contact.page.ts");







let ContactPageModule = class ContactPageModule {
};
ContactPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _contact_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactPageRoutingModule"]
        ],
        declarations: [_contact_page__WEBPACK_IMPORTED_MODULE_6__["ContactPage"]]
    })
], ContactPageModule);



/***/ }),

/***/ "./src/app/pages/menu/contact/contact.page.scss":
/*!******************************************************!*\
  !*** ./src/app/pages/menu/contact/contact.page.scss ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".logo {\n  width: 70vw;\n  height: 70vw;\n  max-width: 500px;\n  max-height: 500px;\n}\n\n.flex-container {\n  padding: 16px;\n  text-align: center;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWVudS9jb250YWN0L2NvbnRhY3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FBQUo7O0FBR0E7RUFDSSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBQUoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9tZW51L2NvbnRhY3QvY29udGFjdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9nbyB7XHJcblxyXG4gICAgd2lkdGg6IDcwdnc7XHJcbiAgICBoZWlnaHQ6IDcwdnc7XHJcbiAgICBtYXgtd2lkdGg6IDUwMHB4O1xyXG4gICAgbWF4LWhlaWdodDogNTAwcHg7XHJcblxyXG59XHJcbi5mbGV4LWNvbnRhaW5lciB7XHJcbiAgICBwYWRkaW5nOiAxNnB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/menu/contact/contact.page.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/menu/contact/contact.page.ts ***!
  \****************************************************/
/*! exports provided: ContactPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactPage", function() { return ContactPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/data.service */ "./src/app/services/data.service.ts");
/* harmony import */ var _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/launch-navigator/ngx */ "./node_modules/@ionic-native/launch-navigator/__ivy_ngcc__/ngx/index.js");





let ContactPage = class ContactPage {
    constructor(data, ds, launchNavigator) {
        this.data = data;
        this.ds = ds;
        this.launchNavigator = launchNavigator;
        this.d = data.appData.data;
    }
    ngOnInit() {
        this.data.getAppData().then(cloudData => {
            this.d = this.data.appData.data;
        });
    }
    getMapUrl() {
        return this.ds.bypassSecurityTrustResourceUrl(this.d.mapSource);
    }
    openRoute() {
        this.launchNavigator.isAppAvailable(this.launchNavigator.APP.GOOGLE_MAPS).then(isAvailable => {
            var app;
            if (isAvailable) {
                app = this.launchNavigator.APP.GOOGLE_MAPS;
            }
            else {
                console.warn("Google Maps not available - falling back to user selection");
                app = this.launchNavigator.APP.USER_SELECT;
            }
            this.launchNavigator.navigate(this.d.addressRoute, {
                app: app
            });
        });
    }
};
ContactPage.ctorParameters = () => [
    { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] },
    { type: _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_4__["LaunchNavigator"] }
];
ContactPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-contact',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./contact.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/contact/contact.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./contact.page.scss */ "./src/app/pages/menu/contact/contact.page.scss")).default]
    })
], ContactPage);



/***/ })

}]);
//# sourceMappingURL=contact-contact-module-es2015.js.map