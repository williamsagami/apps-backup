import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Redd's Barber Shop",

      homeDescription: 
        `Established in the 1950’s. One of the oldest barbershop in Pacific Beach. <br>
        We are dedicated to the craft of classic barbering with modern & classic techniques. <br>
        One of the few barbershops in the county to offer a proper hot towel straight razor shave that we know you will enjoy. <br>
        A cold drink and a good conversation are just a few of the perks we offer here.`,

        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fhome%2F1s.jpg?alt=media&token=8b83308a-48f8-496b-9392-0bb1fb856fd9",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fhome%2F2s.jpg?alt=media&token=8d6f8e82-22e6-4cb0-b626-30abf7586864",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fhome%2F3s.jpg?alt=media&token=dc3922ac-7e21-4868-acbc-0c0489f2611b",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fhome%2F4s.jpg?alt=media&token=057843fd-9e4f-4908-996a-3f6849e0120d",
        ],

      homeServices: [
        "Hot Towel with every hair Cut or Trim", 
        "Straight Razor Shave Finish with every Hair Cut", 
        "Classic Straight Razor Shaves", 
        "Classic Gentlemen Hair Cuts", 
        "Urban Hair Cuts", 
        "Kids Of Any Age",
        "Military Friendly", 
      ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "Close",
        },
        {
          day: "Tuesday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Wednesday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Thursday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Friday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Saturday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Sunday",
          hour: "12:00 am – 05:00 pm",
        },
      ],
      
      homeVideos: [
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fhome%2F1.mp4?alt=media&token=0dea874a-9eae-481d-88d2-9e23fb1d73a7",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "858 483 1514",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F1.jpg?alt=media&token=9ad8671e-b60e-4611-a739-78a30daee9da",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F8.jpg?alt=media&token=b1b95563-0b98-4eb7-a823-9cf75fba7fe3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F10.jpg?alt=media&token=fae81204-3537-4dd7-8e71-ba7714014b4c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F11.JPG?alt=media&token=dd3bfec0-cddf-4a01-bd90-896ae076dda3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F12.jpg?alt=media&token=a4337119-5c11-43f4-a203-76eaaa496f2d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F9.jpg?alt=media&token=0e744016-dd8d-4555-9220-6f40213140a1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F13.jpg?alt=media&token=39bb8c9b-a7dd-4b16-b3ab-71eab4ff4181",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F23.jpg?alt=media&token=8323ee26-408e-4a54-93e0-dba0b2059bb5",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F14.jpg?alt=media&token=8aa05783-8d0e-426d-ab69-f359e563d21b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F15.jpg?alt=media&token=2b1e92db-1adc-4454-8f0a-509b3171749c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F16.jpg?alt=media&token=37288b27-780f-4fef-b818-2d9e71f2b669",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F17.jpg?alt=media&token=001be9aa-0c5d-4620-8bad-ce57ddfab7c9",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F18.jpg?alt=media&token=74e49b0b-36bb-431c-8f9a-0b917490d4c3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F19.jpg?alt=media&token=da41edef-4717-4978-a20f-dbd8fc4f118a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F2.jpg?alt=media&token=4e7e462d-6c36-4be8-845e-7bd28b5576c4",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F31.jpg?alt=media&token=ab012afa-7a99-46af-b878-db9068fd3006",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F20.jpg?alt=media&token=9044c8f0-c553-4bfc-8b91-1d1ca9c414ae",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F21.jpg?alt=media&token=aab82b5b-04eb-4851-bfc5-9bbf7293aa6f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F21.jpg?alt=media&token=aab82b5b-04eb-4851-bfc5-9bbf7293aa6f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F22.jpg?alt=media&token=d7a2d513-51d3-4b27-8cc6-f6a8894b6004",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F24.jpg?alt=media&token=d22cb067-ecdb-47bd-855e-758646c28d48",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F25.jpg?alt=media&token=8f730f2f-f4d8-4868-b141-c761f9924a7b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F26.jpg?alt=media&token=c7ab8b8b-0640-4763-b397-ee63cbef3764",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F27.jpg?alt=media&token=41fb66d1-ece2-4351-b364-50b8c5294313",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F28.jpg?alt=media&token=93970c16-3fbf-47b6-aeac-b1cce1e52d9d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F29.jpg?alt=media&token=f0ff5e6d-3d2e-400b-8b5a-26588b2a7daa",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F3.jpg?alt=media&token=bd6f1f6a-7996-40e9-a11f-702e3214e68b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F30.jpg?alt=media&token=c6a1e718-3363-4962-a0d8-9360ef203c76",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F4.jpg?alt=media&token=0fd7e0b2-6b2e-472e-9f10-89633dcbf8f1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F5.jpg?alt=media&token=35a36b88-aebb-4926-b426-5d368e0a77df",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F6.jpg?alt=media&token=abe8cfb3-d9f6-4d23-879b-aa9435b001bd",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F7.jpg?alt=media&token=f2f544e2-f00e-4f18-99ed-616e28021885",
        
      ],


      socialNetworks: [
        {
          text: "redds_barbershop",
          link: "https://www.instagram.com/redds_barbershop/?hl=es",
          icon: "logo-instagram",
        },
        {
          text: "Redd’s Barbershop Club",
          link: "https://www.facebook.com/Redds-Barbershop-Club-2037954232890689/",
          icon: "logo-Facebook",
        },
        {
          text: "Redd's Barber Shop",
          link: "https://www.yelp.com/biz/redds-barber-shop-san-diego-2",
          icon: "logo-yahoo",
        },
        {
          text: "reddsbarbershopclub",
          link: "http://reddsbarbershopclub.com/",
          icon: "globe",
        }
      ],

      address: "1738 Garnet Ave, San Diego, CA 92109, USA",

      mapSource: "https://maps.google.com/maps?q=Redd%E2%80%99s%20Barber%20Shop%20Club&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

 // this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
