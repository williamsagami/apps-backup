import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Redd's Barber Shop",

      homeDescription: 
        `Established in the 1950’s. One of the oldest barbershop in Pacific Beach. <br>
        We are dedicated to the craft of classic barbering with modern & classic techniques. <br>
        One of the few barbershops in the county to offer a proper hot towel straight razor shave that we know you will enjoy. <br>
        A cold drink and a good conversation are just a few of the perks we offer here.`,

        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fhome%2F1s.jpg?alt=media&token=8b83308a-48f8-496b-9392-0bb1fb856fd9",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fhome%2F2s.jpg?alt=media&token=8d6f8e82-22e6-4cb0-b626-30abf7586864",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fhome%2F3s.jpg?alt=media&token=dc3922ac-7e21-4868-acbc-0c0489f2611b",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F32.jpg?alt=media&token=badeeaf3-535b-4cce-831b-7540681b5eb2",
        ],

      homeServices: [
        "Hot Towel with every hair Cut or Trim", 
        "Straight Razor Shave Finish with every Hair Cut", 
        "Classic Straight Razor Shaves", 
        "Classic Gentlemen Hair Cuts", 
        "Urban Hair Cuts", 
        "Kids Of Any Age",
        "Military Friendly", 
      ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "Close",
        },
        {
          day: "Tuesday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Wednesday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Thursday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Friday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Saturday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Sunday",
          hour: "12:00 am – 05:00 pm",
        },
      ],
      
      homeVideos: [
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fhome%2F1.mp4?alt=media&token=0dea874a-9eae-481d-88d2-9e23fb1d73a7",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fhome%2F2.mp4?alt=media&token=e3d863d5-6203-438d-851e-989a4cecb68e",
        ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "858 483 1514",
        },
      ],

      gallery:[
       // "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F1.jpg?alt=media&token=9ad8671e-b60e-4611-a739-78a30daee9da",
        //"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F8.jpg?alt=media&token=b1b95563-0b98-4eb7-a823-9cf75fba7fe3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F10.jpg?alt=media&token=fae81204-3537-4dd7-8e71-ba7714014b4c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F11.JPG?alt=media&token=dd3bfec0-cddf-4a01-bd90-896ae076dda3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F12.jpg?alt=media&token=a4337119-5c11-43f4-a203-76eaaa496f2d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F35.jpg?alt=media&token=ca84fbe8-4ee3-4e14-bd1a-b4c8e37856e2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F13.jpg?alt=media&token=39bb8c9b-a7dd-4b16-b3ab-71eab4ff4181",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F23.jpg?alt=media&token=8323ee26-408e-4a54-93e0-dba0b2059bb5",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F14.jpg?alt=media&token=8aa05783-8d0e-426d-ab69-f359e563d21b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F36.jpg?alt=media&token=988c0472-c19c-436e-8bf6-d86971ecc89c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F15.jpg?alt=media&token=2b1e92db-1adc-4454-8f0a-509b3171749c",
       // "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F16.jpg?alt=media&token=37288b27-780f-4fef-b818-2d9e71f2b669",
        //"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F17.jpg?alt=media&token=001be9aa-0c5d-4620-8bad-ce57ddfab7c9",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F18.jpg?alt=media&token=74e49b0b-36bb-431c-8f9a-0b917490d4c3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F19.jpg?alt=media&token=da41edef-4717-4978-a20f-dbd8fc4f118a",
        //"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F2.jpg?alt=media&token=4e7e462d-6c36-4be8-845e-7bd28b5576c4",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F31.jpg?alt=media&token=ab012afa-7a99-46af-b878-db9068fd3006",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F20.jpg?alt=media&token=9044c8f0-c553-4bfc-8b91-1d1ca9c414ae",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F34.jpg?alt=media&token=13d9826b-73e3-4749-88e8-865e895f7e6a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F21.jpg?alt=media&token=aab82b5b-04eb-4851-bfc5-9bbf7293aa6f",
        //"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F21.jpg?alt=media&token=aab82b5b-04eb-4851-bfc5-9bbf7293aa6f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F22.jpg?alt=media&token=d7a2d513-51d3-4b27-8cc6-f6a8894b6004",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F24.jpg?alt=media&token=d22cb067-ecdb-47bd-855e-758646c28d48",
        //"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F25.jpg?alt=media&token=8f730f2f-f4d8-4868-b141-c761f9924a7b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F26.jpg?alt=media&token=c7ab8b8b-0640-4763-b397-ee63cbef3764",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F27.jpg?alt=media&token=41fb66d1-ece2-4351-b364-50b8c5294313",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F28.jpg?alt=media&token=93970c16-3fbf-47b6-aeac-b1cce1e52d9d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F29.jpg?alt=media&token=f0ff5e6d-3d2e-400b-8b5a-26588b2a7daa",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F3.jpg?alt=media&token=bd6f1f6a-7996-40e9-a11f-702e3214e68b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F30.jpg?alt=media&token=c6a1e718-3363-4962-a0d8-9360ef203c76",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F4.jpg?alt=media&token=0fd7e0b2-6b2e-472e-9f10-89633dcbf8f1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F5.jpg?alt=media&token=35a36b88-aebb-4926-b426-5d368e0a77df",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F6.jpg?alt=media&token=abe8cfb3-d9f6-4d23-879b-aa9435b001bd",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F7.jpg?alt=media&token=f2f544e2-f00e-4f18-99ed-616e28021885",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fgallery%2F33.jpg?alt=media&token=798bd7dc-35aa-4f17-a28f-8fc77346772a",
        
        
      ],

      products:[
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2F1.jpg?alt=media&token=1100260f-4c2f-4b2d-9b26-9b1cd43787ef",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2F16.jpg?alt=media&token=4d1caf24-fd77-42b8-b2b4-3709e1cc695b",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2F2.jpg?alt=media&token=c7de1bb8-610c-4bb8-b314-ff273e086fdb",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2F25.jpg?alt=media&token=4798b32e-d152-437b-87bd-b2f42f113355",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2F8.jpg?alt=media&token=47e54011-4ab2-4637-9e99-fbabfe6db8c2",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2F9.jpg?alt=media&token=57df0c3e-17ed-44b5-a647-f03ac4b262d4",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2Fp10.jpg?alt=media&token=739cf086-e104-48ab-81c4-9f6d26d02224",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2Fp11.jpg?alt=media&token=9da69071-14ad-46a2-a2be-2b6081e1cf2f",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2Fp2.jpg?alt=media&token=4619234c-3501-47f3-937f-f81a7008a837",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2Fp5.jpg?alt=media&token=41696297-feaf-4571-97f6-1bd51514639a",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2Fp6.jpg?alt=media&token=4e858267-5174-4ae7-a400-7c67464eafc9",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2Fp8.jpg?alt=media&token=75079f8c-7ba9-4538-acbc-8509e0d42156",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2Fp9.jpg?alt=media&token=818ed2ca-9197-4bf2-bde5-b389eeb3a92a",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2Fp7.jpg?alt=media&token=aefb7613-3eb6-4a58-84a6-1cbaa72558e8",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2Fp4.jpg?alt=media&token=fbf70e6d-315f-418a-91a9-803085ac4961",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2Fp3.jpg?alt=media&token=4319efa7-959d-4d4d-8bc1-fd603bbe4af6",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2Fp12.jpg?alt=media&token=318b8602-83d8-4587-b99f-073611fead94",
 // "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRedd's%20Barber%20Shop%2Fproduct%2F17.jpg?alt=media&token=f02ed8ba-3564-467f-a005-67faebb0a95a", 
      ],


      socialNetworks: [
        {
          text: "redds_barbershop",
          link: "https://www.instagram.com/redds_barbershop/?hl=es",
          icon: "logo-instagram",
        },
        {
          text: "Redd’s Barbershop Club",
          link: "https://www.facebook.com/Redds-Barbershop-Club-2037954232890689/",
          icon: "logo-Facebook",
        },
        {
          text: "Redd's Barber Shop",
          link: "https://www.yelp.com/biz/redds-barber-shop-san-diego-2",
          icon: "logo-yahoo",
        },
        {
          text: "reddsbarbershopclub",
          link: "http://reddsbarbershopclub.com/",
          icon: "globe",
        }
      ],

      address: "1738 Garnet Ave, San Diego, CA 92109, USA",

      mapSource: "https://maps.google.com/maps?q=Redd%E2%80%99s%20Barber%20Shop%20Club&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

  //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
