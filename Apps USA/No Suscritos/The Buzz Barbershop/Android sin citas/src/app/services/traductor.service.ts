import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: "The Buzz Barbershop",
      en: "The Buzz Barbershop"
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'shampoo $5',
    'beard trim $15',
    'kids $15',
    'teen $20',
    'line up $10',
    'haircuts $25',
    'Razor shaves fades and designs',
  ];
  

  translateIndex = [
    {
    es:`The Buzz Barbershop, es una de las mejores barberias de Denver, brindando mucha experiencia. <br>
    y dando los mejores cortes  y los mejores diseños porque para nosotros tu corte de pelo es de lo mas importante. <br>
    Buscamos la satisfaccion del cliente brindando un ambiente agradable contando con una reputacion espléndida, porque damos lo mejor de nosotros en cada uno de nuestros cortes,
    no dudes en angendar tu cita AHORA! y tendras el mejor corte con la mejor calidad`,
    en:`The Buzz Barbershop, is the best barbershop in Denver, providing a lot of experience. <br>
    Giving the best cuts and the best designs, because for us your haircut is very important. <br>
    We seek customer satisfaction by providing a pleasant environment with a splendid reputation,
    because we give the best of ourselves in each of our cuts, do not hesitate to make your appointment NOW!, let's be from 10am to 8pm and you will have the best cut with the best quality`
    },
    {
      es: 'Price and services',
      en: 'Price and services'
    },
  ];

  translateProcedures = [
    {
      es: 'shampoo $5',
      en: 'shampoo $5'
    },
    {
      es: 'beard trim $15',
      en: 'beard trim $15'
    },
    {
      es: 'kids $15',
      en: 'kids $15'
    },
    {
      es: 'teen $20',
      en: 'teen $20'
    },
    {
      es: 'line up $10',
      en: 'line up $10',
    },
    {
      es: 'haircuts $25',
      en: 'haircuts $25',
    }, 
    {
      es: 'Razor shaves fades and designs',
      en: 'Razor shaves fades and designs',
    }, 
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = true;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });


      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
