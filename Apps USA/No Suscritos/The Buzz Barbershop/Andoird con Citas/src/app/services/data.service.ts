import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "The Buzz Barbershop",

      homeDescription: 
        `The Buzz Barbershop, is the best barbershop in Denver, providing a lot of experience. <br>
        Giving the best cuts and the best designs, because for us your haircut is very important. <br>
        We seek customer satisfaction by providing a pleasant environment with a splendid reputation,
        because we give the best of ourselves in each of our cuts, do not hesitate to make your appointment NOW!, let's be from 10am to 8pm and you will have the best cut with the best quality`,

      homeServices: [
        {
          name:"shampoo",
          price: "$5"
        },
        {
          name:"beard trim",
          price: "$15"
        },
        {
          name:"kids",
          price: "$15"
        },
        {
          name:"line up",
          price: "$10"
        },
        {
          name:"haircuts",
          price: "$25"
        },
        {
          name:"Razor shaves fades and designs",
          price: ""
        },
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "303 750 0152",
        },
        {
          text: "Phone",
          number: "720 416 5050",
        }
      ],

      socialNetworks: [
        {
          text: "The Buzz Barbershop LLC",
          link: "https://www.facebook.com/catchthabuzz/",
          icon: "logo-facebook",
        },

        {
          text: "raysundesigns",
          link: "https://www.instagram.com/catchthebuzzbarbershop/",
          icon: "logo-instagram",
        }
      ],

      address: "2295 s Chambers rd Aurora, CO 80014 steN",

      mapSource: "https://maps.google.com/maps?q=The%20Buzz%20Barbershop%20LLC,%202295%20S%20Chambers%20Rd,%20Aurora,%20CO%2080014,%20EE.%20UU.&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

 // this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
