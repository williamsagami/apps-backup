// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyAaEeQX2bqKuaRK9ZjRJNbQ046dv9M4wig",
  authDomain: "slamart-applications-3.firebaseapp.com",
  projectId: "slamart-applications-3",
  storageBucket: "slamart-applications-3.appspot.com",
  messagingSenderId: "1017338459377",
  appId: "1:1017338459377:web:8cae0a878a756f38aa6550",
  measurementId: "G-GJ54KFBM5V"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
