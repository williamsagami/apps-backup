import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Trojan's Barber Shop",

      homeDescription: 
        `We are one of the best barbershops in Los Angeles, with many years of experience, known for the impressive quality and innovation in our cuts. <br>
        We help our clients to find a unique hair style they been searching for. <br>
        For us the client is always right, and deserves the best service. <br>
        Come with us and enjoy the experience at incredible prices. <br>
        We have large parking for our clients!!!`,


        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fhome%2F1s_512x512.jpg?alt=media&token=abc03f9b-60cb-425e-95cc-d11cb88b79ad",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fhome%2F2s_512x512.jpg?alt=media&token=1c27166b-8dd9-4138-a43e-92aaa6d1e402",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fhome%2F3s_512x512.jpg?alt=media&token=3d3a3b42-8456-4c29-a612-eb4a74fbf1ef",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fhome%2F14_512x512.jpg?alt=media&token=d7ad07c7-2c15-47fe-828a-a3c821ace9fa",
        ],


      homeServices: [
       
      ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "9:30 am – 7:00 pm",
        },
        {
          day: "Tuesday",
          hour: "9:30 am – 7:00 pm",
        },
        {
          day: "Wednesday",
          hour: "9:30 am – 7:00 pm",
        },
        {
          day: "Thursday",
          hour: "9:30 am – 7:00 pm",
        },
        {
          day: "Friday",
          hour: "9:30 am – 7:00 pm",
        },
        {
          day: "Saturday",
          hour: "9:30 am – 7:00 pm",
        },
        {
          day: "Sunday",
          hour: "10:30 am - 3:00 pm",
        },
      ],
      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fhome%2F1.mp4?alt=media&token=0d2d4aa3-a229-414f-99f4-8dad472ede2b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fhome%2F2.mp4?alt=media&token=e62ba018-7734-4eb7-a2bb-5391f0c80eaf",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fhome%2F3.mp4?alt=media&token=234118ac-32dd-464a-bab7-7bcf75c9ce7f",

      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "213 944 1119",
        },
        {
          text: "Phone",
          number: "661 361 8579",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F9_512x512.jpg?alt=media&token=3f157a27-625a-43a9-bfd7-8cd24e1a4ec2",  
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F8_512x512.jpg?alt=media&token=bb3d258d-7752-40de-8ead-e639a6a38dde", 
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F7_512x512.jpg?alt=media&token=05f3b75a-897a-45f5-bced-77744c0fac0e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F6_512x512.jpg?alt=media&token=9958d83c-5363-4893-bde3-e6e1ebee1648", 
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F5_512x512.jpg?alt=media&token=9c497a68-32db-48b6-b684-1f825137ab88",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F4_512x512.jpg?alt=media&token=3b7770cc-fc21-4d66-9ac6-f0c4d36e3fa6", 
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F3_512x512.jpg?alt=media&token=00a19996-d9a3-4d97-8610-c04fbf6fe1cf",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F2_512x512.jpg?alt=media&token=e29a5974-bc29-404e-9243-29858166a55d", 
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F27_512x512.jpg?alt=media&token=7272d902-f797-4cf6-a32f-e374a7d83fcd",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F26_512x512.jpg?alt=media&token=836aec38-8da9-4ebb-aedc-83025cb61855", 
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F25_512x512.jpg?alt=media&token=878fcce2-eb91-48ff-9fb0-066008aa0a41",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F24_512x512.jpg?alt=media&token=99a62d4b-6843-4164-b744-2220888b3402", 
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F23_512x512.jpg?alt=media&token=df0fec97-90a5-411b-8fc5-06372b8a1a20",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F22_512x512.jpg?alt=media&token=cedf90ff-54f5-48e4-b57d-f5943958eac2", 
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F21_512x512.jpg?alt=media&token=56f77c79-8f37-4a44-820c-66a27294ced6",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F20_512x512.jpg?alt=media&token=4902716b-434a-4d4e-9376-fde38b1e916a", 
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F1_512x512.jpg?alt=media&token=85a29a85-b8b2-4367-89d2-05d626974ef1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F19_512x512.jpg?alt=media&token=748f3de7-7bed-4e15-8174-5a817cd0ff78", 
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F18_512x512.jpg?alt=media&token=7c4f1dc2-8f09-47c2-b0df-c22adc40cc91",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F17_512x512.jpg?alt=media&token=15fa5719-3c0d-484d-ba98-f5cea8e45b5f", 
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F16_512x512.jpg?alt=media&token=1c1e8107-3d9d-4125-be84-2633f60a5357",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F15_512x512.jpg?alt=media&token=c04981b7-b461-4e10-b5bf-93658452a178", 
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F14_512x512.jpg?alt=media&token=8ab1a6ff-91b7-40db-90d6-e4ad41479ed4",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F13_512x512.jpg?alt=media&token=5f736ed6-bc38-4767-bd1f-12c4bc6b7dcb", 
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F12_512x512.jpg?alt=media&token=6ec3a224-3709-4182-b115-0cf67034590a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F11_512x512.jpg?alt=media&token=0aeda426-1b51-4e11-a530-5334e42f01c1", 
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FTrojan's%20Barber%20Shop%2Fgallery%2F10_512x512.jpg?alt=media&token=c2189fae-f7a6-4c19-822d-34c7437026f6",
      ],

      socialNetworks: [
        {
          text: "Trojan’s Mobile Barbershop",
          link: "https://www.facebook.com/Trojans-Mobile-Barbershop--1968887056482625",
          icon: "logo-facebook",
        },
        {
          text: "trojansbarbershopmobile",
          link: "https://www.instagram.com/trojansbarbershopmobile/",
          icon: "logo-instagram",
        },
        {
          text: "Trojan's Barber Shop",
          link: "https://www.yelp.com/biz/trojans-barber-shop-los-angeles",
          icon: "logo-yahoo",
        },
        {
          text: "trojansbarbershop",
          link: "https://trojansbarbershop.wordpress.com/",
          icon: "globe",
        },
      ],

      address: "2716 S Vermont Ave Ste 2 Los Angeles, CA 90007",

      addressRoute: "2716 S Vermont Ave Ste 2 Los Angeles, CA 90007",

      mapSource: "https://maps.google.com/maps?q=Trojan's%20Barber%20Shop&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

  //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)}); 
  }


  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
