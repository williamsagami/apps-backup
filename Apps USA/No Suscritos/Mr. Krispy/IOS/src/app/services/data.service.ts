import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  hours=[
    // {
    //   text: "00:00 AM",
    //   value: moment().hours(0).minutes(0).seconds(0)
    // },
    // {
    //   text: "00:30 AM",
    //   value: moment().hours(0).minutes(30).seconds(0)
    // },
    // {
    //   text: "01:00 AM",
    //   value: moment().hours(1).minutes(0).seconds(0)
    // },
    // {
    //   text: "01:30 AM",
    //   value: moment().hours(1).minutes(30).seconds(0)
    // },
    // {
    //   text: "02:00 AM",
    //   value: moment().hours(2).minutes(0).seconds(0)
    // },
    // {
    //   text: "02:30 AM",
    //   value: moment().hours(2).minutes(30).seconds(0)
    // },
    // {
    //   text: "03:00 AM",
    //   value: moment().hours(3).minutes(0).seconds(0)
    // },
    // {
    //   text: "03:30 AM",
    //   value: moment().hours(3).minutes(30).seconds(0)
    // },
    // {
    //   text: "04:00 AM",
    //   value: moment().hours(4).minutes(0).seconds(0)
    // },
    // {
    //   text: "04:30 AM",
    //   value: moment().hours(4).minutes(30).seconds(0)
    // },
    // {
    //   text: "05:00 AM",
    //   value: moment().hours(5).minutes(0).seconds(0)
    // },
    // {
    //   text: "05:30 AM",
    //   value: moment().hours(5).minutes(30).seconds(0)
    // },
    // {
    //   text: "06:00 AM",
    //   hours: 6,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "06:30 AM",
    //   hours: 6,
    //   minutes:30,
    //   seconds: 0
    // },
    {
      text: "07:00 AM",
      hours: 7,
      minutes:0,
      seconds: 0
    },
    {
      text: "07:30 AM",
      hours: 7,
      minutes:30,
      seconds: 0
    },
    {
      text: "08:00 AM",
      hours: 8,
      minutes:0,
      seconds: 0
    },
    {
      text: "08:30 AM",
      hours: 8,
      minutes:30,
      seconds: 0
    },
    {
      text: "09:00 AM",
      hours: 9,
      minutes:0,
      seconds: 0
    },
    {
      text: "09:30 AM",
      hours: 9,
      minutes:30,
      seconds: 0
    },
    {
      text: "10:00 AM",
      hours: 10,
      minutes:0,
      seconds: 0
    },
    {
      text: "10:30 AM",
      hours: 10,
      minutes:30,
      seconds: 0
    },
    {
      text: "11:00 AM",
      hours: 11,
      minutes:0,
      seconds: 0
    },
    {
      text: "11:30 AM",
      hours: 11,
      minutes:30,
      seconds: 0
    },

    {
      text: "12:00 PM",
      hours: 12,
      minutes:0,
      seconds: 0
    },
    {
      text: "12:30 PM",
      hours: 12,
      minutes:30,
      seconds: 0
    },
    {
      text: "1:00 PM",
      hours: 13,
      minutes:0,
      seconds: 0
    },
    {
      text: "01:30 PM",
      hours: 13,
      minutes:30,
      seconds: 0
    },
    {
      text: "02:00 PM",
      hours: 14,
      minutes:0,
      seconds: 0
    },
    {
      text: "02:30 PM",
      hours: 14,
      minutes:30,
      seconds: 0
    },
    {
      text: "03:00 PM",
      hours: 15,
      minutes:0,
      seconds: 0
    },
    {
      text: "03:30 PM",
      hours: 15,
      minutes:30,
      seconds: 0
    },
    {
      text: "04:00 PM",
      hours: 16,
      minutes:0,
      seconds: 0
    },
    {
      text: "04:30 PM",
      hours: 16,
      minutes:30,
      seconds: 0
    },
    {
      text: "05:00 PM",
      hours: 17,
      minutes:0,
      seconds: 0
    },
    {
      text: "05:30 PM",
      hours: 17,
      minutes:30,
      seconds: 0
    },
    {
      text: "06:00 PM",
      hours: 18,
      minutes:0,
      seconds: 0
    },
    {
      text: "06:30 PM",
      hours: 18,
      minutes:30,
      seconds: 0
    },
    {
      text: "07:00 PM",
      hours: 19,
      minutes:0,
      seconds: 0
    },
    // {
    //   text: "07:30 PM",
    //   value: moment().hours(19).minutes(30).seconds(0)
    // },
    // {
    //   text: "08:00 PM",
    //   value: moment().hours(20).minutes(0).seconds(0)
    // },
    // {
    //   text: "08:30 PM",
    //   value: moment().hours(20).minutes(30).seconds(0)
    // },
    // {
    //   text: "09:00 PM",
    //   value: moment().hours(21).minutes(0).seconds(0)
    // },
    // {
    //   text: "09:30 PM",
    //   value: moment().hours(21).minutes(30).seconds(0)
    // },
    // {
    //   text: "10:00 PM",
    //   value: moment().hours(22).minutes(0).seconds(0)
    // },
    // {
    //   text: "10:30 PM",
    //   value: moment().hours(22).minutes(30).seconds(0)
    // },
    // {
    //   text: "11:00 PM",
    //   value: moment().hours(23).minutes(0).seconds(0)
    // },
    // {
    //   text: "11:30 PM",
    //   value: moment().hours(23).minutes(30).seconds(0)
    // },
  ]
  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {

    //Datos de Contacto
    data: {

      appTitle: "Mr. Krispy",

      homeDescription: 
        `Do you want to try a new style such as the short hair twist? 
        Looking for a specialized salon where you will be able to find expert advice and services? 
        Then, your search terminates here. Mr. Krispy Professional Barbershop is the place where all of your hair needs are met and exceeded. <br>
        We specialize in hairstyling and you can rely on our licensed and insured salon in Oakland, CA. <br>
        Mr. Krispy Professional Barbershop is a barbershop that offers haircuts, styling, twisting, razor shaving, and more.
        Our stylists have 10+ years of experience in the industry backing them up and each of us loves to see the smile on our happy clients’ faces.`,

        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F12_512x512.jpeg?alt=media&token=47334421-fe79-4c87-9198-b9ae67115d7e",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fhome%2F1_512x512.jpg?alt=media&token=e50f6e01-528b-4dc8-8d09-b1b4058b8fe5",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F13_512x512.jpg?alt=media&token=34ac3ffd-9960-4b43-ae64-f94142f7402f",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fhome%2Fp6_512x512.jpg?alt=media&token=05ce600b-71ac-43f8-a2ae-5f23181921d9",
        ],


      homeServices: [
        {
          name:"Mens Haircut and Facee",
          time:"50 minutes",
          //description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
          price: "$40"
        },
        {
          name:"Tapers and Fades",
          time:"30 minutes",
          //description:"Extra 15$-20$",
          price: "$25"
        },
        {
          name:"Men Haircut withoutface",
          time:"30 minutes",
          description:"High fade low fade",
          price: "$35"
        },
        {
          name:"Mohawk and Face",
          time:"50 minutes",
          //description:"Razor line",
          price: "$45"
        },
        {
          name:"Men's Combover (scissor work)",
          time:"40 minutes",
          //description:"Razor line",
          price: "$35"
        },
        {
          name:"Bald head",
          time:"20 minutes",
          //description:"Face treatments $15.00",
          price: "$25"
        },
        {
          name:"Men hair line (head only)",
          time:"15 minutes",
         // description:"Desings 15 dollars total",
          price: "$15"
        },
        {
          name:"Bald Head w/face",
          time:"30 minutes",
         // description:"Head Shave",
          price: "$30"
        },
        {
          name:"Men's Mohawk",
          time:"50 minutes",
          //description:"Trim stache",
          price: "$40"
        },
        {
          name:"Men's Afros",
          time:"45 minutes",
         // description:"Taper Fade Shave",
          price: "$40"
        },
        {
          name:"Mens Line Up (head &face)",
          time:"20 minutes",
        //description:"Hair Line With Shave",
          price: "$20"
        },
        {
          name:"Women's Haircut",
          time:"35 minutes",
          //description:"Razor on head & beard",
          price: "$30"
        },
        {
          name:"Women's Undercut",
          time:"20 minutes",
         // description:"Razor",
          price: "$20"
        },
        {
          name:"Women's Neck Taper",
          time:"20 minutes",
          //description:"Bald Fade And Shave",
          price: "$20"
        },
        {
          name:"Kid's Combover (scissor work)",
          time:"30 minutes",
         // description:"Haircuts",
          price: "$30"
        },
        {
          name:"All even with line",
          time:"25 minutes",
          //description:"Faded Sides whit line",
          price: "$20"
        },
        {
          name:"Lines",
          time:"10 minutes",
         // description:"Please Call me let me know what`s going on",
          price: "$15"
        },
        {
          name:"Afros and Mohawks",
          time:"40 minutes",
          //description:"If ur late",
          price: "$30"
        },
        {
          name:"Shave face with razor",
          time:"30 minutes",
          //description:"Hot towel",
          price: "$25"
        },
        {
          name:"Beard Fades",
          time:"30 minutes",
          //description:"Astringents",
          price: "$20"
        },
        {
          name:"Beard trim",
          time:"20 minutes",
         // description:"One level",
          price: "$25"
        },   
      ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "9:00 AM - 6:00 PM",
        },
        {
          day: "Tuesday",
          hour: "7:00 AM - 7:00 PM",
        },
        {
          day: "Wednesday",
          hour: "7:00 AM - 7:00 PM",
        },
        {
          day: "Thursday",
          hour: "7:00 AM - 7:00 PM",
        },
        {
          day: "Friday",
          hour: "7:00 AM - 7:00 PM",
        },
        {
          day: "Saturday",
          hour: "6:00 AM - 8:00 PM",
        },
        {
          day: "Sunday",
          hour: "7:00 AM - 5:00 PM",
        },
      ],

        // homeBreaktime: [
        //   {
        //     day: "Monday",
        //     hour: "Close",
        //   },
        //   {
        //     day: "Tuesday",
        //     hour: "12:00 pm – 01:15 pm",
        //   },
        //   {
        //     day: "Wednesday",
        //     hour: "12:00 pm – 01:15 pm",
        //   },
        //   {
        //     day: "Thursday",
        //     hour: "12:00 pm – 01:15 pm",
        //   },
        //   {
        //     day: "Friday",
        //     hour: "12:00 pm – 01:15 pm",
        //   },
        //   {
        //     day: "Saturday",
        //     hour: "12:00 pm – 01:15 pm",
        //   },
        //   {
        //     day: "Sunday",
        //     hour: "Close",
        //   },
        // ],

      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fhome%2F1.mp4?alt=media&token=7009f70a-6cff-49a5-840a-29e8d30acbc6",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "510 240 8530",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F7_512x512.jpg?alt=media&token=d17abf20-45ff-4cef-a6d4-0eba4cfc9652",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F11_512x512.jpg?alt=media&token=96101be7-2a27-484f-8d30-4842f331d029",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F12_512x512.jpeg?alt=media&token=47334421-fe79-4c87-9198-b9ae67115d7e",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F13_512x512.jpg?alt=media&token=34ac3ffd-9960-4b43-ae64-f94142f7402f",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F14_512x512.jpg?alt=media&token=078415ab-b179-4fbe-9086-9ab9097ecc42",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F15_512x512.jpg?alt=media&token=9940ddb5-2038-4cbf-b88a-e89ed0554192",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F16_512x512.jpg?alt=media&token=efb3c47f-33b0-451c-a4ad-fdae4095a9dd",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F17_512x512.jpg?alt=media&token=baae1edc-29cc-4904-af8c-73334681899e",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F18_512x512.jpg?alt=media&token=8ce223b9-de02-4301-a72b-1ecbc6fe8af2",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F19_512x512.jpg?alt=media&token=5357936d-5159-4538-a610-e77583ad3f23",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F1_512x512.jpg?alt=media&token=4f2460c1-21e3-4047-b35b-cf34edfa36c9",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F20_512x512.jpg?alt=media&token=72911622-17f2-4b01-88ab-b1505df123db",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F21_512x512.jpg?alt=media&token=4897c6b2-6931-4c55-9b96-0e44cc86639f",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F22_512x512.jpg?alt=media&token=65c98366-dd51-4af6-bca6-9230dcf4f1b9",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F23_512x512.jpg?alt=media&token=5dbe158e-b235-400c-a0c1-bd52f266f07c",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F24_512x512.jpg?alt=media&token=6a54c569-18fc-4878-84f7-6beedcd690e3",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F25_512x512.jpg?alt=media&token=7b3e06e6-c74d-4229-8394-8430d348933c",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F26_512x512.jpg?alt=media&token=0a60bc23-ae2f-4658-ac3c-1de48ffa9d95",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F27_512x512.jpg?alt=media&token=c3d04434-92a9-488e-940c-df7e82ba621e",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F2_512x512.jpg?alt=media&token=61cae7da-f034-4d18-af4a-5ad5994f9259",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F3_512x512.jpg?alt=media&token=111274e7-48f5-4887-a93c-9546fe0c77ab",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F4_512x512.jpg?alt=media&token=0acc569f-e960-4c33-82eb-3320657d215b",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F5_512x512.jpeg?alt=media&token=98ca4b15-ec2f-4f0d-ab95-b5aed0ace417",
      //"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fgallery%2F6_512x512.jpg?alt=media&token=c53b8103-d8c4-4170-b17a-4bd18fec78cb",   
      ],

      products:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fproducts%2Fp10_512x512.jpg?alt=media&token=b1166af7-be56-4f33-bc82-eb23b548db59",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fproducts%2Fp11_512x512.png?alt=media&token=035f4b1a-e1aa-433e-b1fc-4095369fb861",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fproducts%2Fp12_512x512.jpg?alt=media&token=17d086ae-04cf-4d2e-b407-4cbe4e65cd86",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fproducts%2Fp1_512x512.jpeg?alt=media&token=ecfe1a7a-c8e1-4e94-8588-25662d719222",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fproducts%2Fp2_512x512.jpeg?alt=media&token=59160682-92ff-43c7-a776-76e078ecc8e6",
       // "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fproducts%2Fp3_512x512.jpeg?alt=media&token=fe7ddc2c-3e93-492a-afcc-ff14d3c1b829",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fproducts%2Fp4_512x512.jpg?alt=media&token=938dc114-9f71-4c51-b5c9-efec2b2bb5b6",
       // "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fproducts%2Fp6_512x512.jpg?alt=media&token=e387340f-544d-4e8b-ab0d-fc56be2e418c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fproducts%2Fp7_512x512.jpg?alt=media&token=3a434d02-56a7-45f2-ba96-b483c1ae7c25",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fproducts%2Fp8_512x512.jpg?alt=media&token=4ef9beb9-8c65-4fa8-bf02-0ec0d2b8b16e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fproducts%2Fp9_512x512.jpg?alt=media&token=ac09a7d4-9dd7-4e89-8219-1cabc0b491ee",
      ],

      socialNetworks: [
        {
          text: "mrkrispyhaircare",
          link: "https://mrkrispyhaircare.com/",
          icon: "globe",
        },
        {
          text: "mrkrispythabarber0",
          link: "https://www.instagram.com/mrkrispythabarber/",
          icon: "logo-instagram",
        },
        {
          text: "MrKrispy Professional Barbershop",
          link: "https://booksy.com/en-us/143391_mrkrispy-professional-barbershop_barber-shop_134730_oakland",
          icon: "book",
        },
        {
          text: "Mr Krispy Professional Barbershop",
          link: "https://www.yelp.com/biz/mr-krispy-professional-barbershop-oakland-2",
          icon: "logo-yahoo",
        },
      ],

      address: "Address: 1462 High St, suite B Oakland, CA 94601",
      addressRoute: "Address: 1462 High St, suite B Oakland, CA 94601",
      mapSource: "https://maps.google.com/maps?q=Mr.%20Krispy%20Professional%20Barbershop%20-%20Barber%20Shop,%20Razor%20Shaving,%20Short%20Hair%20Twist,%20Men%20Hair%20Braiding,%20Professional%20Barber%20Shop,%20Straight%20Razor%20Shaving&t=&z=15&ie=UTF8&iwloc=&output=embed", 
    
      barbers:[
        {
          name: "Mr. Krispy",
          description: "Boss Barber",
          socialNetworks:[
            {
              text: "mrkrispythabarber",
              link: "https://www.instagram.com/mrkrispythabarber/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMr.%20Krispy%2Fappointments%2F14_512x512.jpg?alt=media&token=c36e3154-5250-478c-962f-329a72d899fe",
        },
      ]
    },

    appointmentsDays:[
      {
        day: 1,
        hours: this.hours,
        closed: false
      },
      {
        day: 2,
        hours: this.hours,
        closed: false
      },
      {
        day: 3,
        hours: this.hours,
        closed: false
      },
      {
        day: 4,
        hours: this.hours,
        closed: false
      },
      {
        day: 5,
        hours: this.hours,
        closed: false
      },
      {
        day: 6,
        hours: this.hours,
        closed: false
      },
      {
        day: 0,
        hours: this.hours,
        closed: false
      },
    ],
    
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
     //this.firebaseService.updateAppData(this.appData).then(t => {console.log(this.appData)}); 
  }


  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
     return this.firebaseService.getAppData().then(appData => {
        if(appData) {
          this.appData = appData as any;
          console.log(appData);
        }
      });
  }
}
