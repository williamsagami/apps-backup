import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  hours=[
    // {
    //   text: "00:00 AM",
    //   value: moment().hours(0).minutes(0).seconds(0)
    // },
    // {
    //   text: "00:30 AM",
    //   value: moment().hours(0).minutes(30).seconds(0)
    // },
    // {
    //   text: "01:00 AM",
    //   value: moment().hours(1).minutes(0).seconds(0)
    // },
    // {
    //   text: "01:30 AM",
    //   value: moment().hours(1).minutes(30).seconds(0)
    // },
    // {
    //   text: "02:00 AM",
    //   value: moment().hours(2).minutes(0).seconds(0)
    // },
    // {
    //   text: "02:30 AM",
    //   value: moment().hours(2).minutes(30).seconds(0)
    // },
    // {
    //   text: "03:00 AM",
    //   value: moment().hours(3).minutes(0).seconds(0)
    // },
    // {
    //   text: "03:30 AM",
    //   value: moment().hours(3).minutes(30).seconds(0)
    // },
    // {
    //   text: "04:00 AM",
    //   value: moment().hours(4).minutes(0).seconds(0)
    // },
    // {
    //   text: "04:30 AM",
    //   value: moment().hours(4).minutes(30).seconds(0)
    // },
    // {
    //   text: "05:00 AM",
    //   value: moment().hours(5).minutes(0).seconds(0)
    // },
    // {
    //   text: "05:30 AM",
    //   value: moment().hours(5).minutes(30).seconds(0)
    // },
    {
      text: "06:00 AM",
      hours: 6,
      minutes:0,
      seconds: 0
    },
    {
      text: "06:30 AM",
      hours: 6,
      minutes:30,
      seconds: 0
    },
    {
      text: "07:00 AM",
      hours: 7,
      minutes:0,
      seconds: 0
    },
    {
      text: "07:30 AM",
      hours: 7,
      minutes:30,
      seconds: 0
    },
    {
      text: "08:00 AM",
      hours: 8,
      minutes:0,
      seconds: 0
    },
    {
      text: "08:30 AM",
      hours: 8,
      minutes:30,
      seconds: 0
    },
    {
      text: "09:00 AM",
      hours: 9,
      minutes:0,
      seconds: 0
    },
    {
      text: "09:30 AM",
      hours: 9,
      minutes:30,
      seconds: 0
    },
    {
      text: "10:00 AM",
      hours: 10,
      minutes:0,
      seconds: 0
    },
    {
      text: "10:30 AM",
      hours: 10,
      minutes:30,
      seconds: 0
    },
    {
      text: "11:00 AM",
      hours: 11,
      minutes:0,
      seconds: 0
    },
    {
      text: "11:30 AM",
      hours: 11,
      minutes:30,
      seconds: 0
    },

    // {
    //   text: "12:00 PM",
    //   value: moment().hours(12).minutes(0).seconds(0)
    // },
    // {
    //   text: "12:30 PM",
    //   value: moment().hours(12).minutes(30).seconds(0)
    // },
    // {
    //   text: "01:00 PM",
    //   value: moment().hours(13).minutes(0).seconds(0)
    // },
    {
      text: "01:30 PM",
      hours: 13,
      minutes:30,
      seconds: 0
    },
    {
      text: "02:00 PM",
      hours: 14,
      minutes:0,
      seconds: 0
    },
    {
      text: "02:30 PM",
      hours: 14,
      minutes:30,
      seconds: 0
    },
    {
      text: "03:00 PM",
      hours: 15,
      minutes:0,
      seconds: 0
    },
    {
      text: "03:30 PM",
      hours: 15,
      minutes:30,
      seconds: 0
    },
    {
      text: "04:00 PM",
      hours: 16,
      minutes:0,
      seconds: 0
    },
    {
      text: "04:30 PM",
      hours: 16,
      minutes:30,
      seconds: 0
    },
    // {
    //   text: "05:00 PM",
    //   value: moment().hours(17).minutes(0).seconds(0)
    // },
    // {
    //   text: "05:30 PM",
    //   value: moment().hours(17).minutes(30).seconds(0)
    // },
    // {
    //   text: "06:00 PM",
    //   value: moment().hours(18).minutes(0).seconds(0)
    // },
    // {
    //   text: "06:30 PM",
    //   value: moment().hours(18).minutes(30).seconds(0)
    // },
    // {
    //   text: "07:00 PM",
    //   value: moment().hours(19).minutes(0).seconds(0)
    // },
    // {
    //   text: "07:30 PM",
    //   value: moment().hours(19).minutes(30).seconds(0)
    // },
    // {
    //   text: "08:00 PM",
    //   value: moment().hours(20).minutes(0).seconds(0)
    // },
    // {
    //   text: "08:30 PM",
    //   value: moment().hours(20).minutes(30).seconds(0)
    // },
    // {
    //   text: "09:00 PM",
    //   value: moment().hours(21).minutes(0).seconds(0)
    // },
    // {
    //   text: "09:30 PM",
    //   value: moment().hours(21).minutes(30).seconds(0)
    // },
    // {
    //   text: "10:00 PM",
    //   value: moment().hours(22).minutes(0).seconds(0)
    // },
    // {
    //   text: "10:30 PM",
    //   value: moment().hours(22).minutes(30).seconds(0)
    // },
    // {
    //   text: "11:00 PM",
    //   value: moment().hours(23).minutes(0).seconds(0)
    // },
    // {
    //   text: "11:30 PM",
    //   value: moment().hours(23).minutes(30).seconds(0)
    // },
  ]
  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {

    //Datos de Contacto
    data: {

      appTitle: "Debonair Cuts Barbershop",

      homeDescription: 
        `Debonair Cuts Barbershop is one of the best of all barbershops in all of Peoria where it guarantees professionalism by complying with social guidelines to protect our clients after each haircut. <br>
        Sanitation is also implemented and the motto is that you will not go the same way you came. <br>
        Debonair cuts Barbershop the place you want to be. <br>
        Do not hesitate to schedule your appointment now we will attend it professionally and you will have the best experiences in your court, because for us your court is the most important. <br>`,

        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fhome%2F1s.jpg?alt=media&token=717851e0-c6c0-4282-b247-1b3a40695176",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fhome%2F2s.jpg?alt=media&token=5fb55f18-15c2-4e8c-bdaf-c32abdd7d5e6",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fhome%2F3s.jpg?alt=media&token=56e1c4ac-362f-4932-bd6d-7ab5a16c922c",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fhome%2F4s.jpg?alt=media&token=80074304-7907-4442-ad55-0aa6c08dabf6",
        ],


      homeServices: [
        {
          name:"Taper Fade",
          time:"30 minutes",
          description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
          price: "$30"
        },
        {
          name:"Shave",
          time:"15 minutes",
          description:"Extra 15$-20$",
          price: "$25"
        },
        {
          name:"Bald Fade",
          time:"30 minutes",
          description:"High fade low fade",
          price: "$30"
        },
        {
          name:"Eyebrows",
          time:"15 minutes",
          description:"Razor line",
          price: "$10"
        },
        {
          name:"Hairline",
          time:"15 minutes",
          description:"Razor line",
          price: "$20"
        },
        {
          name:"Haircut Shave",
          time:"30 minutes",
          description:"Face treatments $15.00",
          price: "$35"
        },
        {
          name:"Kids Haircuts 1-6",
          time:"30 minutes",
          description:"Desings 15 dollars total",
          price: "$20"
        },
        {
          name:"Bald",
          time:"30 minutes",
          description:"Head Shave",
          price: "$20"
        },
        {
          name:"Mustache Trim",
          time:"15 minutes",
          description:"Trim stache",
          price: "$10"
        },
        {
          name:"Taper Fade Cut Shave Hair Wash Room",
          time:"15 minutes",
          description:"Taper Fade Shave",
          price: "$40"
        },
        {
          name:"Hair Line With Shave",
          time:"30 minutes",
          description:"Hair Line With Shave",
          price: "$25"
        },
        {
          name:"Bald Head & Beard",
          time:"30 minutes",
          description:"Razor on head & beard",
          price: "$35"
        },
        {
          name:"Taper-Line-Beard",
          time:"30 minutes",
          description:"Razor",
          price: "$30"
        },
        {
          name:"Bald Fade And Shave",
          time:"30 minutes",
          description:"Bald Fade And Shave",
          price: "$35"
        },
        {
          name:"Ages 7-and up Haircuts",
          time:"30 minutes",
          description:"Haircuts",
          price: "$20"
        },
        {
          name:"Line-Taper",
          time:"30 minutes",
          description:"Faded Sides whit line",
          price: "$20"
        },
        {
          name:"Late Fees 10 Dollars no shows 25 Dollars",
          time:"30 minutes",
          description:"Please Call me let me know what`s going on",
          price: "$25"
        },
        {
          name:"No Call No Show",
          time:"15 minutes",
          description:"If ur late",
          price: "$20"
        },
        {
          name:"Hot Facial Shave",
          time:"30 minutes",
          description:"Hot towel",
          price: "$25"
        },
        {
          name:"Face Treatment",
          time:"30 minutes",
          description:"Astringents",
          price: "$20"
        },
        {
          name:"Just A Haircut No Shave",
          time:"30 minutes",
          description:"One level",
          price: "$30"
        },
        {
          name:"Afro Shape Taper",
          time:"30 minutes",
          description:"Nice trim",
          price: "$30"
        },
        {
          name:"Afro Shape Taper Beardline",
          time:"30 minutes",
          description:"Dentail",
          price: "$35"
        },
        {
          name:"Regular Hair Cut No Shave",
          time:"30 minutes",
          description:"One Level",
          price: "$30"
        },
        {
          name:"Womens Haircut With Parts",
          time:"30 minutes",
          description:"Clean cut",
          price: "$30"
        },
        {
          name:"Taper Line",
          time:"30 minutes",
          description:"Taper sides and back no shapel",
          price: "$25"
        },
        {
          name:"Taper Sides Back Trim Hair",
          time:"35 minutes",
          description:"Taper Trim",
          price: "$30"
        },
        {
          name:"Regular Women`s Haircut",
          time:"30 minutes",
          description:"Regular Haircut",
          price: "$30"
        },
        {
          name:"Kids Line Taper",
          time:"30 minutes",
          description:"Taper Sides and back with lining",
          price: "$20"
        },
      ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "Closed",
        },
        {
          day: "Tuesday",
          hour: "06:00 am – 04:30 pm",
        },
        {
          day: "Wednesday",
          hour: "06:00 am – 04:30 pm",
        },
        {
          day: "Thursday",
          hour: "06:00 am – 04:30 pm",
        },
        {
          day: "Friday",
          hour: "06:00 am – 04:30 pm",
        },
        {
          day: "Saturday",
          hour: "06:00 am – 04:30 pm",
        },
        {
          day: "Sunday",
          hour: "Closed",
        },
      ],

      homeBreaktime: [
        {
          day: "Monday",
          hour: "Close",
        },
        {
          day: "Tuesday",
          hour: "12:00 pm – 01:15 pm",
        },
        {
          day: "Wednesday",
          hour: "12:00 pm – 01:15 pm",
        },
        {
          day: "Thursday",
          hour: "12:00 pm – 01:15 pm",
        },
        {
          day: "Friday",
          hour: "12:00 pm – 01:15 pm",
        },
        {
          day: "Saturday",
          hour: "12:00 pm – 01:15 pm",
        },
        {
          day: "Sunday",
          hour: "Close",
        },
      ],

      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fhome%2F1.mp4?alt=media&token=cd8f12c0-abb9-4419-ab7e-530ef20ee97b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fhome%2F2.mp4?alt=media&token=63e88857-be9e-4053-9109-be329fe0b755",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fhome%2F3.mp4?alt=media&token=6808c507-cb5b-4151-b3f8-973fee4cb337",

      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "319 504 8334",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F26.jpg?alt=media&token=0ce9ad50-e993-43e8-beb9-7ddb777ec47d",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F1.jpg?alt=media&token=cc9ace10-9f4f-41ef-b6e6-3c4fce07bcac",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F24.jpg?alt=media&token=da023a48-d82c-4c8f-9b84-c64ff6b18fb9",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F25.jpg?alt=media&token=b96c0973-1e7b-4ea7-89e2-b7a8900acd4d",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F10.jpg?alt=media&token=9e1c88fd-1430-4b0f-9318-d295472aec83",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F11.jpg?alt=media&token=254f413f-3dd3-4aaa-beb3-49ca800f4a97",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F12.jpg?alt=media&token=7e1f0171-7af4-480e-80e6-1815b1d858f6",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F13.jpg?alt=media&token=ab40aef5-be76-4b61-8444-18f7d174091a",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F14.jpg?alt=media&token=93789334-b700-4e1a-9ee3-16aebe126f2a",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F15.jpg?alt=media&token=a2d8f924-127c-496c-b9f4-0e7dd9e8d403",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F16.jpg?alt=media&token=5fab21da-8813-44de-ac4f-5e0eea39462c",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F17.jpg?alt=media&token=9f76254b-1d3c-4831-b710-d268362d4be8",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F18.jpg?alt=media&token=7f56060d-ca01-494b-8a85-4f19346f8ecb",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F19.jpg?alt=media&token=c68d261e-3813-450a-9d91-2d6b4d1a2f59",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F1s.jpg?alt=media&token=5afc6c1c-a695-46a4-b6fc-e869e2b93b96",
       //"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F2.jpg?alt=media&token=9aba3461-63b0-44ad-b37a-408317d26153",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F20.jpg?alt=media&token=1562cb63-fda0-46d0-a39a-eeda9d64d32b",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F2s.jpg?alt=media&token=0b675364-5df5-4b73-821f-b8e461ac83f2",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F3.jpg?alt=media&token=b1ed555e-688b-4e99-b8c1-eafd5a55db86",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F3s.jpg?alt=media&token=8502d785-bd53-4baa-b04d-e6fa62550b18",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F4s.jpg?alt=media&token=a43c7aaf-4fd0-4cc3-ac91-318244fb3cf9",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F5.jpg?alt=media&token=d6012b52-00e8-4e56-8733-cb7c555e4565",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F6.jpg?alt=media&token=848a1b01-08eb-4327-af05-24d3dc61f155",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F7.jpg?alt=media&token=f11d59a1-adbf-490f-9d60-2d2ef72bd51b",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F8.jpg?alt=media&token=cc365164-a107-47cd-8e19-d7955f750893",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F9.jpg?alt=media&token=bba8566b-e145-4dda-afcc-e6790256c87c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F21.jpg?alt=media&token=81e00ec1-fc9c-458a-8359-28fe65121c85",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F22.jpg?alt=media&token=62125e1d-60d0-4e77-b054-802a1b201703",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDebonair%20Cuts%20Barbershop%2Fgallery%2F23.jpg?alt=media&token=ab3e3d0f-e333-4711-b137-7a42eb762e51",
      
     
      
      ],

      socialNetworks: [
        {
          text: "Debonair Cuts Barbershop",
          link: "https://www.facebook.com/Rodderick23",
          icon: "logo-facebook",
        },
        {
          text: "rodderickdemond2150",
          link: "https://www.instagram.com/rodderickdemond2150/",
          icon: "logo-instagram",
        },
        {
          text: "Ranoited23",
          link: "http://book.thecut.co/Ranoited23",
          icon: "book",
        },
        {
          text: "Debonair Cuts Barbershop",
          link: "https://www.yelp.com/biz/debonair-cuts-barbershop-east-peoria",
          icon: "logo-yahoo",
        },
        {
          text: "Debonair Cuts Barbershop",
          link: "https://debonair-cuts-barbershop.business.site/",
          icon: "globe",
        },
        {
          text: "Join Square and get $1,000",
          link: "https://squareup.com/signup?country_code=mx&signup_token=F4AF9BA1BB",
          icon: "cash",
        },
      ],

      address: "422 E Washington St Suite 300, Peoria, IL 61611, United States",
      addressRoute: "422 E Washington St Suite 300, Peoria, IL 61611, United States",
      mapSource: "https://maps.google.com/maps?q=Debonair%20Cuts%20Barbershop,%20422%20E%20Washington%20St%20Suite%20300,%20Peoria,%20IL%2061611,%20Estados%20Unidos&t=&z=15&ie=UTF8&iwloc=&output=embed", 
    },

    appointmentsDays:[
      {
        day: 1,
        hours: this.hours,
        closed: true
      },
      {
        day: 2,
        hours: this.hours,
        closed: false
      },
      {
        day: 3,
        hours: this.hours,
        closed: false
      },
      {
        day: 4,
        hours: this.hours,
        closed: false
      },
      {
        day: 5,
        hours: this.hours,
        closed: false
      },
      {
        day: 6,
        hours: this.hours,
        closed: false
      },
      {
        day: 0,
        hours: this.hours,
        closed: true
      },
    ]
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
      //this.firebaseService.updateAppData(this.appData).then(t => {console.log(this.appData)}); 
  }


  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
     return this.firebaseService.getAppData().then(appData => {
        if(appData) {
          this.appData = appData as any;
          console.log(appData);
        }
      });
  }
}
