import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Rock Paper Scissors Barbershop",

      homeDescription: 
        `Rock Paper Scissors is a upscale barbershop, Focused on exceptional service and providing a relaxed and enjoyable experience.<br>
         It offers genuinely outstaznduing haircuts, bald fades, beard trim, shaves much more. <br>
         Clients here are not just a number.`,

        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fhome%2F1s.jpeg?alt=media&token=6163b417-fcda-49ed-a647-a910a9778f2e",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fhome%2F2s.jpg?alt=media&token=32b3dd9d-036e-428e-819b-de443c2d5123",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fhome%2F3s.jpg?alt=media&token=47a9e205-e8f6-4cf1-90d2-e8e229745dc7",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fhome%2F4s.jpg?alt=media&token=f701a929-68d9-4c6b-b580-ef37fddcee17",
        ],

        homeServices: [
          {
            name:"Skin Fade",
            price: "$40"
          },
          {
            name:"Razor Cut",
            price: "$45"
          },
          {
            name:"Scissor Cut",
            price: "$50"
          },
          {
            name:"Face Shave",
            price: "$40"
          },
          {
            name:"Beard Trim",
            price: "$45"
          },
          {
            name:"Beard Trim & Shave",
            price: "$50"
          },
          {
            name:"Premium Facial",
            price: "$25"
          },
          {
            name:"Facial Waxing",
            price: "$20"
          },
        ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "11:00 am – 09:00 pm",
        },
        {
          day: "Tuesday",
          hour: "11:00 am – 09:00 pm",
        },
        {
          day: "Wednesday",
          hour: "11:00 am – 09:00 pm",
        },
        {
          day: "Thursday",
          hour: "11:00 am – 09:00 pm",
        },
        {
          day: "Friday",
          hour: "11:00 am – 09:00 pm",
        },
        {
          day: "Saturday",
          hour: "11:00 am – 09:00 pm",
        },
        {
          day: "Sunday",
          hour: "12:00 am – 07:00 pm",
        },
      ],
      
      homeVideos: [
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fhome%2F1.mp4?alt=media&token=bd1cd274-8899-446d-9512-e2a837b2647d",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fhome%2F2.mp4?alt=media&token=2f49a96a-2022-4851-b471-5d16e9a5b6ab",
        ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "206 618 8045",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F7.jpeg?alt=media&token=a42f11df-9b2f-452c-9191-bdf46d2088ea",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F1.png?alt=media&token=ae6fd4dc-2be2-4976-8ff4-a066ba59901e",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F3.jpg?alt=media&token=6d8676ce-9a68-4284-b8b1-5e63d01e3ee5",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F4.png?alt=media&token=6855fb63-df34-41bf-a95f-ad9eb448b098",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F10.jpeg?alt=media&token=63873336-4a1b-4479-9423-89342eb33695",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F13.png?alt=media&token=4405b5d2-31fa-4993-8c68-cb1e0f09480f",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F14.jpg?alt=media&token=9b4099c5-8690-44e9-b497-4a38b0354055",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F15.jpeg?alt=media&token=63e09276-4216-4266-9059-d4abebba08fe",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F16.jpg?alt=media&token=bdcc6ee9-14f6-4040-8d04-745ec75e0571",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F17.jpeg?alt=media&token=282d26f4-d240-4ad0-a697-87c61eca9596",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F1s.jpeg?alt=media&token=8d45bf9e-beaa-4923-9d87-97050a77a24c",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F2s.jpg?alt=media&token=e6ad920c-61b6-4891-97da-e4cbd6941bc4",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F3s.jpg?alt=media&token=9a4e16ed-ad16-4db3-acc2-82e645401481",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F4s.jpg?alt=media&token=fb296fbd-e445-467f-8e9c-1dc0ecac4093",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F9.jpg?alt=media&token=551f166d-0690-46fa-8232-1aa7c97449ec",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F8.jpg?alt=media&token=4ae899bf-a793-41b0-9a78-3a82fa2bdea9",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F18.jpeg?alt=media&token=cfebe64e-f19d-4a6e-9dac-5661c1ed7656",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F11.jpeg?alt=media&token=11ed8864-55c7-45a8-9580-42904f20fe7c",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F12.jpeg?alt=media&token=72448397-1694-433a-9f9f-82fe364394d0",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F2.png?alt=media&token=7680db4d-44e3-4129-9ef8-34162083a1c5",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fgallery%2F20.jpeg?alt=media&token=f1926ed4-0e1b-48ef-af2d-a590ce74e576",
       
      ],

      products:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fproducts%2Fp1.JPG?alt=media&token=d8b28758-d7e9-4629-8740-fda8cfd5ddd2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fproducts%2Fp10.JPG?alt=media&token=0ad54d00-8154-4f87-b7fb-08f25ead0510",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fproducts%2Fp2.JPG?alt=media&token=5c2c81cb-ec32-497a-b3ac-7e6be85459e5",
        //"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fproducts%2Fp3.JPG?alt=media&token=66a49149-100d-480d-b89d-d3aa1cf49068",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fproducts%2Fp4.JPG?alt=media&token=b849d874-6def-4eef-bbaf-62abeb2972c4",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fproducts%2Fp5.JPG?alt=media&token=a8942ee6-1c11-485d-aece-6468fbc2edaf",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fproducts%2Fp6.JPG?alt=media&token=8e94e7d6-0c70-4a5b-88fa-8c275d26796e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fproducts%2Fp7.JPG?alt=media&token=d2854622-4bd8-4a0b-9b5a-800a08e68ee3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fproducts%2Fp8.JPG?alt=media&token=46e40ecf-89d7-49e6-972a-533dd46f03d8",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fproducts%2Fp9.JPG?alt=media&token=efd05693-9928-434e-893f-711ec9f8d596",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FRock%20Paper%20Scissors%20Barbershop%2Fproducts%2F11.jpeg?alt=media&token=a19031a7-38ef-4855-86be-23d645c5d03e",
      ],


      socialNetworks: [
        {
          text: "rockpaperscissorsbarber",
          link: "https://www.instagram.com/rockpaperscissorsbarber/",
          icon: "logo-instagram",
        },
        {
          text: "Rock Paper Scissors Barbershop",
          link: "https://www.facebook.com/Rock-Paper-Scissors-Barbershop-104532291589694/",
          icon: "logo-Facebook",
        },
        {
          text: "Rock Paper Scissors",
          link: "https://www.yelp.com/biz/rock-paper-scissors-bothell-2",
          icon: "logo-yahoo",
        },
        {
          text: "Rock Paper Scissors Barbershop Web",
          link: "https://rockpaperscissorsbarbershop.godaddysites.com/?fbclid=IwAR0GS7iHq4qiMh4G_eantVfD8emP91GSUpk3LWehSsjqeYSw-LuWiv94JBc",
          icon: "globe",
        }
      ],

      address: "22627 Bothell Everett Hwy Suite #124, Bothell, WA 98021, USA",

      addressRoute:"22627 Bothell Everett Hwy Suite #124, Bothell, WA 98021, USA",

      mapSource: "https://maps.google.com/maps?q=rock%20paper%20scissors%20barber%20shop,%20%20Bothell,%20WA%2098021&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

  //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
