import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { productsPage } from './products/products.page';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class productPage implements OnInit {

  d: any;

  images = [
    '../assets/images/30.jpg',
    '../assets/images/31.jpg',
    '../assets/images/32.jpg',
    '../assets/images/33.jpg',
    '../assets/images/34.jpg',
    '../assets/images/35.jpg',
    '../assets/images/36.jpg',
    '../assets/images/37.jpg',
    '../assets/images/38.jpg',
    '../assets/images/39.jpg',
    '../assets/images/40.jpg',
    '../assets/images/41.jpg',
    '../assets/images/42.jpg',
    '../assets/images/43.jpg',
    '../assets/images/44.jpg',
    '../assets/images/45.jpg',
    '../assets/images/46.jpg',
    '../assets/images/47.jpg',
    '../assets/images/48.jpg',
  ];

  constructor(private modalController: ModalController, public data: DataService) { 
    this.d = data.appData.data;
  }

  ngOnInit() {
  }

  async openImage(i: number)
	{
    const modal = await this.modalController.create({
      component: productsPage,
      cssClass: 'modal-transparency',
      componentProps : {
        index: i+1,
        images: this.images,
      }
    });
    await modal.present();
	}

}
 