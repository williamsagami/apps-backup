import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "YANN-BARBER",

      appTitle1: "DAYANBARBER",

      homeDescription: 
        `One of the best barbers in Florida. <br>
        With 4 years of experience giving extraordinary service and impressive quality, seeking daily customer satisfaction. <br> 
        We want our services to be a wonderful experience for our clients and have an impressive visit with us. <br>
        Because your cut is the most important thing for you and all this with amazing prices. <br>
        Do not hesitate to call us or schedule your appointment to have an unimaginable haircut.`,

        homeDescription1: 
        `One of the best barbershops in Florida We have very experienced specialists that will give you the best haircut.
        We will give you an excellent Service with fast and efficient modern machines at nice prices. <br>
        We have a  family friendly so you could bring your whole family and share a 10/10 experience <br>
        Do not hesitate to call and schedule your appointment now!`,

      homeServices: [
        {
          name:"Signature Haircut",
          price: "$30"
        },
        {
          name:"Haircut",
          price: "$25"
        },
        {
          name:"Beard Shaving/Hot Towel",
          price: "$18"
        },
        {
          name:"Beard Shaving/Hot Towel",
          price: "$18"
        },
        {
          name:"Beard Trim",
          price: "$15"
        },
        {
          name:"Kids Haircut",
          price: "$20"
        },
        {
          name:"FULL SERVICE",
          price: "$35"
        }
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "239 300 3757",
        },
        {
          text: "Phone",
          number: "239 231 6917",
        }
      ],

      socialNetworks: [
        {
          text: "yannbarber",
          link: "https://www.instagram.com/yannbarber/?hl=es",
          icon: "logo-instagram",
        },
        {
          text: "Yann-barber",
          link: "https://www.facebook.com/yanndom83",
          icon: "logo-facebook",
        },
        {
          text: "Yann-barber Web",
          link: "https://yandbarbers.wixsite.com/yandd?fbclid=IwAR3_pzEydYIcbyKPQQ5tq7UiMFMMA4yWCceQny8BCWlfnbesRYFZyVkQ3OY",
          icon: "globe",
        },  
        {
          text: "yd_barbers",
          link: "https://www.instagram.com/yd_barbers/",
          icon: "logo-instagram",
        },
        {
          text: "yndbarbers web",
          link: "https://yndbarbers.com/",
          icon: "globe",
        }
      ],

      address: "1333 3rd Ave S #307, Naples, FL 34102, USA",

      addressRoute:"1333 3rd Ave S #307, Naples, FL 34102, USA",

      mapSource: "https://maps.google.com/maps?q=YANN-BARBER&t=&z=15&ie=UTF8&iwloc=&output=embed",
      
      localitation: "Bayfront Professional Center 1333 3rd Ave S, Suite 309 Naples, FL 34102",

      addressRoute1:"1333 3rd Ave S #307, Naples, FL 34102, USA",

      mapSource1: "https://maps.google.com/maps?q=yndbarbers%20Bayfront%20Professional%20Center%201333%203rd%20Ave%20S,%20Suite%20309%20Naples,%20FL%2034102&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

 this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
