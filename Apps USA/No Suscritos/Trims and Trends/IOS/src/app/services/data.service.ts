import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Trims & Trends",

      homeDescription: 
        `Searching for a place to get affordable hair cut in Downtown LA? <br>
        Trims & Trends is the stylist & barber for everyone! Providing the most modern and innovative services in hair care. <br>
        We have been in the industry for 15 years and have experience with styling every type of hair. <br>
        We offer our customers a wide range of hairstyles, treatments, products that reflect their individuality, uniqueness, and personal taste. <br>
        Stop by today and schedule your next appointment!`,


        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fhome%2F11_1024x1024.jpg?alt=media&token=ea193c26-ca7b-44c6-9401-ade72288229f",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fhome%2F12_1024x1024.jpg?alt=media&token=35a38bbb-04f4-424d-b976-6b3ee5b64b7b",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fhome%2F13_1024x1024.jpg?alt=media&token=9f3aa781-1ede-44af-9e1e-0741d15d455b",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fhome%2F14_1024x1024.jpg?alt=media&token=1e4b4110-4bc2-4c7b-a29f-cf15f875c5ed",
        ],


        homeServices: [
          {
            name:"Short Hair Cut",
            time:"30 Minute",
           // description:"Utilization of clippers and scissors (including texturizing shears), optional shaving around the hair edges and optional pomade application.",
           // price: "$37"
          },
          {
            name:"Face mask",
            time:"10 minutes",
           //  description:"Haircut & style followed by massage therapy utilizing our percussive handheld device over the shoulders for relief & relaxation.",
          //  price: "$10"
          },
          {
            name:"Under eye patch",
            time:"10 minutes",
           // description:"For this classic & delicate service, we utilize fine shears (including texturizing shears) to cut the hair to the desired length.",
           // price: "$10"
          },
          {
            name:"Hair treatment and Blow-dry",
            time:"1 hour 15 minutes",
            //description:"Two hot towels, hot lather, and gentle straight-edge razor shave around the scalp, lime aftershave & balm is then applied.",
            //price: "$65"
          },
          {
            name:"Hair Treatment",
            time:"40 minutes",
          //  description:"From low tapers to high fades, we give you our closest, cleanest and long lasting skin fades with state of the art instruments.",
           // price: "$45"
          },
          {
            name:"Hot Towel Shave",
            time:"45 minutes",
           //description:"For special occasions, sometimes you need service in a pinch. Please TEXT (310) 988-5675 to inquire about before/after hours and same day services.",
            //price: "$45"
          },
          {
            name:"Color refresh (toner)",
            time:"1 hour 30 minutes",
           //description:"Experience the tradition of fresh haircut service for you and your son.",
           // price: "$65"
          },
          {
            name:"Airtouch Technique (color)",
            time:"6 hours",
            //description:"Contact us to set a date for a Master Barber to come to you. Prices may vary in accordance with services and distance.",
            //price: "$500"
          },
          {
            name:"Buzz Cut",
            time:"30 minutes",
           //description:"Treat yourself and a family member or friend for a haircut service.",
           // price: "$20"
          },
          {
            name:"Beard Trim",
            time:"15 minutes",
            //description:"Contact us to set a date for a Master Barber to come to you. Prices may vary in accordance with services and distance.",
           // price: "$10"
          },
          {
            name:"Sides touch up (cut or fade)",
            time:"30 minutes",
            //description:"Treat yourself and a family member or friend for a haircut service.",
          //  price: "$25"
          },
          {
            name:"Sides Clean up (clean up around ears and neck)",
            time:"10 minutes",
            //description:"Contact us to set a date for a Master Barber to come to you. Prices may vary in accordance with services and distance.",
          //  price: "$10"
          },
          {
            name:"Bang trim",
            time:"15 minutes",
            //description:"Treat yourself and a family member or friend for a haircut service.",
           // price: "$10"
          },
          {
            name:"Blow Dry",
            time:"45 minutes",
            //description:"Treat yourself and a family member or friend for a haircut service.",
          //  price: "$46"
          },
          {
            name:"Color roots",
            time:"2 Hours",
            //description:"Treat yourself and a family member or friend for a haircut service.",
          //  price: "$85"
          },
          {
            name:"Highlights",
            time:"3 Hours",
            //description:"Treat yourself and a family member or friend for a haircut service.",
           // price: "$165"
          },
          {
            name:"Color complete",
            time:"2 Hours 30 Minutes",
            //description:"Treat yourself and a family member or friend for a haircut service.",
          //  price: "$165"
          },
          {
            name:"Ombré",
            time:"3 Hours 30 Minutes",
            //description:"Treat yourself and a family member or friend for a haircut service.",
          //  price: "$200"
          },
          {
            name:"Man Color",
            time:"1 Hour",
            //description:"Treat yourself and a family member or friend for a haircut service.",
           // price: "$45"
          },
          {
            name:"Color base + highlights",
            time:"4 Hours",
            //description:"Treat yourself and a family member or friend for a haircut service.",
         //   price: "$185"
          },
          {
            name:"Balayage",
            time:"4 Hour",
            //description:"Treat yourself and a family member or friend for a haircut service.",
          //  price: "$180"
          },
          {
            name:"Makeup",
            time:"45 minutes",
            //description:"Treat yourself and a family member or friend for a haircut service.",
           // price: "$60"
          },
          {
            name:"Hair Updo",
            time:"30 minutes",
            //description:"Treat yourself and a family member or friend for a haircut service.",
           // price: "$65"
          },
        ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "9:00 AM – 5:00 PM",
        },
        {
          day: "Tuesday",
          hour: "9:00 AM – 5:00 PM",
        },
        {
          day: "Wednesday",
          hour: "9:00 AM – 5:00 PM",
        },
        {
          day: "Thursday",
          hour: "9:00 AM – 5:00 PM",
        },
        {
          day: "Friday",
          hour: "9:00 AM – 5:00 PM",
        },
        {
          day: "Saturday",
          hour: "Closed",
        },
        {
          day: "Sunday",
          hour: "Closed",
        },
      ],
      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fhome%2F1.mp4?alt=media&token=722d1af8-78ae-43c3-b5ab-61dd55a6b961",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fhome%2F2.mp4?alt=media&token=bcb468a2-23c4-41b1-a507-901da60d4db0",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fhome%2F3.mp4?alt=media&token=dcd60244-b4cf-4615-903e-90db30a5a5ef",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "213 372 5072",
        },
      ],

      gallery:[
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F12_1024x1024.jpg?alt=media&token=45dc946c-fc40-421a-a398-567a655847eb",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F1_1024x1024.jpg?alt=media&token=7c49c8e5-877a-45b2-88eb-e8a07c224263",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F11_1024x1024.jpg?alt=media&token=a9d1d14c-528a-45cf-b626-223530ef7987",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F2_1024x1024.jpg?alt=media&token=6bd2aa52-ce45-4485-a1d6-fd9022797a65",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F22_1024x1024.jpg?alt=media&token=42fbc659-60aa-4347-91a5-ec74d243a874",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F10_1024x1024.jpg?alt=media&token=495302df-dd76-4be0-bc4b-93ebcf2b80a3",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F15_1024x1024.jpg?alt=media&token=748f65b5-1bb0-4baf-8fd1-0579423d98f0",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F5_1024x1024.jpg?alt=media&token=c7a53141-1fb7-4bfe-b553-c88adc217cfb",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F19_1024x1024.jpg?alt=media&token=af74615a-5deb-4ac4-b89d-a6d9bce24921",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F20_1024x1024.jpg?alt=media&token=1aeeed0a-d06e-4102-baf6-63ead4e1ad9d",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F14_1024x1024.jpg?alt=media&token=7025bd9e-af0a-4be5-9a6e-1f9db280da39",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F18_1024x1024.jpg?alt=media&token=6e37f2af-30f2-43ce-85a7-45ddc53de280",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F24_1024x1024.jpg?alt=media&token=1c4d8dd5-e21e-420c-887c-9748f14b03bf",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F17_1024x1024.jpg?alt=media&token=45882985-eed3-4424-a76a-686fb4e333b3",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F7_1024x1024.jpg?alt=media&token=2d457ff1-f5ec-4e8a-a92a-0002e5ccbecf",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F4_1024x1024.jpg?alt=media&token=2d67a1ab-972d-45d8-8731-8148d1f7bcca",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F16_1024x1024.jpg?alt=media&token=7687b334-ca9e-4410-a9ba-6601dd9b27f0",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F8_1024x1024.jpg?alt=media&token=d6646fa7-9061-42cd-bc97-19ca8c7b07de",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F9_1024x1024.jpg?alt=media&token=149e19d9-d509-48e6-b52b-5c887c58cac0",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F23_1024x1024.jpg?alt=media&token=52bcb15d-7e95-42ec-9a5c-c23669eda82d",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F6_1024x1024.jpg?alt=media&token=0a00e11f-094b-4070-9f87-cc232186d9d5",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F21_1024x1024.jpg?alt=media&token=d37a1685-2656-4266-8487-303ca102aad6",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FTrims%20%26%20Trends%2Fgallery%2F3_1024x1024.jpg?alt=media&token=f2dde0d3-fb47-4434-a286-14784e747667",
      ],

      products:[
      
       ],

      socialNetworks: [
        {
          text: "Trims & Trends",
          link: "https://www.yelp.com/biz/vivian-crooks-trims-and-trends-los-angeles",
          icon: "logo-yahoo",
        },
        {
          text: "trimsandtrends",
          link: "https://www.instagram.com/trimsandtrends/?hl=en",
          icon: "logo-instagram",
        },
        {
          text: "Vivian Crooks-Trims & Trends",
          link: "https://www.facebook.com/trimsandtrens/",
          icon: "logo-facebook",
        },
      ],

      address: "444 South Flower St suite 120, Los Angeles, CA 90071",

      addressRoute: "444 South Flower St suite 120, Los Angeles, CA 90071",

      mapSource: "https://maps.google.com/maps?q=Vivian%20Crooks-Trims%20&%20Trends&t=&z=15&ie=UTF8&iwloc=&output=embed",
      
      // barbers:[
      //   {
      //     name: "Edgar",
      //     description: "Veteran Barber",
      //     socialNetworks:[
      //     ],
      //     image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fappointments%2Fb1_512x512.jpg?alt=media&token=924bf9bf-ea69-4d97-a604-de279875500f",
      //   },
      //   {
      //     name: "Jackie",
      //     description: "Profesional Barber",
      //     socialNetworks:[
  
      //     ],
      //     image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fappointments%2Fb2_512x512.jpg?alt=media&token=f1950044-58ae-4011-a70a-eb48fa32f083",
  
      //   },
      //   {
      //     name: "Israel",
      //     description: "Master Barber",
      //     socialNetworks:[
  
      //     ],
      //     image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fappointments%2Fb4_512x512.jpg?alt=media&token=1d8cf132-43fe-459d-9f5e-cbbb44f0f9a6",
  
      //   },
  
      //   {
      //     name: "Mya",
      //     description: "stylist specialist",
      //     socialNetworks:[
  
      //     ],
      //     image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fappointments%2Fb3_512x512.jpg?alt=media&token=5e3ce5b9-e486-4d0f-9bc5-70c5e310c98e",
      //   },
      // ],
    }

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 
    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
    //  this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)}); 
  }



  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
