import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { post, FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-update-post',
  templateUrl: './update-post.page.html',
  styleUrls: ['./update-post.page.scss'],
})
export class UpdatePostPage implements OnInit {

  post: any;
  create = false;
  title = "";
  sale = "";
  content = "";
  adminPosts: any;

  private loading: any;

  constructor( 
    private firebaseService: FirebaseService,
    private modalController: ModalController, 
    private navParams: NavParams,  
    private toastController: ToastController,
    private alertController: AlertController,
    private loadingController: LoadingController) { 
      this.post = navParams.get('post');
      this.create = navParams.get('create');
      this.adminPosts = navParams.get('adminPosts');

      //console.log(this.post);
  }

  ngOnInit() {
    if(!this.create) {
      this.title = this.post.title;
      this.sale = this.post.sale;
      this.content = this.post.content;
    }
  }

  closeView(){
	  this.modalController.dismiss();
  }

  async showInfo(info: string) {
    const toast = await this.toastController.create({
      message: info,
      duration: 6000,
      color: 'secondary',
    });
    toast.present();
  }

  allowPost() {
    return (this.title !== "" && this.content !== "");
  }

  createPost() {
    this.loadingController.create({
      message: 'Loading...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      if(this.allowPost()) {
        var post = {} as post;
        post.id = 'Undefined';
        post.title = this.title;
        post.sale = this.sale;
        post.content = this.content;
        post.postNumber = this.adminPosts.postCount;
  
        this.firebaseService.createPost(post, this.adminPosts).then(close => {
          this.loading.dismiss();
          this.closeView();
          this.showInfo("Post Created Successfully.");
        });
      } else {
        this.showInfo("You must fill all the fields.");
        this.loading.dismiss();
      }
    });
  }

  updatePost() {
    this.loadingController.create({
      message: 'Loading...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      if(this.allowPost()) {
        this.post.title = this.title;
        this.post.sale = this.sale;
        this.post.content = this.content;
  
        this.firebaseService.updatePost(this.post).then(close => {
          this.loading.dismiss();
          this.closeView();
          this.showInfo("Post has been Updated.");
        });
      } else {
        this.showInfo("You must fill all the fields.");
        this.loading.dismiss();
      }
    });
  }

  async deleteConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'cool-alert',
      header: "Delete Post",
      message: "Are you sure you want to Delete this Post?",
      buttons: [
        {
          text: "Cancel",
          role: 'cancel',
          cssClass: 'secondary',
        }, 
        {
          text: "Delete",
          handler: () => {
            this.deletePost();
          }
        }
      ]
    });

    await alert.present();
  }

  deletePost() {
    this.loadingController.create({
      message: 'Loading...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      this.firebaseService.deletePost(this.post, this.adminPosts).then(close => {
        this.loading.dismiss();
        this.closeView();
        this.showInfo("Post has been Deleted.");
      });
    });
  }
}
