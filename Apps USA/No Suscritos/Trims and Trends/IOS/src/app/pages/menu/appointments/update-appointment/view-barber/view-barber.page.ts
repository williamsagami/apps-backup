import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-view-barber',
  templateUrl: './view-barber.page.html',
  styleUrls: ['./view-barber.page.scss'],
})
export class ViewBarberPage implements OnInit {

  barber:any;


  constructor(
    public data: DataService,
    private modalController: ModalController,
    private navParams: NavParams, 
  ) {
    this.barber = navParams.get('barber'); 
   }

  ngOnInit() {
  }

  closeView(){
	  this.modalController.dismiss();
  }

  

}
