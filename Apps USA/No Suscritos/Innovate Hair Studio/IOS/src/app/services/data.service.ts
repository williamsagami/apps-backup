import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Innovate Hair Studio",

      homeDescription: 
        `Innovhst is a hair studio for men and woman a combination of both worlds. <br>
        we bring out the best when it comes to hair we do amazing fades , tapers, long hair styles and short witch ever makes our clients happy. <br>
        we bring out the best in all our services we also do male hair pieces and woman extensions. <br>
        contact us for a consultation today.`,


        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fhome%2F1s.jpg?alt=media&token=0eac9cf0-cc8a-424d-a816-9b379a1964a5",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fhome%2F2s.jpg?alt=media&token=45046908-b568-47cd-a19a-d511a6145e4f",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fhome%2F3s.jpg?alt=media&token=f586c28d-b8a7-4a97-b3b6-67f355cf5125",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fhome%2F4s.jpg?alt=media&token=ac42b89c-3a43-4ad6-9550-5ea865847060",
        ],


      homeServices: [
        {
          name:"house call / Hotel call",
          price: "$200"
        },
        {
          name:"Hair styling only",
          price: "$30"
        },
        {
          name:"A-Regular Hair cut",
          price: "$35"
        },
        {
          name:"Kids cut up to 10 years old.",
          price: "$30"
        },
        {
          name:"Hot towel Head shave only",
          price: "$50"
        },
        {
          name:"Perm for curls",
          price: "$50"
        },
        {
          name:"A-VIP full service",
          price: "$100"
        },
        {
          name:"A-Senior price 65 year +",
          price: "$20"
        },
        {
          name:"In person consultation",
          price: "$25"
        },
        {
          name:"Facial service",
          price: "$25"
        },
        {
          name:"Hair treatment",
          price: "$25"
        },
        {
          name:"Long hair cut",
          price: "$85"
        },
        {
          name:"Beard Trim & Style",
          price: "$45"
        },
        {
          name:"All Scissor mens haircut",
          price: "$45"
        },
        {
          name:"Nose & Ear Hair -wax service",
          price: "$20"
        },
        {
          name:"Eyebrow Trim",
          price: "$25"
        },

        {
          name:"Blck Mask",
          price: "$25"
        },
        {
          name:"Hot towel shave",
          price: "$50"
        },
        {
          name:"Detail Fade —“side line up enhancement” optional",
          price: "$45"
        },

        {
          name:"Hair wash",
          price: "$20"
        },
        {
          name:"Bleach hair",
          price: "$75"
        },
        {
          name:"A-Hair cut & beard trim combo with hot towel",
          price: "$60"
        },
      ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "09:00 am – 07:00 pm",
        },
        {
          day: "Tuesday",
          hour: "09:00 am – 07:00 pm",
        },
        {
          day: "Wednesday",
          hour: "09:00 am – 07:00 pm",
        },
        {
          day: "Thursday",
          hour: "09:00 am – 07:30 pm",
        },
        {
          day: "Friday",
          hour: "09:00 am – 07:00 pm",
        },
        {
          day: "Saturday",
          hour: "09:00 am – 07:00 pm",
        },
        {
          day: "Sunday",
          hour: "Close",
        },
      ],

      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fhome%2F1.mp4?alt=media&token=b346a118-d217-4ba7-ab25-e0e936f6cf84",
        
      ],
      audios: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fhome%2F3.mp3?alt=media&token=34a89416-adbd-4054-8d16-7e64debca5ee",
      ],
      audios1: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fhome%2F2.mp3?alt=media&token=64c5aeb5-7391-46db-b87a-1724b48c643c",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "424 263 2722",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F9.jpg?alt=media&token=515cf415-67a4-4fa4-b4db-9e0133bd7100",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F18_512x512.jpg?alt=media&token=f4686612-5099-4133-acef-8e1b48ac6cef",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F1.jpg?alt=media&token=caf64037-5f1b-45f3-960c-4099166ec2f8",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F5.jpg?alt=media&token=24ea180e-bffd-4f9a-9dd4-0dbef8405ba6",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F8.jpg?alt=media&token=f89c6ee9-0fcf-4500-8581-bec29fd81b62",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F7.jpg?alt=media&token=a04d3c52-a1df-418b-b905-7dd394b808c2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F10.jpg?alt=media&token=46520252-8eab-4a01-b4ae-e01693eecee6",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F11.jpg?alt=media&token=c3982878-143d-440b-ba8c-d8bf2e34a165",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F1s.jpg?alt=media&token=6150d163-91da-4846-92aa-f7c793fc2e5c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F2.jpg?alt=media&token=457f2665-887e-4f4d-a356-59f8b50116a9",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F2s.jpg?alt=media&token=272a7eac-2256-4768-b25e-d7b0bf542c33",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F3s.jpg?alt=media&token=ab02c144-268c-471c-94f5-e7b0b45ead6a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F4.jpg?alt=media&token=e4c1cddb-ad05-4bad-819f-4d61b874dc5d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F4s.jpg?alt=media&token=28496f9b-5acc-4a39-aad3-a1d2699eb0a8",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F6.jpg?alt=media&token=718fc686-b1be-4d6b-bc0a-c9384b1ac092",             
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F12_512x512.jpg?alt=media&token=35bf71c7-0cde-4788-9940-9c4a8acb874a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F13_512x512.jpg?alt=media&token=52a2d9bd-d8cc-4585-8795-6e8c3cf6aec7",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F14_512x512.jpg?alt=media&token=0f1d786e-e45c-41e7-b5e6-eb45b2774166",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F15_512x512.jpg?alt=media&token=50efaeb3-add0-4587-ba8a-a412e28b651a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F16_512x512.jpg?alt=media&token=f3b95647-b5fd-4fbc-8c7f-c2703bf2f968",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fgallery%2F17_512x512.jpg?alt=media&token=614c5348-6d66-413c-9a42-6f6082c938f1",
      ],

      products:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp1.png?alt=media&token=65c973ba-6374-49f0-914b-4476232a5c5b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp2.png?alt=media&token=8fa01a72-0692-4945-b528-4748c0940088",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp3.png?alt=media&token=72e45c68-3b10-4423-9f49-1ae14d0fead5",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp4.png?alt=media&token=f2c20570-2ebc-49e4-98ab-a2e26b342bcd",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp5.png?alt=media&token=1cb07fc4-877a-42d4-bf30-997f42bbee3d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp6.png?alt=media&token=bc8c0c6b-114a-4f26-8a45-ca5ac4bbcfea",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp8.png?alt=media&token=70d96eaa-283b-43cb-958a-55c9c86b1690",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp9.png?alt=media&token=694706e3-e329-4e51-ab03-788ce26b23bc",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp10.png?alt=media&token=f23db676-b438-4737-89a1-2576c669deea",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp11.png?alt=media&token=0222a624-6119-4e0e-9d21-deaeb1974786",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp12.png?alt=media&token=73d3c3a2-57ae-4830-8b3c-93a3feb2ec81",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp13.png?alt=media&token=679cf679-1341-497e-9fa7-ba1a5f1b51c6",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp14.png?alt=media&token=93acec95-01a6-4d94-b90b-b0635c3351a7",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp16.png?alt=media&token=77cd29e2-631b-4d0c-85da-7918530d8736",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp17.png?alt=media&token=0f47e458-5f38-4d8a-87bd-5c8e6b1cf111",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp18.png?alt=media&token=6ac11a24-6169-4b91-a58b-703d538e61b3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp19.png?alt=media&token=658a63af-9651-41ec-8df6-52cbd2a7e1db",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp20.png?alt=media&token=3ab008dc-ae98-4115-a853-df1a94d0d8f2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp21.png?alt=media&token=e8bd7f1d-3416-4c7f-b08a-e16cfa99927f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp22.png?alt=media&token=e647a537-6657-4291-ab5e-d40e7b710a51",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp23.png?alt=media&token=b742e22b-8fab-4893-aa97-39df8f034464",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp24.png?alt=media&token=2676b5e6-b1b6-48ae-bed6-1f6b86590274",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp25.png?alt=media&token=cefbb18d-6fe0-4644-a032-2aca532eb598",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp26.jpeg?alt=media&token=e1db97e6-f04f-4f35-b257-31e9f498e4dd",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp27.jpg?alt=media&token=aa2cccb3-1530-4afe-b204-5c7f3b94f12f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp30.jpg?alt=media&token=f2ce704c-6401-4b45-8562-647954359dd2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp31.jpg?alt=media&token=7854e0fb-940d-4385-b0ac-8f8f7dd9143d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp33.jpg?alt=media&token=55ab5021-c259-4304-affc-6deca425b7f4",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp34.jpg?alt=media&token=d656f721-8d88-405a-86e0-b062c0bcd31b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp35.jpg?alt=media&token=e1b5e053-f7d6-4b41-83d0-7fe8ce116bbf",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp36.jpg?alt=media&token=9774f5b9-5c2d-4bf6-8476-87ce9aa46626",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp37.png?alt=media&token=4daf0f5d-ba80-4b4d-b3e0-48b869461c6e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp37.1.png?alt=media&token=881e4d83-da0f-4a23-9e38-1461e489f1a5",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp38.jpeg?alt=media&token=c7941876-4981-4067-a327-0c9056e2b884",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp39.jpeg?alt=media&token=0bb76276-29af-47a5-90e1-485ec3cd1570",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FInnovate%20Hair%20Studio%2Fproducts%2Fp40.png?alt=media&token=d1c72e8f-2389-461c-bd24-e70650171478",
      ],

      socialNetworks: [
        {
          text: "Innovate Hair Studio",
          link: "https://www.facebook.com/Innovhst",
          icon: "logo-facebook",
        },
        {
          text: "innovhst",
          link: "https://www.instagram.com/innovhst/",
          icon: "logo-instagram",
        },
        {
          text: "innovhst",
          link: "https://innovhst.com/",
          icon: "globe",
        },
        {
          text: "rev-320 Products",
          link: "https://rev-320.com/",
          icon: "cash",
        },

      ],

      address: "1905 Pacific Coast Highway, Lomita, California 90717, United States",

      addressRoute:"1905 Pacific Coast Highway, Lomita, California 90717, United States",

      mapSource: "https://maps.google.com/maps?q=Innovate%20Hair%20Studio,%201905%20Pacific%20Coast%20Hwy,%20Lomita,%20CA%2090717,%20Estados%20Unidos&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

  //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
