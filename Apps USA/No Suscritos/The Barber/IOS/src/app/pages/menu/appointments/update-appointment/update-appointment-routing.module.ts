import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateAppointmentPage } from './update-appointment.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateAppointmentPage
  },
  {
    path: 'view-barber',
    loadChildren: () => import('./view-barber/view-barber.module').then( m => m.ViewBarberPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateAppointmentPageRoutingModule {}
