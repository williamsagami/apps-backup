import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {

    //Datos de Contacto
    data: {

      appTitle: "The Barber",

      homeDescription: 
        `The barber, one of the best barbershops in San Fernando qualified as quality professional service where they are progressing together as individuals and as barbers, 
        a traditional barbershop. <br>
        With affordable prices for our quality that we provide, 
        our professional barbers will make you have spectacular hair and you will have a unique experience, 
        do not hesitate to bring your family and they will all have a cut with prices Adults and children and everyone will have a professional and perfect cut. <br>
        Schedule your appointment NOW for a UNIQUE experience`,

        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fhome%2F1_512x512.jpg?alt=media&token=acd7279d-c10f-4eb5-9e32-0307e1374333",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fhome%2F2_512x512.jpg?alt=media&token=20995db7-c623-4ed6-a02c-d358c529d9cf",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fhome%2F14_512x512.jpg?alt=media&token=518851c8-4386-484f-b00e-746a298146b8",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fhome%2F6_512x512.jpg?alt=media&token=305854f0-31b9-427b-86ed-af9413213096",
        ],


      homeServices: [
        {
          name:"Adult Haircut",
          time:"30 minutes",
          //description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
          price: "$35"
        },
        {
          name:"Kids Haircut",
          time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$30"
        },
        {
          name:"Men’s Haircut & Beard",
          time:"60 minutes",
         // description:"Extra 15$-20$",
          price: "$50"
        },
        {
          name:"Beard Only",
          time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$30"
        },
        {
          name:"Head Shave & Beard",
          time:"45 minutes",
         // description:"Extra 15$-20$",
          price: "$45"
        },
      ],
      
      homeSchedules: [
        {
          day: "Monday",
          hour: "Closed",
        },
        {
          day: "Tuesday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Wednesday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Thursday",
          hour: "09:00 am – 07:00 pm",
        },
        {
          day: "Friday",
          hour: "08:00 am – 08:00 pm",
        },
        {
          day: "Saturday",
          hour: "08:00 am – 05:00 pm",
        },
        {
          day: "Sunday",
          hour: "11:00 am – 03:00 pm",
        },
      ],

      // homeBreaktime: [
      //   {
      //     day: "Monday",
      //     hour: "Close",
      //   },
      //   {
      //     day: "Tuesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Wednesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Thursday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Friday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Saturday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Sunday",
      //     hour: "Close",
      //   },
      // ],

      
      homeVideos: [
      "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fhome%2F1.mp4?alt=media&token=dd4f6fe7-e5ec-4c8d-ad66-bc9a083a3939",
      "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fhome%2F2.mp4?alt=media&token=1e13b252-eff7-43bb-a54b-84441d49b8d4",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "747 999 5784",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F10_512x512.jpg?alt=media&token=9bd45ad0-1499-4953-9611-0f68b3c2fbd8",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F11_512x512.jpg?alt=media&token=1f407eff-2f8a-4295-b9c7-8b7f1252a5f7",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F12_512x512.jpg?alt=media&token=f5b3a246-9b5a-40a1-bde3-039f64ae0f25",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F13_512x512.jpg?alt=media&token=20222545-f7e8-4a09-affe-cca3344eaf9e",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F14_512x512.jpg?alt=media&token=e16e5811-0939-455c-8273-3958169869d3",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F15_512x512.jpg?alt=media&token=0fc3bfe7-606b-4ff2-a4cc-478465bcd58c",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F16_512x512.jpg?alt=media&token=dd8ed14c-d835-4fbc-8458-64eb8e9d395d",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F17_512x512.jpg?alt=media&token=66bce2f0-6e88-4d99-8cfe-38052fbf6ead",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F18_512x512.jpg?alt=media&token=29f85f33-1c5a-4562-ae3c-e1868728ff4e",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F19_512x512.jpg?alt=media&token=3c4ec968-1bc1-4836-b8dd-0d6ee42b9fdd",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F1_512x512.jpg?alt=media&token=9bd8fd39-53b4-47fb-8fa7-b91605e622c0",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F20_512x512.jpg?alt=media&token=f2982e84-2c39-4d72-8a0e-f694a7eec872",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F21_512x512.jpg?alt=media&token=73cd81b4-9216-404b-94d7-b531fbaa504e",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F2_512x512.jpg?alt=media&token=8fac5c5e-97fe-4453-affd-52f06e2d0680",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F3_512x512.jpg?alt=media&token=07bf71d1-b2bb-4ffd-88a3-ec6b4b5297f0",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F4_512x512.jpg?alt=media&token=c2f68aa6-8439-4a9d-9728-f6701012d813",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F5_512x512.jpg?alt=media&token=0a440d61-9d9f-42a9-9e08-4ed2dd4416e5",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F6_512x512.jpg?alt=media&token=330e16f3-3e98-44d7-890a-b69955a558de",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F7_512x512.jpg?alt=media&token=c26e3a68-f75b-4755-9b96-6e4871691c0c",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F8_512x512.jpg?alt=media&token=7ac90e75-df87-4f2f-95f5-c57d56a611ab",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FThe%20Barber%2Fgallery%2F9_512x512.jpg?alt=media&token=d3ebd6ba-d4b3-4ab0-86cd-95d828e780de",
      ],

      socialNetworks: [
        {
          text: "thebarbersf",
          link: "https://www.instagram.com/thebarbersf/",
          icon: "logo-instagram",
        },
        {
          text: "The Barber",
          link: "https://booksy.com/en-us/86864_the-barber_barber-shop_101731_san-fernando",
          icon: "book",
        },
        {
          text: "TheBarber",
          link: "https://www.facebook.com/TheBarberSF",
          icon: "logo-facebook",
        },
      ],

      address: "1108 San Fernando Rd., San Fernando Valley, CA, 91340",
      addressRoute: "1108 San Fernando Rd., San Fernando Valley, CA, 91340",
      mapSource: "https://maps.google.com/maps?q=The%20Barber%203210,%201108%20San%20Fernando%20Rd,&t=&z=15&ie=UTF8&iwloc=&output=embed", 
     
      social1: [
        {
          text: "Appointments",
          url: 'https://booksy.com/en-us/86864_the-barber_barber-shop_101731_san-fernando',
          icon: "list",
        }
      ],
    
      barbers:[
        {
          // name: "Jonny D",
          // description: "Business Owner",
          // // socialNetworks:[
          // //   {
          // //     // text: "straightupbarbers",
          // //     // link: "https://www.instagram.com/straightupbarbers/",
          // //     // icon: "logo-instagram",
          // //   },
          // // ],
          // image:"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FSanta%20Monica%20Hair%20Lounge%2Fappoiments%2FLogo_512x512.png?alt=media&token=dad58947-5a1c-4f43-9c6e-c16950e9828b",
        },
      ]
    },

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
     // this.firebaseService.updateAppData(this.appData).then(t => {console.log(this.appData)}); 
  }


  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
     return this.firebaseService.getAppData().then(appData => {
        if(appData) {
          this.appData = appData as any;
          console.log(appData);
        }
      });
  }
}
