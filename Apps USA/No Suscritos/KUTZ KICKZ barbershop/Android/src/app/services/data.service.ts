import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "KUTZ KICKZ &TEEZ barbershop",

      homeDescription: 
        `KUTZ KICKZ & TEEZ barbershop is one of the best barbershops in California 
        services that currently exist where you and your family can enjoy an incredible experience 
        where our professionals with many years of experience where they will give their best style and You will have your best cut, 
        because for us your cut is one of the most important. <br>
        Do not hesitate to make an appointment with us and you will just sit down, 
        relax and your experience will be wonderful make your appointment right now in our application.`,


        homeSlider:[
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fhome%2F1_1024x1024.png?alt=media&token=6ce328f6-657f-4116-8e33-cf8fe855f054",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fhome%2F4_1024x1024.jpg?alt=media&token=16969f28-2856-491c-9875-ac8c4e2f06c6",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fhome%2F6_1024x1024.jpg?alt=media&token=32b5380c-5098-4c9a-a840-323791176ccc",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fhome%2F7_1024x1024.jpg?alt=media&token=ff61207b-09f4-439b-966d-42b70e30450d",          
        ],


        homeServices: [
          {
            name:"Walk in",
            // time:"30 minutes",
           // description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
            price: "$30"
          },
          {
            name:"Appoinments",
            // time:"30 minutes",
           // description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
            price: "$35"
          },
          {
            name:"Hair cut + shave",
            // time:"30 minutes",
           // description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
            price: "$40"
          },
          {
            name:"kids (3-9)",
            // time:"30 minutes",
           // description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
            price: "$25"
          },

        ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "Closed",
        },
        {
          day: "Tuesday",
          hour: "10:00 am – 06:30 pm",
        },
        {
          day: "Wednesday",
          hour: "10:00 am – 06:30 pm",
        },
        {
          day: "Thursday",
          hour: "10:00 am – 06:30 pm",
        },
        {
          day: "Friday",
          hour: "08:30 am – 07:30 pm",
        },
        {
          day: "Saturday",
          hour: "08:30 am – 07:00 pm",
        },
        {
          day: "Sunday",
          hour: "Closed",
        },
      ],
      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fhome%2F3.mp4?alt=media&token=6955d12e-bb1b-4f2b-ad54-47efe410b8de",
       //"https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fhome%2F1.mp4?alt=media&token=8e1f6e05-9d21-4a36-9bd7-60c2e9934c88", 
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fhome%2F2.mp4?alt=media&token=b88d5921-bbf9-4a55-864d-09909e67647d",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "310 819 8086",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fgallery%2F16_1024x1024.jpg?alt=media&token=9301a9ac-d37d-42a7-9da1-c2c88e9bec26",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fgallery%2F1_1024x1024.png?alt=media&token=6f8d7c0c-e463-41e4-8978-3a41df221c7d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fgallery%2F2_1024x1024.jpg?alt=media&token=d004420c-8db9-44a7-b2df-fc1250a23a65",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fgallery%2F3_1024x1024.jpg?alt=media&token=92e45582-1622-444b-88ce-675baef0c446",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fgallery%2F4_1024x1024.jpg?alt=media&token=274a987d-2be5-470c-a92d-368f2ebc4bfd",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fgallery%2F5_1024x1024.jpg?alt=media&token=05c51062-06ec-41df-a129-a78389212bb1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fgallery%2F7_1024x1024.jpg?alt=media&token=98390560-185f-487a-90ca-b5dc9e0e93ff",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fgallery%2F15_1024x1024.jpg?alt=media&token=8e72d874-610e-4f57-878c-2ddc6b6a46e1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fgallery%2F9_1024x1024.jpg?alt=media&token=d5659692-2bf4-4573-af45-3fcdcf4ff4ba",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fgallery%2F13_1024x1024.jpg?alt=media&token=eb86ae1b-21c7-4f0e-bf14-7cc57894b84b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fgallery%2F8_1024x1024.jpg?alt=media&token=be43d08e-0ba9-49ef-b735-fe422d2f065c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fgallery%2F10_1024x1024.jpg?alt=media&token=1ee25528-82bb-41af-bd3b-6423bf3aeb81",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fgallery%2F14_1024x1024.jpg?alt=media&token=1fc202b4-43ff-4c60-b486-6eae7ce0d16f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fgallery%2F11_1024x1024.jpg?alt=media&token=bb55473f-64b3-4710-9b25-2622fccfce15",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKUTZ%20KICKZ%20TEEZ%20barbershop%2Fgallery%2F17_1024x1024.jpg?alt=media&token=d949bc9e-d7a7-495d-a376-98c82644ac96",  
      ],

      products:[
       ],

      socialNetworks: [
        {
          text: "Kutz Kickz &Teez Appointments",
          link: "https://calendly.com/kutzkickznteez",
          icon: "list", 
        },
        {
          text: "KUTZ KICKZ &TEEZ barbershop",
          link: "https://www.google.com/maps/place/KUTZ+KICKZ+%26TEEZ+barbershop/@33.8969534,-118.326367,15z/data=!4m5!3m4!1s0x0:0x925716d25be5177c!8m2!3d33.8968313!4d-118.3263648",
          icon: "logo-google", 
        },
      ],
      address: "14822 Crenshaw Blvd, Gardena, CA 90249, USA",

      addressRoute: "14822 Crenshaw Blvd, Gardena, CA 90249, USA",

      mapSource: "https://maps.google.com/maps?q=KUTZ%20KICKZ%20&TEEZ%20barbershop&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }
  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 
    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
       // this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)}); 
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
