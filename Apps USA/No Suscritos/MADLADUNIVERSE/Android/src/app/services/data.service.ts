import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "MadLad Universe Barbershop",

      homeDescription: 
        `One of the best artists in the USA with grammy nominated songs being one of the best nominees. <br>
         With one of the best Barbers in All Arizona, with spectacular customer service and leaving high quality cuts.  <br>
         Qualified as professional barbers, a spectacular environment where you enter, 
        relax and you will have an impressive experience because for us your court is the representative of you.`,


        homeSlider:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fhome%2F1s_512x512.jpg?alt=media&token=dc2f8996-0fd4-45f3-9646-a2a241d38888",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fhome%2F2s_512x512.jpg?alt=media&token=5158ffbf-431c-41f4-940e-fef6e80e5a01",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fhome%2F3s_512x512.jpg?alt=media&token=b25b0df3-cb3e-47ca-a844-de3a9c842f51",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fhome%2F4s_512x512.jpg?alt=media&token=9ea0afb5-b7e1-47a1-96a6-df58ff48746d",
        ],


      homeServices: [

      ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "08:00 am – 05:00 pm",
        },
        {
          day: "Tuesday",
          hour: "08:00 am – 05:00 pm",
        },
        {
          day: "Wednesday",
          hour: "08:00 am – 05:00 pm",
        },
        {
          day: "Thursday",
          hour: "08:00 am – 05:00 pm",
        },
        {
          day: "Friday",
          hour: "08:00 am – 05:00 pm",
        },
        {
          day: "Saturday",
          hour: "Closed",
        },
        {
          day: "Sunday",
          hour: "Closed",
        },
      ],

      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fhome%2F1.mp4?alt=media&token=110c4cdf-08e5-4935-963a-6047f5b3e614",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fhome%2F2.mp4?alt=media&token=0bcabeb4-a5e7-4b5b-b0a2-ad1e323c65b6",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fhome%2F3.mp4?alt=media&token=b0225b82-fe09-435e-b9c0-b7ff4e9a9fda",
      ],

      phoneNumbers: [
        {
          text: "MADLADUNIVERSE",
          number: "520 982 2889",
        },
        {
          text: "Dunbar Barber",
          number: "520 624 0131",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F10_512x512.jpg?alt=media&token=84fa04bb-0d1a-44e1-847e-6e0c64fcee69",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F11_512x512.jpg?alt=media&token=89f8c702-15a9-49c1-9707-1d4a334acfe5",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F12_512x512.jpg?alt=media&token=fe7ea638-ecfe-4885-a007-416f3f8e0b8e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F13_512x512.jpg?alt=media&token=21d8178e-d8eb-4705-a3bd-3fda0df0cf11",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F14_512x512.jpg?alt=media&token=d21c402d-e7da-409b-926c-e61fabdeade6",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F15_512x512.jpg?alt=media&token=4ffdac40-69ee-4637-b903-107e5b6857a5",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F16_512x512.jpg?alt=media&token=9637f585-1bcf-4639-9b44-18c5b2e2613e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F17_512x512.jpg?alt=media&token=1d6654be-e61c-4854-a5b4-671e27921280",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F18_512x512.jpg?alt=media&token=230ba1ea-5910-45f7-97ab-d35bbf966bbe",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F19_512x512.jpg?alt=media&token=bd6ceb02-80f2-4452-a6d8-ca92ef498e40",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F1_512x512.jpg?alt=media&token=40bca318-3a84-4b41-8811-291d02723bcb",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F20_512x512.jpg?alt=media&token=c51fa82b-72e5-4e5e-a215-a54a85d23510",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F21_512x512.jpg?alt=media&token=d52884bb-c3df-40cd-a195-1ad2902a2b38",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F22_512x512.jpg?alt=media&token=0510ef9b-55b7-47e3-b654-1f281e176e56",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F23_512x512.jpg?alt=media&token=a5816814-d448-46ab-9a1b-73a003c507f7",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F25_512x512.jpg?alt=media&token=70594246-b64b-49ba-b0f8-3019d537a218",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F2_512x512.jpg?alt=media&token=bf3067da-7ada-46a2-992e-86e1d3741229",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F3_512x512.jpg?alt=media&token=04aae58a-266d-47b3-ab5c-9ef5c4e51d12",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F4_512x512.jpg?alt=media&token=4c0c3a28-7727-4303-a940-59155decff27",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F6_512x512.jpg?alt=media&token=d92ec88d-c650-428d-9cf2-758d4e44d016",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F7_512x512.jpg?alt=media&token=63e4e710-cc04-42bc-98ea-f5c0e5322288",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F8_512x512.jpg?alt=media&token=f4fc912c-f00a-4ade-871d-7a09fba7b679",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMadLadUniverse%2Fgallery%2F9_512x512.jpg?alt=media&token=b08d2284-a0ab-47f5-abc7-a2b9d9076681",
      ],

      products:[
        
      ],

      productsVideos: [
             
      ],
      socialNetworks: [
        {
          text: "MadLad",
          link: "https://www.facebook.com/MADLADUNIVERSE",
          icon: "logo-facebook",
        },
        {
          text: "madladuniverse",
          link: "https://www.instagram.com/madladuniverse/?hl=es-la",
          icon: "logo-instagram",
        },
        {
          text: "MADLAD",
          link: "https://open.spotify.com/artist/7jBGZlrv8bK0E0LDlNXvhl",
          icon: "headset",
        },
        {
          text: "Dunbar Barber Academy",
          link: "https://www.facebook.com/Dunbar-Barber-Academy-163586533724236/",
          icon: "logo-facebook",
        },
        {
          text: "dunbarbarberacademy",
          link: "https://www.instagram.com/dunbarbarberacademy/?hl=es-la",
          icon: "logo-instagram",
        },
        {
          text: "dunbarbarberacademy",
          link: "http://dunbarbarberacademy.com/",
          icon: "globe",
        },

      ],

      address: "325 W 2nd St, Tucson, AZ 85705, EE. UU.",

      addressRoute:"325 W 2nd St, Tucson, AZ 85705, EE. UU.",

      mapSource: "https://maps.google.com/maps?q=325%20W%202nd%20St,%20Tucson,%20AZ%2085705,%20EE.%20UU.&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

   //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
