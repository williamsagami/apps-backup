import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { LlamadaPage } from '../pages/llamada/llamada.page';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  //slider
  sliderImages = [
    '../assets/images/1s.jpg',
    '../assets/images/2s.jpg',
    '../assets/images/3s.jpg',
    '../assets/images/4s.jpg',
  ]

  //galeria
  images = [
    'assets/images/1.jpg',
    'assets/images/2.PNG',
    'assets/images/3.jpg',
    'assets/images/4.jpg',
    'assets/images/5.jpg',
    'assets/images/6.jpg',
    'assets/images/7.jpg',
     'assets/images/8.jpg',
     'assets/images/9.jpg',
     'assets/images/10.jpg',
     'assets/images/11.jpg',
  ]

  //contacto
  phones = [
    {
      number: '+1 716-310-1427', 
      call: '7163101427'
    },
  ];
  direction = 'Utica Business Center 11E.UTICA st. Buffalo NY 14209';

  redesSociales = [
    {
      name: "Alan Lee Jr.",
      link: "https://www.facebook.com/profile.php?id=100000030289710",
      icon: "logo-facebook",
    },
    {
      name: "cuzn2t",
      link: "https://www.instagram.com/cuzn2t/",
      icon: "logo-instagram",
    }
  ];
  constructor(private modalController: ModalController, private iab : InAppBrowser) { }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: LlamadaPage ,
      componentProps : {
        number: phoneNumber
      }
    });
    await modal.present();
  }

  openLink(link: string) {
      this.iab.create(link);
  }

}
