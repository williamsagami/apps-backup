import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: "Doing3much CUTS",
      en: "Doing3much CUTS"
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedures1 = '';
  procedureList1 = [
    'Monday: 9am - 9pm',
    'Tuesday: 9am - 8pm',
    'Wednesday: 9am - 8pm',
    'Thursday: 9am - 8pm',
    'Friday: 9am - 8pm',
    'Saturday: 8am - 8pm',
  ];
  procedureList = [
    'Mens Cuts $25',
    '18-14 $20',
    'Womens $20',
    'Edge Ups, Eyebrows, Beards $15',
    'Designs + Extras $15',
    'Shampoo $15',
    'The "FakeOut" $25',
    '13and Under $15',
  ];
  

  translateIndex = [
    {
    es:`Doing3much CUTS, es una de las mejores barberias de todo New York, 
    con 30 años de experiencia dando los mejores resultados en cada corte. 
    satisfaciendo a todos sus clientes, usando todo tipo de cortes variados.
    Para nosotros el corte es lo mas importante de tu pelo, haz tu reserva ahora!`,
    en:`Doing3much CUTS, is one of the best barbershops in New York,
    with 30 years of experience giving the best results in each cut,
    satisfying all its customers, using all kinds of varied cuts,
    For us, the cut is the most important thing about your hair, make your reservation now!`
    },
    {
      es: 'Schedule',
      en: 'Schedule'
    },
    {
      es: 'Price',
      en: 'Price'
    },
  ];

  translateProcedures = [
    {
      es: 'Mens Cuts $25',
      en: 'Mens Cuts $25'
    },
    {
      es: '18-14 $20',
      en: '18-14 $20'
    },
    {
      es: 'Womens $20',
      en: 'Womens $20'
    },
    {
      es: 'Edge Ups, Eyebrows, Beards $15',
      en: 'Edge Ups, Eyebrows, Beards $15'
    },
    {
      es: 'Designs + Extras $15',
      en: 'Designs + Extras $15'
    },
    {
      es: 'Shampoo $15',
      en: 'Shampoo $15'
    },
    {
      es: 'The "FakeOut" $25',
      en: 'The "FakeOut" $25'
    },
    {
      es: '13and Under $15',
      en: '13and Under $15'
    },
  
  ];

  translateProcedures1 = [
    {
      es: 'Monday: 9am - 9pm',
      en: 'Monday: 9am - 9pm'
    },
    {
      es: 'Tuesday: 9am - 8pm',
      en: 'Tuesday: 9am - 8pm'
    },
    {
      es: 'Wednesday: 9am - 8pm',
      en: 'Wednesday: 9am - 8pm'
    },
    {
      es: 'Thursday: 9am - 8pm',
      en: 'Thursday: 9am - 8pm'
    },
    {
      es: 'Friday: 9am - 8pm',
      en: 'Friday: 9am - 8pm'
    },
    {
      es: 'Saturday: 8am - 8pm',
      en: 'Saturday: 8am - 8pm'
    },
  
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = true;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.procedures1 = this.translateIndex[2].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });
      this.translateProcedures1.forEach((procedure1, index) => {
        this.procedureList1[index] = procedure1.es;
      });
      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.procedures1 = this.translateIndex[2].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });
      this.translateProcedures1.forEach((procedure1, index) => {
        this.procedureList1[index] = procedure1.en;
      });


      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
