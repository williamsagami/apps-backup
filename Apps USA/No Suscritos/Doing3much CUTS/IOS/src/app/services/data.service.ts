import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Doing3much CUTS",

      homeDescription: 
        `Doing3much CUTS, is one of the best barbershops in New York,
        with 30 years of experience giving the best results in each cut,
        satisfying all its customers, using all kinds of varied cuts,
        For us, the cut is the most important thing about your hair, make your reservation now!`,

        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fhome%2F1s_512x512.jpg?alt=media&token=b896d287-75b5-4a86-a8fd-be93113e25ce",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fhome%2F2s_512x512.jpg?alt=media&token=1ecf9ef4-c56a-4dc6-aebc-31a687a4b34f",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fhome%2F3s_512x512.jpg?alt=media&token=8624358d-4f97-4dcd-bdbd-8d074489827b",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fhome%2F4s_512x512.jpg?alt=media&token=7e8966b3-72ea-48ab-b6d2-f7fd387f57a5",
        ],

      homeServices: [
        {
          name:"Mens Cuts",
          price: "$25"
        },
        {
          name:"18-14",
          price: "$20"
        },
        {
          name:"Womens",
          price: "$20"
        },
        {
          name:"Edge Ups, Eyebrows, Beards",
          price: "$15"
        },
        {
          name:"Designs + Extrase",
          price: "$15"
        },
        {
          name:"Shampoo",
          price: "$15"
        },
        {
          name:"The FakeOut",
          price: "$25"
        },
        {
          name:"13and Under",
          price: "$15"
        },
      ],
      homeSchedules: [
        {
          day: "Monday",
          hour: "09:00 AM - 09:00 PM",
        },
        {
          day: "Tuesday",
          hour: "09:00 AM - 08:00 PM",
        },
        {
          day: "Wednesday",
          hour: "09:00 AM - 08:00 PM",
        },
        {
          day: "Thursday",
          hour: "09:00 AM - 08:00 PM",
        },
        {
          day: "Friday",
          hour: "09:00 AM - 08:00 PM",
        },
        {
          day: "Saturday",
          hour: "09:00 AM - 08:00 PM",
        },
        {
          day: "Sunday",
          hour: "08:00 AM - 08:00 PM",
        },
      ],

      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fhome%2F1.mp4?alt=media&token=d65f950c-4070-4aa8-8e44-887d9729d10b",      
      ],

    f: [
      
    ],

    gallery:[
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fgallery%2F1_512x512.jpg?alt=media&token=5f004b17-de12-43b5-9ea6-eb7f9a4e79c2",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fgallery%2F2_512x512.PNG?alt=media&token=d1bc6885-0e80-42b8-86ac-b3470bda317e",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fgallery%2F3_512x512.jpg?alt=media&token=735dd36d-c919-498c-8c29-1f7025ec4bec",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fgallery%2F4_512x512.jpg?alt=media&token=acdada53-8039-43a1-8702-695c53d6034e",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fgallery%2F5_512x512.jpg?alt=media&token=f67f3c87-062e-4ee3-be01-db4c11f0b73a",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fgallery%2F6_512x512.jpg?alt=media&token=6c69f658-ff86-460a-855f-e9e12a2cc93a",
     // "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fgallery%2F7_512x512.jpg?alt=media&token=5ea595ac-a207-4380-ad24-d1def6f0e8ce",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fgallery%2F8_512x512.jpg?alt=media&token=93d2b967-7351-4957-bc27-d9b329472453",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fgallery%2F9_512x512.jpg?alt=media&token=05ad62b1-26ed-4629-8286-294332c6d87a",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fgallery%2F10_512x512.jpg?alt=media&token=3f42bbd1-ae0a-49dc-aedd-bab9acdae8ad",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fgallery%2F11_512x512.jpg?alt=media&token=a9043983-6c9c-4bce-992a-823888638276",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fgallery%2F13_512x512.jpg?alt=media&token=d4b41b20-1b79-46f0-9464-0d8e5e1c8e5f",
     // "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fgallery%2F3s_512x512.jpg?alt=media&token=ede37b2a-4030-4cf9-b23f-4795f504a9fb",
    ],

      phoneNumbers: [
        {
          text: "Alan Lee Jr.",
          number: "716 310 1427",
        },
        {
          text: "Kiki Modo",
          number: "562 221 8719",
        }
      ],

      socialNetworks: [
        {
          text: "Alan Lee Jr.",
          link: "https://www.facebook.com/profile.php?id=100000030289710",
          icon: "logo-facebook",
        },
        {
          text: "cuzn2t",
          link: "https://www.instagram.com/cuzn2t/",
          icon: "logo-instagram",
        },
        {
          text: "http://thecut.co",
          link: "https://www.thecut.co/",
          icon: "globe",
        }
      ],

      address: "Utica Business Center 11E.UTICA st. Buffalo NY 14209",

      mapSource: "https://maps.google.com/maps?q=Doing3much%20CUTS,%20Utica%20Business%20Center%20E.UTICA%20st.%20Buffalo%20New%20York%2014209&t=&z=15&ie=UTF8&iwloc=&output=embed",

      barbers:[
        {
          name: "KIKI MODO",
          description: "Master Barbershop Cash app $VAUSCEPOWERS",
          socialNetworks:[
            {
              text: "modokiki",
              link: "https://www.instagram.com/modokiki/?hl=es-la",
              icon: "logo-instagram",
            },
            {
              text: "KIKI MODO",
              link: "https://www.facebook.com/kiki.modo.56",
              icon: "logo-facebook",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FDoing3much%20CUTS%2Fappointment%2Fb1_512x512.jpg?alt=media&token=8974c080-67d1-4026-94c7-16c040e74682",
        }
      ],
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

  //  this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
