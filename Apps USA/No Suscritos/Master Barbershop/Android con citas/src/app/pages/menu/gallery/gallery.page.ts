import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { ImagePage } from './image/image.page';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.page.html',
  styleUrls: ['./gallery.page.scss'],
})
export class GalleryPage implements OnInit {

  d: any;

  images = [
    'assets/images/1.jpg',
    'assets/images/2.jpg',
    'assets/images/3.jpg',
    'assets/images/4.jpg',
    'assets/images/5.jpg',
    'assets/images/6.jpg',
    'assets/images/7.jpg',
     'assets/images/8.jpg',
     'assets/images/9.jpg',
    'assets/images/10.jpg',
    'assets/images/11.jpg',
    'assets/images/12.jpg',
    'assets/images/13.jpg',
    'assets/images/14.jpg',
    'assets/images/15.jpg',
    'assets/images/16.jpg',
    'assets/images/17.jpg',
    'assets/images/18.jpg',
    'assets/images/19.jpg',
    'assets/images/20.jpg',
    'assets/images/21.jpg',
    'assets/images/22.jpg',
    'assets/images/23.jpg',
    'assets/images/24.jpg',
    'assets/images/25.jpg',
     'assets/images/26.jpg',
     'assets/images/27.jpg',
    'assets/images/28.jpg'
  ];

  constructor(private modalController: ModalController, public data: DataService) { 
    this.d = data.appData.data;
  }

  ngOnInit() {
  }

  async openImage(i: number)
	{
    const modal = await this.modalController.create({
      component: ImagePage,
      cssClass: 'modal-transparency',
      componentProps : {
        index: i+1,
        images: this.images,
      }
    });
    await modal.present();
	}

}
 