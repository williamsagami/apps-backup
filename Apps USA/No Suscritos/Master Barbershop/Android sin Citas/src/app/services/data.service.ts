import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { LlamadaPage } from '../pages/llamada/llamada.page';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  //slider
  sliderImages = [
    '../assets/images/1s.jpg',
    '../assets/images/2s.jpg',
    '../assets/images/3s.jpg',
    '../assets/images/4s.jpg',
  ]

  //galeria
  images = [
    'assets/images/1.jpg',
    'assets/images/2.jpg',
    'assets/images/3.jpg',
    'assets/images/4.jpg',
    'assets/images/5.jpg',
    'assets/images/6.jpg',
    'assets/images/7.jpg',
     'assets/images/8.jpg',
     'assets/images/9.jpg',
    'assets/images/10.jpg',
    'assets/images/11.jpg',
    'assets/images/12.jpg',
    'assets/images/13.jpg',
    'assets/images/14.jpg',
    'assets/images/15.jpg',
    'assets/images/16.jpg',
    'assets/images/17.jpg',
    'assets/images/18.jpg',
    'assets/images/19.jpg',
    'assets/images/20.jpg',
    'assets/images/21.jpg',
    'assets/images/22.jpg',
    'assets/images/23.jpg',
    'assets/images/24.jpg',
    'assets/images/25.jpg',
     'assets/images/26.jpg',
     'assets/images/27.jpg',
    'assets/images/28.jpg'
  ]

  //contacto
  phones = [
    {
      number: '+1 (480) 686-9300', 
      call: '4806869300'
    },
  ];
  direction = '7342 E Shea Blvd Blvd Suite 102, Scottsdale, AZ 85260';

  redesSociales = [
    {
      name: "Master Barbershop",
      link: "https://www.facebook.com/villageonshea",
      icon: "logo-facebook",
    },
    {
      name: "Master Barbershop",
      link: "https://www.pinterest.com/MasterBarbershopAZ/",
      icon: "logo-pinterest",
    }
  ];
  constructor(private modalController: ModalController, private iab : InAppBrowser) { }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: LlamadaPage ,
      componentProps : {
        number: phoneNumber
      }
    });
    await modal.present();
  }

  openLink(link: string) {
      this.iab.create(link);
  }

}
