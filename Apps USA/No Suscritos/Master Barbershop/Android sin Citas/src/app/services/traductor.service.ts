import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: "Master Barbershop",
      en: "Master Barbershop"
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedures1 = '';
  procedureList = [
    'Facial Michael $30',
    'Razor Fade Michael $27',
    'Senior Cut Michael $21',
    'Edge Up Michael $10',
    'Kids Cut Michael $18',
    'Men’s cut Michael $23',
    'Old Fashion Shave Michael $30',
    'Beard trim Michael $15',
    'Goatee Michael $10',
    'Buzz Cut Michael $21',
    'Beard line up Michael $10',
    'The whole deal Michael $80',
    'Senior Cut Nick $21',
    'Men’s Cut Nick $23',
    'Buzz Cut Nick $21',
    'Razor Fade Nick $27',
    'Kids Cut Nick $18',
    'Old Fashion Shave Nick $30',
    'Facial Nick $30',
    'Beard Trim Nick $15',
    'Edge Up Nick $10',
    'Goatee Nick $10',
    'Beard Line Up Nick $10',
    'Moustache trim Nick $7',
    'Hands Treatment $17',
    'Traditional Nick $50',
    'The whole deal Nick $80',
    'Color Nick $35',
    'Women’s Cut Nick $35',
    'Mustache Trim Michael $7',
    'Women’s Cut $25',
    


  ];
  procedureList1 = [
    'Monday to Saturday: 9AM - 6PM',
    'Sunday 9AM - 5PM',
  ];
  

  translateIndex = [
    {
    es:`Master Barbershop is the only place you'll ever need for professional barber shop services in Scottsdale, AZ and the surrounding area. 
    Our team of friendly and highly experienced barbers can handle all of your men's grooming services, facials, and straight razor shaves in one place, 
    ensuring that one simple call gets you access to one of the best and most innovative barber shopmen's hair stylists, kids cuts, fades, or hot towel shaves,
    our staff employ over 20 years of experience and top-quality tools to get the job done. We'll also perform beard trimmings, neck shaves, 
    and handle all men's hairstylesbarber salon service needs in one convenient place for one affordable price. 
    Our barbers can handle all of your facials, scalp treatments, shaves, and haircuts, ensuring your satisfaction with the exceptional work and customer service we are known for providing. 
    Contact Master Barbershop today and we'll give you a comprehensive description of all of the services and products we provide for all your barber shop needs.`,
    en:`Master Barbershop is the only place you'll ever need for professional barber shop services in Scottsdale, AZ and the surrounding area. <br>
    Our team of friendly and highly experienced barbers can handle all of your men's grooming services, facials, and straight razor shaves in one place, 
    ensuring that one simple call gets you access to one of the best and most innovative barber shopmen's hair stylists, kids cuts, fades, or hot towel shaves,
    our staff employ over 20 years of experience and top-quality tools to get the job done. We'll also perform beard trimmings, neck shaves, 
    and handle all men's hairstylesbarber salon service needs in one convenient place for one affordable price. <br>
    Our barbers can handle all of your facials, scalp treatments, shaves, and haircuts, ensuring your satisfaction with the exceptional work and customer service we are known for providing. <br>
    Contact Master Barbershop today and we'll give you a comprehensive description of all of the services and products we provide for all your barber shop needs.`
    },
    {
      es: 'Services',
      en: 'Services'
    },
    {
      es: 'Schedule',
      en: 'Schedule'
    },
  ];

  translateProcedures = [
    {
      es: 'Facial Michael $30',
      en: 'Facial Michael $30'
    },
    {
      es: 'Razor Fade Michael $27',
      en: 'Razor Fade Michael $27'
    },   
    {
      es: 'Senior Cut Michael $21',
      en: 'Senior Cut Michael $21'
    },   
    {
      es: 'Edge Up Michael $10',
      en: 'Edge Up Michael $10'
    }, 
    {
      es: 'Kids Cut Michael $18',
      en: 'Kids Cut Michael $18'
    }, 
    {
      es: 'Men’s cut Michael $23',
      en: 'Men’s cut Michael $23'
    }, 
    {
      es: 'Old Fashion Shave Michael $30',
      en: 'Old Fashion Shave Michael $30'
    }, 
    {
      es: 'Beard trim Michael $15',
      en: 'Beard trim Michael $15'
    },
    {
      es: 'Goatee Michael $10',
      en: 'Goatee Michael $10'
    },
    {
      es: 'Buzz Cut Michael $21',
      en: 'Buzz Cut Michael $21'
    },
    {
      es: 'Beard line up Michael $10',
      en: 'Beard line up Michael $10'
    },
    {
      es: 'The whole deal Michael $80',
      en: 'The whole deal Michael $80'
    },
    {
      es: 'Senior Cut Nick $21',
      en: 'Senior Cut Nick $21'
    },
    {
      es: 'Men’s Cut Nick $23',
      en: 'Men’s Cut Nick $23'
    },
    {
      es: 'Buzz Cut Nick $21',
      en: 'Buzz Cut Nick $21'
    },
    {
      es: 'Razor Fade Nick $27',
      en: 'Razor Fade Nick $27'
    },
    {
      es: 'Kids Cut Nick $18',
      en: 'Kids Cut Nick $18'
    },
    {
      es: 'Old Fashion Shave Nick $30',
      en: 'Old Fashion Shave Nick $30'
    },
    {
      es: 'Facial Nick $30',
      en: 'Facial Nick $30'
    },
    {
      es: 'Beard Trim Nick $15',
      en: 'Beard Trim Nick $15'
    },
    {
      es: 'Edge Up Nick $10',
      en: 'Edge Up Nick $10'
    },
    {
      es: 'Goatee Nick $10',
      en: 'Goatee Nick $10'
    },
    {
      es: 'Beard Line Up Nick $10',
      en: 'Beard Line Up Nick $10'
    },
    {
      es: 'Moustache trim Nick $7',
      en: 'Moustache trim Nick $7'
    },
    {
      es: 'Hands Treatment $17',
      en: 'Hands Treatment $17'
    },
    {
      es: 'Traditional Nick $50',
      en: 'Traditional Nick $50'
    },
    {
      es: 'The whole deal Nick $80',
      en: 'The whole deal Nick $80'
    },
    {
      es: 'Color Nick $35',
      en: 'Color Nick $35'
    },
    {
      es: 'Women’s Cut Nick $35',
      en: 'Women’s Cut Nick $35'
    },
    {
      es: 'Mustache Trim Michael $7',
      en: 'Mustache Trim Michael $7'
    },
    {
      es: 'Women’s Cut $25',
      en: 'Women’s Cut $25'
    },
  ];
  translateProcedures1 = [
    {
      es: 'Monday to Saturday: 9AM - 6PM',
      en: 'Monday to Saturday: 9AM - 6PM'
    },
    {
      es: 'Sunday 9AM - 5PM',
      en: 'Sunday 9AM - 5PM'
    },   
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = true;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.procedures1 = this.translateIndex[2].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });
      this.translateProcedures1.forEach((procedure1, index) => {
        this.procedureList1[index] = procedure1.es;
      });

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.procedures1 = this.translateIndex[2].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });
      this.translateProcedures1.forEach((procedure1, index) => {
        this.procedureList1[index] = procedure1.en;
      });


      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
