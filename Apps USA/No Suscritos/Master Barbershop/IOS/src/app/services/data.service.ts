import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Master Barbershop",

      homeDescription: 
        `Master Barbershop is the only place you'll ever need for professional barber shop services in Scottsdale, AZ and the surrounding area. 
        Our team of friendly and highly experienced barbers can handle all of your men's grooming services, facials, and straight razor shaves in one place, 
        ensuring that one simple call gets you access to one of the best and most innovative barber shopmen's hair stylists, kids cuts, fades, or hot towel shaves,
        our staff employ over 20 years of experience and top-quality tools to get the job done. We'll also perform beard trimmings, neck shaves, 
        and handle all men's hairstylesbarber salon service needs in one convenient place for one affordable price. 
        Our barbers can handle all of your facials, scalp treatments, shaves, and haircuts, ensuring your satisfaction with the exceptional work and customer service we are known for providing. 
        Contact Master Barbershop today and we'll give you a comprehensive description of all of the services and products we provide for all your barber shop needs.`,

      homeServices: [
        {
          name:"Facial Michael",
          price: "$30"
        },
        {
          name:"Razor Fade Michael",
          price: "$27"
        },
        {
          name:"Senior Cut Michael",
          price: "$21"
        },
        {
          name:"Edge Up Michael",
          price: "$10"
        },
        {
          name:"Kids Cut Michael",
          price: "$18"
        },
        {
          name:"Men’s cut Michael",
          price: "$23"
        },
        {
          name:"Old Fashion Shave Michael",
          price: "$30"
        },
        {
          name:"Beard trim Michael",
          price: "$15"
        },
        {
          name:"Goatee Michael",
          price: "$10"
        },
        {
          name:"Buzz Cut Michael",
          price: "$21"
        },
        {
          name:"Beard line up Michael",
          price: "$10"
        },
        {
          name:"The whole deal Michael",
          price: "$80"
        },
        {
          name:"Senior Cut Nick",
          price: "$21"
        },
        {
          name:"Men’s Cut Nick",
          price: "$23"
        },

        {
          name:"Buzz Cut Nick",
          price: "$21"
        },
        {
          name:"Razor Fade Nick",
          price: "$27"
        },
        {
          name:"Kids Cut Nick",
          price: "$18"
        },
        {
          name:"Old Fashion Shave Nick",
          price: "$30"
        },
        {
          name:"Facial Nick",
          price: "$30"
        },
        {
          name:"Beard Trim Nick",
          price: "$15"
        },
        {
          name:"Edge Up Nick",
          price: "$10"
        },


        {
          name:"Goatee Nick",
          price: "$10"
        },
        {
          name:"Beard Line Up Nick",
          price: "$10"
        },
        {
          name:"Moustache trim Nick",
          price: "$7"
        },
        {
          name:"Hands Treatment",
          price: "$17"
        },
        {
          name:"Traditional Nick",
          price: "$50"
        },
        {
          name:"The whole deal Nick",
          price: "$80"
        },
        {
          name:"Color Nick",
          price: "$35"
        },
        {
          name:"Women’s Cut Nick",
          price: "$35"
        },
        {
          name:"Mustache Trim Michael",
          price: "$7"
        },
        {
          name:"Women’s Cut",
          price: "$25"
        },       
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "480 686 9300",
        }
      ],

      socialNetworks: [
        {
          text: "Master Barbershop web",
          link: "https://www.scottsdalemensgrooming.com/",
          icon: "Globe",
        },

        {
          text: "Master Barbershop",
          link: "https://www.yelp.com/biz/master-barbershop-scottsdale",
          icon: "logo-yahoo",
        },
        {
          text: "Master BarberShop",
          link: "https://www.facebook.com/villageonshea",
          icon: "logo-facebook",
        },
        {
          text: "masterbarberscottsdale",
          link: "https://www.instagram.com/masterbarberscottsdale/",
          icon: "logo-instagram",
        },
        {
          text: "MasterBarbershop",
          link: "https://www.pinterest.com/MasterBarbershopAZ/",
          icon: "logo-pinterest",
        },
      ],

      address: "7342 E Shea Blvd Blvd Suite 102, Scottsdale, AZ 85260",

      addressRoute:"7342 E Shea Blvd Blvd Suite 102, Scottsdale, AZ 85260",

      mapSource: "https://maps.google.com/maps?q=Master%20Barbershop,%207342%20E%20Shea%20Blvd%20Suite%20102%20%20Scottsdale,%20AZ%2085260&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

 // this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
