import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { productPageRoutingModule } from './product-routing.module';

import { productPage } from './product.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    productPageRoutingModule
  ],
  declarations: [productPage]
})
export class productPageModule {}
