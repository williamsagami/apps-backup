import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireFunctions } from '@angular/fire/functions';
import { map } from 'rxjs/operators';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { Platform } from '@ionic/angular';

export interface solicitud {
  id: string,
  date: Date,
  creationDate: Date,
  flextime: boolean,
  ftBefore: number,
  ftAfter: number,
  message: string,
  state: number,
  user: usuario,
}

export interface usuario {
  id: string,
  username: string,
  admin: boolean,
  phone: string,
  email: string,
  rating: number,
  cancel: number,
  level: number,
  requestCount: number,
  requestNumber: number,
  allowRequests: boolean,
}

export interface post {
  id: string,
  title: string,
  content: string,
  sale: string,
  postNumber: number,
}

export enum state {
  pending,
  accepted,
  rejected,
}

export enum language {
  ES,
  EN
}

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  private user: any;
  private dbName = "wickedcutz";
  private dbRoute = "appointments/wickedcutz/app"
  private userReady = false;
  public userAuthState: any;
  public userData: any;
  public appData: any;
  public defaultLanguage = language.EN;

  //User Setup
  private authRef: any;
  private userRef: any;

  constructor(
    public fbAuth: AngularFireAuth, 
    private db: AngularFirestore, 
    private functions: AngularFireFunctions,
    public fcm: FirebaseX,
    private platform: Platform) {
      
    this.getAppData().then(appData => {
      this.appData = appData;
    });

  }

  //SETUP DE BASE DE DATOS (USAR AL CREAR)
  // createAppointmentsDataBase() {
  //   const functionRef = this.functions.httpsCallable('createAppointmentsDataBase');
  //   return functionRef({dbRoute: this.dbRoute, dbName: this.dbName}).toPromise();
  // }

  getDataBaseName() { return this.dbName; }

  //SETUP APP --------------------------------------------------------------------
  userSetup() { //Devuelve true si el usuario existe
    return new Promise((resolve, rejected) => {
      if(this.userReady) {
        var userDataExist = this.userData ? true : false;
        resolve(userDataExist);
      }
      else {
        if(this.authRef) this.authRef.unsubscribe(); //Limpia las referencias de suscripcion
        if(this.userRef) this.userRef.unsubscribe();

        this.authRef = this.fbAuth.authState.subscribe(user => { //Autenticacion de Usuario (Real Time)
          this.userAuthState = user;
          //console.log(user);
          if(user) {
            this.userRef = this.getUserRealTime(user.uid).subscribe(userData => { //Usuario (Real Time)
              this.userData = userData;
              //console.log(userData);

              //Si existe el usuario devuelve el true
              if(userData) { 
                this.userReady = true;
                resolve(true);
              }
              //Si no existe se limpia la referencia de suscripcion y se crea un usuario nuevo, para despues tomar los datos en real time
              else {
                this.userRef.unsubscribe();
                this.createUser(user.uid, user.email).then(createdUser => {
                  this.userData = createdUser;

                  this.userRef = this.getUserRealTime(user.uid).subscribe(userData => {
                    this.userData = userData;
                    console.log(userData);
                    this.userReady = true;
                    resolve(true);
                  });
                });
              }
            });
          }
          //Si no esta logeado
          else {
            this.userReady = true;
            this.userData = null;
            resolve(false);
          }
        });
      }
    });
  }

  isUserReady() { return this.userReady; }

  //AUTENTICACION----------------------------------------------------------
  login (email: string, password: string) {
    return new Promise((resolve, rejected) => {
      this.fbAuth.signInWithEmailAndPassword(email, password).then(res => {
        this.user = res.user;
        this.userReady = false;
        resolve(res);
      }).catch(err => rejected(err));
    }).then(setup => {
      this.userSetup().then(saveToken => {
        console.log(this.user.uid);
        this.saveToken(this.user.uid);
      });
    });
  }

  register(email: string, password: string) {
    return new Promise((resolve, reject) => {
      this.fbAuth.createUserWithEmailAndPassword(email, password).then(res => {
        this.user = res.user;
        resolve(res);
      }).catch(err => reject(err));
    }).then(setup => {
      this.createUser(this.user.uid, email).then(userSetup => {
        this.userReady = false;
        this.userSetup();
      });
    });
  }

  logout () {
    this.userReady = false;
    this.userAuthState = null;
    this.user = null;
    return this.fbAuth.signOut();
  }

  //GUARDAR TOKENS-------------------------------------------------------
  updateToken(userId: string, token: string) {
    return this.db.collection(this.dbRoute+'/userData/users').doc(userId).update({token: token});
  }

  saveToken(userId: string) {
    if(this.platform.is('android')) {
      return this.fcm.getToken().then(token => {
        return this.updateToken(userId, token);
      });
    }
    else return null;
  }

  //TRADUCCION----------------------------------------------------------
  updateAppData(appData: any) {
    const functionRef = this.functions.httpsCallable('updateAppData');
    return functionRef({dbRoute: this.dbRoute, appData: appData}).toPromise();
  }

  getAppData() {
    return new Promise((resolve, reject) => {
      if(this.appData) {
        resolve(this.appData);
      }
      else this.db.collection(this.dbRoute).doc('appData').ref.get().then(res => {
        resolve(res.data());
      }).catch(err => reject(err));
    });
  }

  //OPCIONES DE APLICACION-----------------------------------------
  updateAppConfiguration(appConfig: any) {
    return this.db.collection(this.dbRoute).doc('appConfiguration').update({
      dayRangeLimit: appConfig.dayRangeLimit,
      maxUserRequestNumber: appConfig.maxUserRequestNumber,
    });
  }

  getAppConfiguration() {
    return new Promise((resolve, reject) => {
      this.db.collection(this.dbRoute).doc('appConfiguration').ref.get().then(res => {
        resolve(res.data());
      }).catch(err => reject(err));
    });
  }

  //BLOG----------------------------------------------------------
  getPostDataRealTime() {
    return this.db.collection(this.dbRoute).doc('adminPosts').snapshotChanges().pipe(map(postData => {
      return postData.payload.data() as any;
    }));
  }

  getPostsRealTime() {
    return this.db.collection(this.dbRoute+'/adminPosts/posts', ref => ref.orderBy('postNumber', 'desc')).snapshotChanges().pipe(map(posts => {
      return posts.map(post => {
        const postData = post.payload.doc.data() as any;
        postData.id = post.payload.doc.id;
        return postData as post;
      });
    }));
  }

  createPost(post: post, postsData: any) {
    return this.db.collection(this.dbRoute+'/adminPosts/posts').doc('p_'+post.postNumber).set(post).then(postNumber => {
      var pNumber = postsData.postNumber+1;
      var pCount = postsData.postCount+1;
      this.db.collection(this.dbRoute).doc('adminPosts').update({postNumber: pNumber, postCount: pCount});
    });
  }

  updatePost(post: post) {
    return this.db.collection(this.dbRoute+'/adminPosts/posts').doc(post.id).update(post);
  }

  deletePost(post: post, postsData: any) {
    return this.db.collection(this.dbRoute+'/adminPosts/posts').doc(post.id).delete().then(postNumber => {
      const pNumber = postsData.postNumber-1;
      this.db.collection(this.dbRoute).doc('adminPosts').update({postNumber: pNumber});
    });
  }

  //USUARIOS----------------------------------------------------------
  getUser(user_id: string) {
    return new Promise((resolve, reject) => {
      this.db.collection(this.dbRoute+'/userData/users').doc(user_id).ref.get().then(res => {
        resolve(res.data());
      }).catch(err => reject(err));
    });
  }

  getUserRealTime(user_id: string) {
    return this.db.collection(this.dbRoute+'/userData/users').doc(user_id).snapshotChanges().pipe(map(user => {
      return user.payload.data() as any;
    }));
  }

  updateUser(user: any) {
    return this.db.collection(this.dbRoute+'/userData/users').doc(user.id).update(user);
  }

  //CITAS----------------------------------------------------------
  getRequests() {
    return new Promise((resolve, reject) => {
      this.db.collection(this.dbRoute+'/userRequests/requests', ref => ref
      .orderBy('creationDate')).ref.get().then(res => {
        var requests = res.docs.map(request => {
          const requestData = request.data() as any;
          requestData.date = requestData.date.toDate();
          requestData.creationDate = requestData.creationDate.toDate();
          return requestData as solicitud;
        });
        resolve(requests);
      }).catch(err => reject(err));
    });
  }

  getRequestsRealTime() {
    return this.db.collection(this.dbRoute+'/userRequests/requests', ref => ref
    .orderBy('creationDate')).snapshotChanges().pipe(map(requests => {
      return requests.map(request => {
        const requestData = request.payload.doc.data() as any;
        requestData.date = requestData.date.toDate();
        requestData.creationDate = requestData.creationDate.toDate();
        return requestData as solicitud;
      });
    }));
  }

  getUserRequests(user_id: string) {
    return new Promise((resolve, reject) => {
      this.db.collection(this.dbRoute+'/userRequests/requests', ref => ref
      .where("user.id", "==", user_id)).ref.get().then(res => {
        var requests = res.docs.map(request => {
          const requestData = request.data() as any;
          requestData.date = requestData.date.toDate();
          requestData.creationDate = requestData.creationDate.toDate();
          return requestData as solicitud;
        });
        resolve(requests);
      }).catch(err => reject(err));
    });
  }

  getUserRequestsRealTime(user_id: string) {
    return this.db.collection(this.dbRoute+'/userRequests/requests', ref => ref
    .where("user.id", "==", user_id)).snapshotChanges().pipe(map(requests => {
      return requests.map(request => {
        const requestData = request.payload.doc.data() as any;
        requestData.date = requestData.date.toDate();
        requestData.creationDate = requestData.creationDate.toDate();
        return requestData as solicitud;
      });
    }));
  }

  acceptRequest(request: solicitud) {
    request.state = state.accepted;
    this.updateRequest(request);
  }

  rejectRequest(request: solicitud) {
    request.state = state.rejected;
    this.updateRequest(request);
  }

  updateRequest(request: solicitud) {
    const arId = request.user.id+request.id;
    return this.db.collection(this.dbRoute+'/userRequests/requests').doc(arId).update(request);
  }

  createRequest(request: solicitud, userData: any) {
    const arId = request.user.id+request.id;
    const rCount = userData.requestCount+1;
    const rNumber = userData.requestNumber+1;
    return this.db.collection(this.dbRoute+'/userRequests/requests').doc(arId).set(request).then(requestCount => {
      this.db.collection(this.dbRoute+'/userData/users').doc(request.user.id).update({requestCount: rCount, requestNumber: rNumber});
    });
  }

  deleteRequest(request: solicitud, validRequest: boolean) {
    const arId = request.user.id+request.id;
    return this.db.collection(this.dbRoute+'/userRequests/requests').doc(arId).delete().then(requestCount => {
      if(validRequest) {
        this.getUser(request.user.id).then(tempUser => {
          const tu = tempUser as any;
          const cancel =  tu.cancel + 1;
          if(request.state === state.accepted) {
            return this.db.collection(this.dbRoute + '/userData/users').doc(tu.id).update({ cancel: cancel });
          }
        });
      }
    });
  }

  createUser(user_id: string, email: string) {
    var user = {} as usuario;
    user.id = user_id;
    user.username = "Guest";
    user.admin = false;
    user.phone = "Undefined";
    user.email = email;
    user.rating = 0;
    user.cancel = 0;
    user.level = 1;
    user.requestCount = 0;
    user.requestNumber = 0;
    user.allowRequests = true;

    var coolUser = user as any;
    coolUser.language = language.EN;
    coolUser.notifications = false;

    //SE NECESITA REVISION AQUI
    return new Promise((resolve, reject) => {
      this.fcm.getToken().then(token => {
        coolUser.token = token;
        return this.db.collection(this.dbRoute+'/userData/users').doc(user.id).set(coolUser).then(createdUser => {
          resolve(coolUser);
        });
      }).catch(err => {
        coolUser.token = '';
        return this.db.collection(this.dbRoute+'/userData/users').doc(user.id).set(coolUser).then(createdUser => {
          reject(err)
        });
      });
    });
  }
}