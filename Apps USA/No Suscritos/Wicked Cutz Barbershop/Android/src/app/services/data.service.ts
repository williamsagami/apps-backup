import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Wicked Cutz Barbershop",

      homeDescription: 
        `A Haircut Has The Ability To Change Someone's Life. <br>
         Not to sound cliche' or over dramatic about what most would call "Just a Cut". <br>
         That may be so for your chain salons. But when it comes to Wicked Cutz thats not the case,
         every Wicked Barber is extremely passionate about what they do and as a collective understand the importance of ones belief in themselves to succeed. <br>
         What better way to start seeing yourself better and with more confidence than a Wicked Cut. We have always been quality over quantity since we opened 5/2/2016,
         every cut gets the necessary TLC it takes to produce a Friggin Wicked Good Cut. <br>
          As  one barber explains “There's nothing more satisfying than the look on someone's 
          face that has just got the best cut of their life and watching their self-esteem sky rocket”.`,

      homeSlider:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fhome%2F1.jpg?alt=media&token=662811c6-4c81-45da-9d55-38a9f9c6ae5b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fhome%2F3.jpg?alt=media&token=de309d38-5455-4fb3-82b0-6bf0b2a7cef2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fhome%2F4.jpg?alt=media&token=a6a70684-7db4-4734-917f-2c17a2b0fff8",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fhome%2F33.jpg?alt=media&token=a7541259-7c1c-4a0c-95f8-13b649587627",
      ],

      homeServices: [
        "HAIRCUT", 
        "BEARD TRIM", 
        "DESIGN", 
        "EYEBROW", 
        "KID'S HAIRCUT", 
        "LINE UP",
        "HOT TOWEL SHAVE", 
        "COLOR ENHANCEMENT", 
        "RAZOR FADE",
        "BLACK MASK", 
        "HAIR WASH",
      ],
      homeSchedules: [
        {
          day: "Monday",
          hour: "Closed",
        },
        {
          day: "Tuesday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Wednesday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Thursday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Friday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Saturday",
          hour: "10:00 am – 06:00 pm",
        },
        {
          day: "Sunday",
          hour: "12:00 am – 05:00 pm",
        },
      ],
      homeVideos: [
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fhome%2F1.mp4?alt=media&token=453515b3-d80e-410f-b2d7-580e75f94d8b",
         // "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fhome%2F2.mp4?alt=media&token=b01c9c46-115c-44d8-9d4e-2520500c345f",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fhome%2F3.mp4?alt=media&token=2150aea5-8a6e-4cd2-98a2-ad7df13d194f",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "386 872 3694",
        },
        {
          text: "Phone",
          number: "386 274 9260",
        }
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F1.jpg?alt=media&token=196806c1-a52a-4ee8-b604-b1695e806b66",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F10.jpg?alt=media&token=33813632-7b12-48f5-88f1-26a5d293a2d2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F11.jpg?alt=media&token=991cf92c-5b96-409c-ac84-d21cf107eb12",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F12.jpg?alt=media&token=4b3664fe-9f36-4c29-bdc8-3b279c789be3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F13.jpg?alt=media&token=d3f54599-0ba2-4d0e-b7d6-bb413d75ecb6",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F14.jpg?alt=media&token=54da0b15-9de6-476d-a360-770f8d6737bf",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F15.jpg?alt=media&token=2143aecd-61ee-4684-9b5b-4421523d9d40",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F16.jpg?alt=media&token=6b53bdd9-80d7-4763-93db-d10324fb64ac",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F17.jpg?alt=media&token=feef9ff8-3380-4be3-a747-354476a55299",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F18.jpg?alt=media&token=cc0b07d5-7fdb-4a9d-8984-1a4f66992173",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F3.jpg?alt=media&token=224d80d9-8484-43cf-a815-18506bd33c46",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F2.jpg?alt=media&token=db94c6c1-09d0-4431-8c9d-a4c25c93cccc",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F20.jpg?alt=media&token=aa0fd8da-d350-4986-bf88-b0f417f6ee86",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F21.png?alt=media&token=144f8475-152c-40c4-9d37-5ecbcd844480",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F22.png?alt=media&token=200f6fa5-b743-40ce-8217-afa51923ee65",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F23.png?alt=media&token=fcd5af2f-de2c-487f-b265-ac112bfeca48",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F24.png?alt=media&token=e7ca41a0-8710-49e0-8bf8-730ee3b768b1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F25.png?alt=media&token=db862ec6-488e-44a0-988f-369e99c743f1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F26.png?alt=media&token=acda8816-2db3-4865-9e51-1fad935ab280",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F27.png?alt=media&token=61e667e3-e82d-49cd-9c80-78518fb58da3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F29.png?alt=media&token=87a346eb-4f61-433f-ae80-c09f3f00c4db",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F30.png?alt=media&token=e6ce4a5e-c5d2-40f6-b510-18b43960a100",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F31.png?alt=media&token=20ebe504-ec5b-405f-9db3-ffe9e25c7de6",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F32.png?alt=media&token=0075f238-63d8-4d2c-88a0-6e138fb80e8e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F33.png?alt=media&token=cd2bd336-8c4a-48a5-91d9-c408e206bb30",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F34.png?alt=media&token=fb3d64ad-f8e1-4442-a39c-8edda87168c6",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F35.png?alt=media&token=57f1b1a6-06bb-409e-983f-95a910cb43a0",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F36.png?alt=media&token=fd3b2e6b-ada6-42b8-a778-c797a66fe4a7",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F37.png?alt=media&token=f3f7559e-f341-49ea-9b45-c832e228cf58",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F38.png?alt=media&token=b3137f32-8131-4e08-bf80-9efa0c94a588",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F39.png?alt=media&token=d2769250-2680-454d-bf57-a8b1054b493d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F4.jpg?alt=media&token=874d56cd-f1a8-487d-bc03-09936003984d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F41.png?alt=media&token=884290c4-13b6-4d04-9627-74a39c0e0e32",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F42.png?alt=media&token=00b80f2f-b606-4dfa-a037-5fde831cbebf",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F5.jpg?alt=media&token=a38b9bee-a149-4474-b32b-e8094c7f2aae",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F6.jpg?alt=media&token=cd89342e-2533-40b8-965b-b9ca3628af24",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F7.jpg?alt=media&token=90909085-09a1-4dde-a581-82fe1ffcec2b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F8.jpg?alt=media&token=f0c06fbf-9ff9-483e-8261-c2beffa03b23",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F9.jpg?alt=media&token=ba97b51d-6c4b-442f-845d-98ddc82ed42b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F19.jpg?alt=media&token=301e3925-0033-4756-979d-bf82f9939339",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fgallery%2F28.png?alt=media&token=f0e48505-c292-40a8-b53c-a26f0aba7af0",
      ],

      products:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp.jpg?alt=media&token=bb31eb2d-b7ff-4293-be41-cadb32fa33fc",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp1.jpg?alt=media&token=02feaf94-7efc-4b82-9879-52ea0cb1cfe9",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp10.jpg?alt=media&token=b28d86bf-ccf1-4289-aceb-ebfc9fea196c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp11.jpg?alt=media&token=bfe947eb-e194-40b6-b0ed-d5e7d583bcb0",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp12.jpg?alt=media&token=eef387d0-a061-4ab4-9ffa-5b2489e264b0",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp13.jpg?alt=media&token=d9b9c5be-154a-41bc-a3e0-96606f8dff63",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp14.jpg?alt=media&token=984edea1-9e08-4565-b173-59026259268f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp15.jpg?alt=media&token=448ef1ff-05ec-4b3b-93f5-04bef28078af",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp16.jpg?alt=media&token=f1a078b0-4f17-45b3-b94e-6cc7aa03d36f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp17.jpg?alt=media&token=44da5f3c-fe07-44d3-ba89-284375061d7a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp18.jpg?alt=media&token=3508ef00-267c-414f-ad2e-7129cc781d22",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp2.jpg?alt=media&token=bfee5463-4cfc-4d55-b9a4-8df2c444ed60",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp20.jpg?alt=media&token=7f87d461-2a60-4655-bfac-3509ff892697",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp21.jpg?alt=media&token=fff34b0a-9d5b-4cc5-8657-a1c5d55bafc9",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp22.jpg?alt=media&token=fb47641d-63f7-44af-9b19-225c0558b8df",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp23.jpg?alt=media&token=a4050549-d601-4586-b59e-15b18442ee8e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp24.jpg?alt=media&token=438ab238-7840-4547-8195-780f26e783ae",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp25.jpg?alt=media&token=bb1154d7-1f04-4bae-9d8b-24096a81e7b5",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp26.jpg?alt=media&token=6cee0f69-f71b-47af-a0d7-41679d0d4931",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp27.jpg?alt=media&token=f9b4294c-01dd-453e-97cf-f6fddadafa00",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp28.jpg?alt=media&token=d74df442-d4f7-47c6-b04d-379682aeebcc",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp29.jpg?alt=media&token=9311cebd-26c1-45b0-9aa6-12b323e53d58",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp3.jpg?alt=media&token=1e18192a-e817-44c7-9c21-49d193ef158a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp30.jpg?alt=media&token=1ba0cb56-159d-4e99-8f8c-cb13a24c88e7",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp31.jpg?alt=media&token=ca65cd6d-3a0f-4d4b-b886-dd2ad36f39fe",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp32.jpg?alt=media&token=0b1d4814-7bc7-4acc-b782-5b49e03f9aaf",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp33.jpg?alt=media&token=21ec1a75-dd55-4df9-a7a6-4376e5fef20d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp4.jpg?alt=media&token=a18aa422-5f59-485d-a619-fba916d16c84",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp5.jpg?alt=media&token=83a939eb-d651-49c7-8ea0-628238203ded",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp6.jpg?alt=media&token=8e9046fd-f0ff-4704-9ca7-b16afd3536d9",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp7.jpg?alt=media&token=f584889d-dae2-47c9-9998-d61a33117554",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp8.jpg?alt=media&token=c458c423-9682-4030-b01c-2125e3e61893",      
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp19.jpg?alt=media&token=efde8795-7cfe-487a-8e4b-c3fe4edc7325",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2Fproducts%2Fp9.jpg?alt=media&token=ff89bd26-598b-460a-bb4e-48370ad31c2b",
      ],


      socialNetworks: [
        {
          text: "wickedcutzbarbershop",
          link: "https://www.instagram.com/wickedcutzbarbershop/",
          icon: "logo-instagram",
        },
        {
          text: "Wicked Barbers",
          link: "https://www.youtube.com/channel/UC4Mr5Xif0YqgEGPnkBz1zzA",
          icon: "logo-Youtube",
        },
        {
          text: "Wicked Cutz Barber Shop",
          link: "https://www.facebook.com/WickedCutzDaytona/",
          icon: "logo-Facebook",
        },
        {
          text: "Wicked Cutz Barber Shop",
          link: "https://www.yelp.com/biz/wicked-cutz-barber-shop-south-daytona-2",
          icon: "logo-yahoo",
        },
        {
          text: "Wicked Cutz BarberShop Web",
          link: "https://wickedcutzbarbershop.com/",
          icon: "globe",
        }
      ],

      address: "933 Beville Road, Daytona Beach, Florida 32119, United States",

      addressRoute: "933 Beville Road, Daytona Beach, Florida 32119, United States",

      mapSource: "https://maps.google.com/maps?q=Wicked%20Cutz%20Barbershop,%20933%20Beville%20Road,%20Daytona%20Beach,%20Florida%2032119,%20United%20States&t=&z=15&ie=UTF8&iwloc=&output=embed",
    
      barbers:[
        {
          name: "Sammy",
          description: "Baricua Barber",
          socialNetworks:[],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2FAppoiments%2Fi.JPG?alt=media&token=df7d1cc3-7508-4a61-9e9e-d55c5b4fe623",

        },

        {
          name: "O’Neil",
          description: "Professional Barber",
          socialNetworks:[
            {
              text: "barneseyupup",
              link: "https://www.instagram.com/barneseyupup/",
              icon: "logo-instagram",
            }
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2FAppoiments%2Fi1.jpg?alt=media&token=035b8711-654a-4ed9-a129-40856bea3738",

        },

        {
          name: "Gia",
          description: "Pro-Folio",
          socialNetworks:[
            {
              text: "giftedgucci",
              link: "https://www.instagram.com/giftedgucci/",
              icon: "logo-instagram",
            }
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2FAppoiments%2Fi3.jpg?alt=media&token=b68b08a0-179c-43da-9d44-ee210d1d29d7",

        },
        {
          name: "Mel",
          description: "Master Barber",
          socialNetworks:[
            {
              text: "melchapo_barber65",
              link: "https://www.instagram.com/melchapo_barber65/",
              icon: "logo-instagram",
            }
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2FAppoiments%2Fi4.jpg?alt=media&token=e271dd36-acff-461b-8129-55b91bb3d58d",

        },
        {
          name: "Josh",
          description: "A mix of creativity, persistence, efficiency, and effectiveness! Resulting in the highest availing of excellence.",
          socialNetworks:[
            {
              text: "pikasso_kutz",
              link: "https://www.instagram.com/pikasso_kutz/",
              icon: "logo-instagram",
            }
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2FAppoiments%2Fi5.jpg?alt=media&token=fc984d54-d3ec-40a0-b5f4-e94e5a1931fa",

        },
        {
          name: "Charles",
          description: "God Gifted Me.",
          socialNetworks:[
            {
              text: "charlescutz933",
              link: "https://www.instagram.com/charlescutz933/",
              icon: "logo-instagram",
            }
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2Fwickedcutz%2FAppoiments%2Fi6.JPG?alt=media&token=a5efd56a-2530-4009-82d0-a4d9775c4063",

        },
      ],
    }

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

   this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
