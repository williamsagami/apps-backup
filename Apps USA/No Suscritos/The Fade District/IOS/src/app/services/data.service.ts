import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "The Fade District",

      homeDescription: 
        `The Fade District A barbershop with more than 6 years of experience. <br>
        professional barbers focused on giving the best service every day where they will make you and your family have the best haircuts. <br>
        They are the best barbershop in California and the best technology for you, having an incredible experience. <br>
        We have 4.8 / 5 on Boosky and 4.9 / 5 on Facebook so our clients know that we are serious and professional, do not hesitate to make an appointment NOW!`,


        homeSlider:[
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fhome%2F11_1024x1024.jpg?alt=media&token=d9985d7a-0ee8-4d3a-8725-e397ef40778e",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fhome%2F13_1024x1024.jpg?alt=media&token=422fabcd-d566-408f-96a8-d1bac00ecd46",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fhome%2F12_1024x1024.jpg?alt=media&token=0b6035b9-e4d7-47f8-ba87-91f780112924",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fhome%2F23_1024x1024.jpg?alt=media&token=d6324319-7cfe-4a2e-bc1e-7d1eb7fb6194",          
        ],


        homeServices: [
          {
            name:"Walk in",
            // time:"30 minutes",
           // description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
            price: "$30"
          },
          {
            name:"Appoinments",
            // time:"30 minutes",
           // description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
            price: "$35"
          },
          {
            name:"Hair cut + shave",
            // time:"30 minutes",
           // description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
            price: "$40"
          },
          {
            name:"kids (3-9)",
            // time:"30 minutes",
           // description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
            price: "$25"
          },

        ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "11:00 am – 07:00 pm",
        },
        {
          day: "Tuesday",
          hour: "11:00 am – 07:00 pm",
        },
        {
          day: "Wednesday",
          hour: "11:00 am – 07:00 pm",
        },
        {
          day: "Thursday",
          hour: "11:00 am – 07:00 pm",
        },
        {
          day: "Friday",
          hour: "11:00 am – 07:00 pm",
        },
        {
          day: "Saturday",
          hour: "10:00 am – 04:00 pm",
        },
        {
          day: "Sunday",
          hour: "Closed",
        },
      ],
      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fhome%2F1.mp4?alt=media&token=ce99a5eb-225d-4888-b271-300c615c4a66", 
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fhome%2F2.mp4?alt=media&token=6993b11b-a139-4581-8fd0-569aa0743cd9",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "714 886 2816",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F9_1024x1024.jpg?alt=media&token=5131c6d4-387f-449a-84ae-cc0a99947413",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F10_1024x1024.jpg?alt=media&token=1859dc1d-febc-4ab7-a5bf-c331e467b43c",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F11_1024x1024.jpg?alt=media&token=2e2deae2-e925-4dc1-a7d6-b33c4bd7e284",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F12_1024x1024.jpg?alt=media&token=c1cab919-85d7-4ddd-bb83-b892fcaffc1a",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F13_1024x1024.jpg?alt=media&token=bc296c04-a549-47a5-aae8-420a746981f3",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F26_1024x1024.jpg?alt=media&token=69079e98-58f4-47e0-8764-cb409c2d6c54",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F14_1024x1024.jpg?alt=media&token=bc1b22be-23c4-4ed7-af08-7a7ad059dda1",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F15_1024x1024.jpg?alt=media&token=9cc8cccd-a021-4feb-9cdc-a3b7f8549d8b",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F16_1024x1024.jpg?alt=media&token=ad1f9f21-47c9-4cfe-bd21-5a95728189ec",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F17_1024x1024.jpg?alt=media&token=18deea69-2d91-4e59-a2f0-b419dce8b4ff",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F18_1024x1024.jpg?alt=media&token=82ba651c-c467-4210-896b-179f533b9998",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F19_1024x1024.jpg?alt=media&token=62323cc7-3d1a-47de-afb6-f4cf92a95f16",
       //"https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F1_1024x1024.jpg?alt=media&token=3fdefe8f-4b77-4b44-823a-9b6762270f1d",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F20_1024x1024.jpg?alt=media&token=0c55aba1-9330-4da9-a330-fcd4ac2621a5",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F21_1024x1024.jpg?alt=media&token=009a6ba2-5772-4f9b-b9b8-5494e6cb1cec",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F22_1024x1024.jpg?alt=media&token=406af35b-dcbc-4ce2-a867-5bb185310f43",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F23_1024x1024.jpg?alt=media&token=3ba96db3-f139-4b59-8fdb-dd748aaf6fba",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F24_1024x1024.jpg?alt=media&token=53103f25-9dde-4c31-bf01-d3161288ca00",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F25_1024x1024.jpg?alt=media&token=c3bb193c-9b7d-4b5d-86ec-b42cc61af5bf",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F2_1024x1024.jpg?alt=media&token=6c711d14-9d74-470d-92d7-22bbacadb3a2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F3_1024x1024.jpg?alt=media&token=a5954168-126e-4c22-89e6-9511448ccd98",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F4_1024x1024.jpg?alt=media&token=5a80e4e5-79dc-47f5-ad62-72c302602fea",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F5_1024x1024.jpg?alt=media&token=63d71c66-a28b-43a3-870a-14358c5de0fa",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F6_1024x1024.jpg?alt=media&token=332a15a9-6c9a-4ea3-bbbb-5bb337648663",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F7_1024x1024.jpg?alt=media&token=4e8d9202-d388-4fa6-be11-745a8b8c95d7",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FThe%20Fade%20District%2Fgallery%2F8_1024x1024.jpg?alt=media&token=abbcee86-c244-4607-a1f4-9c825f492a37"
      ],

      products:[
       ],

      socialNetworks: [
        {
          text: "The Fade District",
          link: "https://www.facebook.com/thefadedistrictca/",
          icon: "logo-facebook", 
        },
        {
          text: "the_fade_district",
          link: "https://www.instagram.com/the_fade_district/?hl=es",
          icon: "logo-instagram", 
        },
        {
          text: "The Fade District booksy",
          link: "https://booksy.com/en-us/623663_the-fade-district_barber-shop_103710_cypress",
          icon: "book", 
        },
        {
          text: "The Fade District",
          link: "https://www.yelp.com/biz/the-fade-district-cypress",
          icon: "logo-yahoo", 
        },
      ],
      address: "West, 5365 Lincoln Ave, Cypress, CA 90630, USA",

      addressRoute: "West, 5365 Lincoln Ave, Cypress, CA 90630, USA",

      mapSource: "https://maps.google.com/maps?q=The%20Fade%20District&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }
  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 
    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
        //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)}); 
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
