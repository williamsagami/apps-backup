import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "ManCave Barbershop- Layton",

      homeDescription: 
        `One of the best barbershops in Utah, with impressive quality and spectacular professionalism, 
        where our specialists makes the best cuts every day for their customers. Show your haircut like never before. <br>
        You will take an excellent experience and at an incredible price! Do not hesitate to make your appointment now and you will see how your style grows.
        For us the most important thing is  your haircut.`,


        homeSlider:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fhome%2F1s_512x512.jpg?alt=media&token=392a659d-2abf-4dec-aea4-9a555367b61d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fhome%2F2s_512x512.jpg?alt=media&token=225c096f-8e27-413f-a181-f44b8d26790a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fhome%2F3s_512x512.jpg?alt=media&token=6e54a6f9-352b-4b98-b936-f7c09f214767",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fhome%2F4s_512x512.jpg?alt=media&token=9271a37d-2dc5-4588-a199-553f1d3ac486",
        ],


      homeServices: [

      ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Tuesday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Wednesday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Thursday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Friday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Saturday",
          hour: "10:00 am – 07:00 pm",
        },
        {
          day: "Sunday",
          hour: "Closed",
        },
      ],

      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fhome%2F1.mp4?alt=media&token=227c3cb6-35db-40e4-b9f6-bd108f438343",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fhome%2F2.mp4?alt=media&token=89e5b9dc-79a2-4e7d-85f8-ddcae23a6b60",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fhome%2F3.mp4?alt=media&token=39eab194-4676-4584-b85d-d7af3d561bdd",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "385 900 6031",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F34_512x512.jpg?alt=media&token=0ac7cf93-29c0-4cef-83a3-75f84258e7bb",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F39_512x512.jpg?alt=media&token=2ed0aebd-4283-447c-ae09-aa9b3ce584d1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F36_512x512.jpg?alt=media&token=8e9ecbcb-5057-45f7-b260-9a7f39999a42",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F33_512x512.jpg?alt=media&token=5bd8bb1e-8acf-496c-b2b2-3deaa89dc0ce",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F37_512x512.jpg?alt=media&token=718c2e0f-f0b5-436c-bf61-467cd03d7f3c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F31_512x512.jpg?alt=media&token=0719300a-6cb2-453b-8c0b-c88e69381232",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F35_512x512.jpg?alt=media&token=1ced21f4-102d-4fde-af81-cfe141b3c43e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F28_512x512.jpg?alt=media&token=e61993c8-81da-4563-951b-b8868dad6fa8",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F28_512x512.jpg?alt=media&token=e61993c8-81da-4563-951b-b8868dad6fa8",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F32_512x512.jpg?alt=media&token=805e95ca-2e2b-4fc0-aba1-465eedf94ddc",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F29_512x512.jpg?alt=media&token=3da1d5f7-c035-4c30-ad7f-59de695e1dcf",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F30_512x512.jpg?alt=media&token=458785ca-62f1-4f9b-9e64-f3ce4b3b4643",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F9_512x512.jpg?alt=media&token=18339cf9-3a53-46b3-853a-ac2276ee063b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F1_512x512.jpg?alt=media&token=9cbbd215-66dd-4c92-9bb0-3a9c4b1ed09a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F2_512x512.jpg?alt=media&token=935216c7-0186-4efb-af7d-560da460dc52",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F10_512x512.jpg?alt=media&token=ed4bfd6d-a3e7-41e1-b8c1-a020f0a896c9",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F11_512x512.jpg?alt=media&token=90c8ea71-667d-43c4-8f01-c049001cae4a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F12_512x512.jpg?alt=media&token=c46328e3-24d7-4b1a-b5db-78a3d9bb89ac",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F13_512x512.jpg?alt=media&token=5e74603f-a7ac-4939-85b5-bf9ee7887376",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F14_512x512.jpg?alt=media&token=6c83a97d-1b68-45cd-a4d4-1f3251d2a8f3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F15_512x512.jpg?alt=media&token=d6c0000f-b5e2-464a-931e-b058bf52018a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F16_512x512.jpg?alt=media&token=e721a7fc-8712-483b-be7d-e6123c836ee8",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F17_512x512.jpg?alt=media&token=c985f7cf-0955-41e7-9530-0bd91fa80d7d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F18_512x512.jpg?alt=media&token=a22edf4a-1317-40e7-ba8e-8ce495840136",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F19_512x512.jpg?alt=media&token=da058967-eaec-4baa-8b07-e0875501dd5a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F20_512x512.jpg?alt=media&token=46484a45-d13b-4abd-85c5-77afc93e5660",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F21_512x512.jpg?alt=media&token=820d7288-ad06-4106-b747-9a7e42d20f79",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F22_512x512.jpg?alt=media&token=c41308f0-93c5-45e7-b51c-28d4d91bcef5",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F23_512x512.jpg?alt=media&token=ecb8da11-576b-4dca-903b-79850fcfe145",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F24_512x512.jpg?alt=media&token=8357c816-6c45-4c1d-af4f-e2fb642504b4",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F25_512x512.jpg?alt=media&token=20d382b4-8f2a-4ffe-bb8a-1be34dc5bafe",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F26_512x512.jpg?alt=media&token=50c6ae26-dd6d-45f4-beb7-3ccc5ef9a945",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F27_512x512.jpg?alt=media&token=2ec5e339-7951-4496-9222-c877f1288d76",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F3_512x512.jpg?alt=media&token=95ce0adf-8e75-4236-9902-6d510e4d59bf",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F4_512x512.jpg?alt=media&token=4c4bdf24-211e-49c3-94c8-a3814ff973d6",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F5_512x512.jpg?alt=media&token=8a86b58f-ef4d-4abc-a317-82a6e757d5c2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F6_512x512.jpg?alt=media&token=a53b868a-c1ec-41ca-b164-330fbc6b2f75",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F7_512x512.jpg?alt=media&token=c003d1f0-9bf5-459e-bd96-4ec728645999",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fgallery%2F8_512x512.jpg?alt=media&token=1f5acc8f-a5f3-4ce4-af16-8dc426111f6d",  
      ],

      products:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp11_512x512.jpg?alt=media&token=80ce3b08-90bb-4cac-b247-bdc47ed4ffa1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp12_512x512.jpg?alt=media&token=8d90f517-d30a-45d9-93c2-42e52c895835",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp13_512x512.jpg?alt=media&token=f7c660fd-060e-4d04-9b48-7446103b2533",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp14_512x512.jpg?alt=media&token=0b92cf53-8fdf-4bbf-b30a-25c806010f8c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp15_512x512.jpg?alt=media&token=93c6383f-0fe5-4f7a-a6e4-8aa62e18eb97",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp16_512x512.jpg?alt=media&token=94e25e0f-9d78-46ee-bafe-39d9670dffa5",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp17_512x512.jpg?alt=media&token=5d90e026-37ef-4752-bd54-155856aba15e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp18_512x512.jpg?alt=media&token=775e7e4a-2687-4958-b20b-51804babce23",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp19_512x512.jpg?alt=media&token=dbcdacdd-9382-469c-8c7f-1a9edf86f611",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp20_512x512.jpg?alt=media&token=aeccdd8e-9dd9-4386-b847-871281c4648a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp20_512x512.jpg?alt=media&token=aeccdd8e-9dd9-4386-b847-871281c4648a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp21_512x512.jpg?alt=media&token=e9c6693d-6485-4564-9b75-54c890bbf6c7",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp22_512x512.jpg?alt=media&token=ec73ad89-776d-4c11-92bf-7a7f270c8ed0",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp23_512x512.jpg?alt=media&token=f90b4471-6b44-41b1-8e0c-d320ca575072",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp24_512x512.jpg?alt=media&token=5516de50-8ece-4dac-b39a-a6ba24d8d668",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp25_512x512.jpg?alt=media&token=75d275d2-7d71-43b2-997d-5494fd52ba3a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp26_512x512.jpg?alt=media&token=966e7ad6-84dc-402c-8a99-c53e71454fe5",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp27_512x512.jpg?alt=media&token=6b170454-c851-4714-aba9-49b4713d2d32",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp28_512x512.jpg?alt=media&token=bbeb208c-6ff0-4a6a-bf1e-bbac60108e53",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp29_512x512.jpg?alt=media&token=ac3d2830-ed14-4cda-8505-71928940c0c2",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp1_512x512.jpg?alt=media&token=03caf166-f381-4c41-818b-1b110cdc41dd",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp3_512x512.jpg?alt=media&token=083380a6-96f3-41c4-8f1b-69ad113b93da",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp4_512x512.jpg?alt=media&token=580cec4e-83d6-4d3a-bbad-4928538f9b69",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp5_512x512.jpg?alt=media&token=2151c91f-90c4-4cf9-b748-5e5214550b8c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp6_512x512.jpg?alt=media&token=72339bc6-7d3b-49b2-8581-865678477692",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp7_512x512.jpg?alt=media&token=f430a3c6-1bc8-49ac-ac41-e0280bbc8a07",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp8_512x512.jpg?alt=media&token=3051bb03-f3c9-40fa-b923-e7276aa7e381",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp9_512x512.jpg?alt=media&token=676d2f91-3b9c-4593-8a7c-b06af37bd1b7",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp10_512x512.jpg?alt=media&token=ccdb1aeb-9438-46a8-9d4a-43305d85256a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fp2_512x512.jpg?alt=media&token=a65e6d64-74a4-42e5-9fe1-5fe800d43037",
      ],

      productsVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FManCave%20Barbershop%20Layton%2Fproducts%2Fpv.mp4?alt=media&token=4515ed17-3607-4245-9da2-4f388ed00eda",     
      ],
      socialNetworks: [
        {
          text: "ManCave Barbershop- Layton",
          link: "https://www.facebook.com/ManCave801/",
          icon: "logo-facebook",
        },
        {
          text: "mancave_barber_shop",
          link: "https://www.instagram.com/mancave_barber_shop/",
          icon: "logo-instagram",
        },
        {
          text: "ManCave Barbershop Layton",
          link: "https://mcfades.com/",
          icon: "globe",
        },

      ],

      address: "2146 N Main St Suite 526, Layton, UT 84041, United States",

      addressRoute:"2146 N Main St Suite 526, Layton, UT 84041, United States",

      mapSource: "https://maps.google.com/maps?q=ManCave%20Barbershop-%20Layton,%202146%20N%20Main%20St%20Suite%20526,%20Layton,%20UT%2084041,%20Estados%20Unidos&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

  //  this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
