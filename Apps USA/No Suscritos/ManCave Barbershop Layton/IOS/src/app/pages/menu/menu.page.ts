import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { MenuController, ModalController, Platform, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  d: any;
  userReady = false;
  user: any;
  exit: any;

  selectedUrl = '/menu/inicio';
  pages = [
    {
      text: "Home",
      url: '/menu/home',
      icon: "home",
    },
    {
      text: "Appointments",
      url: '/menu/appointments',
      icon: "book",
    },
    // {
    //   text: "Gallery",
    //   url: '/menu/gallery',
    //   icon: "images",
    // },
    // {
    //   text: "Products",
    //   url: '/menu/product',
    //   icon: "cash",
    // },
    // {
    //   text: "Contact",
    //   url: '/menu/contact',
    //   icon: "person",
    // }
  ]; 

  social1 =[
    {
      text: "Visit Store ",
      url: 'https://mcfades.com/shop-products',
      icon: "cart",
    }
  ]

  // pages1 = [
  //   {
  //     text: "Gallery",
  //     url: '/menu/gallery',
  //     icon: "images",
  //   },
  //   {
  //     text: "Products",
  //     url: '/menu/product',
  //     icon: "cash",
  //   },
  //   {
  //     text: "Contact",
  //     url: '/menu/contact',
  //     icon: "person",
  //   }
  // ]; 
  constructor(private router: Router, public data: DataService, 
    private firebaseService: FirebaseService, 
    private toastController: ToastController, 
    private platform: Platform, 
    private modalController: ModalController,
    private menuController: MenuController) { 
      this.d = data.appData.data;
  }

  ionViewDidEnter(){
    /*this.router.events.subscribe((event: RouterEvent) => {
      this.selectedUrl = event.url ? event.url : this.selectedUrl;
    }); */

    this.exit = this.platform.backButton.subscribe(()=>{
      this.modalController.getTop().then(m => {
        if(!m) {
          this.menuController.isOpen().then(a => {
            if(!a) {
              if(this.selectedUrl === '/menu/inicio') navigator['app'].exitApp();
              else {
                this.router.navigate(['/menu/inicio']);
                this.selectedUrl = '/menu/inicio';
              }
            }
          });
        }
      });
    });
  }


  ngOnInit() {
    this.firebaseService.userSetup().then(userDataExist => {
      this.userReady = userDataExist as any;
    });
  }

  async showInfo(info: string) {
    const toast = await this.toastController.create({
      message: info,
      duration: 6000,
      color: 'secondary',
    });
    toast.present();
  }

  showSubscriptionButton() {
    if(this.userReady) {
      if(this.firebaseService.userData) {
        if(!this.firebaseService.userData.admin) return !this.firebaseService.userData.notifications;
        else return false;
      } 
    }
    else return false;
  }

  toggleSalesNotifications() {
    if(this.userReady) {
      if(!this.firebaseService.userData.notifications) {
        var userData = this.firebaseService.userData;
        userData.notifications = true;
        this.firebaseService.updateUser(userData).then(updateUser => { 
          this.firebaseService.fcm.subscribe(this.firebaseService.getDataBaseName()+"-sales").then(showInfo => {
            this.showInfo("Notifications On.");
          });
        });
      }
    }
    else {
      this.router.navigate(['/menu/login']);
    }
  }
}
