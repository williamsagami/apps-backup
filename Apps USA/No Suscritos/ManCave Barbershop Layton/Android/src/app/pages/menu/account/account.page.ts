import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseService } from 'src/app/services/firebase.service';
import { LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  userReady = false;
  private userData: any;
  appConfiguration: any;
  activateNotifications = false;

  private loading: any;

  constructor(
    public firebaseService: FirebaseService, 
    private router: Router, 
    private loadingController: LoadingController, 
    private toastController: ToastController) { }

  ngOnInit() {
    this.getAppData();
  }

  getAppData(){
    this.loadingController.create({
      message: 'Loading...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      //Tomar Configuracion
      this.firebaseService.getAppConfiguration().then(appConfiguration => {
        this.appConfiguration = appConfiguration as any;

        //Datos de Usuario
        this.firebaseService.userSetup().then(userDataExist => {
          this.userReady = userDataExist as any;
          if(userDataExist) {
            this.activateNotifications = this.firebaseService.userData.notifications;
            this.loading.dismiss();
          }
        });
      });
    });
  }

  async showInfo(info: string) {
    const toast = await this.toastController.create({
      message: info,
      duration: 6000,
      color: 'secondary',
    });
    toast.present();
  }

  logout() {
    this.loadingController.create({
      message: 'Loging Out...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      this.firebaseService.logout().then(redirect => {
        this.loading.dismiss();
        this.router.navigate(['/menu/login']);
      });
    });
  }

  setMaxRequests(event) {
    this.appConfiguration.maxUserRequestNumber = event.detail.value;
  }

  setDayRangeLimit(event) {
    this.appConfiguration.dayRangeLimit = event.detail.value;
  }

  getAdminText() {
    return this.firebaseService.userData.admin ? "Admin" : "User";
  }
  
  saveAccountChanges() {
    this.loadingController.create({
      message: 'Saving...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      this.userData = this.firebaseService.userData;
      this.userData.notifications = this.activateNotifications;
      if(this.userData.admin) {
        this.firebaseService.updateUser(this.userData).then(updateAppConfig => {
          const appConfig = {
            maxUserRequestNumber: this.appConfiguration.maxUserRequestNumber, 
            dayRangeLimit: this.appConfiguration.dayRangeLimit,
            maxUserRating: 10,
          };
          this.firebaseService.updateAppConfiguration(appConfig).then(showInfo => {
              this.loading.dismiss();
              this.showInfo("The changes have been saved successfully.");
          });
        });
      } else {
        this.firebaseService.updateUser(this.userData).then(showInfo => {
          this.loading.dismiss();
          this.showInfo("The changes have been saved successfully.");
        });
      }
      this.toggleSalesNotifications();
    });
  }

  toggleSalesNotifications() {
    if(!this.activateNotifications) {
      this.firebaseService.fcm.unsubscribe(this.firebaseService.getDataBaseName()+"-sales").then(showInfo => {
        this.showInfo("Notifications Off");
      });
    }
    else {
      if(!this.firebaseService.userData.admin)
      {
        this.firebaseService.fcm.subscribe(this.firebaseService.getDataBaseName()+"-sales").then(showInfo => {
          this.showInfo("Notifications On");
        });
      }
    }
  }
}
