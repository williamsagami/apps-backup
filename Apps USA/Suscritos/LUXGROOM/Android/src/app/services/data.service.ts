import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "LUXGROOM",

      homeDescription: 
        `LUXGROOM Mobile Barber Technologies is a modern barber shop that offers today’s gentlemen a clean and safe setting to sit back,
         relax and experience the “New Age” style of male grooming services. <br>
         LUXGROOM caters to today’s adult professional, urban market and a discerning customer base - adding tremendous value to the local community. <br>
         The combination of “World Class Services” coupled with a clean and comforting atmosphere will firmly establish LUXGROOM as THE innovative service that provides today’s client with a mobile, private environment and quality grooming services`,
        
         homeVideo:[
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2Fluxgroom%2Fhome%2F204537664_774013460171859_7419237627389897483_n.mp4?alt=media&token=7615ce91-6e47-4183-97af-45a7cef4bc1d",
          ],

         homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2Fluxgroom%2Fhome%2F1_512x512.jpg?alt=media&token=1c7bd54c-7f73-43d3-881e-ece270f63624",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2Fluxgroom%2Fhome%2F2_512x512.jpg?alt=media&token=435cfa88-ed13-4287-902e-2dbbe231bc19",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2Fluxgroom%2Fhome%2F3_512x512.jpg?alt=media&token=a73b74b9-37cf-4b72-9196-c8fca862aeba",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2Fluxgroom%2Fhome%2F4_512x512.jpg?alt=media&token=30699e7e-8c74-4a25-955a-6d2095091782",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2Fluxgroom%2Fhome%2F5_512x512.jpg?alt=media&token=485a7c4f-a922-4246-8b00-af9f5b29cce2",
          ],

      homeServices: [
        {
          name:"The LuxGroom Package",
          price: "$200"
        },
        {
          name:"The League MVP",
          price: "$150"
        },
        {
          name:"The Franchise Player",
          price: "$90"
        },
        {
          name:"The Starter",
          price: "$60"
        },
        {
          name:"The Head Shave",
          price: "$50"
        },
        {
          name:"Hydrothermal Head Shave",
          price: "$65"
        },
        {
          name:"The Perfect Shave",
          price: "$50"
        },
      ],


      homeSchedules: [
        {
          day: "Monday",
          hour: "On Demand Call for Appt",
        },
        {
          day: "Tuesday",
          hour: "Union Square",
        },
        {
          day: "Wednesday",
          hour: "Mission Bay",
        },
        {
          day: "Thursday",
          hour: "Peninsula On Demand Call for Appt",
        },
        {
          day: "Friday",
          hour: "Union Square",
        },
        {
          day: "Saturday",
          hour: "Oakland Lake Merritt",
        },
        {
          day: "Sunday",
          hour: "Closed",
        },
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "415 994 6699",
        }
      ],

      socialNetworks: [
        {
          text: "LuxGroom",
          link: "https://www.facebook.com/LUXGROOM",
          icon: "logo-facebook",
        },
        {
          text: "luxgroom",
          link: "https://www.instagram.com/luxgroom/",
          icon: "logo-instagram",
        },
        {
          text: "@luxgroom",
          link: "https://twitter.com/luxgroom",
          icon: "logo-twitter",
        },
        {
          text: "LUXGROOM Schedule Now",
          link: "https://www.schedulicity.com/scheduling/dbhgg6",
          icon: "globe",
        },
        {
          text: "LUXGROOM Appointment",
          link: "https://luxgroom.net/?fbclid=IwAR2ydZyLNUVQc_8SwFnfNfua1BNJEl82AWJzBSxhXUNGInPPpyAks384bRE",
          icon: "calendar",
        }
      ],

      address: "555 Sutter St 3rd floor, San Francisco, CA 94102, USA",

      addressRoute:"555 Sutter St 3rd floor, San Francisco, CA 94102, USA",

      mapSource: "https://maps.google.com/maps?q=LUXGROOM&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

      //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
