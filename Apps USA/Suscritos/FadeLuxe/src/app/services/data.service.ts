import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Fade Lux Barbershop",

      homeDescription: 
        `Fade Luxe Barbershop is appointment only. We are licensed professionals and we take pride in our craft. <br>
        We strive to meet your needs and present you with a clean haircut and end result. We keep things sanitary for yours and our protection. <br>
        We’ve got it under control here at Fade Luxe. Come get a haircut.`,

      homeServices: [
        "Nails", 
        "Hair", 
        "Microblading", 
        "Manicure and Pedicure", 
        "Eyelashes", "Hair Extensions", 
        "Facials and Much More",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "323 433 4400",
        }
      ],

      socialNetworks: [
        /*{
          text: "Madelin's Salon",
          link: "https://www.facebook.com/Madelins-Salon-108182611103315/",
          icon: "logo-facebook",
        },*/
        {
          text: "fadeluxebarbershop",
          link: "https://www.instagram.com/fadeluxebarbershop/",
          icon: "logo-instagram",
        }
      ],

      address: "646 N Fuller Ave, Los Angeles, CA 90036, USA",

      mapSource: "https://maps.google.com/maps?q=fade%20lux%20barbershop&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

    //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
