import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  hours=[
    // {
    //   text: "00:00 AM",
    //   value: moment().hours(0).minutes(0).seconds(0)
    // },
    // {
    //   text: "00:30 AM",
    //   value: moment().hours(0).minutes(30).seconds(0)
    // },
    // {
    //   text: "01:00 AM",
    //   value: moment().hours(1).minutes(0).seconds(0)
    // },
    // {
    //   text: "01:30 AM",
    //   value: moment().hours(1).minutes(30).seconds(0)
    // },
    // {
    //   text: "02:00 AM",
    //   value: moment().hours(2).minutes(0).seconds(0)
    // },
    // {
    //   text: "02:30 AM",
    //   value: moment().hours(2).minutes(30).seconds(0)
    // },
    // {
    //   text: "03:00 AM",
    //   value: moment().hours(3).minutes(0).seconds(0)
    // },
    // {
    //   text: "03:30 AM",
    //   value: moment().hours(3).minutes(30).seconds(0)
    // },
    // {
    //   text: "04:00 AM",
    //   value: moment().hours(4).minutes(0).seconds(0)
    // },
    // {
    //   text: "04:30 AM",
    //   value: moment().hours(4).minutes(30).seconds(0)
    // },
    // {
    //   text: "05:00 AM",
    //   value: moment().hours(5).minutes(0).seconds(0)
    // },
    // {
    //   text: "05:30 AM",
    //   value: moment().hours(5).minutes(30).seconds(0)
    // },
    // {
    //   text: "06:00 AM",
    //   hours: 6,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "06:30 AM",
    //   hours: 6,
    //   minutes:30,
    //   seconds: 0
    // },
    //  {
    //   text: "07:00 AM",
    //   hours: 7,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "07:30 AM",
    //   hours: 7,
    //   minutes:30,
    //   seconds: 0
    // },
    // {
    //   text: "08:00 AM",
    //   hours: 8,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "08:30 AM",
    //   hours: 8,
    //   minutes:30,
    //   seconds: 0
    // },
    // {
    //   text: "09:00 AM",
    //   hours: 9,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "09:30 AM",
    //   hours: 9,
    //   minutes:30,
    //   seconds: 0
    // },
    {
      text: "10:00 AM",
      hours: 10,
      minutes:0,
      seconds: 0
    },
    {
      text: "10:30 AM",
      hours: 10,
      minutes:30,
      seconds: 0
    },
    {
      text: "11:00 AM",
      hours: 11,
      minutes:0,
      seconds: 0
    },
    {
      text: "11:30 AM",
      hours: 11,
      minutes:30,
      seconds: 0
    },

    {
      text: "12:00 AM",
      hours: 12,
      minutes:0,
      seconds: 0
    },
    {
      text: "12:30 AM",
      hours: 12,
      minutes:30,
      seconds: 0
    },
    {
      text: "13:00 AM",
      hours: 13,
      minutes:0,
      seconds: 0
    },
    {
      text: "01:30 PM",
      hours: 13,
      minutes:30,
      seconds: 0
    },
    {
      text: "02:00 PM",
      hours: 14,
      minutes:0,
      seconds: 0
    },
    {
      text: "02:30 PM",
      hours: 14,
      minutes:30,
      seconds: 0
    },
    {
      text: "03:00 PM",
      hours: 15,
      minutes:0,
      seconds: 0
    },
    {
      text: "03:30 PM",
      hours: 15,
      minutes:30,
      seconds: 0
    },
    {
      text: "04:00 PM",
      hours: 16,
      minutes:0,
      seconds: 0
    },
    {
      text: "04:30 PM",
      hours: 16,
      minutes:30,
      seconds: 0
    },
    {
      text: "05:00 PM",
      hours: 17,
      minutes:0,
      seconds: 0
    },
    // {
    //   text: "05:30 PM",
    //   hours: 17,
    //   minutes:30,
    //   seconds: 0
    // },
    // {
    //   text: "06:00 PM",
    //   hours: 18,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "06:30 PM",
    //   hours: 18,
    //   minutes:30,
    //   seconds: 0
    // },
    // {
    //   text: "07:00 PM",
    //   hours: 19,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "07:30 PM",
    //   hours: 19,
    //   minutes:30,
    //   seconds: 0
    // },
    // {
    //   text: "08:00 PM",
    //   value: moment().hours(20).minutes(0).seconds(0)
    // },
    // {
    //   text: "08:30 PM",
    //   value: moment().hours(20).minutes(30).seconds(0)
    // },
    // {
    //   text: "09:00 PM",
    //   value: moment().hours(21).minutes(0).seconds(0)
    // },
    // {
    //   text: "09:30 PM",
    //   value: moment().hours(21).minutes(30).seconds(0)
    // },
    // {
    //   text: "10:00 PM",
    //   value: moment().hours(22).minutes(0).seconds(0)
    // },
    // {
    //   text: "10:30 PM",
    //   value: moment().hours(22).minutes(30).seconds(0)
    // },
    // {
    //   text: "11:00 PM",
    //   value: moment().hours(23).minutes(0).seconds(0)
    // },
    // {
    //   text: "11:30 PM",
    //   value: moment().hours(23).minutes(30).seconds(0)
    // },
  ]

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 

  appData = {

    //Datos de Contacto
    data: {

      localAppointments: {
        active: true,
       // url: "https://squareup.com/appointments/book/oblyg280s7nof3/F2JT8YPQXF0S7/services",
      },

      appTitle: "KB Hair Studio and Barbershop",

      homeDescription: 
        ` One of the best barbershops and Hair Studio in East Los Angeles.  
        that also has high quality products and with the best results where your family can enter and receive a wonderful experience 
        with one of the best cuts you will have, 
        do not hesitate to come and check it for yourself where you will have the option of not only having the best cut but also buying the best products our 
        hair studio is 100% family owned so do not hesitate to bring your family and together co-try a unique experience with us book your APPOINTMENT NOW`,

        homeSlider:[
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fhome%2F13_1024x1024.jpg?alt=media&token=28f5e585-bf8e-46f1-bf88-a01ac9296f31",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fhome%2F17_1024x1024.jpg?alt=media&token=5eee298a-0037-4c27-b896-282967c933b2",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fhome%2F18_1024x1024.jpg?alt=media&token=8bc49b33-499a-4211-9d02-897abe854a10",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fhome%2F19_1024x1024.jpg?alt=media&token=39ef844b-6684-4d66-a600-aa09e592a10a",
        ],


      homeServices: [
        // {
        //   name:"Beard Overhaul",
        //   time:"30 minutes",
        //   description:"Hot towels, massage, deep conditioning and lineup to tame the unruliest of beards.",
        //   price: "$35"
        // },
        // {
        //   name:"Executive Cut",
        //   time:"60 minutes",
        //   description:"Traditional cut with shampoo & Condition plus an invigorating head, neck, shoulder, & hand massage",
        //   price: "$50"
        // },
        // {
        //   name:"Beard Trim",
        //   time:"30 minutes",
        //   //description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
        //   price: "$20"
        // },
        // {
        //   name:"Traditional Haircuts",
        //   time:"45 minutes",
        //   //description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
        //   price: "$45"
        // },
      ],
      
      homeSchedules: [
        {
          day: "Monday",
          hour: "10:00 am - 5:00 pm",
        },
        {
          day: "Tuesday",
          hour: "10:00 am - 5:00 pm",
        },
        {
          day: "Wednesday",
          hour: "10:00 am - 5:00 pm",
        },
        {
          day: "Thursday",
          hour: "10:00 am - 5:00 pm",
        },
        {
          day: "Friday",
          hour: "10:00 am - 5:00 pm",
        },
        {
          day: "Saturday",
          hour: "Closed",
        },
        {
          day: "Sunday",
          hour: "Closed",
        },
      ],

      // homeBreaktime: [
      //   {
      //     day: "Monday",
      //     hour: "Close",
      //   },
      //   {
      //     day: "Tuesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Wednesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Thursday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Friday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Saturday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Sunday",
      //     hour: "Close",
      //   },
      // ],

      
      homeVideos: [
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fhome%2Fv1.mp4?alt=media&token=59c8df00-6e1b-4292-8b5b-3c22bba4274c",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fhome%2Fv2.mp4?alt=media&token=26e04ba1-8ac0-42d9-a119-ee3bb8f6a7b1",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fhome%2Fv3.mp4?alt=media&token=73ed74e7-5ae0-4808-8381-e22a22c825d8",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fhome%2Fv4.mp4?alt=media&token=035e0344-c221-4b30-bc4c-802faea23185",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "323 879 5083",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F9_1024x1024.jpg?alt=media&token=6e97ff34-4943-4886-ae49-d242e5e94fba",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F10_1024x1024.jpg?alt=media&token=a702d10d-c4de-4012-9383-885a7827340c",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F11_1024x1024.jpg?alt=media&token=50752e8d-cc68-4fe4-815d-5f900804b1d1",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F12_1024x1024.jpg?alt=media&token=4b8314e7-a3f8-4805-a248-79cebae0412f",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F13_1024x1024.jpg?alt=media&token=badf1a8c-0ea6-45c9-8abe-a85ab49560fe",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F14_1024x1024.jpg?alt=media&token=cba05be8-3a51-4a15-b2d0-8b3694487390",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F15_1024x1024.jpg?alt=media&token=252ffa11-01c7-4f91-a397-1f0aaf3ba247",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F16_1024x1024.jpg?alt=media&token=b288afdb-b603-497a-8da3-e2e942396f46",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F17_1024x1024.jpg?alt=media&token=1db518c9-026c-455c-ae87-8dd283178cae",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F18_1024x1024.jpg?alt=media&token=bea26dae-575b-472a-ac55-400defd799bf",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F19_1024x1024.jpg?alt=media&token=c029df3c-1c3b-499b-8578-647faf450413",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F1_1024x1024.jpg?alt=media&token=395cbfe4-a6c5-4c50-acdf-c48c6c1d572c",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F20_1024x1024.jpg?alt=media&token=4abd1df1-6363-472a-94ee-37f6bf669a08",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F2_1024x1024.jpg?alt=media&token=a171c8ed-687a-41d9-afb4-fb6ed65e0ea2",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F3_1024x1024.jpg?alt=media&token=ea86b054-1355-4a69-8cf3-8a4c5dc08556",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F4_1024x1024.jpg?alt=media&token=038f7d2f-767a-4068-8b12-1ac9cc19dbdf",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F5_1024x1024.jpg?alt=media&token=4bc43604-0bfe-4c7b-922f-394d8e7bdacc",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F6_1024x1024.jpg?alt=media&token=729c8950-e287-48ba-9396-73e56e2cc1bb",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F7_1024x1024.jpg?alt=media&token=5dd53298-ecc5-4c84-b70e-a00bad3f4d66",
      //"https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fgalerry%2F8_1024x1024.jpg?alt=media&token=cb9162a5-8d00-44a4-9bc3-3b47a33a0215",
      ],

      product:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fproduct%2Fp10_1024x1024.jpg?alt=media&token=592dbf85-c018-45c9-b6f4-5aa677c7c6a7",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fproduct%2Fp11_1024x1024.jpg?alt=media&token=1acd2517-9616-4dea-973a-5a7985679590",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fproduct%2Fp12_1024x1024.jpg?alt=media&token=4434d2fc-2b6a-4148-bb06-ecca6b35f753",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fproduct%2Fp13_1024x1024.jpg?alt=media&token=42663af4-609a-465a-8014-1329f0a0f3d3",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fproduct%2Fp14_1024x1024.jpg?alt=media&token=79f592f8-8c0f-485e-ac05-ecdb99dc2e7c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fproduct%2Fp16_1024x1024.jpg?alt=media&token=eea2a0d4-fd8d-4953-adda-5c51c711f48c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fproduct%2Fp1_1024x1024.jpg?alt=media&token=f6867bc2-d60a-4dbd-8524-935d4c3f0048",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fproduct%2Fp2_1024x1024.jpg?alt=media&token=8952e76f-fc2c-42c3-aef4-a5dc569cfe29",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fproduct%2Fp3_1024x1024.jpg?alt=media&token=6fa83303-8df4-41c0-95e7-ec8ab83bb894",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fproduct%2Fp4_1024x1024.jpg?alt=media&token=3edf5fb9-3bca-465e-927a-67bfa095cd48",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fproduct%2Fp5_1024x1024.jpg?alt=media&token=b2fbf3ee-324b-4a10-b23d-dc31b9b7e296",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fproduct%2Fp6_1024x1024.jpg?alt=media&token=3088c74a-98d8-4f12-9104-5c413e4d5154",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fproduct%2Fp7_1024x1024.jpg?alt=media&token=f8584c23-e132-4cb3-b872-4581128c7b27",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fproduct%2Fp8_1024x1024.jpg?alt=media&token=f20738e0-b49a-4f0b-89f2-c79700decee3",
        // "https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fproduct%2Fp9_1024x1024.jpg?alt=media&token=6c9a16e6-ac37-4935-aeae-0526b81b2ec0", 
      ],

      socialNetworks: [
        // {
        //   text: "@fureys_old_town_barber ",
        //   link: "https://www.instagram.com/fureys_old_town_barber/",
        //   icon: "logo-instagram",
        // },
        {
          text: "KB Hair Studio",
          link: "https://www.facebook.com/KBHSTUDIO/",
          icon: "logo-facebook",
        },
        {
          text: "KB Hair Studio & Barbershop",
          link: "https://kb-hair-studio-barbershop.business.site/?utm_source=gmb&utm_medium=referral",
          icon: "globe",
        },
      ],

      // social1: [
      //   {
      //     text: "Appointments",
      //     url: 'https://dsbarbershopny.com/contact-us/',
      //     icon: "list",
      //   },
      // ], 

      address: "750 S Kern Ave, East Los Angeles, CA 90022",
      addressRoute: "750 S Kern Ave, East Los Angeles, CA 90022",
      mapSource: "https://maps.google.com/maps?q=KB%20Hair%20Studio,%20750%20S%20Kern%20Ave,%20East%20Los%20Angeles,%20CA%2090022&t=&z=15&ie=UTF8&iwloc=&output=embed", 
     
      // social1: [
      //   {
      //     text: "Appointments",
      //     url: 'https://shops.getsquire.com/presidential-barber-lounge-lynwood',
      //     icon: "list",
      //   }
      // ],
    
      barbers:[
        {
          name: "KB Hair Studio and Barbershop",
          description: "KB Hair Studio and Barbershop Application",
          socialNetworks:[
            // {
            //   text: "wavyblends16_",
            //   link: "https://www.instagram.com/wavyblends16_/",
            //   icon: "logo-instagram",
            // },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-2.appspot.com/o/appointments%2FKB%20Hair%20Studio%20and%20Barbershop%2Fbarber%2FLogo_1024x1024.png?alt=media&token=bd373422-141c-41dd-bc5b-7f669fbfd4a5",
        }
      ]
    },

    appointmentsDays:[
      {
        day: 1,
        hours: this.hours,
        closed: false
      },
      {
        day: 2,
        hours: this.hours,
        closed: false
      },
      {
        day: 3,
        hours: this.hours,
        closed: false
      },
      {
        day: 4,
        hours: this.hours,
        closed: false
      },
      {
        day: 5,
        hours: this.hours,
        closed: false
      },
      {
        day: 6,
        hours: this.hours,
        closed: true
      },
      {
        day: 0,
        hours: this.hours,
        closed: true
      },
    ],

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
 //  this.firebaseService.updateAppData(this.appData).then(t => {console.log(this.appData)}); 
  }


  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
     return this.firebaseService.getAppData().then(appData => {
        if(appData) {
          this.appData = appData as any;
          console.log(appData);
        }
      });
  }
}
