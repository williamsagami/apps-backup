import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  d: any;

  constructor(public data: DataService, private ds: DomSanitizer) { 
    this.d = data.appData.data;
  }

  ngOnInit() {
    this.data.getAppData().then(cloudData => {
      this.d = this.data.appData.data;
    });
  }

  getMapUrl() {
    return this.ds.bypassSecurityTrustResourceUrl(this.d.mapSource);
  }

}
 