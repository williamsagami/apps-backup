import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  hours=[
    // {
    //   text: "00:00 AM",
    //   value: moment().hours(0).minutes(0).seconds(0)
    // },
    // {
    //   text: "00:30 AM",
    //   value: moment().hours(0).minutes(30).seconds(0)
    // },
    // {
    //   text: "01:00 AM",
    //   value: moment().hours(1).minutes(0).seconds(0)
    // },
    // {
    //   text: "01:30 AM",
    //   value: moment().hours(1).minutes(30).seconds(0)
    // },
    // {
    //   text: "02:00 AM",
    //   value: moment().hours(2).minutes(0).seconds(0)
    // },
    // {
    //   text: "02:30 AM",
    //   value: moment().hours(2).minutes(30).seconds(0)
    // },
    // {
    //   text: "03:00 AM",
    //   value: moment().hours(3).minutes(0).seconds(0)
    // },
    // {
    //   text: "03:30 AM",
    //   value: moment().hours(3).minutes(30).seconds(0)
    // },
    // {
    //   text: "04:00 AM",
    //   value: moment().hours(4).minutes(0).seconds(0)
    // },
    // {
    //   text: "04:30 AM",
    //   value: moment().hours(4).minutes(30).seconds(0)
    // },
    // {
    //   text: "05:00 AM",
    //   value: moment().hours(5).minutes(0).seconds(0)
    // },
    // {
    //   text: "05:30 AM",
    //   value: moment().hours(5).minutes(30).seconds(0)
    // },
    // {
    //   text: "06:00 AM",
    //   hours: 6,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "06:30 AM",
    //   hours: 6,
    //   minutes:30,
    //   seconds: 0
    // },
     {
      text: "07:00 AM",
      hours: 7,
      minutes:0,
      seconds: 0
    },
    {
      text: "07:30 AM",
      hours: 7,
      minutes:30,
      seconds: 0
    },
    {
      text: "08:00 AM",
      hours: 8,
      minutes:0,
      seconds: 0
    },
    {
      text: "08:30 AM",
      hours: 8,
      minutes:30,
      seconds: 0
    },
    {
      text: "09:00 AM",
      hours: 9,
      minutes:0,
      seconds: 0
    },
    {
      text: "09:30 AM",
      hours: 9,
      minutes:30,
      seconds: 0
    },
    {
      text: "10:00 AM",
      hours: 10,
      minutes:0,
      seconds: 0
    },
    {
      text: "10:30 AM",
      hours: 10,
      minutes:30,
      seconds: 0
    },
    {
      text: "11:00 AM",
      hours: 11,
      minutes:0,
      seconds: 0
    },
    {
      text: "11:30 AM",
      hours: 11,
      minutes:30,
      seconds: 0
    },

    {
      text: "12:00 AM",
      hours: 12,
      minutes:0,
      seconds: 0
    },
    {
      text: "12:30 AM",
      hours: 12,
      minutes:30,
      seconds: 0
    },
    {
      text: "13:00 AM",
      hours: 13,
      minutes:0,
      seconds: 0
    },
    {
      text: "01:30 PM",
      hours: 13,
      minutes:30,
      seconds: 0
    },
    {
      text: "02:00 PM",
      hours: 14,
      minutes:0,
      seconds: 0
    },
    {
      text: "02:30 PM",
      hours: 14,
      minutes:30,
      seconds: 0
    },
    {
      text: "03:00 PM",
      hours: 15,
      minutes:0,
      seconds: 0
    },
    {
      text: "03:30 PM",
      hours: 15,
      minutes:30,
      seconds: 0
    },
    {
      text: "04:00 PM",
      hours: 16,
      minutes:0,
      seconds: 0
    },
    {
      text: "04:30 PM",
      hours: 16,
      minutes:30,
      seconds: 0
    },
    {
      text: "05:00 PM",
      hours: 17,
      minutes:0,
      seconds: 0
    },
    {
      text: "05:30 PM",
      hours: 17,
      minutes:30,
      seconds: 0
    },
    {
      text: "06:00 PM",
      hours: 18,
      minutes:0,
      seconds: 0
    },
    {
      text: "06:30 PM",
      hours: 18,
      minutes:30,
      seconds: 0
    },
    {
      text: "07:00 PM",
      hours: 19,
      minutes:0,
      seconds: 0
    },
    {
      text: "07:30 PM",
      hours: 19,
      minutes:30,
      seconds: 0
    },
    // {
    //   text: "08:00 PM",
    //   value: moment().hours(20).minutes(0).seconds(0)
    // },
    // {
    //   text: "08:30 PM",
    //   value: moment().hours(20).minutes(30).seconds(0)
    // },
    // {
    //   text: "09:00 PM",
    //   value: moment().hours(21).minutes(0).seconds(0)
    // },
    // {
    //   text: "09:30 PM",
    //   value: moment().hours(21).minutes(30).seconds(0)
    // },
    // {
    //   text: "10:00 PM",
    //   value: moment().hours(22).minutes(0).seconds(0)
    // },
    // {
    //   text: "10:30 PM",
    //   value: moment().hours(22).minutes(30).seconds(0)
    // },
    // {
    //   text: "11:00 PM",
    //   value: moment().hours(23).minutes(0).seconds(0)
    // },
    // {
    //   text: "11:30 PM",
    //   value: moment().hours(23).minutes(30).seconds(0)
    // },
  ]

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 

  appData = {

    //Datos de Contacto
    data: {

      appTitle: "Authentic Image Barbershop",

      homeDescription: 
        `Specialties <br>
        Our business specializes in creating a proper authentic image for our clients, restoring confidence and shine in our clients!
        <br>
        History <br>
        Established in 2017.
        <br>
        We’ve been in the industry for 7 years and barbering for over 10 years. 
        With an opportunity to own a shop I had to take it and now meet high expectations. 
        Barbering is a craft that is a lot of attention to detail and right here we specialize in a proper authentic haircut and style .`,

        homeSlider:[
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fhome%2F14_512x512.jpg?alt=media&token=f5bb87d8-9153-4617-888a-70ebd2e4e4c7",
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fhome%2F6_512x512.jpg?alt=media&token=1cf207d3-c6c3-444e-ac95-143e3650cfd0",
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fhome%2F8_512x512.jpg?alt=media&token=ec9adc94-8025-41a0-91c9-0bce36d7feed",
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fhome%2F9_512x512.jpg?alt=media&token=3a929523-f3d2-4e49-a7bc-41617505dcd1",
        ],


      homeServices: [
        {
          name:"Shills Facial Mask",
          //time:"30 minutes",
          //description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
          price: "$8"
        },
        {
          name:"Suevecito Grooming Spray",
          //time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$12"
        },
        {
          name:"Suavecito Frime Hold",
          //time:"60 minutes",
         // description:"Extra 15$-20$",
          price: "$14"
        },
        {
          name:"Suavecito Matte Paste",
          //time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$16"
        },
        {
          name:"Bonafide SUPERHOLD",
          //time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$17"
        },
        {
          name:"Bonafide ORGINAL HOLD",
          //time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$17"
        },
        {
          name:"SUAVECITO Texture Powder",
          //time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$20"
        },
        {
          name:"SUAVECITO REG.GEL",
          //time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$12"
        },
      ],
      
      homeSchedules: [
        {
          day: "Monday",
          hour: "10:00 AM - 7:00 PM",
        },
        {
          day: "Tuesday",
          hour: "10:00 AM - 7:00 PM",
        },
        {
          day: "Wednesday",
          hour: "10:00 AM - 7:00 PM",
        },
        {
          day: "Thursday",
          hour: "	9:00 AM - 8:00 PM",
        },
        {
          day: "Friday",
          hour: "9:00 AM - 8:00 PM",
        },
        {
          day: "Saturday",
          hour: "09:00 am – 05:00 pm",
        },
        {
          day: "Sunday",
          hour: "	10:00 AM - 2:45 PM",
        },
      ],

      // homeBreaktime: [
      //   {
      //     day: "Monday",
      //     hour: "Close",
      //   },
      //   {
      //     day: "Tuesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Wednesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Thursday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Friday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Saturday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Sunday",
      //     hour: "Close",
      //   },
      // ],

      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fhome%2F4.MOV?alt=media&token=f6459dbf-0826-4c79-a69a-a736584f5c5e",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fhome%2F1.mp4?alt=media&token=9ccb3ca0-6a83-49f0-b45d-4abfbe25b7b9",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fhome%2F2.mp4?alt=media&token=a49f5552-1169-4d04-bc1e-cf21070b2718",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fhome%2F3.mp4?alt=media&token=8a6fd2e9-7e36-47d1-a848-a9f5cb75918b",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "714 723 0281",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F10_512x512.jpg?alt=media&token=c44a3393-ad65-42c1-a8b0-930946b47432",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F11_512x512.jpg?alt=media&token=53afc1bc-7fe5-4d53-a325-99896241eac5",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F12_512x512.jpg?alt=media&token=3f4e7c63-978a-4298-86db-f679b49f2eff",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F13_512x512.jpg?alt=media&token=cab3c15e-83e1-4882-888d-230d50c680ec",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F14_512x512.jpg?alt=media&token=df57ebdd-7ed9-479f-8658-b55fd17919be",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F15_512x512.jpg?alt=media&token=d85f0989-6304-4eb3-93e4-1cb41d266ec4",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F16_512x512.jpg?alt=media&token=53b98fe6-349f-4309-8c9f-5b2a6d22dffa",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F17_512x512.jpg?alt=media&token=4e2d11cf-2b43-4a29-8a6c-c44645eb9dc7",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F18_512x512.jpg?alt=media&token=9799d00b-17cf-480a-943c-11c26906c134",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F19_512x512.jpg?alt=media&token=f98264ed-b884-43d8-9bbc-e7d3e87f459f",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F1_512x512.jpg?alt=media&token=87905a16-60a3-4afc-8543-b4cfce4ad2b1",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F20_512x512.jpg?alt=media&token=7dffd496-0d87-4891-8db6-90f6099358c4",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F2_512x512.jpg?alt=media&token=b84fdaa8-d60e-4f24-9c85-477856a825aa",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F3_512x512.jpg?alt=media&token=18585d3d-8756-4d90-bc51-402f4c99d03f",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F4_512x512.jpg?alt=media&token=97c2f307-f059-4625-ae88-ca3c96ff227a",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F5_512x512.jpg?alt=media&token=7569e75d-380a-4724-84a8-a3c11637efab",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F6_512x512.jpg?alt=media&token=756423af-253d-4ee6-aeea-e96045877440",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F7_512x512.jpg?alt=media&token=52e29aad-93ae-48ec-b147-22739af153e8",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F8_512x512.jpg?alt=media&token=4f52ee77-e846-4498-8ebb-1b0ee2ec0132",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FAuthentic%20Image%20Barbershop%2Fgallery%2F9_512x512.jpg?alt=media&token=5d032308-6161-4ca5-9f74-eba33321a7ad",
      ],

      socialNetworks: [
        {
          text: "Appointments",
          link: "https://shops.getsquire.com/book/authentic-image-barbershop-anaheim/professional",
          icon: "list",
        },
        {
          text: "imagenautenticabarberia",
          link: "https://www.instagram.com/authenticimagebarbershop/",
          icon: "logo-instagram",
        },
        {
          text: "Authentic Image Barbershop",
          link: "https://getsquire.com/barbershops/book/authentic-image-barbershop-anaheim/professional",
          icon: "book",
        },
        {
          text: "Authentic Image Barbershop",
          link: "https://www.yelp.com/biz/authentic-image-barbershop-anaheim-3?start=20",
          icon: "logo-yahoo",
        },
        {
          text: "Authentic Image Barbershop",
          link: "https://www.facebook.com/authenticimagebarbershop/?ref=page_internal",
          icon: "logo-facebook",
        },
      ],

      address: "1215 S Beach Blvd Ste D Anaheim, CA 92804",
      addressRoute: "1215 S Beach Blvd Ste D Anaheim, CA 92804",
      mapSource: "https://maps.google.com/maps?q=Authentic%20Image%20Barbershop&t=&z=15&ie=UTF8&iwloc=&output=embed", 
     
      // social1: [
      //   {
      //     text: "Appointments",
      //     url: 'https://shops.getsquire.com/presidential-barber-lounge-lynwood',
      //     icon: "list",
      //   }
      // ],
    
      barbers:[
        {
          name: "yaya",
          description: "Visitas a domicilio $100 Horas <br> antes/después $60 <br> Citas $35+",
          socialNetworks:[
            {
              text: "wavyblends16_",
              link: "https://www.instagram.com/wavyblends16_/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fappointments%2FLogo_512x512.png?alt=media&token=9c325e46-fc25-4680-9ff4-127f936f3a97",
        },
        {
          name: "Steve",
          description: "no lo pienses demasiado",
          socialNetworks:[
            {
              text: "stevefades",
              link: "https://www.instagram.com/stevefades/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fappointments%2FLogo_512x512.png?alt=media&token=9c325e46-fc25-4680-9ff4-127f936f3a97",
        },
        {
          name: "Oscar",
          description: "The Big Barber",
          socialNetworks:[
            {
              text: "oscar_can_cut",
              link: "https://www.instagram.com/oscar_can_cut/",
              icon: "logo-instagram",
            },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fappointments%2FLogo_512x512.png?alt=media&token=9c325e46-fc25-4680-9ff4-127f936f3a97",
        },
        {
          name: "Jose",
          description: "The Veteran Barber",
          // socialNetworks:[
          //   {
          //     text: "oscar_can_cut",
          //     link: "https://www.instagram.com/oscar_can_cut/",
          //     icon: "logo-instagram",
          //   },
          // ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fappointments%2FLogo_512x512.png?alt=media&token=9c325e46-fc25-4680-9ff4-127f936f3a97",
        },
      ]
    },

    appointmentsDays:[
      {
        day: 1,
        hours: this.hours,
        closed: false
      },
      {
        day: 2,
        hours: this.hours,
        closed: false
      },
      {
        day: 3,
        hours: this.hours,
        closed: false
      },
      {
        day: 4,
        hours: this.hours,
        closed: false
      },
      {
        day: 5,
        hours: this.hours,
        closed: false
      },
      {
        day: 6,
        hours: this.hours,
        closed: false
      },
      {
        day: 0,
        hours: this.hours,
        closed: false
      },
    ],

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
     //    this.firebaseService.updateAppData(this.appData).then(t => {console.log(this.appData)}); 
  }


  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
     return this.firebaseService.getAppData().then(appData => {
        if(appData) {
          this.appData = appData as any;
          console.log(appData);
        }
      });
  }
}
