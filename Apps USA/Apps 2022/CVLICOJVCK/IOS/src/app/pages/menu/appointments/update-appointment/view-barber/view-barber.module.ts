import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewBarberPageRoutingModule } from './view-barber-routing.module';

import { ViewBarberPage } from './view-barber.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewBarberPageRoutingModule
  ],
  declarations: [ViewBarberPage]
})
export class ViewBarberPageModule {}
