import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewBarberPage } from './view-barber.page';

describe('ViewBarberPage', () => {
  let component: ViewBarberPage;
  let fixture: ComponentFixture<ViewBarberPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBarberPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewBarberPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
