import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "CVLICOJVCK",

      homeDescription: 
        `Our business specializes in men's grooming, including haircuts, hot towel shaves, beard trims, and kid's cuts. <br>
        CVLICOJVCK (pronounced calicojack) was created as a men’s lifestyle brand that embodies all things men... <br>
        cue cvlicojvck barbershop, the ultimate men’s getaway. Owner, Oscar, has been cutting hair for over 15 years, professionally licensed for 10, 
        and is dedicated to making your barbershop experience like no other. <br>
        His love for his craft and his attention to detail is unmatched. <br>
        Here at cvlicojvck you can get groomed up while sipping a cold beer and overlooking the Newport Harbor. <br>
        No barbershop has views like this one. If you’re looking for the best barbers and barbershop around, you’ve found it! We look forward to meeting you`,


        homeSlider:[
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fhome%2F1s_512x512.jpg?alt=media&token=69205cf5-c12b-4d6a-bc06-7dc5b332d24e",    
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fhome%2F2s_512x512.jpg?alt=media&token=e66388cc-deda-4721-a022-d4d6d5cfbc26",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fhome%2F3s_512x512.jpg?alt=media&token=5a813495-22b2-4e9d-98f1-5f2f0156361c",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fhome%2F4s_512x512.jpg?alt=media&token=d687e089-8947-452d-b178-daae094b699e",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fhome%2F5s_512x512.jpg?alt=media&token=c03df290-d675-4b75-830a-5455fe084af1",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fhome%2F6s_512x512.jpg?alt=media&token=eb110c61-2b34-403b-8fd0-61a078ea00ca",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fhome%2F7s_512x512.jpg?alt=media&token=f11e5465-aaad-445d-992c-0968ffb8c083",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fhome%2F8s_512x512.jpg?alt=media&token=28007996-70f9-4d5f-b305-4b10b80c9137",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fhome%2F9s_512x512.jpg?alt=media&token=4b3ec842-d716-4a57-9824-1ed315bd8e24",
      ],


        homeServices: [
          {
            name:"Haircut",
            //time:"30 Minute",
           // description:"Utilization of clippers and scissors (including texturizing shears), optional shaving around the hair edges and optional pomade application.",
           //price: "$25"
          },
          {
            name:"Hot Towel Shave",
            //time:"10 minutes",
           //  description:"Haircut & style followed by massage therapy utilizing our percussive handheld device over the shoulders for relief & relaxation.",
           // price: "$30"
          },
          {
            name:"Beard Trim",
          //  time:"10 minutes",
           // description:"For this classic & delicate service, we utilize fine shears (including texturizing shears) to cut the hair to the desired length.",
          //  price: "$20"
          },
          {
            name:"Beard Trim",
           // time:"1 hour 15 minutes",
            //description:"Two hot towels, hot lather, and gentle straight-edge razor shave around the scalp, lime aftershave & balm is then applied.",
            //price: "$150"
          },
          {
            name:"All other services, please call shop.",
           // time:"40 minutes",
          //  description:"From low tapers to high fades, we give you our closest, cleanest and long lasting skin fades with state of the art instruments.",
          //  price: "$175"
          },
        ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "8:00 AM - 2:00 PM",
        },
        {
          day: "Tuesday",
          hour: "9:00 AM - 7:00 PM",
        },
        {
          day: "Wednesday",
          hour: "	9:00 AM - 7:00 PM",
        },
        {
          day: "Thursday",
          hour: "9:00 AM - 7:00 PM",
        },
        {
          day: "Friday",
          hour: "9:00 AM - 7:00 PM",
        },
        {
          day: "Saturday",
          hour: "8:00 AM - 2:00 PM",
        },
        {
          day: "Sunday",
          hour: "8:00 AM - 2:00 PM",
        },
      ],
      
      homeVideos: [
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fhome%2F1.MOV?alt=media&token=567a48e5-6f37-4dff-8b7e-fefe267e541e",
        ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "949 423 7088",
        },
      ],

      gallery:[
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F9_512x512.jpg?alt=media&token=df8f7646-4c41-4bf9-8691-fc1f98b08334",
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F10_512x512.jpg?alt=media&token=328149d2-55a5-4b53-bde3-fc09ef3ad084",
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F11_512x512.jpg?alt=media&token=b79a1548-e373-4423-a1ee-0c9b8e6b5eff",
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F12_512x512.jpg?alt=media&token=f6e7336b-443c-4f99-907c-80ef1e22095a",
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F13_512x512.jpg?alt=media&token=5221bb8b-5d65-4cfd-80da-f77a8772bca2",
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F14_512x512.jpg?alt=media&token=62f40fbb-4bd2-4bdc-9a07-b4d9986d3a01",
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F15_512x512.jpg?alt=media&token=4fcc2fef-ff5d-4248-b45e-b3d5e8ea3250",
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F1_512x512.jpg?alt=media&token=348e6f6f-d804-40fe-b1df-0a40ce780643",
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F2_512x512.jpg?alt=media&token=e5741a8f-a958-46c9-b0cc-901151f716ad",
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F3_512x512.jpg?alt=media&token=09601558-8183-435c-92e8-d918af41cf37",
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F4_512x512.jpg?alt=media&token=9afede00-cced-4854-a304-3060fd2d85ed",
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F5_512x512.jpg?alt=media&token=f2031405-0eba-4fd2-812c-33edbcc4c8ae",
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F6_512x512.jpg?alt=media&token=9e4ce347-afbe-4483-a8ae-397aa2e160f6",
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F7_512x512.jpg?alt=media&token=1b1a9e96-43c9-4bde-b408-cd275549bf8c",
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F8_512x512.jpg?alt=media&token=ebd02b9c-e09f-45b4-898c-56cff4fa70ef",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F16_512x512.jpg?alt=media&token=ca98fa24-dcb9-4180-a677-249f30e67e0f",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F17_512x512.jpg?alt=media&token=9d31a444-1f30-4c21-a7b7-4fc3cc51cbd6",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F18_512x512.jpg?alt=media&token=6a60ece7-4c14-4793-831b-3f481791755a",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F19_512x512.JPG?alt=media&token=97d2fc87-737a-47a4-bca6-a2e6f633d949",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fgallery%2F20_512x512.jpg?alt=media&token=b2976097-4d33-4624-9622-ba37d41c1d4e",
      ],

      products:[  
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fproducts%2Fp9_512x512.PNG?alt=media&token=4462a379-d38b-44f0-af02-14b3bde61e4d",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fproducts%2Fp1_512x512.jpg?alt=media&token=4600c667-5574-41f3-a36e-f04709779ce6",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fproducts%2Fp2_512x512.jpg?alt=media&token=1ebb622f-af6d-41a5-a5f1-a8e0beda3159",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fproducts%2Fp3_512x512.jpg?alt=media&token=58f45161-e0f6-4954-85d9-d0dcb964d11c",
        //"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fproducts%2Fp4_512x512.JPG?alt=media&token=3e80ae7e-e0f9-4a78-b683-02eb790bd146",
        //"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fproducts%2Fp5_512x512.jpg?alt=media&token=6ea7fbd0-2ab2-4f02-88d2-4120f9124ef0",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fproducts%2Fp6_512x512.JPG?alt=media&token=7ca917ca-1f7e-4b70-b338-ded0161ef323",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fproducts%2Fp7_512x512.jpg?alt=media&token=19ac55d6-e1e1-4937-9422-53d97c562db7",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FCVLICOJVCK%2Fproducts%2Fp8_512x512.JPG?alt=media&token=ad83c4c9-e594-4a17-a9c4-8c38c52d7cf2",  
       ],

      socialNetworks: [
        {
          text: "cvlicojvck",
          link: "https://cvlicojvck.com/",
          icon: "globe",
        },
        {
          text: "CVLICOJVCK",
          link: "https://www.yelp.com/biz/cvlicojvck-newport-beach",
          icon: "logo-yahoo",
        },
        {
          text: "cvlicojvck",
          link: "https://www.instagram.com/cvlicojvck/?hl=es",
          icon: "logo-instagram",
        },
        {
          text: "cvlicojvck",
          link: "https://www.facebook.com/cvlicojvck/",
          icon: "logo-facebook",
        },
      ],

      address: "2507 W Coast Hwy Newport Beach, CA 92663",

      addressRoute: "2507 W Coast Hwy Newport Beach, CA 92663",

      mapSource: "https://maps.google.com/maps?q=CVLICOJVCK,%202507%20W%20Coast%20Hwy%20Newport%20Beach,%20CA%2092663&t=&z=15&ie=UTF8&iwloc=&output=embed",

      social1: [
        {
          text: "Appointments",
          url: 'https://cvlicojvck.com/pages/get-serviced',
          icon: "list",
        }
      ],
      
      // barbers:[
      //   {
      //     name: "Edgar",
      //     description: "Veteran Barber",
      //     socialNetworks:[
      //     ],
      //     image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fappointments%2Fb1_512x512.jpg?alt=media&token=924bf9bf-ea69-4d97-a604-de279875500f",
      //   },

 
    }

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 
    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
    //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)}); 
  }



  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
