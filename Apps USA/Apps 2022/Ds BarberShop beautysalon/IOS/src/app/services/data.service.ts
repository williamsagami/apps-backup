import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  hours=[
    // {
    //   text: "00:00 AM",
    //   value: moment().hours(0).minutes(0).seconds(0)
    // },
    // {
    //   text: "00:30 AM",
    //   value: moment().hours(0).minutes(30).seconds(0)
    // },
    // {
    //   text: "01:00 AM",
    //   value: moment().hours(1).minutes(0).seconds(0)
    // },
    // {
    //   text: "01:30 AM",
    //   value: moment().hours(1).minutes(30).seconds(0)
    // },
    // {
    //   text: "02:00 AM",
    //   value: moment().hours(2).minutes(0).seconds(0)
    // },
    // {
    //   text: "02:30 AM",
    //   value: moment().hours(2).minutes(30).seconds(0)
    // },
    // {
    //   text: "03:00 AM",
    //   value: moment().hours(3).minutes(0).seconds(0)
    // },
    // {
    //   text: "03:30 AM",
    //   value: moment().hours(3).minutes(30).seconds(0)
    // },
    // {
    //   text: "04:00 AM",
    //   value: moment().hours(4).minutes(0).seconds(0)
    // },
    // {
    //   text: "04:30 AM",
    //   value: moment().hours(4).minutes(30).seconds(0)
    // },
    // {
    //   text: "05:00 AM",
    //   value: moment().hours(5).minutes(0).seconds(0)
    // },
    // {
    //   text: "05:30 AM",
    //   value: moment().hours(5).minutes(30).seconds(0)
    // },
    // {
    //   text: "06:00 AM",
    //   hours: 6,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "06:30 AM",
    //   hours: 6,
    //   minutes:30,
    //   seconds: 0
    // },
     {
      text: "07:00 AM",
      hours: 7,
      minutes:0,
      seconds: 0
    },
    {
      text: "07:30 AM",
      hours: 7,
      minutes:30,
      seconds: 0
    },
    {
      text: "08:00 AM",
      hours: 8,
      minutes:0,
      seconds: 0
    },
    {
      text: "08:30 AM",
      hours: 8,
      minutes:30,
      seconds: 0
    },
    {
      text: "09:00 AM",
      hours: 9,
      minutes:0,
      seconds: 0
    },
    {
      text: "09:30 AM",
      hours: 9,
      minutes:30,
      seconds: 0
    },
    {
      text: "10:00 AM",
      hours: 10,
      minutes:0,
      seconds: 0
    },
    {
      text: "10:30 AM",
      hours: 10,
      minutes:30,
      seconds: 0
    },
    {
      text: "11:00 AM",
      hours: 11,
      minutes:0,
      seconds: 0
    },
    {
      text: "11:30 AM",
      hours: 11,
      minutes:30,
      seconds: 0
    },

    {
      text: "12:00 AM",
      hours: 12,
      minutes:0,
      seconds: 0
    },
    {
      text: "12:30 AM",
      hours: 12,
      minutes:30,
      seconds: 0
    },
    {
      text: "13:00 AM",
      hours: 13,
      minutes:0,
      seconds: 0
    },
    {
      text: "01:30 PM",
      hours: 13,
      minutes:30,
      seconds: 0
    },
    {
      text: "02:00 PM",
      hours: 14,
      minutes:0,
      seconds: 0
    },
    {
      text: "02:30 PM",
      hours: 14,
      minutes:30,
      seconds: 0
    },
    {
      text: "03:00 PM",
      hours: 15,
      minutes:0,
      seconds: 0
    },
    {
      text: "03:30 PM",
      hours: 15,
      minutes:30,
      seconds: 0
    },
    {
      text: "04:00 PM",
      hours: 16,
      minutes:0,
      seconds: 0
    },
    {
      text: "04:30 PM",
      hours: 16,
      minutes:30,
      seconds: 0
    },
    {
      text: "05:00 PM",
      hours: 17,
      minutes:0,
      seconds: 0
    },
    {
      text: "05:30 PM",
      hours: 17,
      minutes:30,
      seconds: 0
    },
    {
      text: "06:00 PM",
      hours: 18,
      minutes:0,
      seconds: 0
    },
    {
      text: "06:30 PM",
      hours: 18,
      minutes:30,
      seconds: 0
    },
    {
      text: "07:00 PM",
      hours: 19,
      minutes:0,
      seconds: 0
    },
    {
      text: "07:30 PM",
      hours: 19,
      minutes:30,
      seconds: 0
    },
    // {
    //   text: "08:00 PM",
    //   value: moment().hours(20).minutes(0).seconds(0)
    // },
    // {
    //   text: "08:30 PM",
    //   value: moment().hours(20).minutes(30).seconds(0)
    // },
    // {
    //   text: "09:00 PM",
    //   value: moment().hours(21).minutes(0).seconds(0)
    // },
    // {
    //   text: "09:30 PM",
    //   value: moment().hours(21).minutes(30).seconds(0)
    // },
    // {
    //   text: "10:00 PM",
    //   value: moment().hours(22).minutes(0).seconds(0)
    // },
    // {
    //   text: "10:30 PM",
    //   value: moment().hours(22).minutes(30).seconds(0)
    // },
    // {
    //   text: "11:00 PM",
    //   value: moment().hours(23).minutes(0).seconds(0)
    // },
    // {
    //   text: "11:30 PM",
    //   value: moment().hours(23).minutes(30).seconds(0)
    // },
  ]

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 

  appData = {

    //Datos de Contacto
    data: {

      appTitle: "Ds BarberShop and beautysalon",

      homeDescription: 
        `We are absolutely confident that considering all the fuzz and the chaos in our modern lives, 
        there simply must be some place where a man can go to, sit for a nice small talk with his barber, 
        and while doing that also get their facial and head hair taken care of.`,

        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F2_512x512.jpg?alt=media&token=e5d96a84-61d4-4e6d-a15c-52433a8043de",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F3_512x512.jpg?alt=media&token=3143445c-ce09-4e3b-99c5-9aab0aeca23e",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F4_512x512.jpg?alt=media&token=e0d71cfd-1f30-400a-b04d-98f3d77a89e7",
          "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F6_512x512.jpg?alt=media&token=2a3bb7ea-23d1-40fc-88ae-2c0b70075880",
        ],


      homeServices: [
        {
          name:"Mohawk",
          //time:"30 minutes",
          //description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
          price: "$25"
        },
        {
          name:"Forehawk",
          //time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$25"
        },
        {
          name:"Skin Fade	",
          //time:"60 minutes",
         // description:"Extra 15$-20$",
          price: "$25"
        },
        {
          name:"Dark/Light Ceaser",
          //time:"30 minutes",
          //description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
          price: "$25"
        },
        {
          name:"Tape-Ups",
          //time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$25"
        },
        {
          name:"Bald Shave",
          //time:"60 minutes",
         // description:"Extra 15$-20$",
          price: "$25"
        },
      ],
      
      homeSchedules: [
        {
          day: "Monday",
          hour: "Closed",
        },
        {
          day: "Tuesday",
          hour: "10:00 AM - 07:00 PM",
        },
        {
          day: "Wednesday",
          hour: "10:00 AM - 07:00 PM",
        },
        {
          day: "Thursday",
          hour: "10:00 AM - 07:00 PM",
        },
        {
          day: "Friday",
          hour: "10:00 AM - 07:00 PM",
        },
        {
          day: "Saturday",
          hour: "10:00 AM - 07:00 PM",
        },
        {
          day: "Sunday",
          hour: "10:00 AM - 4:00 PM",
        },
      ],

      // homeBreaktime: [
      //   {
      //     day: "Monday",
      //     hour: "Close",
      //   },
      //   {
      //     day: "Tuesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Wednesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Thursday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Friday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Saturday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Sunday",
      //     hour: "Close",
      //   },
      // ],

      
      homeVideos: [
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fhome%2Fv1.mp4?alt=media&token=9268c534-6d84-414a-a921-8a58c4ab2876",
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fhome%2Fv2.mp4?alt=media&token=a38cea07-850d-4791-83ea-631e9433c027",
        // "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fhome%2Fv3.mp4?alt=media&token=d9466893-6875-4602-a500-085da41b6e6c",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "718 468 2937",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F10_512x512.jpg?alt=media&token=e57a2ac6-163a-449f-92f7-d2a80386f2c4",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F11_512x512.jpg?alt=media&token=660116d3-ce3e-4acd-b61b-fefb4e736247",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F12_512x512.jpg?alt=media&token=0fa557d4-c21a-4ac4-87c4-894a977cd51c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F13_512x512.jpg?alt=media&token=5996bab7-e11a-4fd7-ab8f-b5e196f7e25a",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F14_512x512.jpg?alt=media&token=c3c18da0-7136-4db3-b364-8424be96465d",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F15_512x512.jpg?alt=media&token=9b26b77f-b0ac-4f36-ab43-5a60a235f7ba",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F16_512x512.jpg?alt=media&token=2a670cc4-4bf5-45d4-82d4-4634dc641ec1",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F17_512x512.jpg?alt=media&token=579fdf08-fc83-49d6-9959-bdfb1006d6f9",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F18_512x512.jpg?alt=media&token=75ce6f7b-a34e-446e-be98-0d98cc1ee4fa",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F19_512x512.jpg?alt=media&token=50ef759f-d9fa-45c7-b844-7c5d677da88f",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F1_512x512.jpg?alt=media&token=4c3cf246-2348-4a32-a173-f4308115333c",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F20_512x512.jpg?alt=media&token=732b9ebb-4ff5-4178-b483-9f9599f0a0da",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F2_512x512.jpg?alt=media&token=e5d96a84-61d4-4e6d-a15c-52433a8043de",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F3_512x512.jpg?alt=media&token=3143445c-ce09-4e3b-99c5-9aab0aeca23e",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F4_512x512.jpg?alt=media&token=e0d71cfd-1f30-400a-b04d-98f3d77a89e7",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F5_512x512.jpg?alt=media&token=7126c4c9-15ce-40a9-9975-ee20bca69eba",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F6_512x512.jpg?alt=media&token=2a3bb7ea-23d1-40fc-88ae-2c0b70075880",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F7_512x512.jpg?alt=media&token=8ac5131c-95e4-4193-8da7-7cbf8b21761b",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F8_512x512.jpg?alt=media&token=bc825ca1-1ec3-415d-9487-c668f0becb93",
        "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FDs%20BarberShop%20and%20beautysalon%2Fgallery%2F9_512x512.jpg?alt=media&token=60b7716a-4265-469d-8a0a-2b777b9fe546",
      ],

      socialNetworks: [
        {
          text: "ds_barbershop_beautysal",
          link: "https://www.instagram.com/ds_barbershop_beautysalon/?hl=es",
          icon: "logo-instagram",
        },
        {
          text: "dsbarbershopny",
          link: "http://dsbarbershopny.com/",
          icon: "Globe",
        },
        // {
        //   text: "Authentic Image Barbershop",
        //   link: "https://www.yelp.com/biz/authentic-image-barbershop-anaheim-3?start=20",
        //   icon: "logo-yahoo",
        // },
        {
          text: "Dee Barbershop",
          link: "https://www.facebook.com/dee.barbershop.3",
          icon: "logo-facebook",
        },
      ],

      social1: [
        {
          text: "Appointments",
          url: 'https://dsbarbershopny.com/contact-us/',
          icon: "list",
        },
      ], 

      address: "188-06C Jamaica Ave Queens, NY 11423",
      addressRoute: "188-06C Jamaica Ave Queens, NY 11423",
      mapSource: "https://maps.google.com/maps?q=188-06c,%20Jamaica%20Ave,%20Hollis,%20NY%2011423,%20D's%20BarberShop&t=&z=15&ie=UTF8&iwloc=&output=embed", 
     
      // social1: [
      //   {
      //     text: "Appointments",
      //     url: 'https://shops.getsquire.com/presidential-barber-lounge-lynwood',
      //     icon: "list",
      //   }
      // ],
    
      barbers:[
        {
         
        }
      ]
    },

    appointmentsDays:[
      {
        day: 1,
        hours: this.hours,
        closed: false
      },
      {
        day: 2,
        hours: this.hours,
        closed: false
      },
      {
        day: 3,
        hours: this.hours,
        closed: false
      },
      {
        day: 4,
        hours: this.hours,
        closed: false
      },
      {
        day: 5,
        hours: this.hours,
        closed: false
      },
      {
        day: 6,
        hours: this.hours,
        closed: false
      },
      {
        day: 0,
        hours: this.hours,
        closed: false
      },
    ],

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
   this.firebaseService.updateAppData(this.appData).then(t => {console.log(this.appData)}); 
  }


  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
     return this.firebaseService.getAppData().then(appData => {
        if(appData) {
          this.appData = appData as any;
          console.log(appData);
        }
      });
  }
}
