import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = {
    //Datos de Contacto
    data: {
      appTitle: "Hair Addicts",

      homeDescription: 
        `Hair Addicts is a full service beauty salon dedicated to ensuring that our Cypress clients look and feel their best from head to toe. <br>
        Choose from a range of services for special occasions or as part of a commitment to a regular beauty regimen. <br>
        Taking care of your hair, skin and nails is not a luxury in today’s fast-paced world, but essential to good health and your general wellbeing.`,


        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fhome%2F10_512x512.jpg?alt=media&token=ab808e6e-ee5f-47f5-a915-cc5b53dc6fc3",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fhome%2F11_512x512.jpg?alt=media&token=4f9dc643-c6e8-417b-b98f-0321b5ede534",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fhome%2F12_512x512.jpg?alt=media&token=1b1b4c58-4e67-4b71-8536-4b44a163d299",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fhome%2F2_512x512.jpg?alt=media&token=c0ea9992-bd50-4405-9954-18dd0561ae9c",
        ],


        homeServices: [
          {
            name:"Men's Haircut",
            //time:"30 Minute",
           // description:"Utilization of clippers and scissors (including texturizing shears), optional shaving around the hair edges and optional pomade application.",
           price: "$25"
          },
          {
            name:"Women's Haircuts",
            //time:"10 minutes",
           //  description:"Haircut & style followed by massage therapy utilizing our percussive handheld device over the shoulders for relief & relaxation.",
            price: "$30"
          },
          {
            name:"Chilren's Haircuts",
          //  time:"10 minutes",
           // description:"For this classic & delicate service, we utilize fine shears (including texturizing shears) to cut the hair to the desired length.",
            price: "$20"
          },
          {
            name:"Dreadlocks",
           // time:"1 hour 15 minutes",
            //description:"Two hot towels, hot lather, and gentle straight-edge razor shave around the scalp, lime aftershave & balm is then applied.",
            price: "$150"
          },
          {
            name:"Brazilian Blowout",
           // time:"40 minutes",
          //  description:"From low tapers to high fades, we give you our closest, cleanest and long lasting skin fades with state of the art instruments.",
            price: "$175"
          },
        ],

      homeSchedules: [
        {
          day: "Monday",
          hour: "Closed",
        },
        {
          day: "Tuesday",
          hour: "10:00 AM – 7:00 PM",
        },
        {
          day: "Wednesday",
          hour: "10:00 AM – 7:00 PM",
        },
        {
          day: "Thursday",
          hour: "10:00 AM – 7:00 PM",
        },
        {
          day: "Friday",
          hour: "10:00 AM – 7:00 PM",
        },
        {
          day: "Saturday",
          hour: "8:00 AM – 6:00 PM",
        },
        {
          day: "Sunday",
          hour: "Closed",
        },
      ],
      
      homeVideos: [
       
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "714 220 9390",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F10_512x512.jpg?alt=media&token=937b5dfb-b885-46c4-96d7-c628e5b1fee7",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F11_512x512.jpg?alt=media&token=02ddacb0-9d91-4f82-aa67-8e684d7841ad",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F12_512x512.jpg?alt=media&token=1b639bd2-88e9-4428-975b-6ffb0f46ac0c",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F13_512x512.jpg?alt=media&token=9eaeeb4f-9e75-4bfe-81ef-0558465e237d",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F14_512x512.jpg?alt=media&token=0e901ada-4764-4047-b931-00e872f704a6",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F15_512x512.jpg?alt=media&token=094753b8-e7e6-4850-aee2-da5c507141a0",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F16_512x512.jpg?alt=media&token=0e68d631-b35b-4067-9fe1-5f3574258e52",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F1_512x512.jpg?alt=media&token=22620547-9a7c-4fba-9cf3-d60e81a1a8ed",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F2_512x512.jpg?alt=media&token=6b207d88-4d1e-47aa-9be4-2fa91c1e8d99",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F3_512x512.jpg?alt=media&token=9537a116-7cac-499d-ac7f-d3ac504c10dd",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F4_512x512.jpg?alt=media&token=31101498-4f66-43a3-b013-2e08c91c57ef",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F5_512x512.jpg?alt=media&token=df395d49-3823-4b4a-a3a6-01ee84a9a43b",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F6_512x512.jpg?alt=media&token=b49cd476-4b0b-41ad-928e-f7fa39df28e1",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F7_512x512.jpg?alt=media&token=020ab02c-de93-4565-bed8-7002c3e419c6",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F8_512x512.jpg?alt=media&token=8e7288ae-83ff-4ad4-8411-ed1055a1b988",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FHair%20Addicts%2Fgallery%2F9_512x512.jpg?alt=media&token=19c429ea-4e25-44ab-a7c4-f8e63a72f6cb",
      ],

      products:[
      
       ],

      socialNetworks: [
        {
          text: "hairaddicts",
          link: "https://hairaddicts.com/services/",
          icon: "globe",
        },
        {
          text: "Hair Addicts",
          link: "https://www.yelp.com/biz/hair-addicts-anaheim-2",
          icon: "logo-yahoo",
        },
        {
          text: "officialhairaddicts",
          link: "https://www.instagram.com/officialhairaddicts/",
          icon: "logo-instagram",
        },
        {
          text: "Hair Addicts",
          link: "https://www.facebook.com/hair.addicts.7",
          icon: "logo-facebook",
        },
      ],

      address: "8932 Katella Ave Ste 101 Anaheim, CA 92804",

      addressRoute: "8932 Katella Ave Ste 101 Anaheim, CA 92804",

      mapSource: "https://maps.google.com/maps?q=Hair%20Addicts&t=&z=15&ie=UTF8&iwloc=&output=embed",

      social1: [
        {
          text: "Appointments",
          url: 'https://hairaddicts.schedulista.com/?preview_from=https%3A%2F%2Fwww.schedulista.com%2Fhome',
          icon: "list",
        }
      ],
      
      // barbers:[
      //   {
      //     name: "Edgar",
      //     description: "Veteran Barber",
      //     socialNetworks:[
      //     ],
      //     image:"https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FMasters%20Barbers%20LA%2Fappointments%2Fb1_512x512.jpg?alt=media&token=924bf9bf-ea69-4d97-a604-de279875500f",
      //   },

 
    }

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 
    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
      // this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)}); 
  }



  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        //console.log(appData);
      }
    });
  }
}
