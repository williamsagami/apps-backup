import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  hours=[
    // {
    //   text: "00:00 AM",
    //   value: moment().hours(0).minutes(0).seconds(0)
    // },
    // {
    //   text: "00:30 AM",
    //   value: moment().hours(0).minutes(30).seconds(0)
    // },
    // {
    //   text: "01:00 AM",
    //   value: moment().hours(1).minutes(0).seconds(0)
    // },
    // {
    //   text: "01:30 AM",
    //   value: moment().hours(1).minutes(30).seconds(0)
    // },
    // {
    //   text: "02:00 AM",
    //   value: moment().hours(2).minutes(0).seconds(0)
    // },
    // {
    //   text: "02:30 AM",
    //   value: moment().hours(2).minutes(30).seconds(0)
    // },
    // {
    //   text: "03:00 AM",
    //   value: moment().hours(3).minutes(0).seconds(0)
    // },
    // {
    //   text: "03:30 AM",
    //   value: moment().hours(3).minutes(30).seconds(0)
    // },
    // {
    //   text: "04:00 AM",
    //   value: moment().hours(4).minutes(0).seconds(0)
    // },
    // {
    //   text: "04:30 AM",
    //   value: moment().hours(4).minutes(30).seconds(0)
    // },
    // {
    //   text: "05:00 AM",
    //   value: moment().hours(5).minutes(0).seconds(0)
    // },
    // {
    //   text: "05:30 AM",
    //   value: moment().hours(5).minutes(30).seconds(0)
    // },
    // {
    //   text: "06:00 AM",
    //   hours: 6,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "06:30 AM",
    //   hours: 6,
    //   minutes:30,
    //   seconds: 0
    // },
     {
      text: "07:00 AM",
      hours: 7,
      minutes:0,
      seconds: 0
    },
    {
      text: "07:30 AM",
      hours: 7,
      minutes:30,
      seconds: 0
    },
    {
      text: "08:00 AM",
      hours: 8,
      minutes:0,
      seconds: 0
    },
    {
      text: "08:30 AM",
      hours: 8,
      minutes:30,
      seconds: 0
    },
    {
      text: "09:00 AM",
      hours: 9,
      minutes:0,
      seconds: 0
    },
    {
      text: "09:30 AM",
      hours: 9,
      minutes:30,
      seconds: 0
    },
    {
      text: "10:00 AM",
      hours: 10,
      minutes:0,
      seconds: 0
    },
    {
      text: "10:30 AM",
      hours: 10,
      minutes:30,
      seconds: 0
    },
    {
      text: "11:00 AM",
      hours: 11,
      minutes:0,
      seconds: 0
    },
    {
      text: "11:30 AM",
      hours: 11,
      minutes:30,
      seconds: 0
    },

    {
      text: "12:00 AM",
      hours: 12,
      minutes:0,
      seconds: 0
    },
    {
      text: "12:30 AM",
      hours: 12,
      minutes:30,
      seconds: 0
    },
    {
      text: "13:00 AM",
      hours: 13,
      minutes:0,
      seconds: 0
    },
    {
      text: "01:30 PM",
      hours: 13,
      minutes:30,
      seconds: 0
    },
    {
      text: "02:00 PM",
      hours: 14,
      minutes:0,
      seconds: 0
    },
    {
      text: "02:30 PM",
      hours: 14,
      minutes:30,
      seconds: 0
    },
    {
      text: "03:00 PM",
      hours: 15,
      minutes:0,
      seconds: 0
    },
    {
      text: "03:30 PM",
      hours: 15,
      minutes:30,
      seconds: 0
    },
    {
      text: "04:00 PM",
      hours: 16,
      minutes:0,
      seconds: 0
    },
    {
      text: "04:30 PM",
      hours: 16,
      minutes:30,
      seconds: 0
    },
    {
      text: "05:00 PM",
      hours: 17,
      minutes:0,
      seconds: 0
    },
    {
      text: "05:30 PM",
      hours: 17,
      minutes:30,
      seconds: 0
    },
    {
      text: "06:00 PM",
      hours: 18,
      minutes:0,
      seconds: 0
    },
    {
      text: "06:30 PM",
      hours: 18,
      minutes:30,
      seconds: 0
    },
    {
      text: "07:00 PM",
      hours: 19,
      minutes:0,
      seconds: 0
    },
    {
      text: "07:30 PM",
      hours: 19,
      minutes:30,
      seconds: 0
    },
    // {
    //   text: "08:00 PM",
    //   value: moment().hours(20).minutes(0).seconds(0)
    // },
    // {
    //   text: "08:30 PM",
    //   value: moment().hours(20).minutes(30).seconds(0)
    // },
    // {
    //   text: "09:00 PM",
    //   value: moment().hours(21).minutes(0).seconds(0)
    // },
    // {
    //   text: "09:30 PM",
    //   value: moment().hours(21).minutes(30).seconds(0)
    // },
    // {
    //   text: "10:00 PM",
    //   value: moment().hours(22).minutes(0).seconds(0)
    // },
    // {
    //   text: "10:30 PM",
    //   value: moment().hours(22).minutes(30).seconds(0)
    // },
    // {
    //   text: "11:00 PM",
    //   value: moment().hours(23).minutes(0).seconds(0)
    // },
    // {
    //   text: "11:30 PM",
    //   value: moment().hours(23).minutes(30).seconds(0)
    // },
  ]

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 

  appData = {

    //Datos de Contacto
    data: {

      localAppointments: {
        active: false,
        url: "https://www.lasfinestbarbershop.com/",
      },

      appTitle: "LA's Finest BarberShop",

      homeDescription: 
        `Here at LA's Finest Barber Shop we strive to not only make you look good, but feel good. <br>
        We are an independently ran barber shop with family morals who will accommodate any person that walks through our doors. <br>
        There is no judgement of any kind here. <br>
        Just great service, great barbers, and an amazing experience we're sure you'll enjoy time and time again.".`,

        homeSlider:[
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fhome%2F16_512x512.jpg?alt=media&token=7ba2d3d4-cf95-47e9-8ca9-dc6aa1106ce8",
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fhome%2F17_512x512.jpg?alt=media&token=0e0308ee-1a44-4e11-87f5-856133d3ddca",
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fhome%2F1_512x512.jpg?alt=media&token=c7607a4f-ab5a-45a8-a54d-110393921c69",
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fhome%2F4_512x512.jpg?alt=media&token=d4d5a844-a50f-4fc5-845d-4b7dabb69256",
        ],


      homeServices: [
        {
          name:"Haircut/Kids Haircut",
          //time:"30 minutes",
          //description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
          //price: "$25"
        },
        {
          name:"Beard trimming",
          //time:"30 minutes",
         // description:"Extra 15$-20$",
         // price: "$25"
        },
        {
          name:"Hot towel Shave",
          //time:"60 minutes",
         // description:"Extra 15$-20$",
          //price: "$25"
        },
      ],
      
      homeSchedules: [
        {
          day: "Monday",
          hour: "10AM - 6:30PM",
        },
        {
          day: "Tuesday",
          hour: "10AM - 6:30PM",
        },
        {
          day: "Wednesday",
          hour: "10AM - 6:30PM",
        },
        {
          day: "Thursday",
          hour: "8AM - 6:30PM",
        },
        {
          day: "Friday",
          hour: "8AM - 6:30PM",
        },
        {
          day: "Saturday",
          hour: "8AM - 5:30PM",
        },
        {
          day: "Sunday",
          hour: "10AM - 2:30PM",
        },
      ],

      // homeBreaktime: [
      //   {
      //     day: "Monday",
      //     hour: "Close",
      //   },
      //   {
      //     day: "Tuesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Wednesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Thursday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Friday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Saturday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Sunday",
      //     hour: "Close",
      //   },
      // ],

      
      homeVideos: [
      "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fhome%2Fv1.mp4?alt=media&token=2a107308-05e0-44bd-a8b6-9375dfae4dd3",
      "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fhome%2Fv2.mp4?alt=media&token=57eed5d1-7238-46c2-9c8f-45990f5ef958",
      ],

      phoneNumbers: [
        {
          text: "LA's FINEST LAKEWOOD",
          number: "562 421 1140",
        },
      ],

      phoneNumbers1: [
        {
          text: "LA's FINEST CYPRESS",
          number: "714 821 5015",
        },
      ],

      gallery:[
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F10_512x512.jpg?alt=media&token=ee021940-fcf5-4a5a-9be6-4f14b70add2e",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F11_512x512.jpg?alt=media&token=fea3c53a-1f0b-4253-898e-9e63fb6a598d",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F12_512x512.jpg?alt=media&token=0bc838cf-9f31-4767-b844-c9310f6f524c",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F13_512x512.jpg?alt=media&token=2003def9-2e50-47c7-86f3-7077eb831054",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F14_512x512.jpg?alt=media&token=ac673668-68ff-4f73-8312-0f346534a008",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F15_512x512.jpg?alt=media&token=7abf6aab-0a4e-4571-ac8c-2d397a7ddebc",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F16_512x512.jpg?alt=media&token=bf5df996-8c06-4345-99e7-ecd23abf2f95",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F17_512x512.jpg?alt=media&token=c51f60cb-247a-4193-9c3a-0734b50c23c7",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F18_512x512.jpg?alt=media&token=7b609285-3394-4f24-8526-d08ed400f6dd",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F19_512x512.jpg?alt=media&token=06781fa5-5e35-484a-b6cd-a1d89c0d07a6",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F1_512x512.jpg?alt=media&token=7dcfd7ac-27e7-49d3-87aa-90f600a5d630",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F20_512x512.jpg?alt=media&token=24e5adce-2989-4e5a-8666-3bde0f8d6a97",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F2_512x512.jpg?alt=media&token=44489c9a-389d-4941-8e03-83e64edb327b",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F3_512x512.jpg?alt=media&token=d5d3d140-e3dc-4552-b094-f577b3e60602",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F4_512x512.jpg?alt=media&token=a8129ba3-1f43-40c7-8fa3-a1514f97b98d",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F5_512x512.jpg?alt=media&token=58352989-d133-4928-80cb-02b0e8e030b1",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F6_512x512.jpg?alt=media&token=ddbf5d0c-9fcd-4ea4-8372-481180af56b8",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F7_512x512.jpg?alt=media&token=b7b2c580-cb5a-45b0-8641-50a1c22c8cc4",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F8_512x512.jpg?alt=media&token=a30dfe5d-3faa-4c27-9256-a2accf999aa2",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FLA's%20Finest%20BarberShop%2Fgallery%2F9_512x512.jpg?alt=media&token=bcf83ef3-34d1-4da1-a033-2280565ebd57",
      ],

      socialNetworks: [
        {
          text: "lasfinestbarbershop",
          link: "https://www.instagram.com/lasfinestbarbershop/",
          icon: "logo-instagram",
        },
        {
          text: "lasfinestbarbershop",
          link: "https://www.lasfinestbarbershop.com/",
          icon: "Globe",
        },
        {
          text: "La's Finest Barber Shop",
          link: "https://www.yelp.com/biz/las-finest-barber-shop-cypress",
          icon: "logo-yahoo",
        },
        {
          text: "LA's Finest Barbershop",
          link: "https://www.facebook.com/Lasfinestbarbershop/",
          icon: "logo-facebook",
        },
      ],

      // social1: [
      //   {
      //     text: "Appointments",
      //     url: 'https://dsbarbershopny.com/contact-us/',
      //     icon: "list",
      //   },
      // ], 

      address: "4172 Woodruff Ave Lakewood CA ",
      addressRoute: "4172 Woodruff Ave Lakewood CA ",
      address1: "4959 Katella Ave Cypress, CA, 90720",
      addressRoute1: "4959 Katella Ave Cypress, CA, 90720",
      mapSource: "https://maps.google.com/maps?q=4172%20Woodruff%20Ave%20Lakewood%20CA,%20LA's%20FINEST&t=&z=15&ie=UTF8&iwloc=&output=embed", 
      mapSource1: "https://maps.google.com/maps?q=LA's%20FINEST%20CYPRESS%204959%20Katella%20Ave%20&t=&z=15&ie=UTF8&iwloc=&output=embed",
     
      // social1: [
      //   {
      //     text: "Appointments",
      //     url: 'https://shops.getsquire.com/presidential-barber-lounge-lynwood',
      //     icon: "list",
      //   }
      // ],
    
      barbers:[
        {
         
        }
      ]
    },

    appointmentsDays:[
      {
        day: 1,
        hours: this.hours,
        closed: false
      },
      {
        day: 2,
        hours: this.hours,
        closed: false
      },
      {
        day: 3,
        hours: this.hours,
        closed: false
      },
      {
        day: 4,
        hours: this.hours,
        closed: false
      },
      {
        day: 5,
        hours: this.hours,
        closed: false
      },
      {
        day: 6,
        hours: this.hours,
        closed: false
      },
      {
        day: 0,
        hours: this.hours,
        closed: false
      },
    ],

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
  // this.firebaseService.updateAppData(this.appData).then(t => {console.log(this.appData)}); 
  }


  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
     return this.firebaseService.getAppData().then(appData => {
        if(appData) {
          this.appData = appData as any;
          console.log(appData);
        }
      });
  }
}
