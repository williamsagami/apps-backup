import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  hours=[
    // {
    //   text: "00:00 AM",
    //   value: moment().hours(0).minutes(0).seconds(0)
    // },
    // {
    //   text: "00:30 AM",
    //   value: moment().hours(0).minutes(30).seconds(0)
    // },
    // {
    //   text: "01:00 AM",
    //   value: moment().hours(1).minutes(0).seconds(0)
    // },
    // {
    //   text: "01:30 AM",
    //   value: moment().hours(1).minutes(30).seconds(0)
    // },
    // {
    //   text: "02:00 AM",
    //   value: moment().hours(2).minutes(0).seconds(0)
    // },
    // {
    //   text: "02:30 AM",
    //   value: moment().hours(2).minutes(30).seconds(0)
    // },
    // {
    //   text: "03:00 AM",
    //   value: moment().hours(3).minutes(0).seconds(0)
    // },
    // {
    //   text: "03:30 AM",
    //   value: moment().hours(3).minutes(30).seconds(0)
    // },
    // {
    //   text: "04:00 AM",
    //   value: moment().hours(4).minutes(0).seconds(0)
    // },
    // {
    //   text: "04:30 AM",
    //   value: moment().hours(4).minutes(30).seconds(0)
    // },
    // {
    //   text: "05:00 AM",
    //   value: moment().hours(5).minutes(0).seconds(0)
    // },
    // {
    //   text: "05:30 AM",
    //   value: moment().hours(5).minutes(30).seconds(0)
    // },
    // {
    //   text: "06:00 AM",
    //   hours: 6,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "06:30 AM",
    //   hours: 6,
    //   minutes:30,
    //   seconds: 0
    // },
    // {
    //   text: "07:00 AM",
    //   hours: 7,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "07:30 AM",
    //   hours: 7,
    //   minutes:30,
    //   seconds: 0
    // },
    // {
    //   text: "08:00 AM",
    //   hours: 8,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "08:30 AM",
    //   hours: 8,
    //   minutes:30,
    //   seconds: 0
    // },
    // {
    //   text: "09:00 AM",
    //   hours: 9,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "09:30 AM",
    //   hours: 9,
    //   minutes:30,
    //   seconds: 0
    // },
    {
      text: "10:00 AM",
      hours: 10,
      minutes:0,
      seconds: 0
    },
    {
      text: "10:30 AM",
      hours: 10,
      minutes:30,
      seconds: 0
    },
    {
      text: "11:00 AM",
      hours: 11,
      minutes:0,
      seconds: 0
    },
    {
      text: "11:30 AM",
      hours: 11,
      minutes:30,
      seconds: 0
    },

    {
      text: "12:00 AM",
      hours: 12,
      minutes:0,
      seconds: 0
    },
    {
      text: "12:30 AM",
      hours: 12,
      minutes:30,
      seconds: 0
    },
    {
      text: "13:00 AM",
      hours: 13,
      minutes:0,
      seconds: 0
    },
    {
      text: "01:30 PM",
      hours: 13,
      minutes:30,
      seconds: 0
    },
    {
      text: "02:00 PM",
      hours: 14,
      minutes:0,
      seconds: 0
    },
    {
      text: "02:30 PM",
      hours: 14,
      minutes:30,
      seconds: 0
    },
    {
      text: "03:00 PM",
      hours: 15,
      minutes:0,
      seconds: 0
    },
    {
      text: "03:30 PM",
      hours: 15,
      minutes:30,
      seconds: 0
    },
    {
      text: "04:00 PM",
      hours: 16,
      minutes:0,
      seconds: 0
    },
    {
      text: "04:30 PM",
      hours: 16,
      minutes:30,
      seconds: 0
    },
    {
      text: "05:00 PM",
      hours: 17,
      minutes:0,
      seconds: 0
    },
    {
      text: "05:30 PM",
      hours: 17,
      minutes:30,
      seconds: 0
    },
    {
      text: "06:00 PM",
      hours: 18,
      minutes:0,
      seconds: 0
    },
    {
      text: "06:30 PM",
      hours: 18,
      minutes:30,
      seconds: 0
    },
    {
      text: "07:00 PM",
      hours: 19,
      minutes:0,
      seconds: 0
    },
    {
      text: "07:30 PM",
      hours: 19,
      minutes:30,
      seconds: 0
    },
    // {
    //   text: "08:00 PM",
    //   value: moment().hours(20).minutes(0).seconds(0)
    // },
    // {
    //   text: "08:30 PM",
    //   value: moment().hours(20).minutes(30).seconds(0)
    // },
    // {
    //   text: "09:00 PM",
    //   value: moment().hours(21).minutes(0).seconds(0)
    // },
    // {
    //   text: "09:30 PM",
    //   value: moment().hours(21).minutes(30).seconds(0)
    // },
    // {
    //   text: "10:00 PM",
    //   value: moment().hours(22).minutes(0).seconds(0)
    // },
    // {
    //   text: "10:30 PM",
    //   value: moment().hours(22).minutes(30).seconds(0)
    // },
    // {
    //   text: "11:00 PM",
    //   value: moment().hours(23).minutes(0).seconds(0)
    // },
    // {
    //   text: "11:30 PM",
    //   value: moment().hours(23).minutes(30).seconds(0)
    // },
  ]

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 

  appData = {

    //Datos de Contacto
    data: {

      appTitle: "Tony The Professor",

      homeDescription: 
        ` A great barber born and raised in Southern California. Always delivering professional quality grooming services with may years of experience. <br>
        I'm constantly improving my techniques and keeping up with the latest styles. My goal is to not only stay innovative but to also give great haircutz. <br>
        I want my clients to just arrive knowing they can relax and have an incredible experience. <br>

        Also an instructor at LISC Learning Institute of Southern California. <br>
        Tony AKA The Professor brings knowledge of the industry from an educational standpoint point, as well as from a business and personal one. <br>
        Re-educating to stay with the changes in the industry. <br>
    
        SO DON'T HESITATE. Schedule your appointment with me right now, and bring the whole family. They can enjoy an incredible experience to`,

        appTitle1: "LEARNING INSTITUTE OF SOUTHER CALIFORNIA",

        homeDescription1: 
          `Tony the Professor is a Great Instructor from Southern California. <br>
          Here at the Learning Institute of Southern California I share my experiences and techniques for cutting hair and being a professional barber. <br>
          With both book and hands on training combined, the students always strive to be the best. <br>
          They also are always trying to find ways to raise the bar here at the Learning Institute as am I. <br>

          The Professor`,

        homeSlider:[
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F10_512x512.jpg?alt=media&token=8992a66e-0c64-497e-a8ba-35fcd03ac91b",
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F1_512x512.jpg?alt=media&token=0ad9893b-aee0-4098-aae6-b3c6a29d94e1",
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F11_512x512.jpg?alt=media&token=d308c9d8-5429-4e08-9ce0-0e58c1e4623c",
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F8_512x512.jpg?alt=media&token=f04823f7-3fec-4298-b76b-b3125b61643f",
        ],


      homeServices: [
        // {
        //   name:"Adult Haircut",
        //   time:"30 minutes",
        //   //description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
        //   price: "$35"
        // },
        // {
        //   name:"Kids Haircut",
        //   time:"30 minutes",
        //  // description:"Extra 15$-20$",
        //   price: "$30"
        // },
        // {
        //   name:"Men’s Haircut & Beard",
        //   time:"60 minutes",
        //  // description:"Extra 15$-20$",
        //   price: "$50"
        // },
        // {
        //   name:"Beard Only",
        //   time:"30 minutes",
        //  // description:"Extra 15$-20$",
        //   price: "$30"
        // },
        // {
        //   name:"Head Shave & Beard",
        //   time:"45 minutes",
        //  // description:"Extra 15$-20$",
        //   price: "$45"
        // },
      ],
      
      homeSchedules: [
        // {
        //   day: "Monday",
        //   hour: "Closed",
        // },
        // {
        //   day: "Tuesday",
        //   hour: "10:00 am – 07:00 pm",
        // },
        // {
        //   day: "Wednesday",
        //   hour: "10:00 am – 07:00 pm",
        // },
        // {
        //   day: "Thursday",
        //   hour: "09:00 am – 07:00 pm",
        // },
        // {
        //   day: "Friday",
        //   hour: "08:00 am – 08:00 pm",
        // },
        // {
        //   day: "Saturday",
        //   hour: "08:00 am – 05:00 pm",
        // },
        // {
        //   day: "Sunday",
        //   hour: "11:00 am – 03:00 pm",
        // },
      ],

      // homeBreaktime: [
      //   {
      //     day: "Monday",
      //     hour: "Close",
      //   },
      //   {
      //     day: "Tuesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Wednesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Thursday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Friday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Saturday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Sunday",
      //     hour: "Close",
      //   },
      // ],

    imagenes: [
      "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F23_512x512.jpeg?alt=media&token=d5eba4c8-a483-4f03-9c00-c00dcfc3035b",
    ],
     
      homeVideos: [
      "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fhome%2F1.mp4?alt=media&token=1ed93537-9d53-4a07-8a15-bf98b5622c2b",
      "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fhome%2F2.mp4?alt=media&token=445f9849-8dc1-4458-9e0a-64a9b81618cd",
      "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fhome%2F3.mp4?alt=media&token=07c3ca1f-c880-4000-97d1-b1ba233d3030",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "760 604 9278",
        },
      ],

      gallery:[
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F10_512x512.jpg?alt=media&token=8992a66e-0c64-497e-a8ba-35fcd03ac91b",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F11_512x512.jpg?alt=media&token=d308c9d8-5429-4e08-9ce0-0e58c1e4623c",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F12_512x512.jpg?alt=media&token=52f910a7-e772-452a-af6c-09f85799cf89",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F13_512x512.jpg?alt=media&token=c9cefbf7-a366-42c2-bde0-c3254704ae33",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F14_512x512.jpg?alt=media&token=476edd55-1e69-4ab9-ba17-7410792694ce",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F15_512x512.jpg?alt=media&token=b16422ab-e1cd-404b-8772-8922b80eabc4",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F16_512x512.jpg?alt=media&token=2bd71e20-f487-4c69-8cff-e09b844c35f6",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F17_512x512.jpg?alt=media&token=b96de5a8-5efa-46e5-b34c-39736478b62f",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F18_512x512.jpg?alt=media&token=3d3488e7-5810-46e5-8ee7-147a85d4cafc",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F19_512x512.jpg?alt=media&token=3175b23f-0450-4ced-81d0-fd820c0dc0d3",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F1_512x512.jpg?alt=media&token=0ad9893b-aee0-4098-aae6-b3c6a29d94e1",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F20_512x512.jpg?alt=media&token=da50e2fc-a3d2-41ba-bb16-2ce58173a1b6",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F21_512x512.jpg?alt=media&token=e2e576ca-54c2-4940-8ae9-ea2942efba2f",
          //"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F22_512x512.jpeg?alt=media&token=53ddbf62-8595-416a-9172-7c090559a173",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F23_512x512.jpeg?alt=media&token=d5eba4c8-a483-4f03-9c00-c00dcfc3035b",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F24_512x512.jpg?alt=media&token=3babe1b7-aa53-4a97-b391-51419a362dd0",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F25_512x512.jpg?alt=media&token=c165ae76-c32e-4aee-8de6-2a9e1048896e",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F26_512x512.jpg?alt=media&token=b53f5e2f-5733-4c2b-9f72-9b986ccc682a",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F27_512x512.jpg?alt=media&token=080c2385-41a6-476e-8b65-642e0ad1db9b",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F28_512x512.jpg?alt=media&token=e744bba6-db76-4bc1-bc8a-8db75f3beda3",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F29_512x512.jpg?alt=media&token=8575e43a-8caf-45c3-836e-2b8c5ca87f93",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F2_512x512.jpg?alt=media&token=fa0b7636-2762-4683-acd8-13df0a798475",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F30_512x512.jpg?alt=media&token=e32e16c4-c72d-4989-baf9-759fee1bb95f",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F31_512x512.jpg?alt=media&token=017ada26-34a3-4b30-9d14-135156f274fc",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F32_512x512.jpg?alt=media&token=ced7cc2e-7d26-4350-98cc-5454cbc1eda9",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F33_512x512.jpg?alt=media&token=bad2227f-51d5-4356-986e-b9bfe623133d",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F3_512x512.jpg?alt=media&token=6ce2012e-3994-49d9-af2e-5ec2ac3680f1",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F4_512x512.jpg?alt=media&token=6a5b5b0e-f6a7-43d5-ac6b-9baf6b0b32e3",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F5_512x512.jpg?alt=media&token=2b18c870-4f93-4686-84f1-d69aac23d072",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F6_512x512.jpg?alt=media&token=62b88958-c1e2-4b26-acb0-1c308403e65f",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F7_512x512.jpg?alt=media&token=d29f1907-48f6-42ff-865b-56b05d354e36",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F8_512x512.jpg?alt=media&token=f04823f7-3fec-4298-b76b-b3125b61643f",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F9_512x512.jpg?alt=media&token=a1ecb74a-1af6-49bc-8fa6-c08d96fd54da",
      ],

      socialNetworks: [
        // {
        //   text: "thebarbersf",
        //   link: "https://www.instagram.com/thebarbersf/",
        //   icon: "logo-instagram",
        // },
        // {
        //   text: "The Barber",
        //   link: "https://booksy.com/en-us/86864_the-barber_barber-shop_101731_san-fernando",
        //   icon: "book",
        // },
        // {
        //   text: "TheBarber",
        //   link: "https://www.facebook.com/TheBarberSF",
        //   icon: "logo-facebook",
        // },
      ],

      address: "1151 S 4th St Suite G, El Centro, CA 92243",
      address1: "1790 N Imperial Ave, El Centro, CA 92243",
      addressRoute: "1151 S 4th St Suite G, El Centro, CA 92243",
      addressRoute1: "1790 N Imperial Ave, El Centro, CA 92243",
      mapSource: "https://maps.google.com/maps?q=1151%20S%204th%20St,%20El%20Centro,%20CA%2092243&t=&z=15&ie=UTF8&iwloc=&output=embed", 
      mapSource1: "https://maps.google.com/maps?q=LEARNING%20INSTITUTE%20OF%20SOUTHER%20CALIFORNIA%20barber%20&%20cosmetology&t=&z=15&ie=UTF8&iwloc=&output=embed", 
      // social1: [
      //   {
      //     text: "Appointments",
      //     url: 'https://booksy.com/en-us/86864_the-barber_barber-shop_101731_san-fernando',
      //     icon: "list",
      //   }
      // ],
    
      barbers:[
        {
          name: "Tony The Professor",
          description: "The Professor",
          // socialNetworks:[
          //   {
          //     // text: "straightupbarbers",
          //     // link: "https://www.instagram.com/straightupbarbers/",
          //     // icon: "logo-instagram",
          //   },
          // ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FExecutive%20cutz%20By%20Tony%20Da%20Barber%2Fgallery%2F22_512x512.jpeg?alt=media&token=53ddbf62-8595-416a-9172-7c090559a173",
        },
      ]
    },

    appointmentsDays:[
      {
        day: 1,
        hours: this.hours,
        closed: false
      },
      {
        day: 2,
        hours: this.hours,
        closed: false
      },
      {
        day: 3,
        hours: this.hours,
        closed: false
      },
      {
        day: 4,
        hours: this.hours,
        closed: false
      },
      {
        day: 5,
        hours: this.hours,
        closed: false
      },
      {
        day: 6,
        hours: this.hours,
        closed: false
      },
      {
        day: 0,
        hours: this.hours,
        closed: false
      },
    ],

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
    //   this.firebaseService.updateAppData(this.appData).then(t => {console.log(this.appData)}); 
  }


  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
     return this.firebaseService.getAppData().then(appData => {
        if(appData) {
          this.appData = appData as any;
          console.log(appData);
        }
      });
  }
}
