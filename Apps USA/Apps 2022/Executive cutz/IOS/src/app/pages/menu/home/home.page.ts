import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { ModalController } from '@ionic/angular';
import { UpdatePostPage } from './update-post/update-post.page';
import { post} from 'src/app/services/firebase.service';
import { ImagePage } from './image/image.page';
import { productsPage } from './products/products.page';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  @ViewChildren('player') videoPlayers: QueryList<any>;

  d: any; //App Data
  posts = [];

  postsReady = false;
  userData: any;
  adminPosts: any;
  selectedTab= "home";
  currentPlaying=null;
  private postsRef: any;
  private adminPostsRef: any;

  slider = [

  ];

  ima = [

  ];

  slideSales = {
    autoplay: {
      delay: 4000,
    }
  };

  slideOpts = {
    loop: true,
    autoplay: {
      delay: 2000,
    }
  };

  schedules = [
   
  ];
  breaks = [
   
  ];
  videos1= [

  ];

  videos= [

  ];
  audios=[

  ];
  audios1=[

  ];

  images = [
  ];

  images1 = [
  ];

  private index: number;
  constructor(
    private ds: DomSanitizer,
    public data: DataService, 
    private firebaseService: FirebaseService, 
    private modalController: ModalController,) {
    this.d = data.appData.data;
  }

  ngOnInit() {
    //Obtener Traduccion Dinamica
    this.data.getAppData().then(cloudTranslation => {
      this.d = this.data.appData.data;

      this.slider=this.d.homeSlider;
      this.schedules=this.d.homeSchedules;
      this.breaks=this.d.homeBreaktime;
      this.videos1=this.d.homeVideos1;
      this.videos=this.d.homeVideos;
      this.audios=this.d.audios;
      this.audios1=this.d.audios1;
      this.images=this.d.gallery;
      this.images1=this.d.products;
      this.ima=this.d.imagenes
    });


    

    //Obtener Posts
    if(this.postsRef) this.postsRef.unsubscribe();
    this.postsRef = this.firebaseService.getPostsRealTime().subscribe(posts => {
      if(posts) this.posts = posts;
      this.postsReady = true;
    });

    if(this.adminPostsRef) this.adminPostsRef.unsubscribe();
    this.adminPostsRef = this.firebaseService.getPostDataRealTime().subscribe(adminPosts => {
      this.adminPosts = adminPosts;
    });
  }
  
  ionViewDidEnter(){
    this.didScroll()
  }

  didScroll(){

    if(this.currentPlaying && this.isElementInViewport(this.currentPlaying)) {
      return;
    } else if (this.currentPlaying && !this.isElementInViewport(this.currentPlaying)){
      this.currentPlaying.pause();
      this.currentPlaying = null;
    }
    this.videoPlayers.forEach(player => {
    
      if (this.currentPlaying){
        return;
      }
      const nativeElement = player.nativeElement;
      const inView = this.isElementInViewport(nativeElement);

      if(inView){
        this.currentPlaying = nativeElement;
        this.currentPlaying.muted=true;
        this.currentPlaying.play();
      }
    });
  }

  isElementInViewport(el) {
    const rect  = el.getBoundingClientRect();
    return(
      rect.top>= 0 &&
      rect.left>= 0 &&
      rect.bottom<= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
  }

  showPostEdit() {
    if(this.firebaseService.userData) return this.firebaseService.userData.admin; 
    else return false;
  }

  showPostCreate() {
    if(this.adminPosts) {
      return (this.showPostEdit() && this.adminPosts.postNumber < this.adminPosts.maxPostNumber);
    } 
    else return false;
  }

  async viewPost(post: post) {
    const va = await this.modalController.create({
      component: UpdatePostPage,
      componentProps : {
        post: post,
        create: false,
        adminPosts: this.adminPosts,
      }
    });
    await va.present();
  }

  async createPost() {
    const va = await this.modalController.create({
      component: UpdatePostPage,
      componentProps : {
        post: null,
        create: true,
        adminPosts: this.adminPosts,
      }
    });
    await va.present();
  }

  async openImage(i: number)
	{
    const modal = await this.modalController.create({
      component: ImagePage,
      cssClass: 'modal-transparency',
      componentProps : {
        index: i+1,
        images: this.images,
      }
    });
    await modal.present();
	}
  
  async openImage1(i: number)
	{
    const modal = await this.modalController.create({
      component: productsPage,
      cssClass: 'modal-transparency',
      componentProps : {
        index: i+1,
        images1: this.images1,
      }
    });
    await modal.present();
	}

  getMapUrl() {
    return this.ds.bypassSecurityTrustResourceUrl(this.d.mapSource);
  }
}
