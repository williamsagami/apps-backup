// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyCKWzS_DQLZcHBkqzBywwkD18ZtYt0nadg",
  authDomain: "slamart.firebaseapp.com",
  databaseURL: "https://slamart.firebaseio.com",
  projectId: "slamart",
  storageBucket: "slamart.appspot.com",
  messagingSenderId: "328252660508",
  appId: "1:328252660508:web:cd6f715310764b0ebb78d9",
  measurementId: "G-ZQG2D3W7PR"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
