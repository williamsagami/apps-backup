import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  hours=[
    // {
    //   text: "00:00 AM",
    //   value: moment().hours(0).minutes(0).seconds(0)
    // },
    // {
    //   text: "00:30 AM",
    //   value: moment().hours(0).minutes(30).seconds(0)
    // },
    // {
    //   text: "01:00 AM",
    //   value: moment().hours(1).minutes(0).seconds(0)
    // },
    // {
    //   text: "01:30 AM",
    //   value: moment().hours(1).minutes(30).seconds(0)
    // },
    // {
    //   text: "02:00 AM",
    //   value: moment().hours(2).minutes(0).seconds(0)
    // },
    // {
    //   text: "02:30 AM",
    //   value: moment().hours(2).minutes(30).seconds(0)
    // },
    // {
    //   text: "03:00 AM",
    //   value: moment().hours(3).minutes(0).seconds(0)
    // },
    // {
    //   text: "03:30 AM",
    //   value: moment().hours(3).minutes(30).seconds(0)
    // },
    // {
    //   text: "04:00 AM",
    //   value: moment().hours(4).minutes(0).seconds(0)
    // },
    // {
    //   text: "04:30 AM",
    //   value: moment().hours(4).minutes(30).seconds(0)
    // },
    // {
    //   text: "05:00 AM",
    //   value: moment().hours(5).minutes(0).seconds(0)
    // },
    // {
    //   text: "05:30 AM",
    //   value: moment().hours(5).minutes(30).seconds(0)
    // },
    // {
    //   text: "06:00 AM",
    //   hours: 6,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "06:30 AM",
    //   hours: 6,
    //   minutes:30,
    //   seconds: 0
    // },
     {
      text: "07:00 AM",
      hours: 7,
      minutes:0,
      seconds: 0
    },
    {
      text: "07:30 AM",
      hours: 7,
      minutes:30,
      seconds: 0
    },
    {
      text: "08:00 AM",
      hours: 8,
      minutes:0,
      seconds: 0
    },
    {
      text: "08:30 AM",
      hours: 8,
      minutes:30,
      seconds: 0
    },
    {
      text: "09:00 AM",
      hours: 9,
      minutes:0,
      seconds: 0
    },
    {
      text: "09:30 AM",
      hours: 9,
      minutes:30,
      seconds: 0
    },
    {
      text: "10:00 AM",
      hours: 10,
      minutes:0,
      seconds: 0
    },
    {
      text: "10:30 AM",
      hours: 10,
      minutes:30,
      seconds: 0
    },
    {
      text: "11:00 AM",
      hours: 11,
      minutes:0,
      seconds: 0
    },
    {
      text: "11:30 AM",
      hours: 11,
      minutes:30,
      seconds: 0
    },

    {
      text: "12:00 AM",
      hours: 12,
      minutes:0,
      seconds: 0
    },
    {
      text: "12:30 AM",
      hours: 12,
      minutes:30,
      seconds: 0
    },
    {
      text: "13:00 AM",
      hours: 13,
      minutes:0,
      seconds: 0
    },
    {
      text: "01:30 PM",
      hours: 13,
      minutes:30,
      seconds: 0
    },
    {
      text: "02:00 PM",
      hours: 14,
      minutes:0,
      seconds: 0
    },
    {
      text: "02:30 PM",
      hours: 14,
      minutes:30,
      seconds: 0
    },
    {
      text: "03:00 PM",
      hours: 15,
      minutes:0,
      seconds: 0
    },
    {
      text: "03:30 PM",
      hours: 15,
      minutes:30,
      seconds: 0
    },
    {
      text: "04:00 PM",
      hours: 16,
      minutes:0,
      seconds: 0
    },
    {
      text: "04:30 PM",
      hours: 16,
      minutes:30,
      seconds: 0
    },
    {
      text: "05:00 PM",
      hours: 17,
      minutes:0,
      seconds: 0
    },
    {
      text: "05:30 PM",
      hours: 17,
      minutes:30,
      seconds: 0
    },
    {
      text: "06:00 PM",
      hours: 18,
      minutes:0,
      seconds: 0
    },
    {
      text: "06:30 PM",
      hours: 18,
      minutes:30,
      seconds: 0
    },
    {
      text: "07:00 PM",
      hours: 19,
      minutes:0,
      seconds: 0
    },
    {
      text: "07:30 PM",
      hours: 19,
      minutes:30,
      seconds: 0
    },
    // {
    //   text: "08:00 PM",
    //   value: moment().hours(20).minutes(0).seconds(0)
    // },
    // {
    //   text: "08:30 PM",
    //   value: moment().hours(20).minutes(30).seconds(0)
    // },
    // {
    //   text: "09:00 PM",
    //   value: moment().hours(21).minutes(0).seconds(0)
    // },
    // {
    //   text: "09:30 PM",
    //   value: moment().hours(21).minutes(30).seconds(0)
    // },
    // {
    //   text: "10:00 PM",
    //   value: moment().hours(22).minutes(0).seconds(0)
    // },
    // {
    //   text: "10:30 PM",
    //   value: moment().hours(22).minutes(30).seconds(0)
    // },
    // {
    //   text: "11:00 PM",
    //   value: moment().hours(23).minutes(0).seconds(0)
    // },
    // {
    //   text: "11:30 PM",
    //   value: moment().hours(23).minutes(30).seconds(0)
    // },
  ]

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 

  appData = {

    //Datos de Contacto
    data: {

      appTitle: "MX Barbershop",

      homeDescription: 
        `One of the best barbershops in Fort Worth 
        where we have years of experience and we have professional barbers who offer their best customer service where you just sit back, 
        relax and have a unique experience in our haircuts, 
        we have good references and We are always innovating our cuts, if you want to have one of your unique cuts, 
        do not hesitate to visit us and make your appointment in our application, because with us we are the best options for you and your family.`,

        homeSlider:[
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fhome%2F18_512x512.jpg?alt=media&token=134fbca4-1b1d-4c6c-ab61-1b852a713d41",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fhome%2F1_512x512.jpg?alt=media&token=03edec89-0f9c-4498-af5a-a556300f01cf",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fhome%2F2_512x512.jpg?alt=media&token=01372098-1479-4da6-8d3e-763ebefc433c",
          "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fhome%2F8_512x512.jpg?alt=media&token=bd209281-1889-4d49-884c-db32b814cad8",
        ],


      homeServices: [
        {
          name:"REGULAR HAIR CUT",
          //time:"30 minutes",
          //description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
          price: "$15"
        },
        {
          name:"BEARD & CUT",
          //time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$30"
        },
        {
          name:"WOMAN HAIR CUT",
          //time:"60 minutes",
         // description:"Extra 15$-20$",
          price: "$18"
        },
      ],
      
      homeSchedules: [
        {
          day: "Monday",
          hour: "Closed",
        },
        {
          day: "Tuesday",
          hour: "10:00 AM - 07:00 PM",
        },
        {
          day: "Wednesday",
          hour: "10:00 AM - 07:00 PM",
        },
        {
          day: "Thursday",
          hour: "10:00 AM - 07:00 PM",
        },
        {
          day: "Friday",
          hour: "10:00 AM - 07:00 PM",
        },
        {
          day: "Saturday",
          hour: "10:00 AM - 07:00 PM",
        },
        {
          day: "Sunday",
          hour: "10:00 AM - 4:00 PM",
        },
      ],

      // homeBreaktime: [
      //   {
      //     day: "Monday",
      //     hour: "Close",
      //   },
      //   {
      //     day: "Tuesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Wednesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Thursday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Friday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Saturday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Sunday",
      //     hour: "Close",
      //   },
      // ],

      
      homeVideos: [
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fhome%2Fv1.mp4?alt=media&token=9268c534-6d84-414a-a921-8a58c4ab2876",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fhome%2Fv2.mp4?alt=media&token=a38cea07-850d-4791-83ea-631e9433c027",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fhome%2Fv3.mp4?alt=media&token=d9466893-6875-4602-a500-085da41b6e6c",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "817 806 7464",
        },
      ],

      gallery:[
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F10_512x512.jpg?alt=media&token=bb1d95fb-ee49-4844-b151-fb9315a0116f",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F11_512x512.jpg?alt=media&token=b8a0a203-052b-44b7-b75c-2d0475bf813f",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F12_512x512.jpg?alt=media&token=824c5a56-07f7-4656-bbc1-a8b0c4d79d9b",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F13_512x512.jpg?alt=media&token=f3b026dc-87d1-4ea4-996e-63b0ef7d1ef6",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F14_512x512.jpg?alt=media&token=cb1f2718-fcfa-4111-acfe-989c240cd4c3",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F15_512x512.jpg?alt=media&token=a12be70a-87cd-470e-b484-a7588ae5951b",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F16_512x512.jpg?alt=media&token=ac763611-4547-4628-b621-0d3a0129f612",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F17_512x512.jpg?alt=media&token=aa46c221-4ab5-428d-b989-4cbefada3ce8",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F18_512x512.jpg?alt=media&token=905e2268-b9fc-49f0-a87e-8f08724cc7ce",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F19_512x512.jpg?alt=media&token=84946836-de75-477c-ae50-ba7bdc1e7d34",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F1_512x512.jpg?alt=media&token=44b599c0-f5e5-4aab-82fc-894be001cfc8",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F20_512x512.jpg?alt=media&token=506ab97d-37b1-42ba-9fff-52de98ff1a17",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F21_512x512.jpg?alt=media&token=306b01cc-4a0c-49ae-81e6-ce95e0682704",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F22_512x512.jpg?alt=media&token=86769a23-5fb3-488c-b76e-9a8bde02471f",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F23_512x512.jpg?alt=media&token=8050b8c0-8534-4cbe-baf7-f5366ac99fbe",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F24_512x512.jpg?alt=media&token=5d683849-0a2c-4f67-9ca3-df0bd545706e",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F25_512x512.jpg?alt=media&token=2ba9caca-f32d-4955-859e-aeb8a867b2a7",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F26_512x512.jpg?alt=media&token=8be0068e-b6c5-487b-b58e-485cb5ccc68c",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F2_512x512.jpg?alt=media&token=8fe18afe-185c-4336-8798-44dc030c6d64",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F3_512x512.jpg?alt=media&token=208bc248-f189-4ff9-93c0-b5a622ea5f9a",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F4_512x512.jpg?alt=media&token=1d7cdb77-e1d7-4207-8bf1-dc9d9e0aab44",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F5_512x512.jpg?alt=media&token=3ccf37ec-f2e2-4592-9ac9-537c1d1eb60d",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F6_512x512.jpg?alt=media&token=c82a4a1b-4ac8-471c-b573-e34a0a215495",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F7_512x512.jpg?alt=media&token=28e696bb-a29d-4113-8748-06ef0a75d2da",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F8_512x512.jpg?alt=media&token=cd119f32-7717-4ef5-8ac2-2bd217adff70",
       "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fgallery%2F9_512x512.jpg?alt=media&token=48639b56-7e52-4f29-933c-b08ab48ed4f7",  
      ],

      socialNetworks: [
        {
          text: "mxbarbershop__",
          link: "https://www.instagram.com/mxbarbershop__/",
          icon: "logo-instagram",
        },
        {
          text: "mxbarbershopsalon",
          link: "https://mxbarbershopsalon.wixsite.com/website",
          icon: "Globe",
        },
        // {
        //   text: "Authentic Image Barbershop",
        //   link: "https://www.yelp.com/biz/authentic-image-barbershop-anaheim-3?start=20",
        //   icon: "logo-yahoo",
        // },
        {
          text: "MX Barber Shop & Salon",
          link: "https://www.facebook.com/barberlupita/",
          icon: "logo-facebook",
        },
      ],

      address: "239 NE 28th St, Fort Worth, TX 76164, United States",
      addressRoute: "239 NE 28th St, Fort Worth, TX 76164, United States",
      mapSource: "https://maps.google.com/maps?q=MX%20BarberShop,%20239%20NE%2028th%20St,%20Fort%20Worth,%20TX%2076164,%20United%20States&t=&z=15&ie=UTF8&iwloc=&output=embed", 
     
      // social1: [
      //   {
      //     text: "Appointments",
      //     url: 'https://shops.getsquire.com/presidential-barber-lounge-lynwood',
      //     icon: "list",
      //   }
      // ],
    
      barbers:[
        {
          name: "MX Barbershop",
          description: "MX Barbershop Application",
          socialNetworks:[
            // {
            //   text: "wavyblends16_",
            //   link: "https://www.instagram.com/wavyblends16_/",
            //   icon: "logo-instagram",
            // },
          ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FMX%20Barbershop%2Fappointments%2FLogo_512x512.png?alt=media&token=814e8c91-f3c0-4594-b892-2c45b2947c37",
        }
      ]
    },

    appointmentsDays:[
      {
        day: 1,
        hours: this.hours,
        closed: false
      },
      {
        day: 2,
        hours: this.hours,
        closed: false
      },
      {
        day: 3,
        hours: this.hours,
        closed: false
      },
      {
        day: 4,
        hours: this.hours,
        closed: false
      },
      {
        day: 5,
        hours: this.hours,
        closed: false
      },
      {
        day: 6,
        hours: this.hours,
        closed: false
      },
      {
        day: 0,
        hours: this.hours,
        closed: false
      },
    ],

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
    // this.firebaseService.updateAppData(this.appData).then(t => {console.log(this.appData)}); 
  }


  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
     return this.firebaseService.getAppData().then(appData => {
        if(appData) {
          this.appData = appData as any;
          console.log(appData);
        }
      });
  }
}
