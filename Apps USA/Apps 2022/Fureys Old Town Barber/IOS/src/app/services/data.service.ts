import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  hours=[
    // {
    //   text: "00:00 AM",
    //   value: moment().hours(0).minutes(0).seconds(0)
    // },
    // {
    //   text: "00:30 AM",
    //   value: moment().hours(0).minutes(30).seconds(0)
    // },
    // {
    //   text: "01:00 AM",
    //   value: moment().hours(1).minutes(0).seconds(0)
    // },
    // {
    //   text: "01:30 AM",
    //   value: moment().hours(1).minutes(30).seconds(0)
    // },
    // {
    //   text: "02:00 AM",
    //   value: moment().hours(2).minutes(0).seconds(0)
    // },
    // {
    //   text: "02:30 AM",
    //   value: moment().hours(2).minutes(30).seconds(0)
    // },
    // {
    //   text: "03:00 AM",
    //   value: moment().hours(3).minutes(0).seconds(0)
    // },
    // {
    //   text: "03:30 AM",
    //   value: moment().hours(3).minutes(30).seconds(0)
    // },
    // {
    //   text: "04:00 AM",
    //   value: moment().hours(4).minutes(0).seconds(0)
    // },
    // {
    //   text: "04:30 AM",
    //   value: moment().hours(4).minutes(30).seconds(0)
    // },
    // {
    //   text: "05:00 AM",
    //   value: moment().hours(5).minutes(0).seconds(0)
    // },
    // {
    //   text: "05:30 AM",
    //   value: moment().hours(5).minutes(30).seconds(0)
    // },
    // {
    //   text: "06:00 AM",
    //   hours: 6,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "06:30 AM",
    //   hours: 6,
    //   minutes:30,
    //   seconds: 0
    // },
    //  {
    //   text: "07:00 AM",
    //   hours: 7,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "07:30 AM",
    //   hours: 7,
    //   minutes:30,
    //   seconds: 0
    // },
    // {
    //   text: "08:00 AM",
    //   hours: 8,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "08:30 AM",
    //   hours: 8,
    //   minutes:30,
    //   seconds: 0
    // },
    {
      text: "09:00 AM",
      hours: 9,
      minutes:0,
      seconds: 0
    },
    {
      text: "09:30 AM",
      hours: 9,
      minutes:30,
      seconds: 0
    },
    {
      text: "10:00 AM",
      hours: 10,
      minutes:0,
      seconds: 0
    },
    {
      text: "10:30 AM",
      hours: 10,
      minutes:30,
      seconds: 0
    },
    {
      text: "11:00 AM",
      hours: 11,
      minutes:0,
      seconds: 0
    },
    {
      text: "11:30 AM",
      hours: 11,
      minutes:30,
      seconds: 0
    },

    {
      text: "12:00 AM",
      hours: 12,
      minutes:0,
      seconds: 0
    },
    {
      text: "12:30 AM",
      hours: 12,
      minutes:30,
      seconds: 0
    },
    {
      text: "13:00 AM",
      hours: 13,
      minutes:0,
      seconds: 0
    },
    {
      text: "01:30 PM",
      hours: 13,
      minutes:30,
      seconds: 0
    },
    {
      text: "02:00 PM",
      hours: 14,
      minutes:0,
      seconds: 0
    },
    {
      text: "02:30 PM",
      hours: 14,
      minutes:30,
      seconds: 0
    },
    {
      text: "03:00 PM",
      hours: 15,
      minutes:0,
      seconds: 0
    },
    {
      text: "03:30 PM",
      hours: 15,
      minutes:30,
      seconds: 0
    },
    {
      text: "04:00 PM",
      hours: 16,
      minutes:0,
      seconds: 0
    },
    {
      text: "04:30 PM",
      hours: 16,
      minutes:30,
      seconds: 0
    },
    {
      text: "05:00 PM",
      hours: 17,
      minutes:0,
      seconds: 0
    },
    {
      text: "05:30 PM",
      hours: 17,
      minutes:30,
      seconds: 0
    },
    {
      text: "06:00 PM",
      hours: 18,
      minutes:0,
      seconds: 0
    },
    {
      text: "06:30 PM",
      hours: 18,
      minutes:30,
      seconds: 0
    },
    // {
    //   text: "07:00 PM",
    //   hours: 19,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "07:30 PM",
    //   hours: 19,
    //   minutes:30,
    //   seconds: 0
    // },
    // {
    //   text: "08:00 PM",
    //   value: moment().hours(20).minutes(0).seconds(0)
    // },
    // {
    //   text: "08:30 PM",
    //   value: moment().hours(20).minutes(30).seconds(0)
    // },
    // {
    //   text: "09:00 PM",
    //   value: moment().hours(21).minutes(0).seconds(0)
    // },
    // {
    //   text: "09:30 PM",
    //   value: moment().hours(21).minutes(30).seconds(0)
    // },
    // {
    //   text: "10:00 PM",
    //   value: moment().hours(22).minutes(0).seconds(0)
    // },
    // {
    //   text: "10:30 PM",
    //   value: moment().hours(22).minutes(30).seconds(0)
    // },
    // {
    //   text: "11:00 PM",
    //   value: moment().hours(23).minutes(0).seconds(0)
    // },
    // {
    //   text: "11:30 PM",
    //   value: moment().hours(23).minutes(30).seconds(0)
    // },
  ]

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 

  appData = {

    //Datos de Contacto
    data: {

      localAppointments: {
        active: true,
        url: "https://squareup.com/appointments/book/oblyg280s7nof3/F2JT8YPQXF0S7/services",
      },

      appTitle: "Furey's Old Town Barber",

      homeDescription: 
        `
        Our mission is to keep the spirit of traditional barbering alive. <br>
        We bring the nostalgia of yesteryear with a modern twist.. <br>
        From our real barber chairs to our old fashioned hot lather and straight razor shaves to our relaxing head and neck massages. <br>
        Furey's sets a standard for barbershops on the central coast. <br>
        Whether you are just looking for a traditional haircut or a more contemporary style, 
        we invite you to visit our shop and see for yourself why Furey's is a cut above the rest. <br>
        Our team of barbers and cosmetologists will strive to make your experience professional and enjoyable.
        <br>
        <br>
        **Please appreciate that all of our services and products are available to the public regardless of race, gender, and/or sexual orientation.`,

        homeSlider:[
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F18_512x512.jpg?alt=media&token=5a97423a-bfff-4a38-85f3-c8597182f178",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F19_512x512.jpg?alt=media&token=006941eb-24af-4a46-b907-20bcf2f2fd54",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F22_512x512.jpg?alt=media&token=b39ba308-b09c-49ad-b019-042d786b1ad6",
         "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F1_512x512.jpg?alt=media&token=904c166d-6381-4f7f-a728-f69c2e2b1f54",
        ],


      homeServices: [
        {
          name:"Beard Overhaul",
          time:"30 minutes",
          description:"Hot towels, massage, deep conditioning and lineup to tame the unruliest of beards.",
          price: "$35"
        },
        {
          name:"Executive Cut",
          time:"60 minutes",
          description:"Traditional cut with shampoo & Condition plus an invigorating head, neck, shoulder, & hand massage",
          price: "$50"
        },
        {
          name:"Beard Trim",
          time:"30 minutes",
          //description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
          price: "$20"
        },
        {
          name:"Traditional Haircuts",
          time:"45 minutes",
          //description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
          price: "$45"
        },
      ],
      
      homeSchedules: [
        {
          day: "Monday",
          hour: "9:00 am - 6:00 pm",
        },
        {
          day: "Tuesday",
          hour: "9:00 am - 6:00 pm",
        },
        {
          day: "Wednesday",
          hour: "9:00 am - 6:00 pm",
        },
        {
          day: "Thursday",
          hour: "9:00 am - 6:00 pm",
        },
        {
          day: "Friday",
          hour: "9:00 am - 6:00 pm",
        },
        {
          day: "Saturday",
          hour: "9:00 am - 3:00 pm",
        },
        {
          day: "Sunday",
          hour: "9:00 am - 3:00 pm",
        },
      ],

      // homeBreaktime: [
      //   {
      //     day: "Monday",
      //     hour: "Close",
      //   },
      //   {
      //     day: "Tuesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Wednesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Thursday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Friday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Saturday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Sunday",
      //     hour: "Close",
      //   },
      // ],

      
      homeVideos: [
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fhome%2Fv1.mp4?alt=media&token=f22a26c1-2481-4ec5-9b93-dcc4abbfd209",
      "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fhome%2Fv2.mp4?alt=media&token=b76f6318-c68c-4285-b7d1-9a3b6f0b3b51",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "831 261-4556",
        },
      ],

      gallery:[
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F10_512x512.jpg?alt=media&token=a5000d59-eaaa-4dbc-8a06-e757b22e0218",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F11_512x512.jpg?alt=media&token=bd9a02b3-e4f8-4a40-9c1a-d72c3d914553",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F12_512x512.jpg?alt=media&token=26d8689f-0b2b-4eac-a1e2-59a9e0af777a",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F13_512x512.jpg?alt=media&token=acd6b003-d86b-49a8-9535-2b18f799290d",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F14_512x512.jpg?alt=media&token=5e6ada4d-db7c-4f93-a9a8-60c2b6019500",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F15_512x512.jpg?alt=media&token=783f5be6-0a12-4e31-9568-1661551f810b",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F16_512x512.jpg?alt=media&token=b5e74357-2948-4bd8-8de9-05908c84ebb4",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F17_512x512.jpg?alt=media&token=caf2b0fc-07ba-4cce-8c2b-70f5806654b9",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F18_512x512.jpg?alt=media&token=5a97423a-bfff-4a38-85f3-c8597182f178",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F19_512x512.jpg?alt=media&token=006941eb-24af-4a46-b907-20bcf2f2fd54",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F1_512x512.jpg?alt=media&token=904c166d-6381-4f7f-a728-f69c2e2b1f54",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F20_512x512.jpg?alt=media&token=0bbabe53-da5a-4842-9a9c-b8bfc85bd421",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F21_512x512.jpg?alt=media&token=fb4a2a9e-23a8-4df9-98b1-d67d784ed641",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F22_512x512.jpg?alt=media&token=b39ba308-b09c-49ad-b019-042d786b1ad6",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F23_512x512.jpg?alt=media&token=29df8902-f3c1-4c50-a9be-485f85c28684",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F24_512x512.jpg?alt=media&token=99c763b1-95a1-423f-8f75-debe9b6fc6ae",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F2_512x512.jpg?alt=media&token=58e00870-d77e-4247-8e6c-d60df8e00186",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F3_512x512.jpg?alt=media&token=4efd77bd-1634-4249-b3b5-d2fb0dd9791e",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F4_512x512.jpg?alt=media&token=48aa411d-04b2-4e64-91dd-ca6feedbd99f",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F5_512x512.jpg?alt=media&token=8f65dad0-7d2c-4781-8fd1-0b7ff3b0b8c7",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F6_512x512.jpg?alt=media&token=5ac78893-3994-4e76-8d08-00784beb6635",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F7_512x512.jpg?alt=media&token=c7146e81-5b54-40f4-877e-e362c1bba6db",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F8_512x512.jpg?alt=media&token=e3f1ab6f-263c-4588-89fd-9962c471d373",
       "https://firebasestorage.googleapis.com/v0/b/slamart-applications-3.appspot.com/o/Appoiments%2FFurey's%20Old%20Town%20Barber%2Fgallery%2F9_512x512.jpg?alt=media&token=e80a23f8-090c-4887-8ccd-b331c609fc04",
      ],

      socialNetworks: [
        {
          text: "@fureys_old_town_barber ",
          link: "https://www.instagram.com/fureys_old_town_barber/",
          icon: "logo-instagram",
        },
        {
          text: "fureysoldtownbarber",
          link: "https://www.fureysoldtownbarber.com/",
          icon: "Globe",
        },
        {
          text: "Furey's Old Town Barber",
          link: "https://www.yelp.com/biz/fureys-old-town-barber-salinas",
          icon: "logo-yahoo",
        },
        {
          text: "@fureysoldtownbarber",
          link: "https://www.facebook.com/FureysOldTownBarber",
          icon: "logo-facebook",
        },
        {
          text: "Waitlist",
          link: "https://www.waitly.com/checkin/HctzD1vKYuHwSmG7bcjL",
          icon: "list",
        },
        {
          text: "Digital Gift Cards",
          link: "https://squareup.com/gift/FZGR7FRKW3AD9/order?external_source=postoffice-egift-widget&external_token=Z9y5kC11EkyOmVEY",
          icon: "Gift",
        },
      ],

      // social1: [
      //   {
      //     text: "Appointments",
      //     url: 'https://dsbarbershopny.com/contact-us/',
      //     icon: "list",
      //   },
      // ], 

      address: "Fureys Old Town Barber 159 Main St. Salinas, California 93901",
      addressRoute: "Fureys Old Town Barber 159 Main St. Salinas, California 93901",
      mapSource: "https://maps.google.com/maps?q=Furey's%20Old%20Town%20Barber&t=&z=15&ie=UTF8&iwloc=&output=embed", 
     
      // social1: [
      //   {
      //     text: "Appointments",
      //     url: 'https://shops.getsquire.com/presidential-barber-lounge-lynwood',
      //     icon: "list",
      //   }
      // ],
    
      barbers:[
        {
         
        }
      ]
    },

    appointmentsDays:[
      {
        day: 1,
        hours: this.hours,
        closed: false
      },
      {
        day: 2,
        hours: this.hours,
        closed: false
      },
      {
        day: 3,
        hours: this.hours,
        closed: false
      },
      {
        day: 4,
        hours: this.hours,
        closed: false
      },
      {
        day: 5,
        hours: this.hours,
        closed: false
      },
      {
        day: 6,
        hours: this.hours,
        closed: false
      },
      {
        day: 0,
        hours: this.hours,
        closed: false
      },
    ],

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
  // this.firebaseService.updateAppData(this.appData).then(t => {console.log(this.appData)}); 
  }


  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
     return this.firebaseService.getAppData().then(appData => {
        if(appData) {
          this.appData = appData as any;
          console.log(appData);
        }
      });
  }
}
