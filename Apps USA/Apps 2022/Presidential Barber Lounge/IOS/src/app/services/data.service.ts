import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/menu/call/call.page';
import { FirebaseService } from './firebase.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  hours=[
    // {
    //   text: "00:00 AM",
    //   value: moment().hours(0).minutes(0).seconds(0)
    // },
    // {
    //   text: "00:30 AM",
    //   value: moment().hours(0).minutes(30).seconds(0)
    // },
    // {
    //   text: "01:00 AM",
    //   value: moment().hours(1).minutes(0).seconds(0)
    // },
    // {
    //   text: "01:30 AM",
    //   value: moment().hours(1).minutes(30).seconds(0)
    // },
    // {
    //   text: "02:00 AM",
    //   value: moment().hours(2).minutes(0).seconds(0)
    // },
    // {
    //   text: "02:30 AM",
    //   value: moment().hours(2).minutes(30).seconds(0)
    // },
    // {
    //   text: "03:00 AM",
    //   value: moment().hours(3).minutes(0).seconds(0)
    // },
    // {
    //   text: "03:30 AM",
    //   value: moment().hours(3).minutes(30).seconds(0)
    // },
    // {
    //   text: "04:00 AM",
    //   value: moment().hours(4).minutes(0).seconds(0)
    // },
    // {
    //   text: "04:30 AM",
    //   value: moment().hours(4).minutes(30).seconds(0)
    // },
    // {
    //   text: "05:00 AM",
    //   value: moment().hours(5).minutes(0).seconds(0)
    // },
    // {
    //   text: "05:30 AM",
    //   value: moment().hours(5).minutes(30).seconds(0)
    // },
    // {
    //   text: "06:00 AM",
    //   hours: 6,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "06:30 AM",
    //   hours: 6,
    //   minutes:30,
    //   seconds: 0
    // },
    // {
    //   text: "07:00 AM",
    //   hours: 7,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "07:30 AM",
    //   hours: 7,
    //   minutes:30,
    //   seconds: 0
    // },
    // {
    //   text: "08:00 AM",
    //   hours: 8,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "08:30 AM",
    //   hours: 8,
    //   minutes:30,
    //   seconds: 0
    // },
    // {
    //   text: "09:00 AM",
    //   hours: 9,
    //   minutes:0,
    //   seconds: 0
    // },
    // {
    //   text: "09:30 AM",
    //   hours: 9,
    //   minutes:30,
    //   seconds: 0
    // },
    {
      text: "10:00 AM",
      hours: 10,
      minutes:0,
      seconds: 0
    },
    {
      text: "10:30 AM",
      hours: 10,
      minutes:30,
      seconds: 0
    },
    {
      text: "11:00 AM",
      hours: 11,
      minutes:0,
      seconds: 0
    },
    {
      text: "11:30 AM",
      hours: 11,
      minutes:30,
      seconds: 0
    },

    {
      text: "12:00 AM",
      hours: 12,
      minutes:0,
      seconds: 0
    },
    {
      text: "12:30 AM",
      hours: 12,
      minutes:30,
      seconds: 0
    },
    {
      text: "13:00 AM",
      hours: 13,
      minutes:0,
      seconds: 0
    },
    {
      text: "01:30 PM",
      hours: 13,
      minutes:30,
      seconds: 0
    },
    {
      text: "02:00 PM",
      hours: 14,
      minutes:0,
      seconds: 0
    },
    {
      text: "02:30 PM",
      hours: 14,
      minutes:30,
      seconds: 0
    },
    {
      text: "03:00 PM",
      hours: 15,
      minutes:0,
      seconds: 0
    },
    {
      text: "03:30 PM",
      hours: 15,
      minutes:30,
      seconds: 0
    },
    {
      text: "04:00 PM",
      hours: 16,
      minutes:0,
      seconds: 0
    },
    {
      text: "04:30 PM",
      hours: 16,
      minutes:30,
      seconds: 0
    },
    {
      text: "05:00 PM",
      hours: 17,
      minutes:0,
      seconds: 0
    },
    {
      text: "05:30 PM",
      hours: 17,
      minutes:30,
      seconds: 0
    },
    {
      text: "06:00 PM",
      hours: 18,
      minutes:0,
      seconds: 0
    },
    {
      text: "06:30 PM",
      hours: 18,
      minutes:30,
      seconds: 0
    },
    {
      text: "07:00 PM",
      hours: 19,
      minutes:0,
      seconds: 0
    },
    {
      text: "07:30 PM",
      hours: 19,
      minutes:30,
      seconds: 0
    },
    // {
    //   text: "08:00 PM",
    //   value: moment().hours(20).minutes(0).seconds(0)
    // },
    // {
    //   text: "08:30 PM",
    //   value: moment().hours(20).minutes(30).seconds(0)
    // },
    // {
    //   text: "09:00 PM",
    //   value: moment().hours(21).minutes(0).seconds(0)
    // },
    // {
    //   text: "09:30 PM",
    //   value: moment().hours(21).minutes(30).seconds(0)
    // },
    // {
    //   text: "10:00 PM",
    //   value: moment().hours(22).minutes(0).seconds(0)
    // },
    // {
    //   text: "10:30 PM",
    //   value: moment().hours(22).minutes(30).seconds(0)
    // },
    // {
    //   text: "11:00 PM",
    //   value: moment().hours(23).minutes(0).seconds(0)
    // },
    // {
    //   text: "11:30 PM",
    //   value: moment().hours(23).minutes(30).seconds(0)
    // },
  ]

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 

  appData = {

    //Datos de Contacto
    data: {

      appTitle: "Presidential Barber Shop",

      homeDescription: 
        `Our business is dedicated to fulfill all customer’s desired hairstyles and new trends. <br>
        We specialize in all types of men haircuts ! We also provide different services like full facial service, 
        steam shaves, hot towel shaves, professional hair designs, beard color enhancement treatments, etc etc... <br>
        For our accommodated vip clients we provide a separate section accommodated with all a well furnished area equipped with our golden croc barber chairs, 
        led mirrors, chandelier lights decor to bring a luxury theme setup. All the extra needs for a vip service that anyone can easily book at the establishment!`,

        homeSlider:[
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fhome%2F12_512x512.jpg?alt=media&token=d8974328-eee1-4f00-89c2-ecc478500cd4",
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fhome%2F21_512x512.jpg?alt=media&token=ab393b84-c9b9-4403-b1a1-52f5ce355389",
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fhome%2F13_512x512.jpg?alt=media&token=e6a7f319-f454-4531-8a97-8e268ee9ebed",
         "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fhome%2F16_512x512.jpg?alt=media&token=069b7a40-a4ba-40bc-b763-c419ba7973a6",
        ],


      homeServices: [
        {
          name:"Haircut",
          //time:"30 minutes",
          //description:"Taper Haircut shave 20.00 full serivce 25.00 regular haircut 15.00 eyebrows 5.00 kids cuts 1-6 10.00",
          price: "$30"
        },
        {
          name:"Haircut & Beard",
          //time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$40"
        },
        {
          name:"Hot Towel Full Shave",
          //time:"60 minutes",
         // description:"Extra 15$-20$",
          price: "$30"
        },
        {
          name:"Beard Only",
          //time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$15"
        },
        {
          name:"Senior +55",
          //time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$25"
        },
        {
          name:"Designs",
          //time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$10+"
        },
        {
          name:"Eyebows",
          //time:"30 minutes",
         // description:"Extra 15$-20$",
          price: "$5+"
        },
      ],
      
      homeSchedules: [
        {
          day: "Monday",
          hour: "10:00 AM - 7:30 PM",
        },
        {
          day: "Tuesday",
          hour: "10:00 AM - 7:30 PM",
        },
        {
          day: "Wednesday",
          hour: "10:00 AM - 7:30 PM",
        },
        {
          day: "Thursday",
          hour: "10:00 AM - 7:30 PM",
        },
        {
          day: "Friday",
          hour: "09:00 am – 08:00 pm",
        },
        {
          day: "Saturday",
          hour: "09:00 am – 08:00 pm",
        },
        {
          day: "Sunday",
          hour: "11:00 am – 05:00 pm",
        },
      ],

      // homeBreaktime: [
      //   {
      //     day: "Monday",
      //     hour: "Close",
      //   },
      //   {
      //     day: "Tuesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Wednesday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Thursday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Friday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Saturday",
      //     hour: "12:00 pm – 01:15 pm",
      //   },
      //   {
      //     day: "Sunday",
      //     hour: "Close",
      //   },
      // ],

      
      homeVideos: [
      "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fhome%2F1.mp4?alt=media&token=54dfa739-c5c8-444d-8cd0-8956f872b11d",
      "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fhome%2F2.mp4?alt=media&token=11a55da0-4910-484f-9c9c-1a8107119886",
      "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fhome%2F3.mp4?alt=media&token=0d1f3850-872b-4890-bc95-fc2dca05a215",
      ],

      phoneNumbers: [
        {
          text: "Phone",
          number: "310 213 1299",
        },
      ],

      gallery:[
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F20_512x512.jpg?alt=media&token=eab6b951-51f3-42ff-86e6-86c872702227",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F5_512x512.jpg?alt=media&token=95a46218-391f-4ab2-b8fd-827e3fce8ad3",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F1_512x512.jpg?alt=media&token=aafcc696-2944-4f20-b329-7f719d558714",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F14_512x512.jpg?alt=media&token=93fc7459-33d0-4f39-bf42-5a1fed3efe9a",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F7_512x512.jpg?alt=media&token=2a4676b5-d149-40a9-ab63-2b7639cf08a9",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F13_512x512.jpg?alt=media&token=be7392b3-af64-4cac-82e6-d28d814d20a6",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F6_512x512.jpg?alt=media&token=f671caa4-0178-46e8-9d84-2c0748947092",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F18_512x512.jpg?alt=media&token=28bb68b5-aebb-4217-a2a9-dc2260ae0cf6",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F3_512x512.jpg?alt=media&token=65944512-641b-47ef-9879-c46e239de3b9",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F10_512x512.jpg?alt=media&token=c86be59e-d92c-4d40-9509-f5bc541235f1",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F8_512x512.jpg?alt=media&token=2123f74c-0456-46f8-936e-9a79be1fa7ec",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F15_512x512.jpg?alt=media&token=24e4cc76-6fc9-4a72-85aa-91f51c0459e7",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F19_512x512.jpg?alt=media&token=9323a478-9c2c-4946-81b0-556545a40805",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F12_512x512.jpg?alt=media&token=6d807256-d35e-4c75-b0b4-8d1114ed6e1b",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F2_512x512.jpg?alt=media&token=eb6aa7f2-4fba-4f32-b8ca-9c1a0eef4274",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F11_512x512.jpg?alt=media&token=bd7a884d-913d-42c6-b634-a8e42912478f",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F22_512x512.jpg?alt=media&token=e6444982-c8d4-4971-ae32-368ebd8c8322",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F16_512x512.jpg?alt=media&token=9f0e6ee6-56df-4acb-af76-8c8ff4235cb8",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F21_512x512.jpg?alt=media&token=c8bb18d2-d646-45a0-a9ef-5e8cfeb5d9bb",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F9_512x512.jpg?alt=media&token=d27b5564-2820-4bd5-b073-63745f45bccd",
        "https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F4_512x512.jpg?alt=media&token=0af2a6e5-0448-4e98-a0ca-d9945b415b80",
        //"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fgallery%2F17_512x512.jpg?alt=media&token=c13aca64-8854-4e79-a9db-a9128a575723",
       
      ],

      socialNetworks: [
        {
          text: "presidentialbarberlounge",
          link: "https://www.instagram.com/presidentialbarberlounge/",
          icon: "logo-instagram",
        },
        {
          text: "Presidential Barber Shop",
          link: "https://shops.getsquire.com/book/presidential-barber-lounge-lynwood/professional",
          icon: "book",
        },
        {
          text: "Presidential Barber Shop",
          link: "https://www.yelp.com/biz/presidential-barber-lounge-lynwood",
          icon: "logo-yahoo",
        },
        {
          text: "Presidential Barber Lounge",
          link: "https://www.facebook.com/presidentialbarberlounge/",
          icon: "logo-facebook",
        },
      ],

      address: "11327 Atlantic Ave Lynwood, CA 90262",
      addressRoute: "11327 Atlantic Ave Lynwood, CA 90262",
      mapSource: "https://maps.google.com/maps?q=Presidential,%2011327%20Atlantic%20Ave%20Lynwood,%20CA%2090262%20&t=&z=15&ie=UTF8&iwloc=&output=embed", 
     
      social1: [
        {
          text: "Appointments",
          url: 'https://shops.getsquire.com/presidential-barber-lounge-lynwood',
          icon: "list",
        }
      ],
    
      barbers:[
        {
          name: "Presidential Barber Shop",
          description: "Boss Barber",
          // socialNetworks:[
          //   {
          //     // text: "straightupbarbers",
          //     // link: "https://www.instagram.com/straightupbarbers/",
          //     // icon: "logo-instagram",
          //   },
          // ],
          image:"https://firebasestorage.googleapis.com/v0/b/slamart.appspot.com/o/appoiments%2FPresidential%20Barber%20Lounge%2Fappointments%2FLogo_512x512.png?alt=media&token=9c325e46-fc25-4680-9ff4-127f936f3a97",
        },
      ]
    },

    appointmentsDays:[
      {
        day: 1,
        hours: this.hours,
        closed: false
      },
      {
        day: 2,
        hours: this.hours,
        closed: false
      },
      {
        day: 3,
        hours: this.hours,
        closed: false
      },
      {
        day: 4,
        hours: this.hours,
        closed: false
      },
      {
        day: 5,
        hours: this.hours,
        closed: false
      },
      {
        day: 6,
        hours: this.hours,
        closed: false
      },
      {
        day: 0,
        hours: this.hours,
        closed: false
      },
    ],

  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
     //   this.firebaseService.updateAppData(this.appData).then(t => {console.log(this.appData)}); 
  }


  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  getAppData() {
     return this.firebaseService.getAppData().then(appData => {
        if(appData) {
          this.appData = appData as any;
          console.log(appData);
        }
      });
  }
}
