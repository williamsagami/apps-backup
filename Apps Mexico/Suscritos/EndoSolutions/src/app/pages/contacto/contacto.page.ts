import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.page.html',
  styleUrls: ['./contacto.page.scss'],
})
export class ContactoPage implements OnInit {

  t: any;
  d: any;

  constructor(private data: DataService, private ds: DomSanitizer) { 
    this.t = data.appDataLocal.translate;
    this.d = data.appDataLocal.data;
  }

  ngOnInit() {
    this.data.getAppData().then(cloudTranslation => {
      this.t = this.data.appDataLocal.translate;
    });
  }

  getMapUrl() {
    return this.ds.bypassSecurityTrustResourceUrl(this.d.mapSource);
  }

}
