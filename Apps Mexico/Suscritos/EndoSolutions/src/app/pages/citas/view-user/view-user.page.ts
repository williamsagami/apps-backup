import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController, NavParams, LoadingController } from '@ionic/angular';
import { FirebaseService } from '../../../services/firebase.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.page.html',
  styleUrls: ['./view-user.page.scss'],
})
export class ViewUserPage implements OnInit {

  t: any;

  userData: any;
  allowRequests = true;
  rating = 0;

  private loading: any;

  constructor(
    private modalController: ModalController, 
    private toastController: ToastController, 
    private navParams: NavParams, 
    private loadingController: LoadingController,
    private firebaseService: FirebaseService,
    private callNumber: CallNumber,
    private data: DataService) { 

      this.t = data.appDataLocal.translate;

      this.userData = navParams.get('userData');
      this.allowRequests = this.userData.allowRequests;
      this.rating = this.userData.rating;
    }

  ngOnInit() {
  }

  closeView(){
	  this.modalController.dismiss();
  }

  async showInfo(info: string) {
    const toast = await this.toastController.create({
      message: info,
      duration: 6000,
      color: 'secondary',
    });
    toast.present();
  }

  saveUserChanges() {
    this.loadingController.create({
      message: this.t.al_Guardando[this.t.currentTranslation]+'...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      this.userData.allowRequests = this.allowRequests;
      this.userData.rating = this.rating;
      this.firebaseService.updateUser(this.userData).then(showInfo => {
        this.loading.dismiss();
        this.showInfo(this.t.msg_CambiosGuardados[this.t.currentTranslation]);
        this.closeView();
      });
    });
  }

  getAdminText() {
    return this.userData.admin ? this.t.txt_Administrador[this.t.currentTranslation] : this.t.txt_Cliente[this.t.currentTranslation];
  }

  setRating(event) {
    this.rating = event.detail.value;
  }

  callUser() {
	  this.callNumber.callNumber(this.userData.phone, false)
	  .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

}
