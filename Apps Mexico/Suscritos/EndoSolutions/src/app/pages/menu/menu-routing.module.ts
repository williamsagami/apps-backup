import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';

import { MenuPage } from './menu.page';
import { AuthGuard } from '../../guards/auth.guard';
import { LoginGuard } from '../../guards/login.guard';

const routes: Routes = [
  {
    path: 'menu',
    component: MenuPage,
    children: [
      {
        path: 'inicio',
        loadChildren: () => import('../inicio/inicio.module').then( m => m.InicioPageModule)
      },
      {
        path: 'citas',
        loadChildren: () => import('../citas/citas.module').then( m => m.CitasPageModule),
        canActivate: [AuthGuard],
      },
      {
        path: 'galeria',
        loadChildren: () => import('../galeria/galeria.module').then( m => m.GaleriaPageModule)
      },
      {
        path: 'contacto',
        loadChildren: () => import('../contacto/contacto.module').then( m => m.ContactoPageModule)
      },
      {
        path: 'login',
        loadChildren: () => import('../login/login.module').then( m => m.LoginPageModule),
        canActivate: [LoginGuard],
      },
      {
        path: 'cuenta',
        loadChildren: () => import('../cuenta/cuenta.module').then( m => m.CuentaPageModule),
        canActivate: [AuthGuard],
      },
    ]
  },
  {
    path: '',
    redirectTo: '/menu/inicio',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
