import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from '../../services/firebase.service';
import { ModalController } from '@ionic/angular';
import { UpdatePostsPage } from './update-posts/update-posts.page';
import { post} from '../../services/firebase.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  t: any; //Translation
  d: any; //App Data
  posts = [];

  postsReady = false;
  userData: any;
  adminPosts: any;

  private postsRef: any;
  private adminPostsRef: any;

  slider = [
    '../assets/images/13.jpg',
    '../assets/images/14.jpg',
    '../assets/images/15.jpg',
    '../assets/images/16.jpg',
  ];

  slideOfertas = {
    autoplay: {
      delay: 4000,
    }
  };

  slideOpts = {
    loop: true,
    autoplay: {
      delay: 2000,
    }
  };

  horarios = [
    {
      dia: "Lunes",
      hora: "09:00 AM - :00 PM",
    },
    {
      dia: "Martes",
      hora: "10:00 AM - 07:00 PM",
    },
    {
      dia: "Miercoles",
      hora: "10:00 AM - 07:00 PM",
    },
    {
      dia: "Jueves",
      hora: "10:00 AM - 07:00 PM",
    },
    {
      dia: "Viernes",
      hora: "10:00 AM - 08:00 PM",
    },
    {
      dia: "Sabado",
      hora: "10:00 AM - 08:00 PM",
    },
    {
      dia: "Domingo",
      hora: "Cerrado",
    },
  ];

  constructor(
    public data: DataService, 
    private firebaseService: FirebaseService, 
    private modalController: ModalController,) {
    this.t = data.appDataLocal.translate;
    this.d = data.appDataLocal.data;
  }

  ngOnInit() {
    //Obtener Traduccion Dinamica
    this.data.getAppData().then(cloudTranslation => {
      this.t = this.data.appDataLocal.translate;
    });

    //Obtener Posts
    /*this.firebaseService.getPosts().then(posts => {
      if(posts) this.posts = posts as any;
    }).then(setRealTimeValues => {
      //Establecer variables a Real Time despues del Setup
      this.firebaseService.getPostsRealTime().subscribe(posts => {
        if(posts) this.posts = posts;
      });

      this.firebaseService.getPostDataRealTime().subscribe(adminPosts => {
        this.adminPosts = adminPosts;
      });
    });*/

    //Obtener Posts
    if(this.postsRef) this.postsRef.unsubscribe();
    this.postsRef = this.firebaseService.getPostsRealTime().subscribe(posts => {
      if(posts) this.posts = posts;
      this.postsReady = true;
    });

    if(this.adminPostsRef) this.adminPostsRef.unsubscribe();
    this.adminPostsRef = this.firebaseService.getPostDataRealTime().subscribe(adminPosts => {
      this.adminPosts = adminPosts;
    });

    //Datos de Usuario
    /*this.firebaseService.fbAuth.authState.subscribe(user => {
      this.user = user;
      //console.log(user);
      if(this.user) {
        this.firebaseService.getUser(this.user.uid).then(userData => {
          this.userData = userData as any;
          //console.log(this.userData);
  
          //Obtener Datos de Posts
          this.firebaseService.getPostData().then(adminPosts => {
            this.adminPosts = adminPosts as any;
            this.postsReady = true;
            //console.log(this.posts);
          });
        });
      }
      else {
        this.postsReady = true;
        this.userData = null;
        this.adminPosts = null;
      }
    });*/
  }

  showPostEdit() {
    if(this.firebaseService.userData) return this.firebaseService.userData.admin; 
    else return false;
  }

  showPostCreate() {
    if(this.adminPosts) {
      return (this.showPostEdit() && this.adminPosts.postNumber < this.adminPosts.maxPostNumber);
    } 
    else return false;
  }

  isClosed(day: any) {
    if(day.hora === "Cerrado") return this.t.txt_Cerrado[this.t.currentTranslation];
    else return day.hora;
  }

  async viewPost(post: post) {
    const va = await this.modalController.create({
      component: UpdatePostsPage,
      componentProps : {
        post: post,
        create: false,
        adminPosts: this.adminPosts,
      }
    });
    await va.present();
  }

  async createPost() {
    const va = await this.modalController.create({
      component: UpdatePostsPage,
      componentProps : {
        post: null,
        create: true,
        adminPosts: this.adminPosts,
      }
    });
    await va.present();
  }

}
