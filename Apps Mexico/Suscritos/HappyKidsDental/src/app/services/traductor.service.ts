import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'Happy Kids TJ Dental',
      en: 'Happy Kids TJ Dental'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Odontologia Cosmetica',
    'Implantes',
  ];
  video = '';

  translateIndex = [
    {
      es:`Brindando Atención Odontológica especializada en niños y adolescentes por más de 17 años. \n
      Todos los pacientes que visitan nuestro consultorio son tratados con mucha calidez y paciencia en todo momento porqué queremos que sea un agradable experiencia. \n
      Uno de nuestros principales objetivos es brindar una salud dental con una sonrisa encantadora. \n
      Para saber más acerca de nuestros tratamientos y técnicas contáctanos por teléfono o redes sociales será un gusto ayudarte. \n`,
      en:`Providing Dental Care specialized in children and adolescents for more than 17 years. \n
      All patients who visit our office are treated with great warmth and patience at all times because we want it to be a pleasant experience. \n
      One of our main goals is to provide dental health with a charming smile. \n
      To know more about our treatments and techniques, contact us by phone or social networks, we will be happy to help you. \n`
    },
    {
      es: 'Procedimientos:',
      en: 'Procedures:'
    },
    {
      es: 'Clientes Satisfechos',
      en: 'Satisfied Customers'
    },
  ];

  translateProcedures = [
    {
      es: 'Odontologia Cosmetica',
      en: 'Cosmetic Odonthology'
    },
    {
      es: 'Implantes',
      en: 'Implants'
    },
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = false;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });
      this.video = this.translateIndex[2].es;

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });
      this.video = this.translateIndex[2].en;

      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
