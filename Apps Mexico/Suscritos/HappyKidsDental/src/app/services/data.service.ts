import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/call/call.page';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  //Nomenclatura
  /*
  t_ = Titulo
  txt_ = Texto
  b_ = Boton
  msg_ = Mensaje
  al_ = Alerta
  alb_ = Boton de Alerta
  */

  //Datos de Aplicacion locales utilizados para mostrar texto en el app
  appDataLocal = {
    translate: { 
      //General
      currentTranslation: this.firebaseService.defaultLanguage,
      translationNumber: 2,
      appTitle: "Happy Kids TJ Dental",
      al_Cargando: [
        "Cargando", 
        "Loading"
      ],
      al_Guardando: [
        "Guardando",
        "Saving"
      ],
      alb_Cancelar: [
        "Cancelar", 
        "Cancel"
      ],
      alb_Seleccionar: [
        "Seleccionar", 
        "Select"
      ],
      txt_Indefinido: [
        "Indefinido", 
        "Undefined"
      ],
  
      //Menu
      b_Menu: [
        ["INICIO", "HOME"],
        ["CITAS", "APPOINTMENTS"],
        ["GALERIA", "GALLERY"],
        ["CONTACTO", "CONTACT"],
      ],
      b_Lenguaje: [
        "ES", 
        "EN"
      ],
      b_Cuenta: [
        "Cuenta", 
        "Account"
      ],
      b_Suscribirse: [
        "Suscribirse",
        "Subscribe"
      ],
  
      //Login
      t_IniciarSesion: [
        "INICIAR SESION", 
        "LOGIN"
      ],
      txt_Acceder: [
        "Acceder", 
        "Login"
      ],
      txt_Registarse: [
        "Registrarse", 
        "Register"
      ],
      txt_Correo: [
        "Correo",
        "Email"
      ],
      txt_Contrasena: [
        "Contraseña", 
        "Password"
      ],
      txt_RContrasena: [
        "Repetir Contraseña", 
        "Repeat Password"
      ],
      b_Entrar: [
        "Entrar", 
        "Login"
      ],
      b_Registrarse: [
        "Registrarse", 
        "Register"
      ],
      msg_Campos: [
        "Debes llenar todos los campos.", 
        "You must fill all the fields."
      ],
      msg_Correo: [
        "Correo inválido.", 
        "Invalid email."
      ],
      msg_Usuario: [
        "Este correo no esta registrado.", 
        "This email is not registered."
      ],
      msg_Contrasena: [
        "Contraseña incorrecta.", 
        "Incorrect password."
      ],
      msg_ContrasenaChica: [
        "La contraseña debe tener al menos 6 caracteres.", 
        "The password must be at least 6 characters."
      ],
      msg_ContrasenaDiferente: [
        "Las contraseñas no son iguales.", 
        "Passwords are not the same."
      ],
      msg_UsuarioExiste: [
        "Este email ya está en uso.", 
        "This email is already in use."
      ],
      msg_IniciarSesion: [
        `Para agendar citas o ver tu cuenta necesitas iniciar sesion o registrarte.`,
        `To schedule appointments or view your account you need to log in or register.`
      ],
      al_CerrandoSesion: [
        "Cerrando Sesion",
        "Loging Out"
      ],
  
  
      //Inicio
      t_Inicio: [
        "INICIO", 
        "HOME"
      ],
      txt_Descripcion: [
        `Brindando Atención Odontológica especializada en niños y adolescentes por más de 17 años. <br>
        Todos los pacientes que visitan nuestro consultorio son tratados con mucha calidez y paciencia en todo momento porqué queremos que sea un agradable experiencia. <br>
        Uno de nuestros principales objetivos es brindar una salud dental con una sonrisa encantadora. <br>
        Para saber más acerca de nuestros tratamientos y técnicas contáctanos por teléfono o redes sociales será un gusto ayudarte.`,

        `Providing Dental Care specialized in children and adolescents for more than 17 years. <br>
        All patients who visit our office are treated with great warmth and patience at all times because we want it to be a pleasant experience. <br>
        One of our main goals is to provide dental health with a charming smile. <br>
        To know more about our treatments and techniques, contact us by phone or social networks, we will be happy to help you.`
      ],
      t_Servicios: [
        "Servicios", 
        "Services"
      ],
      txt_Servicios: [
        ["Implantes", "Implants"],
        ["Ortodoncia", "Orthodontics"],
        ["Odontologia General", "General Odontology"],
        ["Endodoncia", "Endodontics"],
        ["Cirugia Terceros Molares", "Third Molar Surgery"],
      ],
      txt_Horario: [
        "Clientes Satisfechos",
        "Satisfied customers"
      ],
      txt_HorarioDias: [
        [ "Lunes", "Monday" ],
        [ "Martes", "Tuesday" ],
        [ "Miércoles", "Wednesday" ],
        [ "Jueves", "Thursday" ],
        [ "Viernes", "Friday" ],
        [ "Sabado", "Saturday" ],
        [ "Domingo", "Sunday" ],
      ],
      txt_Cerrado: [
        "Cerrado",
        "Closed"
      ],

      //Blog
      t_Publicar: [
        "PUBLICAR",
        "POST"
      ],
      txt_EditarPublicacion: [
        "Editar Publicacion",
        "Edit Post"
      ],
      txt_TituloPublicacion: [
        "Titulo de Publicacion",
        "Post Title"
      ],
      txt_EscribeTitulo: [
        "Escribe Titulo",
        "Write Title"
      ],
      txt_ContenidoPublicacion: [
        "Contenido de Publicacion",
        "Post Content"
      ],
      txt_EscribeContenido: [
        "Escribe Contenido",
        "Write Content"
      ],
      txt_PorcentajeOferta: [
        "Porcentaje de Oferta",
        "Sale Percentage"
      ],
      txt_EscribeOferta: [
        "Escribe Oferta de 1 a 100",
        "Offer Percentage from 1 to 100"
      ],
      b_CrearPublicacion: [
        "Crear Publicacion",
        "Create Post"
      ],
      b_Publicar: [
        "Publicar",
        "Post"
      ],
      al_BorrarPublicacion: [
        "Borrar Publicacion",
        "Delete Post"
      ],
      al_BorrarPublicacionSeguro: [
        "Estas seguro que deseas Borrar esta Publicacion?",
        "Are you sure you want to Delete this Post?"
      ],
      msg_PublicacionCreada: [
        "Publicación Creada Correctamente.",
        "Post Created Successfully."
      ],
      msg_PublicacionActualizada: [
        "Publicación ha sido Actualizada.",
        "Post has been Updated."
      ],
      msg_PublicacionBorrada: [
        "La Publicación ha sido Borrada.",
        "Post has been Deleted."
      ],

      //Galeria
      t_Galeria: [
        "GALERIA", 
        "GALLERY"
      ],
  
      //Contacto
      t_Contacto: [
        "CONTACTO", 
        "CONTACT"
      ],
      txt_Llamar: [
        "LLAMAR",
        "CALL"
      ],
      txt_Telefonos: [
        "Telefonos", 
        "Phones"
      ],
      txt_Direccion: [
        "Direccion", 
        "Address"
      ],
      txt_Ubicacion: [
        "Ubicacion", 
        "Location"
      ],
      txt_RedesSociales: [
        "Redes Sociales", 
        "Social Networks"
      ],
  
      //Citas
      t_Citas: [
        "CITAS", 
        "APPOINTMENTS"
      ],
      txt_SolicitarCita: [
        "Solicitar Cita", 
        "Request Appointments"
      ],
      b_CrearSolicitud: [
        "Crear Solicitud", 
        "Create Request"
      ],
      txt_Solicitudes: [
        "Solicitudes", 
        "Requests"
      ],
      txt_Pendientes: [
        "Pendientes", 
        "Pending"
      ],
      txt_Aceptadas: [
        "Aceptadas", 
        "Accepted"
      ],
      txt_Rechazadas: [
        "Rechazadas", 
        "Rejected"
      ],
      txt_SolicitudesEnviadas: [
        "Solicitudes Enviadas", 
        "Requests Sent"
      ],
      txt_SolicitudesVacias: [
        "No has enviado ninguna solicitud",
        "You have not sent any request"
      ],
      txt_PendientesVacias: [
        "No hay solicitudes pendientes",
        "No pending requests"
      ],
      txt_AceptadasVacias: [
        "No hay solicitudes aceptadas",
        "No accepted requests"
      ],
      txt_RechazadasVacias: [
        "No hay solicitudes rechazadas",
        "No rejected requests"
      ],
  
      //Solicitud
      txt_CitaPendiente: [
        "Pendiente", 
        "Pending"
      ],
      txt_CitaAceptada: [
        "Aceptada", 
        "Accepted"
      ],
      txt_CitaRechazada: [
        "Rechazada", 
        "Rejected"
      ],
      txt_Fecha: [
        "Fecha", 
        "Date"
      ],
      txt_Semana: [
        ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
        ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      ],
      txt_Mes: [
        ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      ],
      txt_HorarioFlexible: [
        "Horario Flexible", 
        "Flextime"
      ],
      txt_Opcional: [
        "Opcional", 
        "Optional"
      ],
      txt_RangoTiempo: [
        "Rango de Tiempo", 
        "Time Range"
      ],
      txt_Servicio: [
        "Servicio", 
        "Service"
      ],
  
      //Crear Cita
      t_SolicitarCita: [
        "SOLICITAR CITA", 
        "REQUEST APPOINTMENT"
      ],
      txt_CrearSolicitud: [
        "Crear Solicitud", 
        "Create Request"
      ],
      txt_SeleccionarDia: [
        "Seleccionar Dia", 
        "Choose Day"
      ],
      txt_SeleccionarHora: [
        "Seleccionar Hora", 
        "Choose Hour"
      ],
      msg_FechaAnterior: [
        "No puedes asignar una fecha anterior a la actual.", 
        "You cannot assign a date before the current one."
      ],
      msg_RangoTiempo: [
        "Elige el rango antes y despues de la hora elegida que estes disponible.", 
        "Choose the range before and after the chosen time that you are available."
      ],
      msg_Servicio: [
        "Indica el servicio que deseas para agendarte fácilmente.", 
        "Indicate the service you want to easily schedule your appointment."
      ],
      al_NombreUsuario: [
        "nombre de usuario", 
        "username"
      ],
      al_NumeroTelefono: [
        "numero de telefono", 
        "phone number"
      ],
      al_YNumeroTelefono: [
        "y numero de telefono", 
        "and phone number"
      ],
      al_DatosUsuario: [
        "Datos de Usuario", 
        "User Data"
      ],
      al_Recomendamos: [
        "Recomendamos que cambies tu", 
        "We recommend that you change your"
      ],
      al_Agendarte: [
        "en la pantalla de Cuenta para agendarte de manera mas rapida.", 
        "on the Account screen to schedule you more quickly."
      ],
      al_CreandoSolicitud: [
        "Creando Solicitud",
        "Creating Request"
      ],
      alb_Crear: [
        "Crear", 
        "Create"
      ],
  
      //Modificar Cita
      t_ModificarCita: [
        "MODIFICAR CITA", 
        "MODIFY APPOINTMENT"
      ],
      txt_ActualizarSolicitud: [
        "Actualizar Cita", 
        "Update Request"
      ],
      al_RechazarSolicitud: [
        "Rechazar Solicitud", 
        "Reject Request"
      ],
      al_RechazarSeguro: [
        "Seguro que deseas rechazar esta solicitud?", 
        "Are you sure you want to reject this request?"
      ],
      alb_Rechazar: [
        "Rechazar", 
        "Reject"
      ],
      al_BorrarSolicitud: [
        "Borrar Solicitud", 
        "Delete Request"
      ],
      al_BorrarSeguro: [
        "Seguro que deseas borrar esta solicitud?", 
        "Are you sure you want to delete this request?"
      ],
      alb_Borrar: [
        "Borrar", 
        "Delete"
      ],
      b_Borrar: [
        "Borrar", 
        "Delete"
      ],
      al_BorrandoSolicitud: [
        "Borrando Solicitud",
        "Deleting Request"
      ],
      b_Actualizar: [
        "Actualizar", 
        "Update"
      ],
      al_ActualizandoSolicitud: [
        "Actualizando Solicitud",
        "Updating Request"
      ],
  
      //Modificar Usuario / Cuenta
      t_ModificarUsuario: [
        "MODIFICAR USUARIO", 
        "MODIFY USER"
      ],
      t_Cuenta: [
        "CUENTA",
        "ACCOUNT"
      ],
      txt_DatosCuenta: [
        "Datos de Cuenta", 
        "Account Data"
      ],
      txt_Rol: [
        "Rol", 
        "Role"
      ],
      txt_Administrador: [
        "Administrador",
        "Administrator"
      ],
      txt_Cliente: [
        "Usuario",
        "User"
      ],
      txt_Nivel: [
        "Nivel", 
        "Level"
      ],
      txt_Notificaciones: [
        "Notificaciones", 
        "Notifications"
      ],
      txt_Prestigio: [
        "Prestigio", 
        "Ranking"
      ],
      txt_Cancelaciones: [
        "Cancelaciones", 
        "Cancellations"
      ],
      txt_SolicitudesActivas: [
        "Solicitudes Activas", 
        "Active Requests"
      ],
      txt_SolicitudesCreadas: [
        "Solicitudes Creadas", 
        "Requests Created"
      ],
      txt_PermitirSolicitudes: [
        "Permitir Solicitudes", 
        "Allow Requests"
      ],
      b_CerrarSesion: [
        "Cerrar Sesion", 
        "Log Out"
      ],
      txt_OpcionesAplicacion: [
        "Opciones de Aplicacion", 
        "Application Options"
      ],
      txt_LimiteSolicitudes: [
        "Límite de Solicitudes por Usuario", 
        "Limit of Requests per User"
      ],
      txt_RangoDiasSolicitudes: [
        "Rango de Días de Creación de Solicitudes", 
        "Range of Request Creation Days"
      ],
      b_GuardarCambios: [
        "Guardar Cambios", 
        "Save Changes"
      ],
      msg_CambiosGuardados: [
        "Los cambios se han guardado correctamente.",
        "The changes have been saved successfully."
      ],
      msg_NotificacionesDesactivadas: [
        "Notificaciones Desactivadas.",
        "Notifications Off."
      ],
      msg_NotificacionesActivadas: [
        "Notificaciones Activadas.",
        "Notifications On."
      ],
    },

    data: {
      phoneNumbers: [
        {
          text: "Telefono",
          number: "200 2458",
        },
        {
          text: "Telefono",
          number: "619 407 7929",
        }
      ],

      socialNetworks: [
        {
          text: "Happy Kids Dental",
          link: "https://www.facebook.com/profile.php?id=100009487316760",
          icon: "logo-facebook",
        },
        {
          text: "Happy Kids Dental",
          link: "https://www.instagram.com/happykidsental1106/",
          icon: "logo-instagram",
        }
      ],

      address: `Av. Negrete #1841-17, Entre 10 y 11, Plaza Negrete, Zona Centro, Tijuana, B.C.`,

      mapSource: "https://maps.google.com/maps?q=Av.%20Miguel%20Negrete%201841%2C%20Zona%20Centro%2C%2022000%20Tijuana%2C%20B.C.%2C%20Happy%20Kids%20TJ%20Dental&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  //Datos de Aplicacion--------------------------------------------------------------
  //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
  appData = { 

    //Todas las traducciones que posiblemente cambien
    translate: {
      appTitle: "Happy Kids TJ Dental",
      //Contenido de Inicio
      txt_Descripcion: [
        `Brindando Atención Odontológica especializada en niños y adolescentes por más de 17 años. <br>
        Todos los pacientes que visitan nuestro consultorio son tratados con mucha calidez y paciencia en todo momento porqué queremos que sea un agradable experiencia. <br>
        Uno de nuestros principales objetivos es brindar una salud dental con una sonrisa encantadora. <br>
        Para saber más acerca de nuestros tratamientos y técnicas contáctanos por teléfono o redes sociales será un gusto ayudarte.`,
        `Providing Dental Care specialized in children and adolescents for more than 17 years. <br>
        All patients who visit our office are treated with great warmth and patience at all times because we want it to be a pleasant experience. <br>
        One of our main goals is to provide dental health with a charming smile. <br>
        To know more about our treatments and techniques, contact us by phone or social networks, we will be happy to help you.`
      ],
     
      txt_ServiciosContenidoES: 
      ["Implantes", "Ortodoncia", "Odontologia General","Endodoncia", "Cirugia Terceros Molares"],
      txt_ServiciosContenidoEN: 
      ["Implants", "Orthodontics", "General Odontology","Endodontics","Third Molar Surgery"],
    },

    //Datos de Contacto
    data: {
      phoneNumbers: [
        {
          text: "Telefono",
          number: "200 2458",
        },
        {
          text: "Telefono",
          number: "619 407 7929",
        }
      ],

      socialNetworks: [
        {
          text: "Happy Kids Dental",
          link: "https://www.facebook.com/profile.php?id=100009487316760",
          icon: "logo-facebook",
        },   
        {
          text: "Happy Kids Dental",
          link: "https://www.instagram.com/happykidsental1106/",
          icon: "logo-instagram",
        }
      ],

      address: `Av. Negrete #1841-17, Entre 10 y 11, Plaza Negrete, Zona Centro, Tijuana, B.C.`,

      mapSource: "https://maps.google.com/maps?q=Av.%20Miguel%20Negrete%201841%2C%20Zona%20Centro%2C%2022000%20Tijuana%2C%20B.C.%2C%20Happy%20Kids%20TJ%20Dental&t=&z=15&ie=UTF8&iwloc=&output=embed",
    }
  }

  constructor(private firebaseService: FirebaseService, private modalController: ModalController, private iab : InAppBrowser) { 

    // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS

      //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
  }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber,
        data: this.appDataLocal.translate,
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  changeLanguage() {
    this.appDataLocal.translate.currentTranslation++;
    if(this.appDataLocal.translate.currentTranslation >= this.appDataLocal.translate.translationNumber)
    {
      this.appDataLocal.translate.currentTranslation = 0;
    }

    if(this.firebaseService.userData) {
      var coolUser = this.firebaseService.userData;
      coolUser.language = this.appDataLocal.translate.currentTranslation;
      this.firebaseService.updateUser(coolUser);
    }
  }

  setLanguage(language: number) {
    if(language >= 0 && language < this.appDataLocal.translate.translationNumber) { this.appDataLocal.translate.currentTranslation = language }
    else this.appDataLocal.translate.currentTranslation = this.firebaseService.defaultLanguage;
  }

  getServices() {
    var servicios = [];
    this.appDataLocal.translate.txt_Servicios.forEach((service, i) => {
      var servicio = [this.appData.translate.txt_ServiciosContenidoES[i], this.appData.translate.txt_ServiciosContenidoEN[i]]
      servicios.push(servicio);
    });
    return servicios;
  }

  getAppData() {
    return this.firebaseService.getAppData().then(appData => {
      if(appData) {
        this.appData = appData as any;
        this.appDataLocal.translate.appTitle = this.appData.translate.appTitle;
        this.appDataLocal.translate.txt_Descripcion = this.appData.translate.txt_Descripcion;
        this.appDataLocal.translate.txt_Servicios = this.getServices();
        this.appDataLocal.data = this.appData.data;
        //console.log(appData);
      }
    });
  }
}
