import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService, language } from '../../services/firebase.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  t: any;
  userReady = false;
  user: any;

  selectedUrl = '/menu/inicio';

  pages = [
    {
      url: '/menu/inicio',
      icon: "home",
    },
    {
      url: '/menu/citas',
      icon: "list",
    },
    {
      url: '/menu/galeria',
      icon: "images",
    },
    {
      url: '/menu/contacto',
      icon: "person",
    }
  ]; 

  constructor(private router: Router, public data: DataService, private firebaseService: FirebaseService, private toastController: ToastController) { 
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedUrl = event.url != null ? event.url : this.selectedUrl;
    }); 

    this.t = data.appDataLocal.translate;
    // this.firebaseService.fbAuth.authState.subscribe(user => {
    //   this.user = user;
    //   this.userReady = true;
    // });
  }

  ngOnInit() {
    this.firebaseService.userSetup().then(userDataExist => {
      this.userReady = userDataExist as any;
      this.data.setLanguage(this.firebaseService.userData.language);
    });
  }

  async showInfo(info: string) {
    const toast = await this.toastController.create({
      message: info,
      duration: 6000,
      color: 'secondary',
    });
    toast.present();
  }

  showSubscriptionButton() {
    if(this.userReady) {
      if(this.firebaseService.userData) {
        if(!this.firebaseService.userData.admin) return !this.firebaseService.userData.notifications;
        else return false;
      } 
    }
    else return false;
  }

  toggleSalesNotifications() {
    if(this.userReady) {
      if(!this.firebaseService.userData.notifications) {
        var userData = this.firebaseService.userData;
        userData.notifications = true;
        this.firebaseService.updateUser(userData).then(updateUser => { 
          this.firebaseService.fcm.subscribe(this.firebaseService.getDataBaseName()+"-sales").then(showInfo => {
            this.showInfo(this.t.msg_NotificacionesActivadas[this.t.currentTranslation]);
          });
        });
      }
    }
    else {
      this.router.navigate(['/menu/login']);
    }
  }

}
