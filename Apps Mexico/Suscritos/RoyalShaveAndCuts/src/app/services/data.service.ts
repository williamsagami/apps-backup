import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  //slider
  sliderImages = [
    '../assets/images/1s.jpg',
    '../assets/images/2s.jpg',
    '../assets/images/3s.jpg',
    '../assets/images/4s.jpg',
  ]

  //galeria
  images = [
    'assets/images/1.jpg',
    //'assets/images/2.jpg',
    'assets/images/3.jpg',
    //'assets/images/4.jpg',
    'assets/images/5.jpg',
    //'assets/images/6.jpg',
    //'assets/images/7.jpg',
    'assets/images/8.jpg',
    'assets/images/9.jpg',
    'assets/images/10.jpg',
    'assets/images/11.jpg',
    'assets/images/12.jpg',
    'assets/images/13.jpg',
    'assets/images/14.jpg',
    'assets/images/15.jpg',
    'assets/images/16.jpg',
    'assets/images/17.jpg',
    'assets/images/18.jpg',
    'assets/images/19.jpg',
    'assets/images/20.jpg',
    'assets/images/21.jpg',
    'assets/images/22.jpg',
    'assets/images/23.jpg',
    'assets/images/24.jpg',
    'assets/images/25.jpg',
    'assets/images/26.jpg',
  ]

  //contacto
  phones = [
    {
      number: '(664) 518 3291', 
      call: '6645183291'
    },
  ];

  direction = 'Felipe #8250, Carrillo Puerto y o Tercera, Zona Centro, 22000 Tijuana, B.C.';

  constructor() { }
}
