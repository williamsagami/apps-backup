import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DataService } from 'src/app/services/data.service';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.page.html',
  styleUrls: ['./contacto.page.scss'],
})
export class ContactoPage implements OnInit {

  t: any;
  d: any;

  constructor(
    public data: DataService, 
    private ds: DomSanitizer, 
    private launchNavigator: LaunchNavigator)  { 
    this.t = data.appDataLocal.translate;
    this.d = data.appDataLocal.data;
  }

  ngOnInit() {
    this.data.getAppData().then(cloudTranslation => {
      this.t = this.data.appDataLocal.translate;
    });
  }

  getMapUrl() {
    return this.ds.bypassSecurityTrustResourceUrl(this.d.mapSource);
  }

  openRoute() {
    this.launchNavigator.isAppAvailable( this.launchNavigator.APP.GOOGLE_MAPS).then(isAvailable =>{
      var app;
      if(isAvailable){
          app =  this.launchNavigator.APP.GOOGLE_MAPS;
      }else{
          console.warn("Google Maps not available - falling back to user selection");
          app =  this.launchNavigator.APP.USER_SELECT;
      }
      this.launchNavigator.navigate(this.d.addressRoute, {
          app: app
      });
    });
  }

}
