import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController, LoadingController } from '@ionic/angular';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  selectedTab = "login";
  email: string;
  password: string;
  rpassword: string;
  showPassword = false;
  private loading: any;

  constructor(
    private firebaseService: FirebaseService, 
    private router: Router, private toastController: ToastController,
    private loadingController: LoadingController) {
    this.showInfo("To schedule appointments or view your account you need to login or register.");
  }

  ngOnInit() {
  }

  togglePassword() {
    this.showPassword = !this.showPassword;
  }

  login() {
    this.loadingController.create({
      message: 'Loading...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      this.firebaseService.login(this.email, this.password).then (res => {
        this.loading.dismiss();
        this.router.navigate(['/menu/inicio']);
      }).catch(err => { 
        switch(err.code)
        {
          case 'auth/argument-error': { this.showInfo("You must fill all the fields."); } break;
          case 'auth/invalid-email': { this.showInfo("Invalid email."); } break;
          case 'auth/user-not-found': { this.showInfo("This email is not registered."); } break;
          case 'auth/wrong-password': { this.showInfo("Incorrect password."); } break;
        }
        
        this.password = null;
        this.loading.dismiss();
      });
    });
  }

  register() {
    this.loadingController.create({
      message: 'Loading...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      if(this.password === this.rpassword) {
        this.firebaseService.register(this.email, this.password).then (res => {
          this.loading.dismiss();
          this.router.navigate(['/menu/inicio']);
        }).catch(err => { console.log(err.code) 
          switch(err.code)
          {
            case 'auth/argument-error': { this.showInfo("You must fill all the fields."); } break;
            case 'auth/invalid-email': { this.showInfo("Invalid email."); } break;
            case 'auth/weak-password': { this.showInfo("The password must be at least 6 characters."); } break;
            case 'auth/email-already-in-use': { this.showInfo("This email is already in use."); } break;
          }

          this.password = null;
          this.rpassword = null;
          this.loading.dismiss();
        });
      }
      else {
        this.showInfo("Passwords are not the same.");
        this.password = null;
        this.rpassword = null;
        this.loading.dismiss();
      }
    });
  }

  async showInfo(info: string) {
    const toast = await this.toastController.create({
      message: info,
      duration: 6000,
      color: 'secondary',
    });
    toast.present();
  }
}

 