import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LlamadaPage } from './llamada.page';

describe('LlamadaPage', () => {
  let component: LlamadaPage;
  let fixture: ComponentFixture<LlamadaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LlamadaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LlamadaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
