import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  menu = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: "Morena's Taqueria",
      en: "Morena's Taqueria"
    },
    {
      es: 'INICIO',
      en: 'HOME'
    },
    {
      es: 'GALERIA',
      en: 'GALLERY'
    },
    {
      es: 'MENU',
      en: 'MENU'
    },
    {
      es: 'CONTACTO',
      en: 'CONTACT'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Resina',
    'Implantes',
    'Endodoncia',
    'Ortodoncia',
    'Puentes Fijos',
    'Blanqueamiento',
    'Placas Totales',
    'Limpieza Dental',
    'Placas Parciales',
  ];
  video = '';

  translateIndex = [
    {
      es:`Los invitamos a comer comida tipica mexicana con tortillas hechas a mano!\n
      Todos nuestros platillos estan hechos con ingredientes frescos, cocinados con mucha pasion y cariño de parte de nuestro equipo.\n
      Llamanos ahora para hacer un pedido o visitanos para obtener un servicio increible!`,
      en:`We invite you to eat typical Mexican food with handmade tortillas!\n
      All our dishes are made with fresh ingredients, cooked with a lot of passion and care from our team.\n
      Call us now to place an order or visit us for incredible service!`
    },
    {
      es: 'Procedimientos:',
      en: 'Procedures:'
    },
    {
      es: 'Videos',
      en: 'Videos'
    },
  ];

  translateProcedures = [
    {
      es: 'Resina',
      en: 'Resin'
    },
    {
      es: 'Implantes',
      en: 'Implants'
    },
    {
      es: 'Endodoncia',
      en: 'Endodontics'
    },
    {
      es: 'Ortodoncia',
      en: 'Orthodontics'
    },
    {
      es: 'Puentes Fijos',
      en: 'Fixed Bridges'
    },
    {
      es: 'Blanqueamiento',
      en: 'Whitening'
    },
    {
      es: 'Placas Totales',
      en: 'Total Plates'
    },
    {
      es: 'Limpieza Dental',
      en: 'Dental Cleaning'
    },
    {
      es: 'Placas Parciales',
      en: 'Partial Plates'
    },
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = true;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.menu = this.translateMenu[3].es;
      this.contact = this.translateMenu[4].es;
      this.language = this.translateMenu[5].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });
      this.video = this.translateIndex[2].es;

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.menu = this.translateMenu[3].en;
      this.contact = this.translateMenu[4].en;
      this.language = this.translateMenu[5].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });
      this.video = this.translateIndex[2].en;

      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
