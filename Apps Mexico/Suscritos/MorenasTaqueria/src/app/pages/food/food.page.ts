import { Component, OnInit } from '@angular/core';
import { TraductorService } from '../../services/traductor.service';

@Component({
  selector: 'app-food',
  templateUrl: './food.page.html',
  styleUrls: ['./food.page.scss'],
})
export class FoodPage implements OnInit {

  breakfast = [
    {
      name: 'Huevos Con Chorizo',
      price: '8.99',
      description: 'Scrambled eggs with Mexican sausage.',
    },
    {
      name: 'Huevos Rancheros',
      price: '8.99',
      description: 'Topped with mild red sauce.',
    },
    {
      name: 'Huevos Con Jamon',
      price: '8.99',
      description: 'Eggs scrambled with ham.',
    },
    {
      name: 'Breakfast Burrito',
      price: '6.99',
      description: 'Choice of eggs mixed with chorizo, ham or bacon. Rice, beans, potatoes, cheese and sauce.',
    },
    {
      name: 'Huevos a la Mexicana',
      price: '8.99',
      description: 'Scrambled eggs mix with tomatos, onions and jalapeno.',
    },
  ]

  burritos = [
    {
      name: 'Burrito',
      price: '6.99',
      description: 'Choice of meat. Served with rice, beans, salsa, onions and cilantro.',
    },
    {
      name: 'Vegetarian Burrito',
      price: '6.99',
      description: 'Green, yellow and red pepper, mushrooms, salsa, onions, zuchini, cream and cheese.',
    },
    {
      name: 'Super Burrito',
      price: '7.99',
      description: 'Choice of meat. Served with rice, beans, cheese, cream, salsa, onions and cilantro.',
    },
    {
      name: 'Baby Burrito',
      price: '3.99',
      description: 'Served with rice, beans and cheese. Add meat for an additional charge.',
    },
    {
      name: 'Californiaopy of Super Burrito',
      price: '8.99',
      description: 'Choice of meat. Served with rice, beans, cheese, cream, salsa, onions and cilantro, plus french fries.',
    },
  ]

  tacos = [
    {
      name: 'Asada Taco',
      price: '2.69',
    },
    {
      name: 'Carnitas Taco',
      price: '2.69',
    },
    {
      name: 'Pastor Taco',
      price: '2.69',
    },
    {
      name: 'Cabeza Taco',
      price: '2.69',
    },
    {
      name: 'Pollo Asado Taco',
      price: '2.69',
    },
    {
      name: 'Chicarron Taco',
      price: '2.69',
    },
    {
      name: 'Barbacoa de Res Taco',
      price: '2.69',
    },
    {
      name: 'Birria de Chivo Taco',
      price: '2.69',
    },
    {
      name: 'Chile Rojo Taco',
      price: '2.69',
    },
    {
      name: 'Chile Verde Taco',
      price: '2.69',
    },
    {
      name: 'Lengua Taco',
      price: '3.99',
    },
    {
      name: 'Chavindeca and Sincrinizada Taco',
      price: '7.99',
    },
  ]

  kidsMeals = [
    {
      name: 'Kids Chicken Nuggets, French Fries',
      price: '4.99',
      description: 'none',
    },
    {
      name: 'Kids Taco, Rice and Beans',
      price: '4.99',
      description: 'Choice of meat.',
    },
    {
      name: 'Kids Cheese Quesadilla',
      price: '3.99',
      description: 'none',
    },
  ]

  entrees = [
    {
      name: 'Quesadilla',
      price: '2.50+',
      description: 'none',
    },
    {
      name: 'Super Quesadilla',
      price: '8.99',
      description: 'Flour tortilla, cheese, meat and salad.',
    },
    {
      name: 'Tostada',
      price: '2.99',
      description: 'Served with meat, beans, lettuce or cabbage, pico de gallo, sour cream and Mexican cheese.',
    },
    {
      name: 'Picodillo Sope',
      price: '2.99',
      description: 'Beans, lettuce or cabbage, pico de gallo, sour cream and Mexican cheese.',
    },
    {
      name: 'Torta',
      price: '6.99',
      description: 'Served with meat, beans, cheese, lettuce, pico de gallo, sour cream.',
    },
    {
      name: 'Flautas',
      price: '4.99+',
      description: '3 rolled deep fried corn tortilla, garnished with lettuce or cabbage, pico de gallo, sauce and cheese.',
    },
    {
      name: 'Tacos Special',
      price: '3.99+',
      description: '3 crispy tacos (pork and potatoes), cabbage, pico de gallo, sauce and cheese.',
    },
    {
      name: 'Nachos',
      price: '6.99',
      description: 'Beans, pico de gallo, cheese and sour cream.',
    },
    {
      name: 'Super Nachos',
      price: '8.99',
      description: 'Meat, beans, pico de gallo, cheese and sour cream.',
    },
    {
      name: 'Nacho Fries',
      price: '8.99',
      description: 'Meat, beans, pico de gallo, cheese and sour cream.',
    },
    {
      name: 'Taco Salad',
      price: '8.99',
      description: 'Meat, beans, lettuce, pico de gallo, cheese and sour cream.',
    },
    {
      name: 'Pozole',
      price: '9.99',
      description: 'none',
    },
    {
      name: 'Menudo',
      price: '10.99',
      description: 'none',
    },
    {
      name: 'Tamales',
      price: '1.99',
      description: 'none',
    },
  ]

  combinations = [
    {
      name: 'Albondigas Combo',
      price: '10.99',
      description: 'Rice and veggies.',
    },
    {
      name: 'Caldo de Res Combo',
      price: '10.99',
      description: 'Beef stew.',
    },
    {
      name: 'Chile Verde Combo',
      price: '10.99',
      description: 'Pork.',
    },
    {
      name: 'Chile Colorado Combo',
      price: '10.99',
      description: 'Beef.',
    },
    {
      name: 'Chile Relleno Combo',
      price: '10.99',
      description: 'Fillet with cheese, mild sauce.',
    },
    {
      name: 'Steak Ranchero Combo',
      price: '13.99',
      description: 'none',
    },
    {
      name: 'Fajita Combo',
      price: '13.99',
      description: 'Green, yellow and red bellpepper, onions and tomatoes.',
    },
    {
      name: 'Camarones a la Diabla Combo',
      price: '14.99',
      description: 'Spicy red stew.',
    },
    {
      name: 'Camarones Rancheros Combo',
      price: '14.99',
      description: 'none',
    },
    {
      name: 'Barbacoa de Res Combo',
      price: '13.99',
      description: 'none',
    },
    {
      name: 'Birria de Chivo Combo',
      price: '14.99',
      description: 'Goat red sauce stew.',
    },
    {
      name: 'Carnitas Combo',
      price: '10.99',
      description: 'Pork.',
    },
    {
      name: 'Carne Asada Combo',
      price: '10.99',
      description: 'none',
    },
    {
      name: 'Carne al Pastor Combo',
      price: '12.99',
      description: 'Marinated pork steak.',
    },
    {
      name: 'Pollo Asado Combo',
      price: '12.99',
      description: 'Grilled chicken.',
    },
    {
      name: 'Lengua Combo',
      price: '14.99',
      description: 'Beef tongue.',
    },
    {
      name: 'Cabeza Combo',
      price: '12.99',
      description: 'none',
    },
    {
      name: 'Enchiladas Combo',
      price: '10.99',
      description: '4 beef, chicken or pork.',
    },
    {
      name: 'Chilaquiles Verdes Combo',
      price: '9.99',
      description: 'none',
    },
    {
      name: 'Chilaquiles Rojos Combo',
      price: '9.99',
      description: 'none',
    },
  ]

  aLaCarta = [
    {
      name: 'Enchilada',
      price: '1.99',
    },
    {
      name: 'Chile Relleno',
      price: '6.99',
    },
    {
      name: 'Flauta',
      price: '1.99',
    },
    {
      name: 'Taco',
      price: '2.69',
    },
    {
      name: 'Tamal',
      price: '1.99',
    },
    {
      name: 'Sope',
      price: '2.99',
    },
    {
      name: 'Crispy Taco',
      price: '2.50',
    },
    {
      name: 'Tostada',
      price: '2.99',
    },
  ]

  desserts = [
    {
      name: 'Flan',
      price: '2.50',
    },
    {
      name: 'Buñuelo con Nieve',
      price: '3.50',
    },
    {
      name: 'Nieve',
      price: '2.50',
    },
  ]

  constructor(private translate: TraductorService) { }

  ngOnInit() {
  }

}
