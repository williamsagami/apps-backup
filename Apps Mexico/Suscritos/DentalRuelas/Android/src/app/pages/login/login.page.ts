import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController, LoadingController } from '@ionic/angular';
import { FirebaseService } from '../../services/firebase.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  t: any;

  selectedTab = "login";
  email: string;
  password: string;
  rpassword: string;
  showPassword = false;
  private loading: any;

  constructor(
    private firebaseService: FirebaseService, 
    private router: Router, private toastController: ToastController, 
    private data: DataService,
    private loadingController: LoadingController) {   
    this.t = data.appDataLocal.translate;
    this.showInfo(this.t.msg_IniciarSesion[this.t.currentTranslation]);
  }

  ngOnInit() {
  }

  togglePassword() {
    this.showPassword = !this.showPassword;
  }

  login() {
    this.loadingController.create({
      message: this.t.al_Cargando[this.t.currentTranslation]+'...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      this.firebaseService.login(this.email, this.password).then (res => {
        this.loading.dismiss();
        this.router.navigate(['/menu/inicio']);
      }).catch(err => { 
        switch(err.code)
        {
          case 'auth/argument-error': { this.showInfo(this.t.msg_Campos[this.t.currentTranslation]); } break;
          case 'auth/invalid-email': { this.showInfo(this.t.msg_Correo[this.t.currentTranslation]); } break;
          case 'auth/user-not-found': { this.showInfo(this.t.msg_Usuario[this.t.currentTranslation]); } break;
          case 'auth/wrong-password': { this.showInfo(this.t.msg_Contrasena[this.t.currentTranslation]); } break;
        }
        
        this.password = null;
        this.loading.dismiss();
      });
    });
  }

  register() {
    this.loadingController.create({
      message: this.t.al_Cargando[this.t.currentTranslation]+'...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      if(this.password === this.rpassword) {
        this.firebaseService.register(this.email, this.password).then (res => {
          this.loading.dismiss();
          this.router.navigate(['/menu/inicio']);
        }).catch(err => { console.log(err.code) 
          switch(err.code)
          {
            case 'auth/argument-error': { this.showInfo(this.t.msg_Campos[this.t.currentTranslation]); } break;
            case 'auth/invalid-email': { this.showInfo(this.t.msg_Correo[this.t.currentTranslation]); } break;
            case 'auth/weak-password': { this.showInfo(this.t.msg_ContrasenaChica[this.t.currentTranslation]); } break;
            case 'auth/email-already-in-use': { this.showInfo(this.t.msg_UsuarioExiste[this.t.currentTranslation]); } break;
          }

          this.password = null;
          this.rpassword = null;
        });
      }
      else {
        this.showInfo(this.t.msg_ContrasenaDiferente[this.t.currentTranslation]);
        this.password = null;
        this.rpassword = null;
        this.loading.dismiss();
      }
    });
  }

  async showInfo(info: string) {
    const toast = await this.toastController.create({
      message: info,
      duration: 6000,
      color: 'secondary',
    });
    toast.present();
  }
}
