import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-phonecall',
  templateUrl: './phonecall.page.html',
  styleUrls: ['./phonecall.page.scss'],
})
export class PhonecallPage implements OnInit {

  constructor(private callNumber : CallNumber) { }

  ngOnInit() {
  }

  phoneCall(number: string)
  {
    this.callNumber.callNumber(number, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

}
