import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PhonecallPage } from './phonecall.page';

describe('PhonecallPage', () => {
  let component: PhonecallPage;
  let fixture: ComponentFixture<PhonecallPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhonecallPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PhonecallPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
