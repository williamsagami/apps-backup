import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ImagesModalPage } from './images-modal.page';

describe('ImagesModalPage', () => {
  let component: ImagesModalPage;
  let fixture: ComponentFixture<ImagesModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagesModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ImagesModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
