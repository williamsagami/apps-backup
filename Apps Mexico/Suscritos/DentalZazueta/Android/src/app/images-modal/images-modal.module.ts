import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ImagesModalPageRoutingModule } from './images-modal-routing.module';

import { ImagesModalPage } from './images-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImagesModalPageRoutingModule
  ],
  declarations: [ImagesModalPage]
})
export class ImagesModalPageModule {}
