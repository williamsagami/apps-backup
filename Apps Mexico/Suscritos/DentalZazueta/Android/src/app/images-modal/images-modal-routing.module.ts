import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImagesModalPage } from './images-modal.page';

const routes: Routes = [
  {
    path: '',
    component: ImagesModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImagesModalPageRoutingModule {}
