

//   //SETUP DE BASE DE DATOS (USAR AL CREAR)
//   /*createDataBaseStructure(dbName: string, appData: any) {
//     this.db.collection(dbName).doc('adminPosts').set({
//       maxPostCount: 8,
//       postCount: 0,
//       postNumber: 0
//     });
//     this.db.collection(dbName).doc('appConfiguration').set({
//       dayRangeLimit: 10,
//       maxRequests: 2,
//       maxUserRating: 10,
//     });
//     this.db.collection(dbName).doc('appData').set(appData);
//     this.db.collection(dbName).doc('userData').set({});
//     this.db.collection(dbName).doc('userRequests').set({});
//   }*/

//   //TRADUCCION----------------------------------------------------------
//   updateAppData(translation: any) {
//     return this.db.collection(this.dbName).doc('appData').set(translation);
//   }

//   //AUTENTICACION----------------------------------------------------------


//   //OPCIONES DE APLICACION-----------------------------------------
//   updateAppConfiguration(appConfig: any) {
//     return this.db.collection(this.dbName).doc('appConfiguration').update({
//       dayRangeLimit: appConfig.dayRangeLimit,
//       maxRequests: appConfig.maxRequests,
//     });
//   }

//   //BLOG----------------------------------------------------------

//   createPost(post: post) {
//     var adminPosts: any;
//     return this.db.collection(this.dbName+'/adminPosts/posts').add(post).then(postNumber => {
//       this.db.collection(this.dbName).doc('adminPosts').ref.get().then(aPosts => {
//         adminPosts = aPosts.data();
//         var pNumber = adminPosts.postNumber+1;
//         var pCount = adminPosts.postCount+1;
//         this.db.collection(this.dbName).doc('adminPosts').update({postNumber: pNumber, postCount: pCount});
//       })
//     });
//   }

//   updatePost(post: post) {
//     return this.db.collection(this.dbName+'/adminPosts/posts').doc(post.id).set(post);
//   }

//   deletePost(post: post) {
//     var adminPosts: any;
//     return this.db.collection(this.dbName+'/adminPosts/posts').doc(post.id).delete().then(postNumber => {
//       this.db.collection(this.dbName).doc('adminPosts').ref.get().then(aPosts => {
//         adminPosts = aPosts.data();
//         var pNumber = adminPosts.postNumber-1;
//         this.db.collection(this.dbName).doc('adminPosts').update({postNumber: pNumber});
//       })
//     });
//   }

//   //USUARIOS----------------------------------------------------------

//   updateUser(user: usuario) {
//     return this.db.collection(this.dbName+'/userData/users').doc(user.id).set(user);
//   }

//   //CITAS----------------------------------------------------------

//   acceptRequest(request: solicitud) {
//     return this.db.collection(this.dbName+'/userRequests/requests').doc(request.user.id+request.id).update({state: state.accepted});
//   }

//   rejectRequest(request: solicitud) {
//     return this.db.collection(this.dbName+'/userRequests/requests').doc(request.user.id+request.id).update({state: state.rejected});
//   }

//   updateRequest(request: solicitud) {
//     var arId = request.user.id+request.id;
//     return this.db.collection(this.dbName+'/userRequests/requests').doc(arId).set(request);
//   }

//   createRequest(request: solicitud, userData: any) {
//     var arId = request.user.id+request.id;
//     var rCount = userData.requestCount+1;
//     var rNumber = userData.requestNumber+1;
//     return this.db.collection(this.dbName+'/userRequests/requests').doc(arId).set(request).then(requestCount => {
//       this.db.collection(this.dbName+'/userData/users').doc(request.user.id).update({requestCount: rCount, requestNumber: rNumber});
//     });
//   }

//   deleteRequest(request: solicitud, userData: any) {
//     var arId = request.user.id+request.id;
//     if(userData.id === request.user.id) {
//       var rCount = userData.requestCount-1;
//       var cancel = request.state === state.accepted ? userData.cancel+1 : userData.cancel;
//       return this.db.collection(this.dbName+'/userRequests/requests').doc(arId).delete().then(requestCount => {
//         this.db.collection(this.dbName+'/userData/users').doc(request.user.id).update({requestCount: rCount, cancel: cancel});
//       });
//     } else {
//       return this.db.collection(this.dbName+'/userRequests/requests').doc(arId).delete().then(requestCount => {
//         this.getUser(request.user.id).then(tempUser => {
//           var tu = tempUser as any;
//           var rCount = tu.requestCount-1;
//           var cancel = request.state === state.accepted ? tu.cancel+1 : tu.cancel;
//           this.db.collection(this.dbName+'/userData/users').doc(request.user.id).update({requestCount: rCount, cancel: cancel});
//         });
//       });
//     }
//   }

//   deleteInvalidRequest(request: solicitud) {
//     var arId = request.user.id+request.id;
//     return this.db.collection(this.dbName+'/userRequests/requests').doc(arId).delete().then(requestCount => {
//       this.getUser(request.user.id).then(tempUser => {
//         var tu = tempUser as any;
//         var rCount = tu.requestCount-1;
//         var lvl = (request.state === state.accepted ? tu.level+1 : tu.level);
//         this.db.collection(this.dbName+'/userData/users').doc(request.user.id).update({requestCount: rCount, level: lvl});
//       });
//     });
//   }

//   createUser(user_id: string, email: string) {
//     var user = {} as usuario;
//     user.id = user_id;
//     user.username = "Guest";
//     user.admin = false;
//     user.phone = "Undefined";
//     user.email = email;
//     user.rating = 0;
//     user.cancel = 0;
//     user.level = 1;
//     user.requestCount = 0;
//     user.requestNumber = 0;
//     user.allowRequests = true;
//     return this.db.collection(this.dbName+'/userData/users').doc(user_id).set(user);
//   }
// }
