import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-front',
  templateUrl: './front.page.html',
  styleUrls: ['./front.page.scss'],
})
export class FrontPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  slideOpts = {
    loop: true,
    autoplay: {
      delay: 2000,
    }
  };

}
