import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdatePostsPageRoutingModule } from './update-posts-routing.module';

import { UpdatePostsPage } from './update-posts.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdatePostsPageRoutingModule
  ],
  declarations: [UpdatePostsPage]
})
export class UpdatePostsPageModule {}
