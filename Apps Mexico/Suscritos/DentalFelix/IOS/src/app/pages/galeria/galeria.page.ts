import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { ImagePage } from '../image/image.page';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.page.html',
  styleUrls: ['./galeria.page.scss'],
})
export class GaleriaPage implements OnInit {

  t: any;
  d: any;

  images = [
    '../assets/images/1.jfif',
    '../assets/images/2.jfif',
    '../assets/images/3.jfif',
    '../assets/images/4.jfif',
    '../assets/images/5.jfif',
    '../assets/images/6.jfif',
    '../assets/images/7.jfif',
    '../assets/images/8.jfif',
    '../assets/images/9.jfif',
    '../assets/images/10.jpg',
    '../assets/images/17.jfif',
    '../assets/images/13.jpg',
  ];

  constructor(private modalController: ModalController, private data: DataService) { 
    this.t = data.appDataLocal.translate;
    this.d = data.appDataLocal.data;
  }

  ngOnInit() {
  }

  async openImage(i: number)
	{
    const modal = await this.modalController.create({
      component: ImagePage,
      cssClass: 'modal-transparency',
      componentProps : {
        index: i+1,
        images: this.images,
        translate: this.t,
      }
    });
    await modal.present();
	}

}
