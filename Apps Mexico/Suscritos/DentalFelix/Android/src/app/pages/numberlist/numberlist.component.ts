import { Component, OnInit } from '@angular/core';
import { PopoverController, ModalController } from '@ionic/angular';
import { LlamadaPage } from '../llamada/llamada.page';

@Component({
  selector: 'app-numberlist',
  templateUrl: './numberlist.component.html',
  styleUrls: ['./numberlist.component.scss'],
})
export class NumberlistComponent implements OnInit {

  constructor(private modalController: ModalController, private poc: PopoverController) { }

  ngOnInit() {}

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: LlamadaPage,
      componentProps : {
        number: phoneNumber
      }
    });
    await modal.present();
    console.log(modal.componentProps.number);

    this.poc.dismiss();
  }

}
