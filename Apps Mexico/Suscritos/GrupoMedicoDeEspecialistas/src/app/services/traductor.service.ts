import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'Grupo Medico de Especialistas',
      en: 'Medical Group of Specialists'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Implantes',
    'Ortodoncia',
    'Odontologia General',
    'Endodoncia',
    'Cirugia Terceros Molares',
  ];

  translateIndex = [
    {
      es:`Nuestra mision es ofrecer un excelente servicio de Odontologia General a nuestros clientes, por ello
      nos esforzamos todo lo posible para que tengas con una sonrisa increible al salir de nuestras instalaciones.\n
      Contamos con una basta experiencia en el trabajo que realizamos, y lo ofrecemos a un bajo costo para que cualquier persona
      pueda tener unos dientes saludables.\n
      Si te gustaria saber mas de nosotros o deseas una cita, llamanos o contactanos en redes sociales.\n
      Esperamos tu llamada.`,
      en:`Our mission is to offer an excellent General Dentistry service to our clients, therefore
      We strive to make you have an incredible smile when you leave our facilities.\n
      We have vast experience in the work we do, and we offer it at a low cost so that anyone
      can have healthy teeth.\n
      If you would like to know more about us or want an appointment, call us or contact us on social networks.\n
      We look forward to your call.`
    },
    {
      es: 'Procedimientos:',
      en: 'Procedures:'
    },
  ];

  translateProcedures = [
    {
      es: 'Implantes',
      en: 'Implants'
    },
    {
      es: 'Ortodoncia',
      en: 'Orthodontics'
    },
    {
      es: 'Odontologia General',
      en: 'General Odontology'
    },
    {
      es: 'Endodoncia',
      en: 'Endodontics'
    },
    {
      es: 'Cirugia Terceros Molares',
      en: 'Third Molar Surgery'
    },
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = false;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });

      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
