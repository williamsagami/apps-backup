import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { solicitud, state} from '../../services/firebase.service';
import { ViewUserPage } from './view-user/view-user.page';
import { FirebaseService } from '../../services/firebase.service';
import * as moment from 'moment';
import { UpdateAppointmentPage } from './update-appointment/update-appointment.page';


@Component({
  selector: 'app-citas',
  templateUrl: './citas.page.html',
  styleUrls: ['./citas.page.scss'],
})
export class CitasPage implements OnInit {
  t: any;

  userReady = false;
  private tempUserData: any;
  appConfiguration: any;
  public solicitudes: any = [];
  private invalidRequests: any = [];
  selectedTab = 'pending';

  private loading: any;

  private requestRef: any;

  slideOpts = {
    loop: true,
    zoom: true,
    passiveListeners: false,
  };

  constructor(
    private data: DataService, 
    public firebaseService: FirebaseService, 
    private modalController: ModalController, 
    private loadingController: LoadingController,
    private alertController: AlertController) {
      this.t = data.appDataLocal.translate;
    }

  ngOnInit() {
    this.getAppData();
  }

  getAppData() {
    this.loadingController.create({
      message: this.t.al_Cargando[this.t.currentTranslation]+'...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();


      //Tomar Configuracion
      this.firebaseService.getAppConfiguration().then(appConfiguration => {
        this.appConfiguration = appConfiguration as any;

        //Datos de Usuario
        /*this.firebaseService.fbAuth.authState.subscribe(user => {
          this.user = user;

          if(this.user) { 
            this.firebaseService.getUser(this.user.uid).then(userData => {
              this.userData = userData as any;
              this.userExist = (this.userData !== undefined);
              
              if(this.userExist) {
                this.getRequests();
                
              } else {
                this.firebaseService.createUser(this.user.uid, this.user.email).then(userSetup => {
                  this.firebaseService.getUser(this.user.uid).then(userData => {
                    this.userData = userData as any;
                  }).then(requests => {
                    this.getRequests();
                  });
                });
              }
            });
          }
          else {
            this.solicitudes = [];
            this.invalidRequests = [];
          } 
        });*/

        //Datos de Usuario
        this.firebaseService.userSetup().then(userDataExist => {
          //var userExist = userDataExist as any;
          if(this.firebaseService.userData) {
            this.getRequests();
          }
          else {
            this.solicitudes = [];
            this.invalidRequests = [];
          } 
        });
      });
    });
  }

  getRequests() {
    if(this.firebaseService.userData.admin) {
      //Admin Request List
      this.firebaseService.getRequests().then(requests => {
        if(requests !== undefined) this.solicitudes = this.getValidRequests(this.getSortedRequests(requests));
        this.userReady = true;
        this.loading.dismiss();
      });
    }
    else {
      //User Requests List
      this.firebaseService.getUserRequests(this.firebaseService.userData.id).then(requests => {
        if(requests !== undefined) this.solicitudes = this.getValidRequests(requests);
        this.userReady = true;
        this.loading.dismiss();
      });
    }
    this.getDataRealTime();
  }

  getDataRealTime() {
    if(this.firebaseService.userData.admin) {
      //Admin Request List
      if(this.requestRef) this.requestRef.unsubscribe();
      this.requestRef = this.firebaseService.getRequestsRealTime().subscribe(requests => {
        if(requests !== undefined) this.solicitudes = this.getValidRequests(this.getSortedRequests(requests));
      });
    }
    else {
      //User Requests List
      if(this.requestRef) this.requestRef.unsubscribe();
      this.requestRef = this.firebaseService.getUserRequestsRealTime(this.firebaseService.userData.id).subscribe(requests => {
        if(requests !== undefined) this.solicitudes = this.getValidRequests(requests);
      });
    }
  }

  getValidRequests(requests: any) {
    var validRequests = [];
    this.invalidRequests = [];
    var validDate = moment(moment().subtract(1, 'hours')).toDate();
    
    requests.forEach(request => {
      if(moment(validDate).isBefore(request.date)) validRequests.push(request);
      else this.invalidRequests.push(request);
    });

    if(this.invalidRequests.length > 0) this.deleteInvalidRequests();
    return validRequests;
  }

  getSortedRequests(requests: any) {
    var sortedRequests = [];

    requests.forEach(request => {
      if(this.isVIP(request.user.rating)) {
        sortedRequests.push(request);
      }
    });

    requests.forEach(request => {
      if(!this.isVIP(request.user.rating)) {
        sortedRequests.push(request);
      }
    });

    return sortedRequests;
  }

  deleteInvalidRequests() {
    if(this.firebaseService.userData.admin) {
      this.invalidRequests.forEach(request => {
        var invalidRequest = request;
        invalidRequest.valid = false;
        this.firebaseService.updateRequest(invalidRequest).then(deleteRequest => {
          this.firebaseService.deleteRequest(invalidRequest, false);
        });
      });
    }
  }

  getRequestState(requestState: number) {
    //AGREGAR CITA EN CURSO
    switch(requestState)
    {
      case state.pending: return {text: this.t.txt_CitaPendiente[this.t.currentTranslation], color: "warning"};
      case state.accepted: return {text: this.t.txt_CitaAceptada[this.t.currentTranslation], color: "success"};
      case state.rejected: return {text: this.t.txt_CitaRechazada[this.t.currentTranslation], color: "danger"};
      default: return {text: this.t.txt_CitaPendiente[this.t.currentTranslation], color: "warning"};
    }
  }

  allowRequest() {
    return ((this.firebaseService.userData.requestNumber < this.appConfiguration.maxUserRequestNumber && this.firebaseService.userData.allowRequests) || this.firebaseService.userData.admin)
  }

  acceptRequest(request: solicitud) {
    var coolRequest = request as any;
    coolRequest.lastChange = this.firebaseService.userData;
    this.firebaseService.acceptRequest(coolRequest);
  }

  rejectRequest(request: solicitud) {
    var coolRequest = request as any;
    coolRequest.lastChange = this.firebaseService.userData;
    this.firebaseService.rejectRequest(coolRequest);
  } 

  getRequestsByState(state: number) {
    var requests = [];
    this.solicitudes.forEach(request => {
      if(request.state === state) requests.push(request);
    });
    return requests;
  }

  async createAppointment() {
    const ca = await this.modalController.create({
      component: UpdateAppointmentPage,
      componentProps : {
        userData: this.firebaseService.userData,
        appConfiguration: this.appConfiguration,
        translate: this.data.appDataLocal.translate,
      }
    });
    await ca.present();
  }

  async updateAppointment(solicitud: solicitud) {
    const va = await this.modalController.create({
      component: UpdateAppointmentPage,
      componentProps : {
        solicitud: solicitud,
        userData: this.firebaseService.userData,
        appConfiguration: this.appConfiguration,
        translate: this.data.appDataLocal.translate,
      }
    });
    await va.present();
  }

  getTempUser(user_id: string) {
    this.tempUserData = {} as any;

    this.loadingController.create({
      message: this.t.al_Cargando[this.t.currentTranslation]+'...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      this.firebaseService.getUser(user_id).then(userData => {
        this.tempUserData = userData;
        this.viewTempUser();
      });
    });
  }

  viewTempUser() {
    this.modalController.create({
      component: ViewUserPage,
      componentProps : {
        userData: this.tempUserData,
      }
    }).then(viewUserModal => {
      const vu = viewUserModal;
      vu.present();
      this.loading.dismiss();
    });
  }

  isVIP (rating: number) {
    return (rating >= this.appConfiguration.maxUserRating);
  }

  async rejectConfirm(request: solicitud) {
    const alert = await this.alertController.create({
      cssClass: 'cool-alert',
      header: this.t.al_RechazarSolicitud[this.t.currentTranslation],
      message: this.t.al_RechazarSeguro[this.t.currentTranslation],
      buttons: [
        {
          text: this.t.alb_Cancelar[this.t.currentTranslation],
          role: 'cancel',
          cssClass: 'secondary',
        }, 
        {
          text: this.t.alb_Rechazar[this.t.currentTranslation],
          handler: () => {
            this.rejectRequest(request);
          }
        }
      ]
    });

    await alert.present();
  }
}
