import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { ModalController, NavParams, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { post, FirebaseService } from '../../../services/firebase.service';

@Component({
  selector: 'app-update-posts',
  templateUrl: './update-posts.page.html',
  styleUrls: ['./update-posts.page.scss'],
})
export class UpdatePostsPage implements OnInit {

  t: any;
  post: any;
  create = false;
  titulo = "";
  oferta = "";
  contenido = "";
  adminPosts: any;

  private loading: any;

  constructor(
    private data: DataService, 
    private firebaseService: FirebaseService,
    private modalController: ModalController, 
    private navParams: NavParams,  
    private toastController: ToastController,
    private alertController: AlertController,
    private loadingController: LoadingController) { 
      this.t = data.appDataLocal.translate;
      this.post = navParams.get('post');
      this.create = navParams.get('create');
      this.adminPosts = navParams.get('adminPosts');

      //console.log(this.post);
  }

  ngOnInit() {
    if(!this.create) {
      this.titulo = this.post.title;
      this.oferta = this.post.sale;
      this.contenido = this.post.content;
    }
  }

  closeView(){
	  this.modalController.dismiss();
  }

  async showInfo(info: string) {
    const toast = await this.toastController.create({
      message: info,
      duration: 6000,
      color: 'secondary',
    });
    toast.present();
  }

  allowPost() {
    return (this.titulo !== "" && this.contenido !== "");
  }

  createPost() {
    this.loadingController.create({
      message: this.t.al_Cargando[this.t.currentTranslation]+'...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      if(this.allowPost()) {
        var post = {} as post;
        post.id = 'Undefined';
        post.title = this.titulo;
        post.sale = this.oferta;
        post.content = this.contenido;
        post.postNumber = this.adminPosts.postCount;
  
        this.firebaseService.createPost(post, this.adminPosts).then(close => {
          this.loading.dismiss();
          this.closeView();
          this.showInfo(this.t.msg_PublicacionCreada[this.t.currentTranslation]);
        });
      } else {
        this.showInfo(this.t.msg_Campos[this.t.currentTranslation]);
        this.loading.dismiss();
      }
    });
  }

  updatePost() {
    this.loadingController.create({
      message: this.t.al_Cargando[this.t.currentTranslation]+'...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      if(this.allowPost()) {
        this.post.title = this.titulo;
        this.post.sale = this.oferta;
        this.post.content = this.contenido;
  
        this.firebaseService.updatePost(this.post).then(close => {
          this.loading.dismiss();
          this.closeView();
          this.showInfo(this.t.msg_PublicacionActualizada[this.t.currentTranslation]);
        });
      } else {
        this.showInfo(this.t.msg_Campos[this.t.currentTranslation]);
        this.loading.dismiss();
      }
    });
  }

  async deleteConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'cool-alert',
      header: this.t.al_BorrarPublicacion[this.t.currentTranslation],
      message: this.t.al_BorrarPublicacionSeguro[this.t.currentTranslation],
      buttons: [
        {
          text: this.t.alb_Cancelar[this.t.currentTranslation],
          role: 'cancel',
          cssClass: 'secondary',
        }, 
        {
          text: this.t.alb_Borrar[this.t.currentTranslation],
          handler: () => {
            this.deletePost();
          }
        }
      ]
    });

    await alert.present();
  }

  deletePost() {
    this.loadingController.create({
      message: this.t.al_Cargando[this.t.currentTranslation]+'...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      this.firebaseService.deletePost(this.post, this.adminPosts).then(close => {
        this.loading.dismiss();
        this.closeView();
        this.showInfo(this.t.msg_PublicacionBorrada[this.t.currentTranslation]);
      });
    });
  }
}
