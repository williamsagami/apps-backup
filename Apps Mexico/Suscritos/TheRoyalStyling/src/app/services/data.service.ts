import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/call/call.page';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  numeros = [
    "664 865 0621",
    "663 125 5489",
  ];

  redesSociales = [
    {
      name: "The Royal Styling",
      link: "https://www.facebook.com/TheRoyalStyling/",
      icon: "logo-facebook",
    },
    {
      name: "theroyalstyling",
      link: "https://www.instagram.com/theroyalstyling/",
      icon: "logo-instagram",
    }
  ];

  constructor(private modalController: ModalController, private iab : InAppBrowser) { }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  this.iab.create(link);
  }
}
