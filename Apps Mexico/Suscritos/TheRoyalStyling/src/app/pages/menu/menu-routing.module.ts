import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: 'menu',
    component: MenuPage,
    children: [
      {
        path: 'barber-shop',
        loadChildren: () => import('../barber-shop/barber-shop.module').then( m => m.BarberShopPageModule)
      },
      {
        path: 'products',
        loadChildren: () => import('../products/products.module').then( m => m.ProductsPageModule)
      },
      {
        path: 'royal-institute',
        loadChildren: () => import('../royal-institute/royal-institute.module').then( m => m.RoyalInstitutePageModule)
      },
    ]
  },
  {
    path: '',
    redirectTo: '/menu/barber-shop',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
