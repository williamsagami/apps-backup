import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  selectedUrl = '/menu/inicio';

  pages = [
    {
      title: 'Barber Shop',
      url: '/menu/barber-shop',
      icon: "flame",
    },
    {
      title: 'Productos',
      url: '/menu/products',
      icon: "basket",
    },
    {
      title: 'Royal Institute',
      url: '/menu/royal-institute',
      icon: "school",
    }
  ]; 

  constructor(private router: Router) { 
    this.router.events.subscribe((event: RouterEvent) => {
    this.selectedUrl = event.url != null ? event.url : this.selectedUrl;
  }); 
  }

  ngOnInit() {
  }

}
