import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BarberShopPageRoutingModule } from './barber-shop-routing.module';

import { BarberShopPage } from './barber-shop.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BarberShopPageRoutingModule
  ],
  declarations: [BarberShopPage]
})
export class BarberShopPageModule {}
