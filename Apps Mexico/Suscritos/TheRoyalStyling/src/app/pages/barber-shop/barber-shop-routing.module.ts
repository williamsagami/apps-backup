import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BarberShopPage } from './barber-shop.page';

const routes: Routes = [
  {
    path: '',
    component: BarberShopPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BarberShopPageRoutingModule {}
