import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-barber-shop',
  templateUrl: './barber-shop.page.html',
  styleUrls: ['./barber-shop.page.scss'],
})
export class BarberShopPage implements OnInit {

  servicios = [
    "Corte de Cabello Caballero y Niño",
    "Arreglo/Afeitado de Barba",
    "Delineado de Ceja",
    "Pigmento",
    "Faciales y Mucho Más",
  ];

  horarios = [
    {
      dia: "Lunes",
      hora: "10:00 AM - 08:00 PM",
    },
    {
      dia: "Martes",
      hora: "10:00 AM - 08:00 PM",
    },
    {
      dia: "Miercoles",
      hora: "10:00 AM - 08:00 PM",
    },
    {
      dia: "Jueves",
      hora: "10:00 AM - 08:00 PM",
    },
    {
      dia: "Viernes",
      hora: "10:00 AM - 08:00 PM",
    },
    {
      dia: "Sabado",
      hora: "10:00 AM - 08:00 PM",
    },
    {
      dia: "Domingo",
      hora: "10:00 AM - 06:00 PM",
    },
  ];

  slider = [
    '../assets/images/bs1.jpg',
    '../assets/images/bs2.jpg',
    //'../assets/images/bs3.jpg',
    '../assets/images/bs4.jpg',
    '../assets/images/bs5.jpg',
    '../assets/images/bs6.jpg',
  ];

  slideOpts = {
    loop: true,
    autoplay: {
      delay: 2000,
    }
  };

  constructor(private data: DataService) { }

  ngOnInit() {
  }

}
