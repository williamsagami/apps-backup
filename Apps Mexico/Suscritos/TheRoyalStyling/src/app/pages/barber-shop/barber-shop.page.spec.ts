import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BarberShopPage } from './barber-shop.page';

describe('BarberShopPage', () => {
  let component: BarberShopPage;
  let fixture: ComponentFixture<BarberShopPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarberShopPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BarberShopPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
