import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {

  productos = [
    "../assets/images/p1.jpg",
    "../assets/images/p2.jpg",
    "../assets/images/p3.jpg",
    "../assets/images/p4.jpg",
    "../assets/images/p5.jpg",
    "../assets/images/p6.jpg",
    "../assets/images/p7.jpg",
  ];

  constructor(private data: DataService) { }

  ngOnInit() {
  }

}
