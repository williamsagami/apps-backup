import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-royal-institute',
  templateUrl: './royal-institute.page.html',
  styleUrls: ['./royal-institute.page.scss'],
})
export class RoyalInstitutePage implements OnInit {

  servicios = [
    "Curso de Barberia y Peluqueria Profesional",
    "Talleres Exclusivos",
  ];

  clases = [
    "Cuidado y mantenimiento de herramientos que usaras en este oficio.",
    "Atención al cliente y todo lo que conciste en ser un profesiónal.",
    "Cortes clásicos, modernos, diseños, barbas, aplicación de tintes etc.",
    "Usar redes sociales para darte a conocer y crecer tu imagen.",
    "Recomendaciones para abrir tu propia barberia y mucho más.",
  ];

  matutino = [
    {
      dia: "Martes",
      hora: "10:00 AM - 01:00 PM",
    },
    {
      dia: "Miercoles",
      hora: "10:00 AM - 01:00 PM",
    },
    // {
    //   dia: "Jueves",
    //   hora: "09:00 AM - 11:00 AM",
    // },
    // {
    //   dia: "Viernes",
    //   hora: "09:00 AM - 11:00 AM",
    // },
    // {
    //   dia: "Sabado",
    //   hora: "09:00 AM - 12:00 PM",
    // },
  ];

  vespertino = [
    {
      dia: "Martes",
      hora: "05:00 PM - 08:00 PM",
    },
    {
      dia: "Miercoles",
      hora: "05:00 PM - 08:00 PM",
    },
    // {
    //   dia: "Jueves",
    //   hora: "06:00 PM - 08:00 PM",
    // },
    // {
    //   dia: "Viernes",
    //   hora: "06:00 PM - 08:00 PM",
    // },
    {
      dia: "Sabado",
      hora: "09:00 AM - 02:00 PM",
    },
    {
      dia: "Domingo",
      hora: "10:00 AM - 03:00 PM",
    },
  ];

  slider = [
    '../assets/images/ri1.jpg',
    '../assets/images/ri2.jpg',
    '../assets/images/ri3.jpg',
    '../assets/images/ri4.jpg',
    '../assets/images/ri5.jpg',
    '../assets/images/ri6.jpg',
    '../assets/images/ri7.jpg',
    '../assets/images/ri8.jpg',
  ];

  slideOpts = {
    loop: true,
    autoplay: {
      delay: 2000,
    }
  };

  constructor(private data: DataService) { }

  ngOnInit() {
  }

}
