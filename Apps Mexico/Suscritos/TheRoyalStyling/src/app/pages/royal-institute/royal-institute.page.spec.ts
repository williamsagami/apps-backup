import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RoyalInstitutePage } from './royal-institute.page';

describe('RoyalInstitutePage', () => {
  let component: RoyalInstitutePage;
  let fixture: ComponentFixture<RoyalInstitutePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoyalInstitutePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RoyalInstitutePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
