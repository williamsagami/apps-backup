import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoyalInstitutePageRoutingModule } from './royal-institute-routing.module';

import { RoyalInstitutePage } from './royal-institute.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoyalInstitutePageRoutingModule
  ],
  declarations: [RoyalInstitutePage]
})
export class RoyalInstitutePageModule {}
