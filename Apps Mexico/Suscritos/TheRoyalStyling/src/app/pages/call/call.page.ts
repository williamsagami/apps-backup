import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-call',
  templateUrl: './call.page.html',
  styleUrls: ['./call.page.scss'],
})
export class CallPage implements OnInit {

  phoneNumber: string;

  constructor(private modalController: ModalController, private navParams: NavParams, private callNumber: CallNumber) { 
    this.phoneNumber = this.navParams.get('number');
  }

  ngOnInit() {
  }

  phoneCall() {
	  this.callNumber.callNumber(this.phoneNumber, true)
	  .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
  
  closeCall(){
	  this.modalController.dismiss();
  }

}
