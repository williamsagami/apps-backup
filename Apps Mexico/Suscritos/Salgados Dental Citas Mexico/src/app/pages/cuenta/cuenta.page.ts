import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseService } from '../../services/firebase.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-cuenta',
  templateUrl: './cuenta.page.html',
  styleUrls: ['./cuenta.page.scss'],
})
export class CuentaPage implements OnInit {

  t: any;

  userReady = false;
  private userData: any;
  appConfiguration: any;
  activateNotifications = false;

  private loading: any;

  constructor(
    public firebaseService: FirebaseService, 
    private router: Router, 
    private loadingController: LoadingController, 
    private toastController: ToastController,
    private data: DataService) {
      this.t = data.appDataLocal.translate;
    }

  ngOnInit() {
    this.getAppData();
  }

  getAppData(){
    this.loadingController.create({
      message: this.t.al_Cargando[this.t.currentTranslation]+'...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      //Tomar Configuracion
      this.firebaseService.getAppConfiguration().then(appConfiguration => {
        this.appConfiguration = appConfiguration as any;

        //Datos de Usuario
        this.firebaseService.userSetup().then(userDataExist => {
          this.userReady = userDataExist as any;
          if(userDataExist) {
            this.activateNotifications = this.firebaseService.userData.notifications;
            this.loading.dismiss();
          }
        });
      });
    });
  }

  async showInfo(info: string) {
    const toast = await this.toastController.create({
      message: info,
      duration: 6000,
      color: 'secondary',
    });
    toast.present();
  }

  logout() {
    this.loadingController.create({
      message: this.t.al_CerrandoSesion[this.t.currentTranslation]+'...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      this.firebaseService.logout().then(redirect => {
        this.loading.dismiss();
        this.router.navigate(['/menu/login']);
      });
    });
  }

  setMaxRequests(event) {
    this.appConfiguration.maxUserRequestNumber = event.detail.value;
  }

  setDayRangeLimit(event) {
    this.appConfiguration.dayRangeLimit = event.detail.value;
  }

  getAdminText() {
    return this.firebaseService.userData.admin ? this.t.txt_Administrador[this.t.currentTranslation] : this.t.txt_Cliente[this.t.currentTranslation];
  }
  
  saveAccountChanges() {
    this.loadingController.create({
      message: this.t.al_Guardando[this.t.currentTranslation]+'...',
      cssClass: 'cool-loading',
    }).then(overlay => {
      this.loading = overlay;
      this.loading.present();

      this.userData = this.firebaseService.userData;
      this.userData.notifications = this.activateNotifications;
      if(this.userData.admin) {
        this.firebaseService.updateUser(this.userData).then(updateAppConfig => {
          const appConfig = {
            maxUserRequestNumber: this.appConfiguration.maxUserRequestNumber, 
            dayRangeLimit: this.appConfiguration.dayRangeLimit,
            maxUserRating: 10,
          };
          this.firebaseService.updateAppConfiguration(appConfig).then(showInfo => {
              this.loading.dismiss();
              this.showInfo(this.t.msg_CambiosGuardados[this.t.currentTranslation]);
          });
        });
      } else {
        this.firebaseService.updateUser(this.userData).then(showInfo => {
          this.loading.dismiss();
          this.showInfo(this.t.msg_CambiosGuardados[this.t.currentTranslation]);
        });
      }
      this.toggleSalesNotifications();
    });
  }

  toggleSalesNotifications() {
    if(!this.activateNotifications) {
      this.firebaseService.fcm.unsubscribe(this.firebaseService.getDataBaseName()+"-sales").then(showInfo => {
        this.showInfo(this.t.msg_NotificacionesDesactivadas[this.t.currentTranslation]);
      });
    }
    else {
      if(!this.firebaseService.userData.admin)
      {
        this.firebaseService.fcm.subscribe(this.firebaseService.getDataBaseName()+"-sales").then(showInfo => {
          this.showInfo(this.t.msg_NotificacionesActivadas[this.t.currentTranslation]);
        });
      }
    }
  }
}
