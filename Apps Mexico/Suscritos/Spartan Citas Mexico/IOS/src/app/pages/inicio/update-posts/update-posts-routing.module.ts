import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdatePostsPage } from './update-posts.page';

const routes: Routes = [
  {
    path: '',
    component: UpdatePostsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdatePostsPageRoutingModule {}
