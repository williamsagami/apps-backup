import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'imagen',
    loadChildren: () => import('./pages/imagen/imagen.module').then( m => m.ImagenPageModule)
  },
  {
    path: 'llamada',
    loadChildren: () => import('./pages/llamada/llamada.module').then( m => m.LlamadaPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
