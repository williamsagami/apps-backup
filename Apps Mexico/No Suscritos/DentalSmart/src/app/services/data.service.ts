import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  //slider
  sliderImages = [
    '../assets/images/1s.jpg',
    '../assets/images/2s.jpg',
    //'../assets/images/3s.jpg',
    '../assets/images/4s.jpg',
  ]

  //galeria
  images = [
    'assets/images/1.jpg',
    'assets/images/2.jpg',
    'assets/images/3.jpg',
    'assets/images/4.jpg',
    'assets/images/5.jpg',
    'assets/images/6.jpg',
    'assets/images/7.jpg',
    'assets/images/8.jpg',
    'assets/images/9.jpg',
    'assets/images/10.jpg',
    'assets/images/11.jpg',
    'assets/images/12.jpg',
    'assets/images/13.jpg',
    'assets/images/15.jfif',
    'assets/images/16.jfif',
    'assets/images/17.jfif',
    'assets/images/18.jfif',
    'assets/images/19.jfif',
  ]

  //contacto
  phones = [
    {
      number: 'Dra Perla Celiz: 664 110 9459', 
      call: '6641109459'
    },
    {
      number: 'Dra Sheridan Cortez: 664 164 4473',
      call: '6641644473'
    },
    {
      number: 'USA: 619 730 3404',
      call: '6197303404'
    },
  ];

  direction = 'Calle 4ta. Diaz Miron #8064 1-B, entre Constitucion y Niños Heroes, Zona Centro, Tijuana, B.C.';

  constructor() { }
}
