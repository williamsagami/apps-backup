import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  //slider
  sliderImages = [
    '../assets/images/1s.jpg',
    '../assets/images/2s.jpg',
    '../assets/images/3s.jpg',
    '../assets/images/4s.jpg',
  ]

  //galeria
  images = [
    'assets/images/1.jpg',
    'assets/images/2.jpg',
    'assets/images/3.jpg',
    'assets/images/4.jpg',
    'assets/images/5.jpg',
    'assets/images/6.jpg',
    'assets/images/7.jpg',
    'assets/images/8.jpg',
    'assets/images/9.jpg',
    'assets/images/10.jpg',
    'assets/images/11.jpg',
    'assets/images/12.jpg',
    'assets/images/13.jpg',
    'assets/images/14.jpg',
    'assets/images/15.jpg',
    'assets/images/16.jpg',
    'assets/images/17.jpg',
    'assets/images/18.jpg',
    'assets/images/19.jpg',
    'assets/images/20.jpg',
    'assets/images/21.jpg',
    'assets/images/22.jpg',
    'assets/images/23.jpg',
    'assets/images/24.jpg',
    'assets/images/25.jpg',
    'assets/images/26.jpg',
    'assets/images/27.jpg',
    'assets/images/28.jpg',
    'assets/images/29.jpg',
    'assets/images/30.jpg',
    'assets/images/31.jpg',
    'assets/images/32.jpg',
    'assets/images/33.jpg',
    'assets/images/34.jpg',
    'assets/images/35.jpg',
  ]

  //contacto
  phones = [
    {
      number: 'Cel: 664 378 7293', 
      call: '6643787293'
    },
    {
      number: 'Cel: 686 516 2289',
      call: '6865162289'
    },
  ];

  direction = 'Plaza Sendero Local G11, Blvd Lazaro Cardenas 1600 Islas, Mexicali, B.C., CP 21600';
  direction2 = 'Plaza Sendero, Carretera Tijuana - Tecate 25420, 22245 Tijuana, B.C.';

  constructor() { }
}
