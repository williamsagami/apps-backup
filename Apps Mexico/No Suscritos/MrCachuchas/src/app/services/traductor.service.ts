import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'Mr Cachuchas',
      en: 'Mr Cachuchas'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Cachuchas',
    'Sombreros',
    'Cinturones',
    'Boinas',
    'Accesorios Varios',
  ];
  video = '';

  translateIndex = [
    {
      es:`En Mr Cachuchas ofrecemos los mejores cachuchas y servicios de bordado en toda Baja California.\n
      Todos nuestros productos cuentan con materiales de excelente calidad y a un buen precio, garantizamos que quedaras satisfecho con cualquier compra
      que realices!\n
      Para saber más detalles sobre nuestros productos comunicate con nosotros o visita una de nuestras tiendas.\n
      Y recuerda que el servicio al cliente es nuestra prioridad!`,
      en:`At Mr Cachuchas we offer the best caps and embroidery services in Baja California. \ N
      All our products have materials of excellent quality and at a good price, we guarantee that you will be satisfied with any purchase
      you do! \ n
      To find out more details about our products, contact us or visit one of our stores. \ N
      And remember that customer service is our priority!`
    },
    {
      es: 'Productos:',
      en: 'Products:'
    },
    {
      es: 'Videos',
      en: 'Videos'
    },
  ];

  translateProcedures = [
    {
      es: 'Cachuchas',
      en: 'Caps'
    },
    {
      es: 'Sombreros',
      en: 'Hats'
    },
    {
      es: 'Cinturones',
      en: 'Belts'
    },
    {
      es: 'Boinas',
      en: 'Berets'
    },
    {
      es: 'Accesorios Varios',
      en: 'Variety of Accessories'
    },
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = false;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });
      this.video = this.translateIndex[2].es;

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });
      this.video = this.translateIndex[2].en;

      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
