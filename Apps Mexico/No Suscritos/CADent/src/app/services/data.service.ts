import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  //slider
  sliderImages = [
    '../assets/images/1s.jpg',
    '../assets/images/2s.jpg',
    '../assets/images/3s.jpg',
    '../assets/images/4s.jpg',
  ]

  //galeria
  images = [
    'assets/images/1.jfif',
    'assets/images/2.jfif',
    'assets/images/3.jfif',
    'assets/images/4.jfif',
    'assets/images/5.jfif',
    'assets/images/6.jfif',
    'assets/images/7.jfif',
    'assets/images/8.jfif',
    'assets/images/9.jfif',
    'assets/images/10.jfif',
    'assets/images/11.jfif',
    'assets/images/12.jfif',
    'assets/images/13.jfif',
    'assets/images/14.jfif',
    'assets/images/15.jfif',
    'assets/images/16.jfif',
  ]

  //contacto
  phones = [
    {
      number: 'Dr. Omar Sandoval 667 138 7417',
      call: '6671387417'
    },
    {
      number: 'Dr. Ezequiel Sandoval 687 146 8227',
      call: '6871468227'
    },
    {
      number: 'USA 619 659 4234',
      call: '6196594234'
    },
  ];

  direction = 'Condominio Century 2034 Oficina 703, entre Revolución y Madero, Zona Centro, Tijuana, B.C.';

  constructor() { }
}
