import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'Muela Dental',
      en: 'Muela Dental'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Dentaduras o Placas Dentales',
    'Protesis Parciales Removibles',
    'Extracion Dental',
    'Rellenos Dentales',
    'Limpieza Dental y Eliminación de Sarro',
  ];
  

  translateIndex = [
    {
    es:`Muela Dental, es una las mejores opciones como dentistas de todo Tijuana.
    Con más de 25 años de experiencia, 
    con el mejor servicio y la más alta calidad, con un precio fantástico.  
    Haciendo un ambiente de confianza para todos nuestros clientes.
    Dando hasta nuestro mayor esfuerzo en cada una de nuestros trabajos porque para nosotros cuidar de tu sonrisa,
    Es lo más IMPORTANTE!`,
    en:`Muela Dental is one of the best options as dentists in Tijuana.
    With more than 25 years old experience, with the best service and the highest quality, with a fantastic price.
    Creating an environment of trust for all our clients.
    Giving the best effort in each of our jobs, because for us take care of your smile,
    It's really IMPORTANT!`
    },
    {
      es: 'Procedimientos',
      en: 'Procedures'
    },
  ];

  translateProcedures = [
    {
      es: 'Dentaduras o Placas Dentales',
      en: 'Dentures'
    },
    {
      es: 'Protesis Parciales Removibles',
      en: 'Removable Partial Dentures'
    },
    {
      es: 'Extracion Dental',
      en: 'Dental Extration'
    },
    {
      es: 'Endodoncia',
      en: 'Root Canal Therapy'
    },
    {
      es: 'Relleno Dental Blanco',
      en: 'White Dental Filling'
    }, 
    {
      es: 'Limpieza Dental',
      en: 'Dental Cleaning'
    },    
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = false;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });


      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
