import { Component, OnInit } from '@angular/core';
import { TraductorService } from 'src/app/services/traductor.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  constructor(private translate: TraductorService, private data: DataService) { }

  ngOnInit() {
  }

  slideOpts = {
    loop: true,
    autoplay: {
      delay: 2000,
    }
  };

}
