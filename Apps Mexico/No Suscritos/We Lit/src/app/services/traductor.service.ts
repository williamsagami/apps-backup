import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'We Lit Entertainment',
      en: 'We Lit Entertainment'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Seguro aceptado',
    'Evaluacion sin Costo',
    'Protectores Personalizados',
    'Blanqueamiento Dental',
  ];
  

  translateIndex = [
    {
    es:`We Lit se creó originalmente como una línea de ropa para todos y cada uno,
    nos consideramos parte de todas las culturas. Antes de que lo supiéramos
    estábamos involucrados con la industria del entretenimiento, comenzamos a trabajar con fotógrafos,
    producción y edición de videos, decidimos expandirnos a cualquier industria en la que podamos tener un impacto positivo y podamos reflejar lo que representamos.
    ¡ENTRETENIMIENTO! Así nació We Lit Entertainment. 1 año después, en 2020 We Lit tiene muchas sorpresas guardadas, así que estar atentos.`,
    en:`We Lit was originally created as a clothing line for anyone and everyone, 
    we consider ourselves part of every culture. Before we knew it, 
    we were involved with the entertainment industry, we started working with photographer's,
    video production and edition, we decided to expand into any industry we can have a positive impact on and we can reflect what we represent. 
    ENTERTAINMENT! That is how We Lit Entertainment came to life. 1 year later, 2020 We Lit has many surprises in store so stay tuned.`
    },
    {
      es: 'Procedimientos',
      en: 'Procedures'
    },
  ];

  translateProcedures = [
    {
      es: 'Seguro Aceptado',
      en: 'Insurance accepted'
    },
    {
      es: 'Evaluacion sin Costo',
      en: 'Evaluation without Cost'
    },
    {
      es: 'Protectores Personalizados',
      en: 'Mouthguards Custom Made'
    },
    {
      es: 'Blanqueamiento Dental',
      en: 'Dental Whitening'
    },
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = true;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });


      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
