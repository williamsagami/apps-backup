import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'Dentistas Asociados',
      en: 'Dentistas Asociados'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Seguro aceptado',
    'Evaluacion sin Costo',
    'Protectores Personalizados',
    'Blanqueamiento Dental',
  ];
  

  translateIndex = [
    {
      es:`Dentistas Asociados, ofrece servicio desde 1976, así siendo de los mejores dentistas de todo Tijuana.\n
       Aceptamos seguro y haciéndote una evaluación sin ningún costo, ofrecemos protectores bucales a tu medida,\n
       también blanqueamiento dental o dolores dentales.\n
       Porque para nosotros lo primordial es tu sonrisa.`,
      en:`Associate dentists, offering services since 1976 being one of the best dentists in all of Tijuana\n
      We accept insurancem and conduct an evaluation at no cost, we offer mouth guards tailored to your needs,\n
      as well as dental bleaching or dental pain\n
      Because for us the important thing is your smile.`
    },
    {
      es: 'Procedimientos',
      en: 'Procedures'
    },
  ];

  translateProcedures = [
    {
      es: 'Seguro Aceptado',
      en: 'Insurance accepted'
    },
    {
      es: 'Evaluacion sin Costo',
      en: 'Evaluation without Cost'
    },
    {
      es: 'Protectores Personalizados',
      en: 'Mouthguards Custom Made'
    },
    {
      es: 'Blanqueamiento Dental',
      en: 'Dental Whitening'
    },
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = false;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });


      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
