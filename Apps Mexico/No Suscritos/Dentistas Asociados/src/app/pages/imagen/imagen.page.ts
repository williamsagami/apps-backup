import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, LoadingController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-imagen',
  templateUrl: './imagen.page.html',
  styleUrls: ['./imagen.page.scss'],
})
export class ImagenPage implements OnInit {

  imageIndex: number;
  viewEntered = false;
  
  constructor(private modalController : ModalController, private navParams : NavParams, private loadingController: LoadingController, private data: DataService) { 
    this.imageIndex = this.navParams.get('index');
	}

  ngOnInit() {
    this.createLoading();
  }

  ionViewDidEnter() {
    this.viewEntered = true;
  }

  closeImage(){
	  this.modalController.dismiss();
  }

  async createLoading () {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 50,
    });
    await loading.present();
  }
  
  slideOpts = {
    loop: true,
    zoom: true,
    passiveListeners: false,
  };

}
