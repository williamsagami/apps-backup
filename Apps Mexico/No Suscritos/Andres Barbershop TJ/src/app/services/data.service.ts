import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  //slider
  sliderImages = [
    '../assets/images/1s.jpg',
    '../assets/images/2s.jpg',
    '../assets/images/3s.jpg',
    '../assets/images/4s.jpg',
  ]

  //galeria
  images = [
    'assets/images/1.jpg',
    'assets/images/2.jpg',
    'assets/images/3.jpg',
    'assets/images/4.jpg',
    'assets/images/5.jpg',
    //'assets/images/6.jpg',
    'assets/images/7.jpg',
     'assets/images/8.jpg',
     'assets/images/9.jpg',
    'assets/images/10.jpg',
    'assets/images/11.jpg',
    'assets/images/12.jpg',
    'assets/images/13.jpg',
    'assets/images/14.jpg',
    'assets/images/15.jpg',
    'assets/images/16.jpg',
    //'assets/images/17.jpg',
    'assets/images/18.jpg',
    //'assets/images/19.jpg',
    'assets/images/20.jpg',
    'assets/images/21.jpg',
    'assets/images/22.jpg',
    'assets/images/23.jpg',

  ]

  //contacto
  phones = [
    {
      number: 'Citas: (664) 588 1226', 
      call: '6645881226'
    },
    {
      number: '(664) 557 5629', 
      call: '6645881226'
    },
  ];
  direction = 'Carr. Libre Tijuana - Rosarito 5825 local 11 - A Plaza Montesinos Tejamen, Ampliacion. 22635, Tijuana, Baja California, Mexico';

  constructor() { }
}
