import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'Andres Barbershop TJ',
      en: 'Andres Barbershop TJ'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Corte Caballero',
    'Corte Niño (Menor de 9 años)',
    'Corte de Jubilado (62 ó +)',
    'Arreglo de Barba',
    'Afeitado Clasico',
    'Line Up',
    'Cejas',
    'Exfoliación',
    'Mascarilla Negra',
    'Decoracion y Tinte',
  ];
  

  translateIndex = [
    {
    es:`Una de las mejores barberías de todo Tijuana, teniendo de los mejores precio, 
    con paquetes especiales como: Platinos, Golden y Silver, con alta calidad en nuestros cortes para lucir tu estilo.
    Dando siempre nuestros clientes el mejor corte y un cambio de look original.
    Agenda tu cita para cualquier tipo de corte.`,
    en:`One of the best barbershops in all of Tijuana, having the best prices, with special packages such as: Platinum, Golden and Silver, 
    with high quality in our cuts to show off your style.
    Always giving our clients the best cut and have a original look.
    Schedule your appointment for any type of court.`
    },
    {
      es: 'Lista de Precios',
      en: 'Price list'
    },
  ];

  translateProcedures = [
    {
      es: 'Corte Caballero $150 MXN',
      en: 'Knight Court $150 MXN'
    },
    {
      es: 'Corte Niño (Menor de 9 años) $130 MXN',
      en: 'Child Court (Under 9 years) $130 MXN'
    },
    {
      es: 'Corte de Jubilado (62 ó +) $120 MXN',
      en: 'Retiree Court (62 or +) $120 MXN'
    },
    {
      es: 'Arreglo de Barba $150 MXN',
      en: 'Beard grooming $150 MXN'
    },
    {
      es: 'Afeitado Clasico $150 MXN',
      en: 'Classic Shave $150 MXN'
    }, 
    {
      es: 'Line Up $100 MXN',
      en: 'Line Up $100 MXN'
    },
    {
      es: 'Diseños (+) $80 MXN',
      en: 'Designs $80 MXN'
    },
    {
      es: 'Cejas $50 MXN',
      en: 'Eyebrows $50 MXN'
    },
    {
      es: 'Exfoliación $120 MXN',
      en: 'Exfoliation $120 MXN'
    },
    {
      es: 'Mascarilla Negra $120 MXN',
      en: 'black mask $120 MXN'
    },
    {
      es: 'Decoracion y Tinte (+) $700 MXN',
      en: 'Decoration and Dye $700 MXN'
    },
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = false;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });


      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
