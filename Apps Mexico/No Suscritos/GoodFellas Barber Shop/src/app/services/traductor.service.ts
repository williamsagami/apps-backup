import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'GoodFellas Barber Shop',
      en: 'GoodFellas Barber Shop'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Goldpack $400 (MXN',
    'Limpieza de Ceja $50 (MXN)',
    'Lavado de cabello $30 (MXN)',
    'Mascarilla $50 (MXN)',
    'Corte de niño $100 (MXN)',
    'Exfoliación $120 (MXN)',
    'Corte $150 (MXN)',
    'Barba con toalla  $120 (MXN)',
  ];
  

  translateIndex = [
    {
    es:`GoodFellas BarberShop, es una de las mejores barberías de todo Tijuana.
    Donde los invitamos a tener una experiencia de un corte de calidad, ofreciendo paquetes exclusivos Como el Goldpack.
    Incluyendo Corte, Cejas, lavado de cabello, mascarilla anti ojeras, mascarilla de charcoal, vello nasal y orejas, barba o Rasurado con toalla calientea a solo 400 Pesos.  
    También contamos con fibras capilares así llevándote el mejor servicio de la BarberShop.   
    No olvides agendar tu cita!.`,
    en:`GoodFellas Barber Shop, is one of the best barbershops in Tijuana.
    Where we invite you to have an experience of a quality cut, offering exclusive packages such as the Goldpack.
    Including cutting, eyebrows, hair washing, anti-dark circles mask, charcoal mask, nasal and ear hair, beard or shaving with a hot towel at only 400 pesos.
    We also have hair fibers thus bringing you the best service from the BarberShop.
    Do not forget to schedule your appointment`
    },
    {
      es: 'Lista de Precios',
      en: 'Price list'
    },
  ];

  translateProcedures = [
    {
      es: 'Goldpack $400 (MXN)',
      en: 'Goldpack $400 (MXN)'
    },
    {
      es: 'Limpieza de Ceja $50 (MXN)',
      en: 'Eyebrow Cleaning $50 (MXN)'
    },
    {
      es: 'Lavado de cabello $30 (MXN)',
      en: 'Hair wash $30 (MXN)'
    },
    {
      es: 'Mascarilla $50 (MXN)',
      en: 'Mask $50 (MXN)'
    },
    {
      es: 'Corte de niño $100 (MXN)',
      en: 'Boy cut $100 (MXN)'
    }, 
    {
      es: 'Exfoliación $120 (MXN)',
      en: 'Peeling $120 (MXN)'
    }, 
    {
      es: 'Corte $150 (MXN)',
      en: 'Cut $150 (MXN)'
    }, 
    {
      es: 'Barba con toalla  $120 (MXN)',
      en: 'Beard with towel $120 (MXN)'
    }, 
   
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = false;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });


      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
