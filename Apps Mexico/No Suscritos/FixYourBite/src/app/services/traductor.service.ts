import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  products = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'Fix Your Bite',
      en: 'Fix Your Bite'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Productos',
      en: 'Products'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Cambiar el Tamaño, la Forma y la Alineación de los Dientes',
    'Corregir Picaduras',
    'Aclarar Color de los Dientes.',
    'Reparación de Fracturas Dentales',
    'Reemplazar los Dientes Faltantes',
    'Reemplazar Tratamientos Dentales Viejos'
  ];
  video = '';

  translateIndex = [
    {
      es:`Hola! Soy el doctor David. Bienvenido a Fix Your Bite! \n
      Somos una clínica que se especializa en implantes dentales. Aceptamos aseguranzas de USA.
      Lo que nos hace mejores que otras clínicas en Tijuana que hacen implantes dentales es nuestro enfoque en tecnología. \n
      Tenemos la capacidad de hacer implantes dentales que se ven naturales en una sola visita. \n
      El poder fabricar nuestros propios implantes, nos permite darte precios competitivos con calidad y garantía. \n
      Nos enorgullecemos de nuestro servicio al cliente. Responderé tus llamadas y mensajes personalmente para resolver todas tus dudas.`,
      en:`Hi! I am Dr. David. Welcome to Fix Your Bite! \n
      We are a clinic that specializes in dental implants. We accept insurances from the USA. \n
      What makes us better than other clinics in Tijuana that do dental implants is our focus on technology. \n
      We have the ability to make natural looking dental implants in one visit. \n
      Being able to manufacture our own implants allows us to give you competitive prices with quality and guarantee. \n
      We take pride in our customer service. I will answer your calls and messages personally to solve all your doubts.`
    },
    {
      es: 'Procedimientos:',
      en: 'Procedures:'
    },
    {
      es: 'Clientes Satisfechos',
      en: 'Satisfied Customers'
    },
  ];

  translateProcedures = [
    {
      es: 'Implantes Dentales',
      en: 'Dental Implants'
    },
    {
      es: 'Coronas el Mismo Día',
      en: 'Same Day Crowns'
    },
    {
      es: 'Endodoncia',
      en: 'Root Canal Treatment'
    },
    {
      es: 'Cirugía Oral y Maxilofacial',
      en: 'Oral and Maxillofacial Surgery'
    },
    {
      es: 'Periodoncia',
      en: 'Periodontics'
    },
    {
      es: 'Alineadores Transparentes',
      en: 'Clear Aligners'
    },
    {
      es: 'Estética Facial (como Botox)',
      en: 'Facial Esthetics (such as Botox)'
    },
  ];

  //Productos
  jeunesse = '';
  translateJeunesse = {
    es: 'Ordena Ahora!',
    en: 'Order now!'
  }

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = true;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.products = this.translateMenu[3].es;
      this.contact = this.translateMenu[4].es;
      this.language = this.translateMenu[5].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });
      this.video = this.translateIndex[2].es;

      //productos
      this.jeunesse = this.translateJeunesse.es;

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.products = this.translateMenu[3].en;
      this.contact = this.translateMenu[4].en;
      this.language = this.translateMenu[5].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });
      this.video = this.translateIndex[2].en;

      //productos
      this.jeunesse = this.translateJeunesse.en;

      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
