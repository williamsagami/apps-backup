import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  //slider
  sliderImages = [
    '../assets/images/1s.jpg',
    '../assets/images/2s.jpg',
    '../assets/images/3s.jpg',
    '../assets/images/4s.jpg',
  ]

  //galeria
  images = [
    'assets/images/1.jpg',
    'assets/images/2.jpg',
    'assets/images/3.jpg',
    'assets/images/4.jpg',
    'assets/images/5.jpg',
    'assets/images/6.jpg',
    'assets/images/7.jpg',
    'assets/images/8.jpg',
    'assets/images/9.jpg',
    // 'assets/images/10.jfif',
    // 'assets/images/11.jfif',
    // 'assets/images/12.jfif',
    // 'assets/images/13.jfif',
    // 'assets/images/14.jfif',
    // 'assets/images/15.jfif',
    // 'assets/images/16.jfif',
  ]

  //productos
  products = [
    {
      image: 'assets/images/p1.jpg',
      url: 'https://emilyyromero.jeunesseglobal.com/es-MX/luminesce',
    },
    {
      image: 'assets/images/p2.jpg',
      url: 'https://emilyyromero.jeunesseglobal.com/es-MX/m1nd',
    },
    {
      image: 'assets/images/p3.jpg',
      url: 'https://emilyyromero.jeunesseglobal.com/es-MX/reserve',
    },
    {
      image: 'assets/images/p4.jpg',
      url: 'https://emilyyromero.jeunesseglobal.com/es-MX/naara',
    },
    {
      image: 'assets/images/p5.jpg',
      url: 'https://emilyyromero.jeunesseglobal.com/es-MX/restore',
    },
    {
      image: 'assets/images/p6.jpg',
      url: 'https://emilyyromero.jeunesseglobal.com/es-MX/vidacell',
    },
    {
      image: 'assets/images/p7.jpg',
      url: 'https://emilyyromero.jeunesseglobal.com/es-MX/revitablu',
    },
    {
      image: 'assets/images/p8.jpg',
      url: 'https://emilyyromero.jeunesseglobal.com/es-MX/rvl',
    },
  ]

  //contacto
  phones = [
    {
      number: 'MEX 664 656 7066',
      call: '6646567066'
    },
    {
      number: 'USA 619 309 8697',
      call: '6193098697'
    },
  ];

  direction = 'Avenida Mutualismo #931-B Entre Calle Tercera Cuarta y, Zona Centro, 22000 Tijuana, B.C.';

  constructor() { }
}
