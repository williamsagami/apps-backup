import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, LoadingController, IonSlides } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { TraductorService } from 'src/app/services/traductor.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.page.html',
  styleUrls: ['./producto.page.scss'],
})
export class ProductoPage implements OnInit {

  imageIndex: number;
  viewEntered = false;
  
  constructor(private modalController : ModalController, private navParams : NavParams, private loadingController: LoadingController, private data: DataService, private translate: TraductorService, private iab : InAppBrowser) { 
    this.imageIndex = this.navParams.get('index');
	}

  ngOnInit() {
    this.createLoading();
  }

  ionViewDidEnter() {
    this.viewEntered = true;
  }
 
  closeImage(){
	  this.modalController.dismiss();
  }

  openLink(link) {
	  this.iab.create(link);
  }

  async createLoading () {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 50,
    });
    await loading.present();
  }
  
  slideOpts = {
    loop: true,
    zoom: true,
    passiveListeners: false,
  };

}
