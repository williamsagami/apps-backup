import { Component, OnInit } from '@angular/core';
import { ModalController } from "@ionic/angular";
import { ProductoPage } from "../producto/producto.page";
import { TraductorService } from 'src/app/services/traductor.service';
import { DataService } from 'src/app/services/data.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {

  constructor(private modalController: ModalController, private translate: TraductorService, private data: DataService, private iab : InAppBrowser) { }

  ngOnInit() {
  }
 
  openLink(link) {
	  this.iab.create(link);
  }

  async openImage(i: number, l: string)
	{
    const modal = await this.modalController.create({
      component: ProductoPage,
      cssClass: 'modal-transparency',
      componentProps : {
        index: i,
        url: l
      }
    });
    await modal.present();
	}
}
