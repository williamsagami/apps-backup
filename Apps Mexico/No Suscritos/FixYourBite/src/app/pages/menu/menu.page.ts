import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { TraductorService } from 'src/app/services/traductor.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  constructor(private router: Router, private translate: TraductorService) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedUrl = event.url != null ? event.url : this.selectedUrl;
    });
  }

  pages = [
    {
      title: this.translate.home,
      url: '/menu/inicio',
      icon: "home",
      image: 'assets/images/m1.jpg'
    },
    {
      title: this.translate.gallery,
      url: '/menu/galeria',
      icon: "images",
      image: 'assets/images/m2.jpg'
    },
    {
      title: this.translate.products,
      url: '/menu/products',
      icon: "star",
      image: 'assets/images/m4.jpg'
    },
    {
      title: this.translate.contact,
      url: '/menu/contacto',
      icon: "person",
      image: 'assets/images/m3.jpg'
    }
  ];

  selectedUrl = '/menu/inicio';

  ngOnInit() {
    
  }

  changeLanguage() {
    this.translate.changeLanguage();
    this.pages[0].title = this.translate.home;
    this.pages[1].title = this.translate.gallery;
    this.pages[2].title = this.translate.products;
    this.pages[3].title = this.translate.contact;
  }
}
