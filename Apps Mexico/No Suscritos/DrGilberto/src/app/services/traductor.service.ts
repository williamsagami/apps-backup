import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'Dr Gilberto Molina',
      en: 'Dr Gilberto Molina'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Resina',
    'Implantes',
    'Endodoncia',
    'Ortodoncia',
    'Blanqueamiento',
    'Limpieza Dental',
  ];
  video = '';

  translateIndex = [
    {
      es:`Es uno de los mejores dentistas en Tijuana.\n
      Los procedimientos realizados son de excelente calidad y a un buen precio, garantizando que el cliente quedara satisfecho
      con cualquiera de ellos.\n
      Si quieres saber mas detalles o deseas agendar una cita, llamanos.\n
      Recuerda que tu bienestar y sonrisa es lo mas importante!`,
      en:`Is one of the best dentists in Tijuana. \n
      The procedures performed are of excellent quality and at a good price, guaranteeing that the client will be satisfied
      with any of them. \n
      If you want to know more details or want to schedule an appointment, call us. \n
      Remember that your well-being and smile is the most important thing!`
    },
    {
      es: 'Procedimientos:',
      en: 'Procedures:'
    },
    {
      es: 'Clientes Satisfechos',
      en: 'Satisfied Customers'
    },
  ];

  translateProcedures = [
    {
      es: 'Resina',
      en: 'Resin'
    },
    {
      es: 'Implantes',
      en: 'Implants'
    },
    {
      es: 'Endodoncia',
      en: 'Endodontics'
    },
    {
      es: 'Ortodoncia',
      en: 'Orthodontics'
    },
    {
      es: 'Blanqueamiento',
      en: 'Whitening'
    },
    {
      es: 'Limpieza Dental',
      en: 'Dental Cleaning'
    },
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = false;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });
      this.video = this.translateIndex[2].es;

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });
      this.video = this.translateIndex[2].en;

      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
