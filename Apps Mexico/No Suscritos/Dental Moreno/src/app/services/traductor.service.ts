import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'Dental Moreno',
      en: 'Dental Moreno'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    // 'Dentaduras o Placas Dentales',
    // 'Protesis Parciales Removibles',
    // 'Extracion Dental',
    // 'Rellenos Dentales',
    // 'Limpieza Dental y Eliminación de Sarro',
  ];
  

  translateIndex = [
    {
    es:`Dental Moreno es uno de los mejores ortodoncistas de todo Rosarito.
    Ofrecemos los mejores precios y servicios, buscando tu mejor sonrisa, logramos que nuestros clientes queden satisfechos,
    tenemos un buen ambiente familiar para nuestros clientes,
    porque para nosotros tu sonrisa es de lo mas importantes.
    Haz tu cita ahora para obtener unos dientes blancos`,
    en:`Dental Moreno is the best orthodontists in Rosarito.
    We offer the best prices and services, looking for your best smile, we ensure that our clients are satisfied,
    we have a good family atmosphere for our clients,
    because for us your smile is the most important.
    Make your appointment now to get white teeth`
    },
    {
      es: 'Procedimientos',
      en: 'Procedures'
    },
  ];

  translateProcedures = [
    {
      es: 'Dentaduras o Placas Dentales',
      en: 'Dentures'
    },
    {
      es: 'Protesis Parciales Removibles',
      en: 'Removable Partial Dentures'
    },
    {
      es: 'Extracion Dental',
      en: 'Dental Extration'
    },
    {
      es: 'Endodoncia',
      en: 'Root Canal Therapy'
    },
    {
      es: 'Relleno Dental Blanco',
      en: 'White Dental Filling'
    }, 
    {
      es: 'Limpieza Dental',
      en: 'Dental Cleaning'
    },    
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = false;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });


      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
