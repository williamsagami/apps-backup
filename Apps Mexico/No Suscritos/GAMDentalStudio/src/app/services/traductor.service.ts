import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'GAM Dental Studio',
      en: 'GAM Dental Studio'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Resina',
    'Implantes',
    'Endodoncia',
    'Ortodoncia',
    'Puentes Fijos',
    'Blanqueamiento',
    'Placas Totales',
    'Limpieza Dental',
    'Placas Parciales',
  ];
  video = '';

  translateIndex = [
    {
      es:`Ofrecemos los mejores servicios de Odontología General en Tijuana.\n
      Todos los trabajos que realizamos son de excelente calidad y a un buen precio, por ello tenemos la confianza de garantizar que quedaras satisfecho
      con cualquiera de nuestros procedimientos.\n
      Si quieres saber mas detalles sobre nuestro trabajo o deseas agendar una cita, contactanos.\n
      Y Recuerda que para nosotros la sonrisa de nuestros pacientes es lo mas importante!`,
      en:`We offer the Best Services on General Odonthology in Tijuana.\n
      All of the work we do has excellent quality and a good price, and because of that we can guarantee that you will be satisfied
      with any of the precedures we provide you.\n
      If you want to learn more about our work or you want to make a date, contact us.\n
      And Remember that for us the smile of our patients is what matters!`
    },
    {
      es: 'Procedimientos:',
      en: 'Procedures:'
    },
    {
      es: 'Videos',
      en: 'Videos'
    },
  ];

  translateProcedures = [
    {
      es: 'Resina',
      en: 'Resin'
    },
    {
      es: 'Implantes',
      en: 'Implants'
    },
    {
      es: 'Endodoncia',
      en: 'Endodontics'
    },
    {
      es: 'Ortodoncia',
      en: 'Orthodontics'
    },
    {
      es: 'Puentes Fijos',
      en: 'Fixed Bridges'
    },
    {
      es: 'Blanqueamiento',
      en: 'Whitening'
    },
    {
      es: 'Placas Totales',
      en: 'Total Plates'
    },
    {
      es: 'Limpieza Dental',
      en: 'Dental Cleaning'
    },
    {
      es: 'Placas Parciales',
      en: 'Partial Plates'
    },
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = false;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });
      this.video = this.translateIndex[2].es;

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });
      this.video = this.translateIndex[2].en;

      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
