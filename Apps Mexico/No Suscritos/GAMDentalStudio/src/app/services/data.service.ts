import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  //slider
  sliderImages = [
    '../assets/images/1s.jpg',
    '../assets/images/2s.jpg',
    '../assets/images/3s.jpg',
    '../assets/images/4s.jpg',
  ]

  //galeria
  images = [
    'assets/images/1.jpg',
    'assets/images/2.jpg',
    'assets/images/3.jpg',
    'assets/images/4.jpg',
    'assets/images/5.jpg',
    'assets/images/6.jpg',
    'assets/images/7.jpg',
    'assets/images/8.jpg',
    'assets/images/9.jpg',
    'assets/images/10.jpg',
    'assets/images/11.jpg',
    'assets/images/12.jpg',
    // 'assets/images/13.jpg',
    // 'assets/images/14.jpg',
    // 'assets/images/15.jpg',
    // 'assets/images/16.jpg',
    // 'assets/images/17.jpg',
  ]

  //contacto
  phones = [
    {
      number: 'CEL: 664 670 7400', 
      call: '6646707400'
    },
  ];

  direction = 'Calle Tercera 8364, Zona Centro, 22000 Tijuana, B.C.';

  constructor() { }
}
