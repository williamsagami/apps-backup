import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-imagen',
  templateUrl: './imagen.page.html',
  styleUrls: ['./imagen.page.scss'],
})
export class ImagenPage implements OnInit {

  imageIndex: number;
  viewEntered = false;

  images = [
    'assets/images/1.jfif',
    'assets/images/2.jfif',
    'assets/images/3.jfif',
    'assets/images/4.jfif',
    'assets/images/5.jfif',
    'assets/images/6.jfif',
    'assets/images/7.jfif',
    'assets/images/8.jfif',
    'assets/images/10.jfif',
    'assets/images/11.jfif',
    'assets/images/12.jfif',
  ]

  constructor(private modalController : ModalController, private navParams : NavParams, private loadingController: LoadingController) { 
    this.imageIndex = this.navParams.get('index');
	}

  ngOnInit() {
    this.createLoading();
  }

  ionViewDidEnter() {
    this.viewEntered = true;
  }

  closeImage(){
	  this.modalController.dismiss();
  }

  async createLoading () {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 50,
    });
    await loading.present();
  }
  
  slideOpts = {
    loop: true,
    zoom: true,
    passiveListeners: false,
  };

}
