import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-llamada',
  templateUrl: './llamada.page.html',
  styleUrls: ['./llamada.page.scss'],
})
export class LlamadaPage implements OnInit {

  constructor(private callNumber: CallNumber) { }

  ngOnInit() {
  }

  phoneCall(number: string) {
	  this.callNumber.callNumber(number, true)
	  .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
	}

}
