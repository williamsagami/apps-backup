import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'Atelier Dental',
      en: 'Atelier Dental'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Prótesis fijas',
    'Prótesis Removibles',
    'Ortodoncia',
    'Implantes dentales',
    'Coronas de Zirconia',
    'Ortodoncia',
    'Retenedores circunferenciales',
    'Retenedores Hawley',
    'Guardas de acrílico',
    'Juego de Placas',
    'Coronas de Porcelana',
    'Prótesis Dentales',
  ];
  video = '';

  translateIndex = [
    {
      es:`Atelier Dental es una de las mejores empresas de todo Tijuana 
      Comprometida a entrar nuestro mercado para producir las mejores piezas dentales de Baja California,\n
      con un gran equipo técnico especializado con más de 13 años de experiencia.\n
      Contamos con tecnología novedosa digital para prótesis produciendo coronas el mismo día, brindando una excelente calidad en nuestros productos.`,
      en:`Atelier Dental is one of the best companies in Tijuana\n
      Committed to producing the best dental pieces in Baja California\n
      With an excellent specialized technical team\n
      We have 13 years of experience using innovative digital prosthetic technology\n
      Producing dental crown on the same day, giving an excellent quality in our products.`
    },
    {
      es: 'Procedimiento:',
      en: 'Procedures:'
    },
    {
      es: 'Metal Porcelana',
      en: 'Porcelain Metal'
    },
  ];

  translateProcedures = [
    {
      es: 'Prótesis Fijas',
      en: 'Fixed Prostheses'
    },
    {
      es: 'Prótesis Removibles',
      en: 'Removable Prosthetics'
    },
    {
      es: 'Ortodoncia',
      en: 'Orthodontics'
    },
 {
     es: 'Implantes Dentales',
     en: 'Dental Implants'
    },
    {
       es: 'Coronas de Zirconia',
       en: 'Zirconia Crowns'
    },
   {
     es: 'Retenedores Hawley',
     en: 'Hawley Retainers'
   },
     {
       es: 'Retenedores Circunferenciales',
      en: 'Circumferential Retainers'
     },
     {
       es: 'Guardas de Acrílico',
       en: 'Acrylic Guards'
     },
    {
      es: 'Juego de Placas',
      en: 'Plate Set'
    },
    {
      es: 'Coronas de Porcelana',
      en: 'Porcelain Crowns'
    },
    {
      es: 'Prótesis Dentales',
      en: 'Dentures'
    },
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = false;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });
      this.video = this.translateIndex[2].es;

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });
      this.video = this.translateIndex[2].en;

      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
