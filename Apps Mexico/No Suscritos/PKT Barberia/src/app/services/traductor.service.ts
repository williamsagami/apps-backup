import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'PKT Barbería',
      en: 'PKT Barbería'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedures0 = '';
  procedures1 = '';
  procedures2 = '';
  procedureList = [

    'Numero 1 o 1.5',
    'Numero 0 o 0.5',
    'Doble Cero',
    'Rasurado',
  ];
  procedureList1 = [
    'Nnumero 1 o 1.5',
    'Numero 0 o 0.5',
    'Doble Cero',
    'Rasurado',
  ];
  procedureList2 = [
    'Numero 1 o 1.5',
    'Con Numero 0 o 0.5',
    'Con Doble Cero',
    'Con Doble Cero',
    'Rasurado',
  ];
  procedureList0 = [
    'Lunes a Sabado 10:00AM a 8:00PM',
    'Domingo 10:00AM a 4:00PM',
  ];
  

  translateIndex = [
    {
    es:`PKT Barbería una de las mejores barberías de todo Tijuana, con un talento increíble, buscando día a día ir mejorando nuestra calidad y
    dando el mejor servicio en cada uno de nuestros cortes para que así puedas llegar a tener un buen porte.
    Buscando que tú tengas el mejor estilo en tu cabello.
    Agenda tu cita ahora!.`,
    en:`PKT Barbería is one of the best barbershops in Tijuana, with incredible talent, seeking day by day to improve our quality and
    giving the best service in each of our cuts so that you can get to have a good bearing.    
    Looking for you to have the best style in your hair.
    Schedule your appointment now!`
    },
    {
      es: 'Desvanecido Medio',
      en: 'Medium Fade'
    },
    {
      es: 'Desvanecido Bajo',
      en: 'Faded Low'
    },
    {
      es: 'Mohawk',
      en: 'Mohawk'
    },
    {
      es: 'Horario',
      en: 'Mohawk'
    },
  ];
  translateProcedures0 = [
    {
      es: 'Lunes a Sabado 10:00AM a 8:00PM',
      en: 'Monday to Saturday 10:00AM to 8:00PM'
    },
    {
      es: 'Domingo 10:00AM a 4:00PM',
      en: 'Sunday 10:00AM a 4:00PM'
    },
  ];
  translateProcedures = [
    {
      es: 'Numero 1 o 1.5',
      en: 'Number 1 or 1.5'
    },
    {
      es: 'Numero Cero o 0.5',
      en: 'Number 0 o 0.5'
    },
    {
      es: 'Doble Cero',
      en: 'With Double Zero'
    },
    {
      es: 'Rasurado',
      en: 'Shaved'
    },  
  ];
  translateProcedures1 = [
    {
      es: 'Numero 1 o 1.5',
      en: 'Number 1 or 1.5'
    },
    {
      es: 'Numero Cero o 0.5',
      en: 'Number 0 o 0.5'
    },
    {
      es: 'Con Doble Cero',
      en: 'Double Zero'
    },
    {
      es: 'Rasurado',
      en: 'Shaved'
    },  
  ];
  translateProcedures2 = [
    {
      es: 'Numero 2 a 6',
      en: 'Number 2 a 6'
    },
    {
      es: 'Numero 1 o 1.5',
      en: 'Number 1 o 1.5'
    },
    {
      es: 'Numero 0 o 0.5',
      en: 'Number 0 o 0.5'
    },
    {
      es: 'Doble Cero',
      en: 'Double Zero'
    },  
    {
      es: 'Rasurado',
      en: 'Shaved'
    },  
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = false;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.procedures1 = this.translateIndex[2].es;
      this.procedures2 = this.translateIndex[3].es;
      this.procedures0 = this.translateIndex[4].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });
      this.translateProcedures1.forEach((procedure, index) => {
        this.procedureList1[index] = procedure.es;
      });
      this.translateProcedures2.forEach((procedure, index) => {
        this.procedureList2[index] = procedure.es;
      });
      this.translateProcedures0.forEach((procedure, index) => {
        this.procedureList0[index] = procedure.es;
      });
      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.procedures1 = this.translateIndex[2].en;
      this.procedures2 = this.translateIndex[3].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });
      this.translateProcedures1.forEach((procedure, index) => {
        this.procedureList1[index] = procedure.en;
      });
      this.translateProcedures2.forEach((procedure, index) => {
        this.procedureList2[index] = procedure.en;
      });
      this.translateProcedures0.forEach((procedure, index) => {
        this.procedureList0[index] = procedure.en;
      });

      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
