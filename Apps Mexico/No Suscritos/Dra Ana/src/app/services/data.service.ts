import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { LlamadaPage } from '../pages/llamada/llamada.page';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  //slider
  sliderImages = [
    '../assets/images/1s.jpg',
    '../assets/images/2s.jpg',
    '../assets/images/3s.jpg',
    '../assets/images/4s.jpg',
  ]

  //galeria
  images = [
    'assets/images/1.jpg',
    'assets/images/2.jpg',
    'assets/images/3.jpg',
    'assets/images/4.jpg',
    'assets/images/5.jpg',
    'assets/images/6.jpg',
    'assets/images/7.jpg',
     'assets/images/8.jpg',
     'assets/images/9.jpg',
    'assets/images/10.jpg',
    'assets/images/11.jpg',
    'assets/images/12.jpg',
   // 'assets/images/13.jpg',
    'assets/images/14.jpg',
    'assets/images/15.jpg',
    'assets/images/16.jpg',
    'assets/images/17.jpg',
    'assets/images/18.jpg',
    'assets/images/19.jpg',
    'assets/images/20.jpg',
    'assets/images/21.JPG',
    'assets/images/22.JPG',
    'assets/images/23.JPG',
    'assets/images/24.JPG',
    'assets/images/25.JPG',
    'assets/images/26.JPG',
   // 'assets/images/27.JPG',


  ]

  //contacto
  phones = [
    {
      number: 'Citas: (661) 612-1302', 
      call: '6616121302'
    },
    {
      number: 'USA: (619) 906-7564', 
      call: '6199067564'
    },
  ];
  direction = 'Blvd. Benito Juárez 4157/C, HACIENDA FLORESTA, 22703 Rosarito, B.C., Mexico';

  // redesSociales = [
  //   {
  //     name: "Muela Dental",
  //     link: "https://www.facebook.com/Mueladental",
  //     icon: "logo-facebook",
  //   },
  //   {
  //     name: "mueladental",
  //     link: "https://www.instagram.com/mueladental/?hl=es-la",
  //     icon: "logo-instagram",
  //   }
  // ];
  constructor(private modalController: ModalController, private iab : InAppBrowser) { }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: LlamadaPage ,
      componentProps : {
        number: phoneNumber
      }
    });
    await modal.present();
  }

  openLink(link: string) {
      this.iab.create(link);
  }

}
