import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'Clinica Dental',
      en: 'Clinica Dental'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Odontología',
    '-----Preventiva',
    '-----Restauradora',
    '-----Cosmética',
    'Ortodoncia',
    'Endodoncia',
    'Odontopediatría',
    'Cirugía Bucal',
    'Implantes',
    'RX Digital',
  ];
  

  translateIndex = [
    {
    es:`Clínica Dental Dra. Ana Luisa Rodríguez y Asociados es de las mejores clínicas dentales de todo Rosarito.
    Con especialistas en ortodoncia y endodoncia, para atender tu salud bocal gracias a la amplia experiencia de nuestros especialistas,
    Tenemos una gran gama de diferentes servicios, brindando la mayor confianza de tu sonrisa cuidándote a ti y a toda tu familia.
    ¡Agenda tu cita con nosotros, para ser atendido por profesionales!.`,
    en:`Dental Clinic Dra. Ana Luisa Rodríguez y Asociados is the best dental clinics in Rosarito.
    With specialists in orthodontics and endodontics, to attend to your oral health thanks to the extensive experience of our specialists,
    We have a wide range of different services, providing the greatest confidence in your smile taking care of you and your whole family.
    Schedule your appointment with us, to be attended by professionals`
    },
    {
      es: 'Procedimientos',
      en: 'Procedures'
    },
  ];

  translateProcedures = [
    {
      es: 'Odontología',
      en: 'odontology'
    },
    {
      es: '-----Preventiva',
      en: '-----Preventive'
    },
    {
      es: '-----Restauradora',
      en: "-----Restorative"
    },
    {
      es: '-----Cosmética',
      en: '-----Cosmetics'
    },
    {
      es: 'Ortodoncia',
      en: 'Orthodontics'
    }, 
    {
      es: 'Endodoncia',
      en: 'Endodontics'
    },   
    {
      es: 'Odontopediatría',
      en: 'Pediatric Dentistry'
    },   
    {
      es: 'Cirugía Bucal',
      en: 'Oral Surgery'
    },    
    {
      es: 'Implantes',
      en: 'Implants'
    },    
    {
      es: 'RX Digital',
      en: 'RX Digital'
    }, 
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = false;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });


      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
