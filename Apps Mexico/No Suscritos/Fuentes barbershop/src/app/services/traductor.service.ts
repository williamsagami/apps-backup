import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'Fuentes Barbershop',
      en: 'Fuentes Barbershop'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Diseños $100 a $250 (MXN)',
    'Corte y barba $200 (MXN)',
    'Barba $120 (MXN)',
    'Corte de cabello $120(MXN)',
    'Mascarilla pilaten $50.00 (MXN)',
  ];
  

  translateIndex = [
    {
    es:`Una de las mejores BarberShop de Tijuana, 
    contamos con excelentes productos en venta para el cuidado de tu cabello.
    Teniendo de los mejores precios, brindando una experiencia unica y un buen servicio.
    Contáctanos y agenda tu cita.`,
    en:`One of the best BarberShop in Tijuana,
    We have excellent products for sale to care for your hair.
    Having the best prices, providing a unique experience and good service.
    Contact us and schedule your appointment.`
    },
    {
      es: 'Lista de Precios',
      en: 'Price list'
    },
  ];

  translateProcedures = [
    {
      es: 'Diseños $100 a $250 (MXN)',
      en: 'Designs $100 a $250 (MXN)'
    },
    {
      es: 'Corte y barba $200 (MXN)',
      en: 'Cut and beard $200 (MXN)'
    },
    {
      es: 'Barba $120 (MXN)',
      en: 'Beard $ 120 (MXN)'
    },
    {
      es: 'Corte de cabello $120 (MXN)',
      en: 'haircut $120 (MXN)'
    },
    {
      es: 'Mascarilla pilaten $50.00 (MXN)',
      en: 'Pilaten mask $50 (MXN)'
    }, 
   
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = false;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });


      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
