import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-imagen',
  templateUrl: './imagen.page.html',
  styleUrls: ['./imagen.page.scss'],
})
export class ImagenPage implements OnInit {

  imagePath : string;

  constructor(private modalController : ModalController, private navParams : NavParams) { 
	  this.imagePath = this.navParams.get('src');
	}

  ngOnInit() {
  }

  closeImage(){
	  this.modalController.dismiss();
	}

}
