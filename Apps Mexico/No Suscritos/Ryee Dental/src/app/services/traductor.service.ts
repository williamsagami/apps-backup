import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TraductorService {

  //Menu
  appTitle = '';
  home = '';
  gallery = '';
  contact = '';
  language = '';

  translateMenu = [
    {
      es: 'Ryee Dental',
      en: 'Ryee Dental'
    },
    {
      es: 'Inicio',
      en: 'Home'
    },
    {
      es: 'Galeria',
      en: 'Gallery'
    },
    {
      es: 'Contacto',
      en: 'Contact'
    },
    {
      es: 'ES',
      en: 'EN'
    },
  ];

  //Inicio
  description = '';
  procedures = '';
  procedureList = [
    'Odontología',
    '-----Preventiva',
    '-----Restauradora',
    '-----Cosmética',
    'Ortodoncia',
    'Endodoncia',
    'Odontopediatría',
    'Cirugía Bucal',
    'Implantes',
    'RX Digital',
  ];
  

  translateIndex = [
    {
    es:`Ryee Dnetal, es uno de las mejores Clínicas de Todo Rosarito.
    Donde el personal es profesional y atento dedicando la prioridad a nuestros clientes,
    contando con la más alta calidad y con variedad a todos nuestros servicios.
    Brindando satisfacción y seguridad para sus necesidades.`,
    en:`Ryee Dnetal, is the best Clinics in Rosarito.
    Where the staff is professional and attentive, dedicating priority to our clients,
    counting on the highest quality and variety of all our services.
    Providing satisfaction and security for their needs.`
    },
    {
      es: 'Procedimientos',
      en: 'Procedures'
    },
  ];

  translateProcedures = [
    {
      es: 'Odontología',
      en: 'odontology'
    },
    {
      es: '-----Preventiva',
      en: '-----Preventive'
    },
    {
      es: '-----Restauradora',
      en: "-----Restorative"
    },
    {
      es: '-----Cosmética',
      en: '-----Cosmetics'
    },
    {
      es: 'Ortodoncia',
      en: 'Orthodontics'
    }, 
    {
      es: 'Endodoncia',
      en: 'Endodontics'
    },   
    {
      es: 'Odontopediatría',
      en: 'Pediatric Dentistry'
    },   
    {
      es: 'Cirugía Bucal',
      en: 'Oral Surgery'
    },    
    {
      es: 'Implantes',
      en: 'Implants'
    },    
    {
      es: 'RX Digital',
      en: 'RX Digital'
    }, 
  ];

  //Contacto
  phones = '';
  call = '';
  direction = '';
  location ='';
  social = '';

  translateContact = [
    {
      es: 'Telefonos',
      en: 'Phones'
    },
    {
      es: 'Llamar',
      en: 'Call'
    },
    {
      es: 'Direccion',
      en: 'Direction'
    },
    {
      es: 'Ubicacion',
      en: 'Location'
    },
    {
      es: 'Redes Sociales',
      en: 'Social Media'
    },
  ];
  

  constructor() {
    this.changeLanguage();
  }

  translate = false;

  changeLanguage() {
    if(!this.translate) {
      //menu 
      this.appTitle = this.translateMenu[0].es;
      this.home = this.translateMenu[1].es;
      this.gallery = this.translateMenu[2].es;
      this.contact = this.translateMenu[3].es;
      this.language = this.translateMenu[4].es;

      //inicio
      this.description = this.translateIndex[0].es;
      this.procedures = this.translateIndex[1].es;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.es;
      });

      //contacto
      this.phones = this.translateContact[0].es;
      this.call = this.translateContact[1].es;
      this.direction = this.translateContact[2].es;
      this.location =this.translateContact[3].es;
      this.social = this.translateContact[4].es;
    }
    else {
      //menu 
      this.appTitle = this.translateMenu[0].en;
      this.home = this.translateMenu[1].en;
      this.gallery = this.translateMenu[2].en;
      this.contact = this.translateMenu[3].en;
      this.language = this.translateMenu[4].en;

      //inicio
      this.description = this.translateIndex[0].en;
      this.procedures = this.translateIndex[1].en;
      this.translateProcedures.forEach((procedure, index) => {
        this.procedureList[index] = procedure.en;
      });


      //contacto
      this.phones = this.translateContact[0].en;
      this.call = this.translateContact[1].en;
      this.direction = this.translateContact[2].en;
      this.location =this.translateContact[3].en;
      this.social = this.translateContact[4].en;
    }

    this.translate = !this.translate;
  }
}
