import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { LlamadaPage } from '../pages/llamada/llamada.page';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  //slider
  sliderImages = [
    '../assets/images/1s.jpg',
    '../assets/images/2s.jpg',
    '../assets/images/3s.jpg',
    '../assets/images/4s.jpg',
  ]

  //galeria
  images = [
    'assets/images/1.jpg',
    'assets/images/2.jpg',
    'assets/images/3.jpg',
    'assets/images/4.jpg',
    'assets/images/5.jpg',
    'assets/images/6.jpg',
    'assets/images/7.jpg',
    // 'assets/images/8.JPG',
     'assets/images/9.jpg',
  //  'assets/images/10.jpg',
    'assets/images/11.jpg',
    'assets/images/12.jpg',
    'assets/images/13.jpg',
    'assets/images/14.jpg',
    'assets/images/15.jpg',
    'assets/images/16.jpg',
    'assets/images/17.jpg',
    //'assets/images/18.jpg',
    'assets/images/19.jpg',
    'assets/images/20.jpg',
    //'assets/images/21.jpg',
   // 'assets/images/22.JPG',
  ]

  //contacto
  phones = [
    {
      number: 'Citas: (661) 100 1330', 
      call: '6611001330'
    },
    {
      number: 'USA: (619) 906 7436', 
      call: '6199067436'
    },
  ];
  direction = 'Blvd. Ignacio L. Rayón 1001 Fracc. Independencia 22710 Rosarito, Baja California, México';

  redesSociales = [
    {
      name: "Ryee Dental",
      link: "https://www.facebook.com/Ryee-Dental-1735232066516794/",
      icon: "logo-facebook",
    },
    {
      name: "ryeedental.com.mx",
      link: "http://ryeedental.com.mx/home.html",
      icon: "globe",
    }
  ];
  constructor(private modalController: ModalController, private iab : InAppBrowser) { }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: LlamadaPage ,
      componentProps : {
        number: phoneNumber
      }
    });
    await modal.present();
  }

  openLink(link: string) {
      this.iab.create(link);
  }

}
