import { Component, OnInit } from '@angular/core';
import { ModalController } from "@ionic/angular";
import { ImagenPage } from "../imagen/imagen.page";

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.page.html',
  styleUrls: ['./galeria.page.scss'],
})
export class GaleriaPage implements OnInit {

  constructor(private modalController: ModalController) { }

  images = [
    'assets/images/1.jfif',
    'assets/images/2.jfif',
    'assets/images/3.jfif',
    'assets/images/4.jfif',
    'assets/images/5.jfif',
    'assets/images/6.jfif',
    'assets/images/7.jfif',
    'assets/images/8.jfif',
    'assets/images/10.jfif',
    'assets/images/11.jfif',
    'assets/images/12.jfif',
    'assets/images/13.jfif',
    'assets/images/14.jfif',
    'assets/images/15.jfif',
    'assets/images/16.jfif',
    'assets/images/17.jfif',
    'assets/images/18.jfif',
    'assets/images/19.jfif',
    'assets/images/20.jfif',
    'assets/images/21.jfif',
    'assets/images/22.jfif',
    'assets/images/23.jfif',
    'assets/images/24.jfif',
  ]

  ngOnInit() {
  }

  async openImage(i: number)
	{
    const modal = await this.modalController.create({
      component: ImagenPage,
      cssClass: 'modal-transparency',
      componentProps : {
        index: i+1
      }
    });
    await modal.present();
	}
}
