import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  pages = [
    {
      title: 'Inicio',
      url: '/menu/inicio',
      icon: "home",
      image: 'assets/images/m1.jpg'
    },
    {
      title: 'Galeria',
      url: '/menu/galeria',
      icon: "images",
      image: 'assets/images/m2.jpg'
    },
    {
      title: 'Contacto',
      url: '/menu/contacto',
      icon: "person",
      image: 'assets/images/m3.jpg'
    }
  ];

  selectedUrl = '/menu/inicio';

  constructor(private router: Router) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedUrl = event.url != null ? event.url : this.selectedUrl;
    });
   }

  ngOnInit() {
    
  }
}
