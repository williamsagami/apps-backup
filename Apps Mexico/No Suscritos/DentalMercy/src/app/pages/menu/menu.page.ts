import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  selectedUrl = '/menu/inicio';

  pages = [
    {
      url: '/menu/inicio',
      icon: "home",
    },
    /*{
      url: '/menu/citas',
      icon: "list",
    },*/
    {
      url: '/menu/galeria',
      icon: "images",
    },
    {
      url: '/menu/contacto',
      icon: "person",
    }
  ]; 

  constructor(private router: Router, private data: DataService) { 
    this.router.events.subscribe((event: RouterEvent) => {
    this.selectedUrl = event.url != null ? event.url : this.selectedUrl;
  }); 
  }

  ngOnInit() {
  }

}
