import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: 'menu',
    component: MenuPage,
    children: [
      {
        path: 'inicio',
        loadChildren: () => import('../inicio/inicio.module').then( m => m.InicioPageModule)
      },
      /*{
        path: 'citas',
        loadChildren: () => import('../citas/citas.module').then( m => m.CitasPageModule)
      },*/
      {
        path: 'galeria',
        loadChildren: () => import('../galeria/galeria.module').then( m => m.GaleriaPageModule)
      },
      {
        path: 'contacto',
        loadChildren: () => import('../contacto/contacto.module').then( m => m.ContactoPageModule)
      },
    ]
  },
  {
    path: '',
    redirectTo: '/menu/inicio',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
