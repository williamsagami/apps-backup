import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-image',
  templateUrl: './image.page.html',
  styleUrls: ['./image.page.scss'],
})
export class ImagePage implements OnInit {

  imageIndex: number;
  viewEntered = false;
  images = [];
  
  constructor(private modalController : ModalController, private navParams : NavParams, private loadingController: LoadingController) { 
    this.imageIndex = this.navParams.get('index');
    this.images = this.navParams.get('images');
	}

  ngOnInit() {
    this.createLoading();
  }

  ionViewDidEnter() {
    this.viewEntered = true;
  }

  closeImage(){
	  this.modalController.dismiss();
  }

  async createLoading () {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 50,
    });
    await loading.present();
  }
  
  slideOpts = {
    loop: true,
    zoom: true,
    passiveListeners: false,
  };

}
