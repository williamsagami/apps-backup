import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-citas',
  templateUrl: './citas.page.html',
  styleUrls: ['./citas.page.scss'],
})
export class CitasPage implements OnInit {

  solicitudes = [
    {
      user: "Memo",
      state: 0,
      day: "Lunes",
      hour: 10,
      rf: true,
      rfb: 1,
      rfa: 1,
    },
    {
      user: "Qlo",
      day: "Lunes",
      hour: 10,
      rf: false,
      rfb: 1,
      rfa: 1,
    },
    {
      user: "Qlo",
      day: "Lunes",
      hour: 10,
      rf: false,
      rfb: 1,
      rfa: 1,
    },
  ]

  constructor(private data: DataService) { }

  ngOnInit() {
  }

}
