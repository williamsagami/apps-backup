import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallPage } from '../pages/call/call.page';

@Injectable({
  providedIn: 'root'
})

//Citas Funcionalidades
// enum ApointmentState {
//   InProgress = 0,
//   Accepted = 1,
//   Rejected = 2
// }

export class DataService {

  //CONTACT BUTTON
  numeros = [
    "01152 664 685 1863",
    "664 614 9219",
    "664 775 1160",
    "619 796 1265"
  ];

  redesSociales = [
    {
      name: "TJ Mercy Dental",
      link: "https://www.facebook.com/TJ-Mercy-Dental-Implants-Ortho-Braces-101612784796907/",
      icon: "logo-facebook",
    },
    {
      name: "tjmercydentalimplntsnortho",
      link: "https://www.instagram.com/tjmercydentalimplntsnortho/",
      icon: "logo-instagram",
    },
    {
      name: "TJOrtho",
      link: "https://twitter.com/TjOrtho",
      icon: "logo-twitter",
    },
    {
      name: "619 796 1265",
      link: "none",
      icon: "logo-whatsapp",
    }
  ];

  //TRASLATIONS
  currentTranslation = 1;
  translationNumber = 2;
  t_Lenguaje = ["ES", "EN"];
  t_Titulo = "Mercy Dental";

  //Menu
  t_Menu = [
    ["INICIO", "HOME"],
    //["CITAS", "APOINTMENTS"],
    ["GALERIA", "GALLERY"],
    ["CONTACTO", "CONTACT"],
  ];

  //Inicio
  t_Inicio = ["INICIO", "HOME"];
  t_Dato = [
    `Esto no es solo extracción, sino que en el interior del alvéolo hay injerto óseo y membrana. Aconsejamos a nuestros pacientes que en el futuro 
    si quieren implantes, se necesita hueso GRUESO no delgado. <br>
    Poco hueso no podrá hacer implantes, pero las prótesis removibles en el mercado 200 años o el puente duran 5-10-15 años máximo
    dependiendo de la higiene del paciente. 
    (30 años de experiencia son importantes para los beneficios del paciente).`,

    `This is not just extraction but deep inside the socket is bone graft and membrane. We advise our patients so in future 
    if they want implant find THICK bone not slim. <br>
    Little bone won’t be able to do implants but removable dentures that been in market 200 yrs or bridge last May be 5-10-15 yrs max depend on patient’s hygiene. 
    (30 years experience matter for benifits of the patient)`,
  ];
  t_Descripcion = [
    `Estamos humildemente colocando nuestra experiencia internacional de más de 30 años en sus necesidades dentales. <br>
    Le brindamos un servicio dental completo que transforma su sonrisa en lo que ha estado
    buscando a un bajo costo sin dañar su billetera! <br>
    Utilizamos dispositivos de alta tecnología para poner en paralelo lo que había visto pero que no podía experimentar debido al alto costo. <br>
    En nuestro centro trabajamos arduamente para facilitar que nuestros pacientes esten satisfechos! <br>
    Dicho esto, también elegimos nuestra ubicación para estar justo detrás de la FRONTERA donde usted estaciona su vehículo con una 
    pequeña tarifa de un día (9 $) y salga del puente WESTERN que cruza el río TJ en el puente verde hasta el final y cuando 
    ves el primer edificio verde en tu mano izquierda, somos el segundo piso, suit #5. Si elige conducir, siga 
    Google Maps como nuestro Centro Dental detras de COSCO blvd Tobaz Sanchez y Calle Ocampo 3ra llamada Miguel Negrete/Primera, cruzar 
    de la estación de policía antes de la estación de bomberos.`,

    `We are humbly placing our international 30 years of experience in your dental needs. <br>
    We provide you with full dental service in one stop at our center that transforms your smile to the way you have been
    looking for with a low cost/affordable meaning without damaging your wallet! <br>
    We use high tech devices to parallel what you'd seen but could not experience due to high cost. <br>
    We at our center work hard to make it easy on our patients to meet their full satisfaction. <br>
    That been said, we also choose our location to be right behind the BOARDER WALL where you either park your vehicle for a
    small one day fee (9$) and walk off WESTERN bridge crossing TJ river on the green brindge all the way to the end and when
    you notice first green building on your left hand, we are second floor suit #5. If you choose to drive then follow 
    Google Maps as our Dental Center behid COSCO's blvd Tobaz Sanchez and Ocampo 3rd street named Miguel Negrete/Primera cross
    of police station before fire station.`,
  ];
  t_Servicios= ["Servicios", "Services"];
  t_ServiciosContenido = [
    ["Implantes", "Implants",],
    ["Frenos", "Braces"],
    ["Botox", "Botox"],
    ["Circonia", "Zirconia"],
  ];

  //Citas
  t_Citas = ["CITAS", "APOINTMENTS"];
  t_EstadoCita = [
    ["Solicitud en Progreso", "Apointment in Progress"],
    ["Solicitud Aceptada", "Apointment Accepted"],
    ["Solicitud Rechazada", "Apointment Rejected"],
  ];
  t_Mes = [
    ["Enero", "January"],
    ["Febrero", "February"],
    ["Marzo", "March"],
    ["Abril", "April"],
    ["Mayo", "May"],
    ["Junio", "June"],
    ["Julio", "July"],
    ["Septiembre", "September"],
    ["Octubre", "October"],
    ["Noviembre", "November"],
    ["Diciembre", "December"],
  ];
  t_Semana = [
    ["Lunes", "Monday"],
    ["Martes", "Tuesday"],
    ["Miercoles", "Wednesday"],
    ["Jueves", "Thursday"],
    ["Viernes", "Friday"],
    ["Sabado", "Saturday"],
    ["Domingo", "Sunday"],
  ];

  //Galeria
  t_Galeria = ["GALERIA", "GALLERY"];

  //Contacto
  t_Contacto = ["CONTACTO", "CONTACT"];
  t_Telefonos = ["Telefonos", "Phones"];
  t_Direccion = ["Direccion", "Address"];
  t_Ubicacion = ["Ubicacion", "Location"];
  t_RedesSociales = ["Redes Sociales", "Social Networks"];
  t_Llamar = ["Llamar", "Call"];


  constructor(private modalController: ModalController, private iab : InAppBrowser) { }

  async phoneCall (phoneNumber: string) {

    const modal = await this.modalController.create({
      component: CallPage,
      componentProps : {
        number: phoneNumber
      }
    });
    await modal.present();
  }

  openLink(link: string) {
	  if(link != "none") this.iab.create(link);
  }

  changeLanguage() {
    this.currentTranslation++;
    if(this.currentTranslation >= this.translationNumber)
    {
      this.currentTranslation = 0;
    }
  }
}
