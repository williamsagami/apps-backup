import { Component, OnInit } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { PopoverController } from '@ionic/angular';
import { NumberlistComponent } from '../numberlist/numberlist.component';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.page.html',
  styleUrls: ['./contacto.page.scss'],
})
export class ContactoPage implements OnInit {

  constructor(private iab : InAppBrowser, private poc: PopoverController) { }

  ngOnInit() {
  }

  openLink(link) {
	  this.iab.create(link);
  }
  
  async showNumberList(ev) {
    const popover = await this.poc.create({
      component: NumberlistComponent,
      event: ev,
      mode: 'ios',
    });
    return await popover.present();
  }

}
